#!/bin/sh

. /lib/functions/leds.sh

DEFAULT_IMAGEPATH="/tmp/shm/firmware.img"

remount_tmpfs() {
	v "mount /tmp/root as extra tmpfs"
	mkdir /tmp/root
	mount -t tmpfs -o rw,nosuid,nodev,noatime,size=4M tmpfs /tmp/root
	/usr/local/sbin/killalllock.sh netled.sh /var/lock/netled.lock
	/usr/local/sbin/killalllock.sh watchhome.sh /var/lock/watchhome.lock
	led_timer usb     100 100
	led_timer network 100 100
	led_timer system  100 100
}

platform_check_image() {
	v "platform_check_image"

	#should have 8 mtd partitions, from mtd0 to mtd7
	local npart=`cat /proc/mtd | grep mtd | wc -l`
	if [ $npart lt 7 ]; then
		echo "Sysupgrade is not supported on this board yet."
		return 1
	fi


	case "$1" in
		http://*|ftp://*)
			[ ! -f "$DEFAULT_IMAGEPATH" ] &&  eval "wget -O$DEFAULT_IMAGEPATH -q $1"
			IMAGEPATH="$DEFAULT_IMAGEPATH"
			;;
		*)
			[ ! -f "$DEFAULT_IMAGEPATH" ] && eval "cp $1 $DEFAULT_IMAGEPATH"
			IMAGEPATH="$DEFAULT_IMAGEPATH"
			;;
	esac

	if [ -f $IMAGEPATH ]; then
		v "verifying checksum"
		local md5_img=$(dd if="$IMAGEPATH" bs=2 count=16 2>/dev/null)
		local md5_chk=$(dd if="$IMAGEPATH" bs=1024 skip=1 2>/dev/null | md5sum -); md5_chk="${md5_chk%% *}"

		if [ -n "$md5_img" -a -n "$md5_chk" ] && [ "$md5_img" = "$md5_chk" ]; then
			return 0
		else
			echo "Invalid image. Contents do not match checksum (image:$md5_img calculated:$md5_chk)"
			rm $IMAGEPATH
			return 1
		fi
	else
		v "Can not find the firmware file"
		return 1
	fi

}

platform_do_upgrade() {
	# $1: image path/url
	v "platform_do_upgrade start"
	v "image path/url: $1"
	v "image options: $2"
	local bootsize=0
	local hdrsize=1

	IMAGEPATH="$DEFAULT_IMAGEPATH"

	local magic=$(dd if="$IMAGEPATH" bs=1024 skip=$hdrsize count=4 2>/dev/null | hexdump -n 4 -e '1/4 "%x"')

	sync;sync;sync

	if [ "$magic" = "52544b00" ]; then
		bootsize=512
		dd if="$IMAGEPATH" bs=1024 count=64  skip=$((0+$hdrsize)) | mtd write - CONF
		dd if="$IMAGEPATH" bs=1024 count=384 skip=$((64+$hdrsize)) | mtd write - U-Boot
                dd if="$IMAGEPATH" bs=1024 count=64 skip=$((448+$hdrsize)) | mtd write - DTB
	fi 

	dd if="$IMAGEPATH" bs=1024 count=1920 skip=$((0+$bootsize+$hdrsize)) | mtd write - Linux
	dd if="$IMAGEPATH" bs=1024 count=3584 skip=$((1920+$bootsize+$hdrsize)) | mtd write - rootfs

	if [ $SAVE_CONFIG -ne 1 ]; then
		mtd erase data
		dd if="$IMAGEPATH" ibs=1024 obs=256 skip=$((5504+$bootsize+hdrsize)) | mtd write - data
	fi

	v "platform_do_upgrade end (reboot with -f)"

	reboot -f
}

append sysupgrade_pre_upgrade remount_tmpfs
