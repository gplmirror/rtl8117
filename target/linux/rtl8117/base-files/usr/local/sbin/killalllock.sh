#!/bin/sh

# $1 process name
# $2 lock file used

killlist=`pidof $1`

for killpid in $killlist
do
    kill -9 $killpid
done

if [ -f $2 ]; then
    rm $2
fi
