#!/bin/sh

FW_IMG=/tmp/openwrt-rtl8117-factory-bootcode.img

# Wating for the PC enter S5 state
while (true); do
    sleep 5
    pcstate_bit2=$(cat /proc/net/r8168oob/eth0/isolate)

    echo "Wating for the PC enter S5 state..."
    if [ "$pcstate_bit2" == "0" ]; then
        break;
    fi
done
echo "The PC is on S5 state."

echo "Start sysupgrade flash"
echo 3 > /proc/sys/vm/drop_caches
killall vncs && sleep 1 && killall dropbear uhttpd && sleep 1
/sbin/sysupgrade -n $FW_IMG


