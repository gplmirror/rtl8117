#
# Copyright (C) 2012 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/DASH
  NAME:=Linux Based DASH functions
endef

define Profile/DASH/Description
	Linux DASH
endef

define Package/dash-smbus
  SECTION:=dash
  CATEGORY:=Realtek
  TITLE:=Realtek DASH SMBUS
  #DEPENDS:=@TARGET_rtl8117_DASH
endef

define Package/dash-smbus/description
 This package activates SMBUS on dash platform(ex.rtl8117).
endef

$(eval $(call Profile,DASH))
