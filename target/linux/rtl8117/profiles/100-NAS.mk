#
# Copyright (C) 2012 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/NAS
  NAME:=Backup functions
endef

define Profile/NAS/Description
	Features for NAS Light backup functions
endef

$(eval $(call Profile,NAS))
