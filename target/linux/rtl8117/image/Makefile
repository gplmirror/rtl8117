#
# Copyright (C) 2016-2017 Realtek Semiconductor Corp.
#
# phinexhung@realtek.com
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#
include $(TOPDIR)/rules.mk
include $(INCLUDE_DIR)/image.mk

MAJORVER=1
MINORVER=0

ifeq ($(CONFIG_RTL8117_UBOOT_IMG), y)
FACTORY_IMG=$(IMG_PREFIX)-factory-bootcode.img
else
FACTORY_IMG=$(IMG_PREFIX)-factory.img
endif

TMP_IMG_FILE=$(IMG_PREFIX)-tmp.img

define Image/GenVersionInfo
        if [ -f $(TOPDIR)/openwrt.build ]; then \
		echo "FWVER=${MAJORVER}.${MINORVER}.`cat $(TOPDIR)/openwrt.build`" > $(TARGET_DIR)/etc/version.txt;\
	else\
		echo "FWVER=${MAJORVER}.${MINORVER}.`git log  --pretty=oneline | wc -l`" > $(TARGET_DIR)/etc/version.txt;\
	fi;\
	echo -n "KERNEL=" >> $(TARGET_DIR)/etc/version.txt
	cat $(LINUX_DIR)/include/config/kernel.release >> $(TARGET_DIR)/etc/version.txt
	echo -n "OpenWrt=" >> $(TARGET_DIR)/etc/version.txt
	if [ -f $(TOPDIR)/openwrt.release ]; then \
		(cat $(TOPDIR)/openwrt.release >> $(TARGET_DIR)/etc/version.txt);\
	else\
		(cd $(TOPDIR); git describe --tags | awk -F "-" '{print $3}' >> $(TARGET_DIR)/etc/version.txt);\
	fi;\
	if [ "$(CONFIG_RTL8117_UBOOT_IMG)" = "y" ]; then \
		echo -n "U-Boot=" >> $(TARGET_DIR)/etc/version.txt; \
		cat $(TARGET_DIR)/etc/uboot.release >> $(TARGET_DIR)/etc/version.txt;\
	fi;\
	echo -n "BuildDate=" >> $(TARGET_DIR)/etc/version.txt
	date +'%Y-%m-%d' >> $(TARGET_DIR)/etc/version.txt

	echo -n "ASUS=" >> $(TARGET_DIR)/etc/version.txt
	echo "ASUS test string" >> $(TARGET_DIR)/etc/version.txt
endef


define Image/Prepare
	$(CP) ./ubinize.cfg $(KDIR)
	$(call Image/GenVersionInfo)
endef

define Image/Build
	$(call  prepare_generic_squashfs,$(KDIR)/root.squashfs)
	$(STAGING_DIR_HOST)/bin/mkfs.ubifs -q -r $(KDIR)/tmp -m 256 -e 65024 -c 26 -o $(KDIR)/ubifs.img
	( cd $(KDIR); $(STAGING_DIR_HOST)/bin/ubinize -p 64KiB -m 256 -o etc.img ubinize.cfg )
	( \
		if [ "$(CONFIG_RTL8117_UBOOT_IMG)" = "y" ]; then \
			dd if=$(BIN_DIR)/openwrt-$(BOARD)-bootcode.bin bs=256 conv=sync; \
		fi; \
		dd if=$(LINUX_DIR)/arch/$(LINUX_KARCH)/boot/uImage.lzma bs=1920K conv=sync; \
		dd if=$(KDIR)/root.$(1) bs=3584K conv=sync; \
		dd if=$(KDIR)/etc.img bs=256 conv=sync; \
	) >  $(BIN_DIR)/$(FACTORY_IMG)
	md5sum $(BIN_DIR)/$(FACTORY_IMG) | cut -d " " -f 1 | tee $(BIN_DIR)/$(FACTORY_IMG).md5
	( \
		dd if=$(BIN_DIR)/$(FACTORY_IMG).md5 bs=32 count=1 conv=sync; \
		dd if=$(TARGET_DIR)/etc/version.txt bs=992 count=1 conv=sync; \
		dd if=$(BIN_DIR)/$(FACTORY_IMG) bs=256 conv=sync; \
	) > $(BIN_DIR)/$(TMP_IMG_FILE)
	mv $(BIN_DIR)/$(TMP_IMG_FILE) $(BIN_DIR)/$(FACTORY_IMG)
endef


$(eval $(call BuildImage))
