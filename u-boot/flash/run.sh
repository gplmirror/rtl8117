#!/bin/bash

GDB=../toolchains/rsdk-4.9.4/linux/newlib/bin/rsdk-elf-gdb

port=4281

checkfile=0

if [ -f rtl_fp ] ;then
	for filename in dtb.img dash_conf.img all.img
	do
	echo "=============" + "PG file is:" + "$filename" + "================="

	load_start=0x80040000

	case "$filename" in
	            'dash_conf.img')
	                spi_start=0x82020000
	                ;;

	            'dtb.img')
	                spi_start=0x82090000
	                ;;

	            'all.img')
	                spi_start=0x82030000
	                ;;
	esac
	size=`stat -c %s $filename`

	if [ $checkfile -eq 1 ];then
	sed 's/file flashprog_dbg/file flashprog-'$port'_dbg/g;s/set $file_size = .*/set $file_size = '$size'/g;s/  restore tmpfile/  restore '$filename'/g;s/dump binary memory tmpfile/dump binary memory '$filename'.chk/g;s/set $spi_addr = .*/set $spi_addr = '$spi_start'/g;s/set $load_addr = .*/set $load_addr = '$load_start'/g;s/target remote localhost:.*/target remote localhost:'$port'/g' gdb_flash.txt > gdb_flash.scr
	else
	sed 's/file flashprog_dbg/file flashprog-'$port'_dbg/g;s/set $file_size = .*/set $file_size = '$size'/g;s/  restore tmpfile/  restore '$filename'/g;s/dump binary memory tmpfile/#dump binary memory '$filename'.chk/g;s/set $spi_addr = .*/set $spi_addr = '$spi_start'/g;s/set $load_addr = .*/set $load_addr = '$load_start'/g;s/target remote localhost:.*/target remote localhost:'$port'/g' gdb_flash.txt > gdb_flash.scr
	fi

	$GDB -x gdb_flash.scr

	if [ $checkfile -eq 1 ];then
	diff $filename $filename.chk

	if [ $? != 0 ]; then
	  echo $filename " comparison error"
	  break
	else
	  echo $filename " checked successfully"
	fi
	fi
	done
elif [ -f rtl8117 ]; then
	for filename in conf.img all.img
	do
	echo "=============" + "PG file is:" + "$filename" + "================="

	load_start=0x80040000

	case "$filename" in
	            'conf.img')
	                spi_start=0x82020000
	                ;;

	            'all.img')
	                spi_start=0x82030000
	                ;;
	esac
	size=`stat -c %s $filename`

	if [ $checkfile -eq 1 ];then
	sed 's/file flashprog_dbg/file flashprog-'$port'_dbg/g;s/set $file_size = .*/set $file_size = '$size'/g;s/  restore tmpfile/  restore '$filename'/g;s/dump binary memory tmpfile/dump binary memory '$filename'.chk/g;s/set $spi_addr = .*/set $spi_addr = '$spi_start'/g;s/set $load_addr = .*/set $load_addr = '$load_start'/g;s/target remote localhost:.*/target remote localhost:'$port'/g' gdb_flash.txt > gdb_flash.scr
	else
	sed 's/file flashprog_dbg/file flashprog-'$port'_dbg/g;s/set $file_size = .*/set $file_size = '$size'/g;s/  restore tmpfile/  restore '$filename'/g;s/dump binary memory tmpfile/#dump binary memory '$filename'.chk/g;s/set $spi_addr = .*/set $spi_addr = '$spi_start'/g;s/set $load_addr = .*/set $load_addr = '$load_start'/g;s/target remote localhost:.*/target remote localhost:'$port'/g' gdb_flash.txt > gdb_flash.scr
	fi

	$GDB -x gdb_flash.scr

	if [ $checkfile -eq 1 ];then
	diff $filename $filename.chk

	if [ $? != 0 ]; then
	  echo $filename " comparison error"
	  break
	else
	  echo $filename " checked successfully"
	fi
	fi
	done
fi


