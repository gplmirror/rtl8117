#include "bsp.h"
#include "flash.h"

INT32U FLASH_ID;

void bsp_wait(INT32U usec)
{
    INT32U i, j;
    for (i = 0 ; i < usec ; i++)
        //inner loop is around 1u if runnign at 125 MHz
        for (j = 0 ; j < 31; j++)
            asm("nop");

}

void disable_master_engine()
{
    //Check TPPoll to wait TX Finish
    while(REG8(MAC_BASE_ADDR + MAC_TPPoll) & 0x80);
    //TX FIFO is not empty
    while(REG16(MAC_BASE_ADDR + MAC_CMD) & 0x0C00);

    REG8(MAC_BASE_ADDR + MAC_CMD) = 0x00;

    //wait engine goest down
    while(REG16(MAC_BASE_ADDR + MAC_CMD) & 0x300);

    REG8(SMBUS_BASE_ADDR + SEIO_SMEn) &= 0x08;

    REG8(TCR_BASE_ADDR + TCR_CONF0)  = 0x00; //disable tcr

    REG32(TIMER_IOBASE + TIMER_CR) = 0x00000000;  //disable timer
}

int main(void)
{
    INT32U i = 0;
    struct ssi_portmap *ssi_map = (struct ssi_portmap *) FLASH_BASE_ADDR;
    INT32U rdid;
    INT8U flag = 0;
    INT32U flash_addr;
    INT8U *load_addr;
    INT32U load_size;
    INT32U flash_uncached;

    disable_master_engine();
    rlx_icache_invalidate_all();
    rlx_dmem_enable(DMEM_BASE,DMEM_TOP);
    //Set_SPIC_Clock(7);
    FLASH_ID = Flash_RDID(&flash_struct);
    rdid = (FLASH_ID  & 0x00ff0000) >> 16;
    WRSR_Flash_one_two_channel();

    do
    {
        flash_uncached = flash_addr + 0x20000000;

        if (rdid == 0x1f)
            spi_se_unprotect(&flash_struct,(flash_addr & 0x00FFFFFF));

        asm volatile("load_flash_addr:");//gdb break point for gdb_flash.scr use
        spi_blk_erase(&flash_struct, (flash_addr & 0x00FFFFFF));
        memset((void *) load_addr, 0xff, load_size);


        asm volatile("load_memory:");
        for(i = 0 ; i < NUMLOOP/2 ; i++)
            spi_write(&flash_struct, (flash_addr & 0x00FFFFFF)+i*WRSIZE, load_addr+i*WRSIZE, WRSIZE);


        if(memcmp((void *) load_addr, (void *) (flash_addr | 0x20000000), load_size) != 0)
        {
            asm volatile("compare_error:");
            flag = 1;
        }

    } while(flag == 0);

    rlx_icache_invalidate_all();
    rlx_dcache_invalidate_all();

}
