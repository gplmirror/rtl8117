/*
 * Realtek Semiconductor Corp.
 *
 * rtl8117.c:
 *	uboot bsp file
 *
 * Phinex Hung (phinexhung@realtek.com)
 */

#include <common.h>
#include <command.h>
#include <asm/mipsregs.h>
#include <asm/io.h>
#include <netdev.h>
#include <linux/delay.h>
#include <asm/gpio.h>
#include <dm.h>
#include <dt-bindings/gpio/gpio.h>
#include <rtl8117.h>

static void UMAC_RST() //for FPGA release reset pin
{
	volatile u32 temp = 0;
	volatile u32 i = 0;
	//printk("FPGA_INIT===>\r\n");
	// set uphy connect, bit23 = 1

	writel(readl(CPU1_BASE_ADDR+UPHY_REG) | 0x00800000,  CPU1_BASE_ADDR+UPHY_REG);

	// toggle UMAC reset
	writel(readl(CPU2_BASE_ADDR+CPU2_DMY0_REG) & (~0x00000004),  CPU2_BASE_ADDR+CPU2_DMY0_REG);
	writel(readl(CPU2_BASE_ADDR+CPU2_DMY0_REG) | 0x00000004,  CPU2_BASE_ADDR+CPU2_DMY0_REG);

	for(i=0;i<1000;i++){
		temp = readl(CPU1_BASE_ADDR+0x00);
		if((temp&0x00004000) != 0){break;}
	}
	//wait more than 30ms after UMAC reset, otherwise UMAC registers access will be incorrect.
}

static int Rd_UPHY(u8 phyaddr)
{
	u8  temp_count;
	u8  offset;
	u8  Rd_Data;

	offset = phyaddr & 0x0f ;
	writeb((0x90 | offset), CPU1_BASE_ADDR + VCONTROL);

	// vloadm# need pull low ~ 99ns
	for(temp_count = 0; temp_count < 100; temp_count++)
		writeb((0x80 | offset), CPU1_BASE_ADDR + VCONTROL);

	writeb((0x90 | offset), CPU1_BASE_ADDR + VCONTROL);

	offset = (phyaddr & 0xD0) >> 4;
	writeb((0x90 | offset), CPU1_BASE_ADDR + VCONTROL);

	// vloadm# need pull low ~ 99ns
	for(temp_count = 0; temp_count < 100; temp_count++)
		writeb((0x80 | offset), CPU1_BASE_ADDR + VCONTROL);

	writeb((0x90 | offset), CPU1_BASE_ADDR + VCONTROL);

	//Read Register Value
	Rd_Data = readb(CPU1_BASE_ADDR + UPHY_VSTATUS);

	return Rd_Data;
}

static void Wt_UPHY(u8 phyaddr, u8 Wt_Data)
{
	u8 offset;

	// Input Data to usb phy register //
	writeb(Wt_Data, CPU1_BASE_ADDR + VSTATUS);

	offset = phyaddr & 0x0f ;
	writeb((0x90 | offset), CPU1_BASE_ADDR + VCONTROL);

	// vloadm# need pull low ~ 99ns
	writeb((0x80 | offset), CPU1_BASE_ADDR + VCONTROL);

	udelay(100);

	writeb((0x90 | offset), CPU1_BASE_ADDR + VCONTROL);

	offset = (phyaddr & 0xf0) >> 4;
	writeb((0x90 | offset), CPU1_BASE_ADDR + VCONTROL);
	writeb((0x80 | offset), CPU1_BASE_ADDR + VCONTROL);

	udelay(100);

	writeb((0x90 | offset), CPU1_BASE_ADDR + VCONTROL);

	udelay(100);
}

static void usb_phy_init(void)
{
	UMAC_RST();
	mdelay(50); //wait more than 30ms after UMAC reset, otherwise UMAC registers access will be incorrect.
	//change page to page 1
	Wt_UPHY(0xF4, Rd_UPHY(0xD4)|0x20);
	//set REG_EN_OTG=1
	Wt_UPHY(0xE1, Rd_UPHY(0xC1)|0x04);
}

int dram_init(void)
{
    if (fdtdec_setup_memory_size() != 0)
        return -EINVAL;

    return 0;

}

void uart_config(u8 enable)
{
    u32 temp=0 ;
    temp = readl(CPU1_BASE_ADDR + CPU1_CTRL_REG);
    if(enable)
        generic_set_bit(UART_ENABLE, (ulong *) &temp);
    else
        generic_clear_bit(UART_ENABLE, (ulong *) &temp);

    writel(temp, CPU1_BASE_ADDR + CPU1_CTRL_REG);

}

int checkboard(void)
{
    u32 prid;

    prid = read_c0_prid();
    prid &= 0xffff;
    printf("Realtek DASH Platform -- Taroko ");
    switch (prid) {
    case 0x0000dc01:
        printf("RX4281");
        break;
    default:
        printf("unknown");
    }
    printf(" prid=0x%x\n", prid);

    return 0;
}

int board_eth_init(bd_t *bis)
{
    int ret;

    ret = pci_eth_init(bis);
    if (ret < 0)
        return ret;

#ifdef CONFIG_RTL8168OOB
    ret = rtl8168oob_initialize(bis);
    if (ret < 0)
        return ret;
#endif

    return 0;
}

//en_byte: enable byte for read/write
//mode: RISC_WRITE_OP or RISC_READ_OP
//data: for read or write
//offset: register offset
void access_RISC_reg(u8 offset, volatile u32 *data, u8 en_byte, u32 mode)
{
    u8 dcount = 0;

    if(mode == RISC_WRITE_OP)
        writel(*data, CPU2_BASE_ADDR + CPU2_RISC_DATA);

    writel((mode | offset | ( (en_byte & 0xf) << 16 ) ), CPU2_BASE_ADDR + CPU2_RISC_CMD);

    while((readl(CPU2_BASE_ADDR + CPU2_RISC_CMD) & mode))
    {
        mdelay(100);
        if(++dcount == 20)
            printf("Access RISC reg fail\n");
    }

    if(mode == RISC_READ_OP)
        *data = readl(CPU2_BASE_ADDR + CPU2_RISC_DATA);

}

void OOB_access_IB_reg(u16 offset, volatile u32 *data, u8 en_byte, u32 mode)
{
    u32 dcount = 0 ;

    if(mode == OOB_WRITE_OP)
        writel(*data, OOB_MAC_BASE_ADDR + OOB_MAC_OCP_DATA);

    writel( (mode | offset | ((en_byte & 0xf) << 16)), OOB_MAC_BASE_ADDR + OOB_MAC_OCP_ADDR);

    while((readl(OOB_MAC_BASE_ADDR + OOB_MAC_OCP_ADDR) & 0x80000000))
    {
        udelay(10);
        if(++dcount == 200000)
            printf("OOB Access IB fail\n");
    }

    //delay maximal 100ms*20 = 2 seconds

    if(mode == OOB_READ_OP)
        *data = readl(OOB_MAC_BASE_ADDR + OOB_MAC_OCP_DATA);
}

void led_config(u8 enable)
{
    u32 temp = 0;

    //access bit0-bit15 only
    OOB_access_IB_reg(LEDSEL, &temp, 0x3, OOB_READ_OP);
    //default is 0xCA9, LED0 (10M), LED1 (100M), LED2(1000M)
    //change to 0xCB, LED0(10M/100M), LED1(1000M)
    temp = 0xCB;
    OOB_access_IB_reg(LEDSEL, &temp, 0x3, OOB_WRITE_OP);

    //enable LDE0, LED1 only
    OOB_access_IB_reg(LEDCONF, &temp, 0xf, OOB_READ_OP);
    temp &= 0x0000ffff;
    if(enable)
        temp |= 0x031b0000;
    OOB_access_IB_reg(LEDCONF, &temp, 0xf, OOB_WRITE_OP);

    //enable LED LP mode
    OOB_access_IB_reg(MISC_CONFIG, &temp, 0xf, OOB_READ_OP);
    if(enable)
        generic_clear_bit(LED_LP_EN, (ulong *) &temp);
    else
        generic_set_bit(LED_LP_EN, (ulong *) &temp);
    OOB_access_IB_reg(MISC_CONFIG, &temp, 0xf, OOB_WRITE_OP);

}


void jtag_config(u8 enable)
{
    u32 temp = 0;
    OOB_access_IB_reg(UMAC_PAD_CONT_REG, &temp, 0x1, OOB_READ_OP);

    if(enable)
        generic_clear_bit(JTAGCONF, (ulong *) &temp);
    else
        generic_set_bit(JTAGCONF, (ulong *) &temp);

    OOB_access_IB_reg(UMAC_PAD_CONT_REG, &temp, 0x1, OOB_WRITE_OP);

}

void _machine_restart(void)
{
    volatile u32 temp;

    while (1) {
        access_RISC_reg(CPU1_CTRL_REG, &temp, 0x1, RISC_READ_OP);
        temp &= ~(1 << 3);
        access_RISC_reg(CPU1_CTRL_REG, &temp, 0x1, RISC_WRITE_OP);

        access_RISC_reg(CPU1_CTRL_REG, &temp, 0x1, RISC_READ_OP);
        temp |= (1 << 3);
        access_RISC_reg(CPU1_CTRL_REG, &temp, 0x1, RISC_WRITE_OP);
        mdelay(10);
    }

    printf("Restart Platform\n");
}

int board_early_init_f(void)
{
    usb_phy_init();

    //led_config(1);
    //jtag_config(0);

    //clear GPIO6, GPIO8, GPIO9 at early stage
    //writel(0x770700, OOB_MAC_BASE_ADDR + GPIO_CTRL2_SET);

    return 0;
}

int board_late_init(void)
{
    char *cfg;

    cfg = env_get("jtagcfg");
    if(cfg && !strncmp(cfg, "enable",6))
        jtag_config(1);

    cfg = env_get("uartcfg");
    if(cfg && !strncmp(cfg,"enable",6))
    {
        led_config(0);
        uart_config(1);
    }
    else
        uart_config(0);

    uart_config(1);
    jtag_config(1);

    return 0;
}

#ifdef CONFIG_RTL8117_GPIO
int initr_gpio(void)
{
    struct udevice *dev;
    struct gpio_desc power_gpio;
    struct gpio_desc reset_gpio;
    struct gpio_desc cmos_gpio;
    struct gpio_desc passwd_gpio;
    int power_node, reset_node, cmos_node, passwd_node, ret;

    ret = uclass_get_device_by_name(UCLASS_GPIO, "gpio@0", &dev);
    if (ret < 0) {
        printf("Failed to find PMIC pon node. Check device tree\n");
        return 0;
    }

    power_node = fdt_subnode_offset(gd->fdt_blob, dev_of_offset(dev), "power");
    if (power_node < 0) {
        debug("Failed to find key_vol_down node. Check device tree\n");
        return 0;
    }
    if (gpio_request_by_name_nodev(offset_to_ofnode(power_node), "power_gpios", 0, &power_gpio, GPIOD_IS_OUT)) {
        debug("Failed to request key_vol_down button.\n");
        return 0;
    }
    if (dm_gpio_is_valid(&power_gpio)) {
        dm_gpio_set_value(&power_gpio, power_gpio.value);
    }


    reset_node = fdt_subnode_offset(gd->fdt_blob, dev_of_offset(dev), "reset");
    if (reset_node < 0) {
        debug("Failed to find key_vol_down node. Check device tree\n");
        return 0;
    }
    if (gpio_request_by_name_nodev(offset_to_ofnode(reset_node), "reset_gpios", 0, &reset_gpio, GPIOD_IS_OUT)) {
        debug("Failed to request key_vol_down button.\n");
        return 0;
    }
    if (dm_gpio_is_valid(&reset_gpio)) {
        dm_gpio_set_value(&reset_gpio, reset_gpio.value);
    }


    cmos_node = fdt_subnode_offset(gd->fdt_blob, dev_of_offset(dev), "c-cmos");
    if (cmos_node < 0) {
        debug("Failed to find key_vol_down node. Check device tree\n");
        return 0;
    }
    if (gpio_request_by_name_nodev(offset_to_ofnode(cmos_node), "cmos_gpios", 0, &cmos_gpio, GPIOD_IS_OUT)) {
        debug("Failed to request key_vol_down button.\n");
        return 0;
    }
    if (dm_gpio_is_valid(&cmos_gpio)) {
        dm_gpio_set_value(&cmos_gpio, cmos_gpio.value);
    }


    passwd_node = fdt_subnode_offset(gd->fdt_blob, dev_of_offset(dev), "r-passwd");
    if (passwd_node < 0) {
        debug("Failed to find key_vol_down node. Check device tree\n");
        return 0;
    }
    if (gpio_request_by_name_nodev(offset_to_ofnode(passwd_node), "passwd_gpios", 0, &passwd_gpio, GPIOD_IS_IN)) {
        debug("Failed to request key_vol_down button.\n");
        return 0;
    }


    return 0;
}
#endif
