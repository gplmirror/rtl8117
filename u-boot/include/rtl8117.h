#ifndef RTL8117_H
#define RTL8117_H

#define CPU1_BASE_ADDR		(u8 __iomem *)KSEG1ADDR(0xbb000000)
#define CPU2_BASE_ADDR          (u8 __iomem *)KSEG1ADDR(0xbaf00000)
#define OOB_MAC_BASE_ADDR       (u8 __iomem *)KSEG1ADDR(0xbaf70000)
#define UART_BASE_ADDR          (u8 __iomem *)KSEG1ADDR(0xba000000)

#define PRECISION 20
#define DCO_CALIBRATITION_FREQ 388

#define CPU2_DCO_CAL		0x0C

#define CPU1_FREQ_CAL_REG0	0x08
#define CPU1_FREQ_CAL_REG1	0x0C

#define EN_DCO_500M		8
#define REF_DCO_500M		9
#define REF_DCO_500M_VALID	15
#define FRE_REF_COUNT		16
#define FREQ_CAL_EN		1
#define EN_DCO_CLK		10

#define UART_IER	0x04
#define UART_DLL	0x00
#define UART_DLH	0x04
#define UART_LCR	0x0C

#define RISC_READ_OP            0x40000000
#define RISC_WRITE_OP           0x80000000
#define CPU1_CTRL_REG           0x04

#define CPU2_RISC_DATA          0x00
#define CPU2_RISC_CMD           0x04

#define OOB_READ_OP		0x80000000
#define OOB_WRITE_OP		0x80800000

#define UMAC_PAD_CONT_REG       0xDC00
#define LEDSEL			0xDD90
#define LEDCONF 		0xDD94
#define MISC_CONFIG		0xE81C

#define OOB_MAC_OCP_ADDR	0xA4
#define OOB_MAC_OCP_DATA	0xA0

#define UMAC_PAD_CONT_REG	0xDC00
#define UMAC_PIN_OE_REG 	0xDC06
#define PIN_REG 		0xDC0C

#define CLKSW_SET_REG		0xE018

#define UMAC_MISC_1		0xE85A
#define UMAC_CONFIG6_PMCH	0xE90A

#define OOBMAC_EXTR_INT 	0x100
#define GPIO_CTRL1_SET		0x500
#define GPIO_CTRL2_SET		0x504
#define GPIO_CTRL3_SET		0x508
#define GPIO_CTRL4_SET		0x50C
#define GPIO_CTRL5_SET		0x510
#define GPIO_CTRL6_SET		0x514
#define GPIO_CTRL7_SET		0x518
#define GPIO_CTRL8_SET		0x51C

#define CPU1_CTRL_REG		0x04

#define UART_ENABLE		4

#define TESTIO_ACT		2
#define DBG_ACT			10

#define JTAGCONF		7

#define LED_LP_EN		26

#define UPHY_REG                0x0
#define UPHY_VSTATUS        0x00
#define UPHY_SLB                0x01
#define VCONTROL              0x02
#define VSTATUS                   0x03
#define uphy_connect          0x00800000
#define uphy_slb_hs             0x00400000
#define ponrst_n_umac_d2  0x00004000
#define CPU2_DMY0_REG    0x8
void OOB_access_IB_reg(u16 offset, volatile u32 *data, u8 en_byte, u32 mode);
#endif
