/*
 * Realtek Semiconductor Corp.
 *
 * Copyright 2016  Phinex Hung (phinexhung@realtek.com)
 * Copyright 2012  Jethro Hsu (jethro@realtek.com)
 * Copyright 2012  Tony Wu (tonywu@realtek.com)
 */

#ifndef _RTL_FP_H_
#define _RTL_FP_H_


//#define DEBUG
#define CONFIG_ENV_VERSION	4

#define CONFIG_RTL8168OOB
#define CONFIG_CMD_PING
#define CONFIG_ENV_OVERWRITE
#define CONFIG_LZMA
#define CONFIG_BOARD_LATE_INIT

#define	CONFIG_TIMESTAMP		/* Print image info with timestamp */
/*
 * Miscellaneous configurable options
 */
#undef CONFIG_SYS_LONGHELP				/* undef to save memory      */
#define	CONFIG_SYS_CBSIZE		2048		/* Console I/O Buffer Size   */

/* Print Buffer Size */
#define	CONFIG_SYS_PBSIZE 		(CONFIG_SYS_CBSIZE+sizeof(CONFIG_SYS_PROMPT)+16)
#define	CONFIG_SYS_MAXARGS		16		/* max number of command args*/

#define CONFIG_SYS_MALLOC_BASE		0x80040000
#define CONFIG_SYS_MALLOC_LEN		(252 << 10)
#define CONFIG_SYS_BOOTPARAMS_LEN	(2 << 10)
#define CONFIG_SYS_MIPS_TIMER_FREQ	190000000
#define CONFIG_SYS_HZ			1000

#ifndef CONFIG_RELOCATE_L2MEM
#define CONFIG_SYS_SDRAM_BASE		0x88000000     /* Cached addr */
#define CONFIG_SYS_SDRAM_SIZE		(32 << 20)
#define CONFIG_SYS_INIT_SP_ADDR 	0x800f0000
#else
#define CONFIG_SYS_SDRAM_BASE		0x800c0000     /* Cached addr */
#define CONFIG_SYS_SDRAM_SIZE		(192 << 10)
#define CONFIG_SYS_INIT_SP_ADDR 	0x80080000
#undef DEBUG
#endif

#define CONFIG_SYS_LOAD_ADDR		0x88000000     /* default load address	*/

/* The following #defines are needed to get flash environment right */
#define	CONFIG_SYS_MONITOR_BASE		CONFIG_SYS_TEXT_BASE
#define	CONFIG_SYS_MONITOR_LEN		(192 << 10)

#define CONFIG_ENV_OFFSET		0x10000
#define CONFIG_ENV_SIZE 		0x10000
#define CONFIG_ENV_SECT_SIZE		0x10000

#ifdef CONFIG_CMD_USB
#define CONFIG_USB_DWC2
#define CONFIG_USB_DWC2_REG_ADDR	(u8 __iomem *)KSEG1ADDR(0xbb400000)
#define CONFIG_USB_MAX_CONTROLLER_COUNT	1
#endif

#ifdef CONFIG_RTK_SPI
#define CONFIG_ENV_SPI_MAX_HZ 104000000
#define CONFIG_CMD_MTDPARTS
#define CONFIG_MTD_DEVICE
#define CONFIG_MTD_PARTITIONS
#endif

#ifdef CONFIG_PCI

#define CONFIG_CMD_PCI
#define CONFIG_SYS_PCI_CACHE_LINE_SIZE  4

#define CONFIG_SYS_PCI_MEM_BUS  0xC0000000
#define CONFIG_SYS_PCI_MEM_PHYS CONFIG_SYS_PCI_MEM_BUS
#define CONFIG_SYS_PCI_MEM_SIZE 0x10000000
#endif

#define ENV_IS_EMBEDDED

#define ENV_LAYOUT_SETTINGS \
	"factoryimgaddr=0x20000\0" \
	"factoryimgsize=0x7e0000\0" \
	"loadaddr=0x88000000\0" \
	"imgaddr=0x88000400\0" \
	"oftaddr=0x82090000\0" \
	"uimageaddr=0x88800000\0" \
	"spiimgaddr=0x820a0000\0"

#define ENV_SERVER_INFO \
	"gatewayip=192.168.0.254\0"

#define ENV_NAME_SETTINGS \
	"factoryimgname=openwrt-rtl8117-factory-bootcode.img\0" \
	"fdtfile=rtl8117.dtb\0" \
	"board=realtek\0"

#define ENV_CHECK_CMD \
	"check_env=if test -n ${flash_env_version}; " \
	"then env default env_version; " \
	"else env set flash_env_version ${env_version}; env save; " \
	"fi; " \
	"if test ${flash_env_version} -lt ${env_version}; " \
	"then env set flash_env_version ${env_version}; env default -a; env save; " \
	"fi; \0"



/* Linux fails to load the fdt if it's loaded above 512M on a evb-rk3036 board,
 * so limit the fdt reallocation to that */
#define CONFIG_EXTRA_ENV_SETTINGS \
        ENV_NAME_SETTINGS \
	ENV_SERVER_INFO \
	ENV_LAYOUT_SETTINGS \
        ENV_CHECK_CMD \
	"env_version="__stringify(CONFIG_ENV_VERSION)"\0" \
        "bootdelay=3\0" \
        "spiboot=bootm ${spiimgaddr} - ${oftaddr}\0" \
	"usbboot=usb start;fatload usb 0:1 ${uimageaddr} ${bootfile};fatload usb 0:1 ${fdtcontroladdr} ${fdtfile};bootm ${uimageaddr} - ${fdtcontroladdr}\0" \
	"netboot=tftp ${uimageaddr} ${bootfile};tftp ${fdtcontroladdr} ${fdtfile};bootm ${uimageaddr} - ${fdtcontroladdr}\0" \
	"upgrade_img_tftp=tftp ${loadaddr} ${factoryimgname} && setenv upfwtftp 1; if test -n ${upfwtftp}; then sf probe; sf erase ${factoryimgaddr} ${factoryimgsize}; sf write ${imgaddr} ${factoryimgaddr} ${factoryimgsize}; reset; fi;\0" \
	"upgrade_img_usb=usb start;fatload usb 0:1 ${loadaddr} ${factoryimgname} && setenv upfwusb 1; if test -n ${upfwusb}; then sf probe; sf erase ${factoryimgaddr} ${factoryimgsize}; sf write ${imgaddr} ${factoryimgaddr} ${factoryimgsize}; reset; fi;\0"

#define CONFIG_SERVERIP 192.168.0.100
#define CONFIG_IPADDR 192.168.0.10
#define CONFIG_NETMASK 255.255.255.0
#define CONFIG_GATEWAYIP 192.168.0.254
#define CONFIG_BOOTFILE "uImage"

#define CONFIG_PREBOOT \
	"run check_env;"


#define CONFIG_BOOTCOMMAND \
	"run spiboot; " \
	"run netboot; " \
	"run usbboot; " \
	"run upgrade_img_usb;" \
	"run upgrade_img_tftp; "

#endif	/* _RTL8117_H_ */
