#include <common.h>
#include <dm.h>
#include <errno.h>
#include <fdtdec.h>
#include <spi.h>
#include <spi_flash.h>
#include <asm/io.h>
#include <spi_rtk.h>
#include "sf_internal.h"

DECLARE_GLOBAL_DATA_PTR;
//static unsigned char * FLASH_BASE = NULL;
static int sf_rtl8117_read(struct udevice *dev, u32 offset, size_t len,
                            void *buf)
{
    struct udevice *bus = dev->parent;
    struct rtk_spi_priv *priv = dev_get_priv(bus);
    if(!buf)
        return -EINVAL;
    rtk_memcpy(buf, (void *)(priv->flash_base + offset), len);
    return 0;
}

static int sf_rtl8117_write(struct udevice *dev, u32 offset, size_t len,
                             const void *buf)
{
    struct udevice *bus = dev->parent;
    struct rtk_spi_priv *priv = dev_get_priv(bus);
    if(!buf)
        return -EINVAL;

    rtk_flash_write(priv, (void *)(priv->flash_base + offset), (void *)buf, len);
    return 0;
}

static int sf_rtl8117_erase(struct udevice *dev, u32 offset, size_t len)
{
    struct udevice *bus = dev->parent;
    struct rtk_spi_priv *priv = dev_get_priv(bus);
    u32 addr;
    int size;

    addr = priv->flash_base + offset;
    for(size = len ; size > 0 ; size -= 0x1000)
    {
        Flash_erase(priv, (addr&0x01FFFFFF), FLASH_CHIP_SEC);
        addr += 0x1000;
    }
    return 0;
}

static void jedec_probe(struct udevice *dev, struct rtk_spi_priv *priv)
{
    const struct spi_flash_info *params;
    struct spi_flash *spi_flash;

    params = spi_flash_ids;
    for (; params->name != NULL; params++)
    {
        if (!(JEDEC_ID(params)^(priv->flash_id&0x0000FFFF)))
            break;
    }

    if (!params->name) {
        printf("SF: Unsupported flash IDs: ");
        printf("manuf %02x, jedec %04x\n",
               (priv->flash_id&0x00FF0000)>>16, priv->flash_id&0x0000FFFF);
    }

    spi_flash = dev_get_uclass_priv(dev);
    spi_flash->name = params->name;
    spi_flash->sector_size = params->sector_size;
    if (params->flags&SECT_4K)
        spi_flash->erase_size = 0x1000;
    else
        spi_flash->erase_size = 0x10000;
}

static int sf_rtl8117_probe(struct udevice *dev)
{
    struct spi_slave *spi = dev_get_parent_priv(dev);
    struct spi_flash *spi_flash;
    struct udevice *bus = dev->parent;
    struct rtk_spi_priv *priv = dev_get_priv(bus);
    int flash_node;
    u32 map[2];
	char sf_name[30];

    spi_flash = dev_get_uclass_priv(dev);
    spi_flash->dev = dev;

    spi_flash->page_size = 0x100;

	strcpy(sf_name, "/");
	strcat(sf_name, bus->name);
    flash_node = fdt_path_offset(gd->fdt_blob, sf_name);
    priv->regs = fdtdec_get_addr(gd->fdt_blob, flash_node, "reg");
    priv->regs = CKSEG1ADDR(priv->regs);

    spi_flash_rdid(priv);
    jedec_probe(dev, priv);

	strcat(sf_name, "/spi-flash");
    flash_node = fdt_path_offset(gd->fdt_blob, sf_name);
    if (fdtdec_get_int_array(gd->fdt_blob, flash_node, "memory-map", map, 2)) {
        debug("Node memory-map has bad/missing 'reg' property\n");
        return -1;
    }
    priv->flash_base = spi->memory_map = map[0];
    spi_flash->size = map[1];

    /* Assign spi data */
    spi_flash->spi = spi;
    spi_flash->memory_map = spi->memory_map;
#ifndef CONFIG_SPL_BUILD
    printf("SF: Detected %s with page size ", spi_flash->name);
    print_size(spi_flash->page_size, ", erase size ");
    print_size(spi_flash->erase_size, ", total ");
    print_size(spi_flash->size, "");
    if (spi_flash->memory_map)
        printf(", mapped at %p", spi_flash->memory_map);
    puts("\n");
#endif

    return 0;
}

static const struct udevice_id spi_flash_std_ids[] = {
    { .compatible = "spi-flash" },
    { }
};

static const struct dm_spi_flash_ops sf_rtl8117_ops = {
    .read = sf_rtl8117_read,
    .write = sf_rtl8117_write,
    .erase = sf_rtl8117_erase,
};

U_BOOT_DRIVER(spi_flash_std) = {
    .name	= "spi_flash_std",
    .id		= UCLASS_SPI_FLASH,
    .of_match	= spi_flash_std_ids,
    .priv_auto_alloc_size = sizeof(struct spi_flash),
    .probe	= sf_rtl8117_probe,
    .ops	= &sf_rtl8117_ops,
};
