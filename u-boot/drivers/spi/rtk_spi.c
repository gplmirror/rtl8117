/*
*Realtek SPI controller driver.
*
*/
#include <common.h>
#include <dm.h>
#include <errno.h>
#include <malloc.h>
#include <spi.h>
#include <fdtdec.h>
#include <linux/compat.h>
#include <asm/io.h>
#include <spi_rtk.h>

DECLARE_GLOBAL_DATA_PTR;

static inline u32 rtk_readl(struct rtk_spi_priv *priv, u32 offset)
{
    return __raw_readl(priv->regs + offset);
}

static inline void rtk_writel(struct rtk_spi_priv *priv, u32 offset, u32 val)
{
    __raw_writel(val, priv->regs + offset);
}

static inline u16 rtk_readb(struct rtk_spi_priv *priv, u32 offset)
{
    return __raw_readb(priv->regs + offset);
}

static inline void rtk_writeb(struct rtk_spi_priv *priv, u32 offset, u16 val)
{
    __raw_writeb(val, priv->regs + offset);
}

static int rtk_spi_ofdata_to_platdata(struct udevice *bus)
{
    struct rtk_spi_platdata *plat = bus->platdata;
    const void *blob = gd->fdt_blob;
    int node = dev_of_offset(bus);

    plat->regs = (void __iomem *)fdtdec_get_addr(blob, node, "reg");
    return 0;
}

/*detail functions*/
static void Check_SPIC_Busy(struct rtk_spi_priv *priv)
{
    u32   spic_busy;

    do {
        spic_busy=rtk_readl(priv, FLASH_SR);
    } while(spic_busy!=0x06);
}

static inline void spi_enable_chip(struct rtk_spi_priv *priv, int enable)
{
    rtk_writel(priv, FLASH_SSIENR, (enable ? 1 : 0));
}

static void Flash_Exit4byte_Addrmode (struct rtk_spi_priv *priv)
{
    spi_enable_chip(priv, 0);
    rtk_writel(priv, FLASH_SER, 0x01);
    rtk_writel(priv, FLASH_CTRLR1, 0x00);
    rtk_writel(priv, FLASH_CTRLR0, 0x000);
    spi_enable_chip(priv, 1);

    rtk_writeb(priv, FLASH_DR, OPCODE_EX4B);
    Check_SPIC_Busy(priv);
}

void spi_flash_rdid(struct rtk_spi_priv *priv)
{
    u32   flash_ID,spic_busy;

    spi_enable_chip(priv, 0);
    rtk_writel(priv, FLASH_CTRLR0, 0x300);
    rtk_writel(priv, FLASH_SER, 0x01);
    rtk_writel(priv, FLASH_CTRLR1, 0x03);
    spi_enable_chip(priv, 1);
    rtk_writeb(priv, FLASH_DR, FLASH_RDID_COM);
    do {
        spic_busy=rtk_readl(priv, FLASH_SR);
        spic_busy=spic_busy & 0x00000001;
    } while(spic_busy!=0x00);
    flash_ID = (rtk_readb(priv, FLASH_DR)<<16) + (rtk_readb(priv, FLASH_DR)<<8) + rtk_readb(priv, FLASH_DR);
    //winbond 256Mb flash default address mode is 4byte address !!!
    if (((flash_ID>>16) == 0xef)&&((flash_ID&0x0000ff)==0x19)) {
        Flash_Exit4byte_Addrmode(priv);
    }
    priv->flash_id = flash_ID;
}

static void Flash_RDSR(struct rtk_spi_priv *rtks)
{
    u8   spic_busy, flash_busy;

    do {
        spi_enable_chip(rtks, 0);
        rtk_writel(rtks, FLASH_CTRLR0, 0x300);
        rtk_writel(rtks, FLASH_SER, 0x01);
        rtk_writel(rtks, FLASH_CTRLR1, 0x01);
        spi_enable_chip(rtks, 1);
        rtk_writeb(rtks, FLASH_DR, FLASH_RDSR_COM);
        //check spic busy?
        do {
            spic_busy=rtk_readb(rtks, FLASH_SR);
            spic_busy=spic_busy & 0x01;
        } while(spic_busy!=0x00);
        //check flash busy?
        flash_busy=rtk_readb(rtks, FLASH_DR);
        flash_busy=flash_busy & 0x03;
    } while(flash_busy==0x03);
}

//unprotect the FLASH
//when write /erase falsh ,must call this function firstly
//NOTE: because different  rand flash has different manner to declare/set quad enable ,
//so just use one
static void WRSR_Flash_one_two_channel(struct rtk_spi_priv *priv)
{
    //Setup SPIC
    spi_enable_chip(priv, 0);
    rtk_writel(priv, FLASH_VALID_CMD, 0x200);
    rtk_writel(priv, FLASH_ADDR_LENGTH, 0x01);
    rtk_writel(priv, FLASH_VALID_CMD, 0x200);
    rtk_writel(priv, FLASH_SER, 0x01);
    rtk_writel(priv, FLASH_CTRLR1, 0x00);
    rtk_writel(priv, FLASH_CTRLR0, 0x000);
    spi_enable_chip(priv, 1);

    //Write enable
    rtk_writeb(priv, FLASH_DR, FLASH_WREN_COM);
    Check_SPIC_Busy(priv);

    //Write Status
    spi_enable_chip(priv, 0);
    rtk_writeb(priv, FLASH_DR, FLASH_WRSR_COM);
    rtk_writeb(priv, FLASH_DR, 0x00);
    spi_enable_chip(priv, 1);
    Check_SPIC_Busy(priv);
    Flash_RDSR(priv);
}

#if 1
void Flash_Enter4byte_Addrmode(struct rtk_spi_priv *priv, u32 flash_id)
{
    u8 flash_busy;
    u8 mode;

    switch (flash_id >> 16) {
    case 0xC2:/*MACRONIX*/
    case 0xEF: /* winbond */
    case 0x20:/*MICRON*/
        mode=1;
        break;
    default:
        /* Spansion style */
        mode=0;
        break;
    }

    spi_enable_chip(priv, 0);
    rtk_writel(priv, FLASH_ADDR_LENGTH, rtk_readl(priv, FLASH_ADDR_LENGTH)&0xfffcffff);
    rtk_writel(priv, FLASH_ADDR_LENGTH, 0x01);
    rtk_writel(priv, FLASH_SER, 0x01);
    rtk_writel(priv, FLASH_CTRLR1, 0x00);
    rtk_writel(priv, FLASH_CTRLR0, 0x000);
    spi_enable_chip(priv, 1);
    //setup SPIC
    if (mode == 1 ){
        rtk_writeb(priv, FLASH_DR, FLASH_WREN_COM);//Write Enable,MICRON will need WREN berore EN4B 
        Check_SPIC_Busy(priv);
        rtk_writeb(priv, FLASH_DR, OPCODE_EN4B);//enter 4byte command
        Check_SPIC_Busy(priv);
    }else{
        //Write  Bank Address Register
        rtk_writeb(priv, FLASH_DR, OPCODE_BRWR);
        Check_SPIC_Busy(priv);

        //bit 7 :EXTADD Extended Address 1: 4-byte (32-bits) addressing required from command 
        spi_enable_chip(priv, 0);//Disable SPIC
        rtk_writeb(priv, FLASH_DR, 1 << 7);
        spi_enable_chip(priv, 1);//Enable SPIC
        Check_SPIC_Busy(priv);  
    }
}

void WRSR_Flash_quad_channel(struct rtk_spi_priv *priv)
{
    u32   flash_busy;

    //Setup SPIC
    spi_enable_chip(priv, 0);
    rtk_writel(priv, FLASH_VALID_CMD, 0x200);
    rtk_writel(priv, FLASH_ADDR_LENGTH, 0x01);
    rtk_writel(priv, FLASH_SER, 0x01);
    rtk_writel(priv, FLASH_CTRLR1, 0x00);
    rtk_writel(priv, FLASH_CTRLR0, 0x000);
    spi_enable_chip(priv, 1);
    
    //Write enable
    rtk_writeb(priv, FLASH_DR, FLASH_WREN_COM);
    Check_SPIC_Busy(priv);

    //Write Status
    spi_enable_chip(priv, 0);
    rtk_writel(priv, FLASH_DR, FLASH_WRSR_COM);
    rtk_writel(priv, FLASH_DR, 0x40);
    spi_enable_chip(priv, 1);
    Check_SPIC_Busy(priv);  
    do{
        Flash_RDSR(priv);   
        //check flash busy? 
        flash_busy=rtk_readl(priv, FLASH_DR);
        flash_busy=flash_busy & 0x00000003;
    }while(flash_busy==0x03);
    
    do{
        Flash_RDSR(priv);
        flash_busy=rtk_readl(priv, FLASH_DR);
        flash_busy=flash_busy & 0x00000040;
    }while(flash_busy!=0x40);//QUAD Read Enable OK?
}

void Set_SPIC_Write_four_channel(struct rtk_spi_priv *priv)//Kaiwen
{
    spi_enable_chip(priv, 0);
    rtk_writel(priv, FLASH_CTRLR0, 0x0a0000);
    rtk_writel(priv, FLASH_CTRLR1, 0x00);
    rtk_writel(priv, FLASH_VALID_CMD, rtk_readl(priv, FLASH_VALID_CMD)|0x300);
    if (!(rtk_readl(priv, FLASH_AUTO_LENGTH)&0x30000))
        rtk_writel(priv, FLASH_ADDR_LENGTH, 0x00);
    else
        rtk_writel(priv, FLASH_ADDR_LENGTH, 0x03);
    spi_enable_chip(priv, 1);
}

#endif

/* Restart the controller, disable all interrupts, clean rx fifo */
static void spi_hw_init(struct rtk_spi_priv *priv)
{
    spi_enable_chip(priv, 0);
    rtk_writel(priv, FLASH_CTRLR0, 0x0);
    rtk_writel(priv, FLASH_ADDR_CTRLR2, 0x81);
    spi_enable_chip(priv, 1);
    priv->fifo_len = 0x100;
    spi_flash_rdid(priv);

    if ((!(((priv->flash_id>>16)&0x00FF)^0xC2)) && ((((priv->flash_id>>8)&0x00FF) - 0x20 > 0) || ((!((((priv->flash_id>>8)&0x00FF)^0x20))) && (((priv->flash_id&0x000000FF)-0x18)>0))))
    {
        Flash_Enter4byte_Addrmode(priv, priv->flash_id);
    }
    WRSR_Flash_one_two_channel(priv);
}

void Flash_erase(struct rtk_spi_priv *priv, u32 Address, u32 CMD)
{
    u32   DWtmp;

    //Setup SPIC
    spi_enable_chip(priv, 0);
    if (!(rtk_readl(priv, FLASH_AUTO_LENGTH)& 0x30000))
        rtk_writel(priv, FLASH_ADDR_LENGTH, 0x0);
    else
        rtk_writel(priv, FLASH_ADDR_LENGTH, 0x3);
    rtk_writel(priv, FLASH_SER, 0x01);
    rtk_writel(priv, FLASH_CTRLR1, 0x00);
    rtk_writel(priv, FLASH_CTRLR0, 0x000);
    spi_enable_chip(priv, 1);

    //Write enable
    rtk_writeb(priv, FLASH_DR, FLASH_WREN_COM);
    Check_SPIC_Busy(priv);

    if (!(rtk_readl(priv, FLASH_AUTO_LENGTH)& 0x30000))
    {

        DWtmp = (Address >> 24)&0x00FF;
        DWtmp = DWtmp + ((Address >> 8) & 0x0000ff00);
        DWtmp = DWtmp + ((Address << 8) & 0x00ff0000);
        DWtmp = (DWtmp << 8 )+ CMD;
        rtk_writel(priv, FLASH_DR, DWtmp);
        rtk_writeb(priv, FLASH_DR, (u8)(Address & 0x000000ff));
    }
    else
    {
        DWtmp = (Address >> 16)&0x00FF;
        DWtmp = DWtmp + (Address & 0x0000ff00);
        DWtmp = DWtmp + ((Address << 16) & 0x00ff0000);
        DWtmp = (DWtmp << 8 )+ CMD;
        rtk_writel(priv, FLASH_DR, DWtmp);
    }
    /*
    DWtmp = Address >> 16;
    DWtmp = DWtmp + (Address & 0x0000ff00);
    DWtmp = DWtmp + ((Address << 16) & 0x00ff0000);
    DWtmp = (DWtmp << 8 )+ CMD;
    rtk_writel(priv, FLASH_DR, DWtmp);
    */

    Check_SPIC_Busy(priv);
    Flash_RDSR(priv);
}

static void Set_SPIC_Write_one_channel(struct rtk_spi_priv *priv)//Kaiwen
{
    spi_enable_chip(priv, 0);
    rtk_writel(priv, FLASH_CTRLR0, 0x0);
    rtk_writel(priv, FLASH_CTRLR1, 0x00);
    //support 4 byte mode
    if (!(rtk_readl(priv, FLASH_AUTO_LENGTH)&0x30000))
        rtk_writel(priv, FLASH_ADDR_LENGTH, 0x0);
    else
        rtk_writel(priv, FLASH_ADDR_LENGTH, 0x3);

    //rtk_writel(priv, FLASH_ADDR_LENGTH, 0x03);
    rtk_writel(priv, FLASH_VALID_CMD, 0x200);
    spi_enable_chip(priv, 1);
}

/*********************************************************
*description:
    user mode to write data to spi flash
*parameter:
    NDF:byte length
    Address:SPI address(relately 24/32-bit address),
    DReadBuffer: write buffer's point
*Note:ex,when write flash men address 0x82000000,then ,address is 0x00;
    the max  NDF is 128 bytes !!!
**********************************************************/
void Flash_write_one_channel_User(struct rtk_spi_priv *priv, u32 NDF, u32 Address, u32 *DReadBuffer)
{
    u32    DWtmp, i;
    u8    *BReadBuffer;

    Set_SPIC_Write_one_channel(priv);
    BReadBuffer = (u8 *)DReadBuffer;
    rtk_writeb(priv, FLASH_DR, FLASH_WREN_COM);//Write Enable
    Check_SPIC_Busy(priv);
    spi_enable_chip(priv, 0);
    if(!(rtk_readl(priv, FLASH_AUTO_LENGTH)& 0x30000))
    {
        DWtmp = (Address >> 24)&0x00FF;
        DWtmp = DWtmp + ((Address >> 8) & 0x0000ff00);
        DWtmp = DWtmp + ((Address << 8) & 0x00ff0000);
        DWtmp = (DWtmp << 8 )+ FLASH_PP_COM;
        rtk_writel(priv, FLASH_DR, DWtmp);
        rtk_writeb(priv, FLASH_DR, (u8)(Address&0x000000ff));
    }
    else
    {
        DWtmp = (Address >> 16)&0x00FF;
        DWtmp = DWtmp + (Address & 0x0000ff00);
        DWtmp = DWtmp + ((Address << 16) & 0x00ff0000);
        DWtmp = (DWtmp << 8 )+ FLASH_PP_COM;
        rtk_writel(priv, FLASH_DR, DWtmp);
    }
    /*DWtmp = Address >> 16;
    DWtmp = DWtmp + (Address & 0x0000ff00);
    DWtmp = DWtmp + ((Address << 16) & 0x00ff0000);
    DWtmp = (DWtmp << 8 )+ FLASH_PP_COM;
    rtk_writel(priv, FLASH_DR, DWtmp);//Write Command and Address
    */

    if (NDF%4)
    {
        for(i=0; i<NDF; i++)
            rtk_writeb(priv, FLASH_DR, *(BReadBuffer+i));
    }
    else
    {
        for(i=0; i<NDF/4; i++)
            rtk_writel(priv, FLASH_DR, *(DReadBuffer+i));
    }
    spi_enable_chip(priv, 1);
    Check_SPIC_Busy(priv);
    //Read Flash Status
    Flash_RDSR(priv);
}

static int initialized = 0;
static int rtk_spi_probe(struct udevice *bus)
{
    struct rtk_spi_platdata *plat = dev_get_platdata(bus);
    struct rtk_spi_priv *priv = dev_get_priv(bus);
    int cpus_node;
    int flash_node;

    if (initialized == 1)
    {
        initialized = 0;
        return 0;
    }
    else
        initialized = 1;

    priv->regs = CKSEG1ADDR(plat->regs);

    //if ((u32)priv->regs != FLASH_BASE_ADDR)
    //    priv->regs = FLASH_BASE_ADDR;
    priv->flash_excha_addr = fdtdec_get_int(gd->fdt_blob, dev_of_offset(bus),"excha-addr", 0);

    cpus_node = fdt_path_offset(gd->fdt_blob, "/cpus");
    if (cpus_node < 0)
        debug("DTB file doesn't have cpus node.\n");
    else
        priv->cpu_freq = fdtdec_get_int(gd->fdt_blob, cpus_node, "mips-hpt-frequency",0);

    flash_node = fdt_path_offset(gd->fdt_blob, "/rtk_spi/spi-flash@0");
    priv->dummy_cycles = fdtdec_get_int(gd->fdt_blob, flash_node, "spi-dummy-cycles",0);

    spi_hw_init(priv);
    return 0;
}

static void rtk_writer(struct rtk_spi_priv *priv)
{
    Flash_erase(priv, (priv->flash_excha_addr & 0x00FFFFFF), FLASH_CHIP_SEC);
    rtk_flash_write(priv, priv->flash_excha_addr, priv->tx, priv->len);
}

static int rtk_reader(struct rtk_spi_priv *priv)
{
    rtk_memcpy(priv->rx, priv->flash_excha_addr, priv->len);
    return 0;
}

static int rtk_spi_xfer(struct udevice *dev, unsigned int bitlen,
                        const void *dout, void *din, unsigned long flags)
{
    struct udevice *bus = dev->parent;
    struct rtk_spi_priv *priv = dev_get_priv(bus);
    const u8 *tx = dout;
    u8 *rx = din;
    int ret = 0;

    if (bitlen==0)
        return 0;

    /* spi core configured to do 8 bit transfers */
    if (bitlen % 8) {
        debug("Non byte aligned SPI transfer.\n");
        return -1;
    }

    priv->len = bitlen >> 3;
    priv->tx = (void *)tx;
    priv->tx_end = priv->tx + priv->len;
    priv->rx = rx;
    priv->rx_end = priv->rx + priv->len;

    /*support sspi command.*/
    if (priv->tx!= 0x00000000)
        rtk_writer(priv);
    if( priv->rx!= 0x00000000)
        ret = rtk_reader(priv);

    return ret;
}
#define RTK_SPI_BAUD_MAX 8
static int rtk_spi_set_speed(struct udevice *bus, uint speed)
{
    struct rtk_spi_priv *priv = dev_get_priv(bus);
    u16 clk_div=1;

    if (speed == priv->speed)
        return 0;

    if (speed < priv->cpu_freq/(2*RTK_SPI_BAUD_MAX))
        speed = priv->cpu_freq/(2*RTK_SPI_BAUD_MAX);
    else if (speed > priv->cpu_freq/2)
        speed = priv->cpu_freq/2;
    priv->speed = speed;

    /* Disable controller before writing control registers */
    spi_enable_chip(priv, 0);

    while ((clk_div <= RTK_SPI_BAUD_MAX) && ((priv->cpu_freq /(2 << clk_div) > speed)))
        clk_div<<=1;
	if (priv->regs == 0xBC010000)
        clk_div = 2;
    rtk_writel(priv, FLASH_BAUDR, clk_div);
    /*set dummy cycles.*/
    rtk_writel(priv, FLASH_AUTO_LENGTH, ((rtk_readl(priv, FLASH_AUTO_LENGTH) & 0xffff0000) | priv->dummy_cycles));

    /* Enable controller after writing control registers */
    spi_enable_chip(priv, 1);

    debug("%s: regs=%p speed=%d clk_div=%d\n", __func__, priv->regs,
          speed, clk_div);

    return 0;
}

static int rtk_spi_set_mode(struct udevice *bus, uint mode)
{
    return 0;
}

static const struct dm_spi_ops rtk_spi_ops = {
    .xfer		= rtk_spi_xfer,
    .set_speed	= rtk_spi_set_speed,
    .set_mode	= rtk_spi_set_mode,
};

static const struct udevice_id rtk_spi_ids[] = {
    { .compatible = "realtek,rtk-spi" }, //may change this compatiable name.
    { }
};

U_BOOT_DRIVER(rtk_spi) = {
    .name = "rtk_spi",
    .id = UCLASS_SPI,
    .of_match = rtk_spi_ids,
    .ops = &rtk_spi_ops,
    .ofdata_to_platdata = rtk_spi_ofdata_to_platdata,
    .platdata_auto_alloc_size = sizeof(struct rtk_spi_platdata),
    .priv_auto_alloc_size = sizeof(struct rtk_spi_priv),
    .probe = rtk_spi_probe,
    .flags	= DM_FLAG_PRE_RELOC|DM_UC_FLAG_SEQ_ALIAS,
};
