/*
 * rtl8168.c : U-Boot driver for Realtek Gigabit Ethernet controllers
 *             with PCI-Express interface
 *
 * Masami Komiya (mkomiya@sonare.it)
 *
 * Most part is taken from r8168.c of Linux kernel
 *
 */

/**************************************************************************
*    rtl8168.c: Etherboot device driver for Realtek Gigabit Ethernet
*    controllers with PCI-Express interface
*    Written 2013 by Realtek <nicfae@realtek.com>
*
*    This program is free software; you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation; either version 2 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software
*    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
*    Portions of this code based on:
*	 r8168.c: A Realtek Gigabit Ethernet controllers with PCI-Express
*    interface driver for Linux kernel 3.x.
*
***************************************************************************/
#include <common.h>
#include <malloc.h>
#include <net.h>
#include <netdev.h>
#include <asm/io.h>
#include <pci.h>
#include <errno.h>
#include <linux/mii.h>
//#include <asm/arch/system.h>


#ifndef __HWPF_H__
#define __HWPF_H__

//#include <rlx/rlx_types.h>

#define UNICAST_MAR	1
#define MULCAST_MAR	2
#define BROADCAST_MAR	4
 
#define MAC_ADDR_LEN    6
 
#define IPv4 4
#define IPv6 6

#define IP_PROTO_ICMP 0x01
#define IP_PROTO_ICMPv6 0x3A
#define IP_PROTO_UDP 0x11
#define IP_PROTO_TCP 0x06
 
#define IPv4_ADR_LEN 4
#define IPv6_ADR_LEN 16
#define PORT_ADR_LEN 2
#define TYPE_ADR_LEN 2
#define PTL_ADR_LEN 1

#define TCAM_Entry_Invalid 0xFFFFFFFF
#define TCAM_Entry_Available		0xFF
#define TCAM_Entry_Default			0xFE
#define TCAM_PTL_NoExist			0xFF

#define NOSPEC	0xFF

typedef struct
{
 unsigned char ruleNO;
 unsigned char isSet:1, FRIBNotDrop:1, FROOB:1,IBMAR:1, OOBMAR:1;
 unsigned short RSV1;

 unsigned short MARType;
 unsigned short MACIDBitMap; //MACID0: IB, MACID1: IB 
 unsigned short VLANBitMap;
 unsigned short TypeBitMap; //Type0: IPv4, Type1: IPv6, Type2:ARP, Type3:VLAN
 unsigned short IPv4PTLBitMap; //IPv4PTL:TCP, IPv4PTL1:UDP, IPv4PTL2:ICMP, IPv4PTL3:AH, IPv4PTL4:ESP, IPv4PTL5:IPV4-IPv6
 unsigned short IPv6PTLBitMap;
 unsigned short DIPv4BitMap;
 unsigned short DIPv6BitMap;
 unsigned int	DPortBitMap;
}TCAMRule;


/*TCAM Entry Setting*/
typedef struct
{
	/*when read TCAM data to this variable must know if "DontCareBit" not equal to 0xFFFF, the data just one kind of combination*/
	u16 Value;
	/*If don't care bit exist set "1" to this bit position*/
	/*i.e. 0xFFFF=0000 0000 0000 0000->no dont care bit; 0x0007=0000 0000 0000 0111->BIT1,BIT2,BIT3 is don't care bit*/
	u16 DontCareBit;
	u8 Valid;
}TCAM_Entry_Setting_st;

/*TCAM Property*/
typedef enum{
	TCAM_MAC,
	TCAM_VLAN,
	TCAM_TYPE,
	TCAM_PTLv4,
	TCAM_PTLv6,
	TCAM_SIPv4,
	TCAM_DIPv4,
	TCAM_SIPv6,
	TCAM_DIPv6,
	TCAM_SPORT,
	TCAM_DPORT,
	TCAM_Teredo_SPORT,
	TCAM_Teredo_DPORT,
	TCAM_UDP_ESP_SPORT,
	TCAM_UDP_ESP_DPORT,
	TCAM_OFFSET,
	MAX_TCAM_Entry_Type,
	//----------------------
	
	TCAM_MARIB,
	TCAM_MAROOB,
}TCAM_Entry_Type_et;

typedef struct
{
	u16 Start_In_TCAM	[MAX_TCAM_Entry_Type];
	u16 Number_Of_Set	[MAX_TCAM_Entry_Type];
	u16 Entry_Per_Set	[MAX_TCAM_Entry_Type];
}TCAM_Property_st;

typedef struct
{	
   TCAM_Entry_Type_et type;
   unsigned int num;   
   unsigned char* data;
}TCAM_Allocation;


/*TCAM Rule Action*/
typedef struct
{
	u8 number;
	u8 Gamming:1;
	u8 Meter_En:1;
	u8 Meter_No:3;
	u8 Drop:1;
	u8 OOB:1;
	u8 IB:1;
}TCAM_RuleActionSet_st;

typedef enum{
	TCAM_Read=0,
	TCAM_Write,
	TCAM_RW_MAX,
}TCAM_RW_et;

typedef enum{
	RET_FAIL,
	RET_OK,
}Ret_Code_et;

typedef enum{
	Switch_OFF,
	Switch_ON,
}ONOFF_Switch_et;

typedef enum{
	TCAM_data,
	TCAM_care,
}TCAM_ReadType_et;

typedef enum
{
	RULE_NO_0,
	RULE_NO_1,
	RULE_NO_2,
	RULE_NO_3,
	RULE_NO_4,
	RULE_NO_5,
	RULE_NO_6,
	RULE_NO_7,
	RULE_NO_8,
	RULE_NO_9,
	RULE_NO_10,
	RULE_NO_11,
	RULE_NO_12,
	RULE_NO_13,
	RULE_NO_14,
	RULE_NO_15,
	RULE_NO_MAX,
	RULE_SETTING_FAIL,
}Rule_NO_et;
typedef enum{
	MAC_0,//0
	MAC_1,
	MAC_2,
	MAC_3,
	MAC_4,
	MAC_5,
	MAC_6,
	MAC_7,
	MAC_8,
	MAC_9,//9

	MARI,
	MARO,
	BRD,//12

	VLAN_0,
	VLAN_1,
	VLAN_2,//15
	VLAN_3,
	VLAN_4to5,
	VLAN_6to10,
	VLAN_11to15,

	TYPE_0,//20  /*IPv4*/
	TYPE_1,		/*IPv6*/
	TYPE_2,		/*ARP*/
	TYPE_3,		/*VLAN*/
	TYPE_4,
	TYPE_5,
	TYPE_6,
	TYPE_7,
	TYPE_8to11,
	TYPE_12to15,
	
	PTL_0,//30
	PTL_1,
	PTL_2,
	PTL_3,
	PTL_4,
	PTL_5,
	PTL_6,
	PTL_7,
	PTL_8,
	PTL_9,
	PTL_10,//40
	PTL_11,
	
	SIP_0,
	SIP_1,
	SIP_2,
	SIP_3,
	
	DIP_0,
	DIP_1,
	DIP_2,
	DIP_3,
	DIP_4,//50
	DIP_5,
	DIP_6,
	DIP_7,
	DIP_8,
	DIP_9,
	DIP_10,
	DIP_11,
	
	SPORT_0to4,
	SPORT_5to9,
	SPORT_10to14,//60
	SPORT_15to19,
	SPORT_20to24,
	SPORT_25to29,
	SPORT_30to39,
	SPORT_40to49,
	SPORT_50to59,
	SPORT_60to69,
	
	DPORT_0to9,
	DPORT_10to19,
	DPORT_20to29,//70
	DPORT_30to39,
	DPORT_40to49,
	DPORT_50to59,
	DPORT_60to69,
	DPORT_70to79,
	DPORT_80to89,
	DPORT_90to99,
	DPORT_100to109,
	DPORT_110to127,
	
	OFFSET_0to3,//80
	OFFSET_4to7,
	OFFSET_8to11,
	OFFSET_12to15,
	OFFSET_16to19,
	OFFSET_20to23,
	OFFSET_24to27,
	OFFSET_28to31,
	OFFSET_32to35,
	OFFSET_36to39,
	OFFSET_40to43,//90
	OFFSET_44to47,
	OFFSET_48to51,
	OFFSET_52to55,
	OFFSET_56to59,
	OFFSET_60to63,//95
	
	MAX_RULE_NUMBER,
	OUT_OF_RANGE,
}RuleFormat_et;

#define DWBIT00 	0x00000001
#define DWBIT01 	0x00000002
#define DWBIT02 	0x00000004
#define DWBIT03 	0x00000008
#define DWBIT04 	0x00000010
#define DWBIT05 	0x00000020
#define DWBIT06 	0x00000040
#define DWBIT07 	0x00000080
#define DWBIT08 	0x00000100
#define DWBIT09 	0x00000200
#define DWBIT10 	0x00000400
#define DWBIT11 	0x00000800
#define DWBIT12 	0x00001000
#define DWBIT13 	0x00002000
#define DWBIT14 	0x00004000
#define DWBIT15 	0x00008000
#define DWBIT16 	0x00010000
#define DWBIT17 	0x00020000
#define DWBIT18 	0x00040000
#define DWBIT19 	0x00080000
#define DWBIT20 	0x00100000
#define DWBIT21 	0x00200000
#define DWBIT22 	0x00400000
#define DWBIT23 	0x00800000
#define DWBIT24 	0x01000000
#define DWBIT25 	0x02000000
#define DWBIT26 	0x04000000
#define DWBIT27 	0x08000000
#define DWBIT28 	0x10000000
#define DWBIT29 	0x20000000
#define DWBIT30 	0x40000000
#define DWBIT31 	0x80000000

/*-----------------------------
|	  						|
|		TCAM Entry No.		|
|							|
-----------------------------*/
#define TCAM_Entry_Number						512
/*MAC*/
#define TCAM_MAC_Start_In_TCAM				0
#define TCAM_MAC_Number_Of_Set				10
#define TCAM_MAC_Entry_Per_Set				3
#define TCAM_MAC_Total_Entry					TCAM_MAC_Number_Of_Set*TCAM_MAC_Entry_Per_Set
/*VLAN*/
#define TCAM_VLAN_Start_In_TCAM				TCAM_MAC_Start_In_TCAM+TCAM_MAC_Total_Entry
#define TCAM_VLAN_Number_Of_Set				16
#define TCAM_VLAN_Entry_Per_Set				1
#define TCAM_VLAN_Total_Entry					TCAM_VLAN_Number_Of_Set*TCAM_VLAN_Entry_Per_Set
/*TYPE*/
#define TCAM_TYPE_Start_In_TCAM				TCAM_VLAN_Start_In_TCAM+TCAM_VLAN_Total_Entry
#define TCAM_TYPE_Number_Of_Set				16
#define TCAM_TYPE_Entry_Per_Set				1
#define TCAM_TYPE_Total_Entry					TCAM_TYPE_Number_Of_Set*TCAM_TYPE_Entry_Per_Set
/*PTL IPv4*/
#define TCAM_PTLv4_Start_In_TCAM				TCAM_TYPE_Start_In_TCAM+TCAM_TYPE_Total_Entry
#define TCAM_PTLv4_Number_Of_Set				12
#define TCAM_PTLv4_Entry_Per_Set				1
#define TCAM_PTLv4_Total_Entry					TCAM_PTLv4_Number_Of_Set*TCAM_PTLv4_Entry_Per_Set
/*PTL IPv6*/
#define TCAM_PTLv6_Start_In_TCAM				TCAM_PTLv4_Start_In_TCAM+TCAM_PTLv4_Total_Entry
#define TCAM_PTLv6_Number_Of_Set				12
#define TCAM_PTLv6_Entry_Per_Set				1
#define TCAM_PTLv6_Total_Entry					TCAM_PTLv6_Number_Of_Set*TCAM_PTLv6_Entry_Per_Set
/*SIPv4*/
#define TCAM_SIPv4_Start_In_TCAM				TCAM_PTLv6_Start_In_TCAM+TCAM_PTLv6_Total_Entry
#define TCAM_SIPv4_Number_Of_Set				4
#define TCAM_SIPv4_Entry_Per_Set				2
#define TCAM_SIPv4_Total_Entry					TCAM_SIPv4_Number_Of_Set*TCAM_SIPv4_Entry_Per_Set
/*DIPv4*/
#define TCAM_DIPv4_Start_In_TCAM				TCAM_SIPv4_Start_In_TCAM+TCAM_SIPv4_Total_Entry
#define TCAM_DIPv4_Number_Of_Set				12
#define TCAM_DIPv4_Entry_Per_Set				2
#define TCAM_DIPv4_Total_Entry					TCAM_DIPv4_Number_Of_Set*TCAM_DIPv4_Entry_Per_Set
/*SIPv6*/
#define TCAM_SIPv6_Start_In_TCAM				TCAM_DIPv4_Start_In_TCAM+TCAM_DIPv4_Total_Entry
#define TCAM_SIPv6_Number_Of_Set				4
#define TCAM_SIPv6_Entry_Per_Set				8
#define TCAM_SIPv6_Total_Entry					TCAM_SIPv6_Number_Of_Set*TCAM_SIPv6_Entry_Per_Set
/*DIPv6*/
#define TCAM_DIPv6_Start_In_TCAM				TCAM_SIPv6_Start_In_TCAM+TCAM_SIPv6_Total_Entry
#define TCAM_DIPv6_Number_Of_Set				12
#define TCAM_DIPv6_Entry_Per_Set				8
#define TCAM_DIPv6_Total_Entry					TCAM_DIPv6_Number_Of_Set*TCAM_DIPv6_Entry_Per_Set
/*SPORT*/
#define TCAM_SPORT_Start_In_TCAM				TCAM_DIPv6_Start_In_TCAM+TCAM_DIPv6_Total_Entry
#define TCAM_SPORT_Number_Of_Set			70//30
#define TCAM_SPORT_Entry_Per_Set				1
#define TCAM_SPORT_Total_Entry				TCAM_SPORT_Number_Of_Set*TCAM_SPORT_Entry_Per_Set
/*DPORT*/
#define TCAM_DPORT_Start_In_TCAM				TCAM_SPORT_Start_In_TCAM+TCAM_SPORT_Total_Entry
#define TCAM_DPORT_Number_Of_Set			128//72
#define TCAM_DPORT_Entry_Per_Set				1
#define TCAM_DPORT_Total_Entry				TCAM_DPORT_Number_Of_Set*TCAM_DPORT_Entry_Per_Set
/*Teredo SPORT*/
#define TCAM_Teredo_SPORT_Start_In_TCAM		TCAM_DPORT_Start_In_TCAM+TCAM_DPORT_Total_Entry
#define TCAM_Teredo_SPORT_Number_Of_Set		1
#define TCAM_Teredo_SPORT_Entry_Per_Set		1
#define TCAM_Teredo_SPORT_Total_Entry			TCAM_Teredo_SPORT_Number_Of_Set*TCAM_Teredo_SPORT_Entry_Per_Set
/*Teredo DPORT*/
#define TCAM_Teredo_DPORT_Start_In_TCAM		TCAM_Teredo_SPORT_Start_In_TCAM+TCAM_Teredo_SPORT_Total_Entry
#define TCAM_Teredo_DPORT_Number_Of_Set		1
#define TCAM_Teredo_DPORT_Entry_Per_Set		1
#define TCAM_Teredo_DPORT_Total_Entry			TCAM_Teredo_DPORT_Number_Of_Set*TCAM_Teredo_DPORT_Entry_Per_Set
/*UDP_ESP SPORT*/
#define TCAM_UDP_ESP_SPORT_Start_In_TCAM	TCAM_Teredo_DPORT_Start_In_TCAM+TCAM_Teredo_DPORT_Total_Entry
#define TCAM_UDP_ESP_SPORT_Number_Of_Set	1
#define TCAM_UDP_ESP_SPORT_Entry_Per_Set		1
#define TCAM_UDP_ESP_SPORT_Total_Entry		TCAM_UDP_ESP_SPORT_Number_Of_Set*TCAM_UDP_ESP_SPORT_Entry_Per_Set
/*UDP_ESP DPORT*/
#define TCAM_UDP_ESP_DPORT_Start_In_TCAM	TCAM_UDP_ESP_SPORT_Start_In_TCAM+TCAM_UDP_ESP_SPORT_Total_Entry
#define TCAM_UDP_ESP_DPORT_Number_Of_Set	1
#define TCAM_UDP_ESP_DPORT_Entry_Per_Set		1
#define TCAM_UDP_ESP_DPORT_Total_Entry		TCAM_UDP_ESP_DPORT_Number_Of_Set*TCAM_UDP_ESP_DPORT_Entry_Per_Set
/*OFFSET*/
#define TCAM_OFFSET_Start_In_TCAM			TCAM_UDP_ESP_DPORT_Start_In_TCAM+TCAM_UDP_ESP_DPORT_Total_Entry
#define TCAM_OFFSET_Number_Of_Set			64
#define TCAM_OFFSET_Entry_Per_Set				1
#define TCAM_OFFSET_Total_Entry				TCAM_OFFSET_Number_Of_Set*TCAM_OFFSET_Entry_Per_Set

#define REG32(reg)			(*(volatile INT32U *)(reg))
#define REG16(reg)			(*(volatile INT16U *)(reg))
#define REG8(reg)		  	(*(volatile INT8U  *)(reg))

#define IO_TCAM_DATA			0x02B0
#define IO_TCAM_PORT			0x02B4
#define IO_TCAM_DOUT			0x02B8
#define IO_TCAM_VOUT			0x02BC
#define IO_PKT_RULE_ACT0		0x0200
#define IO_PKT_LKBT0_SET0	0x0210	
#define IO_PKT_CLR			0x0250
#define IO_PKT_RULE0			0x0300
#define IO_PKT_RULE1			0x0310
#define IO_PKT_RULE_EN		0x02F0

static const TCAM_Property_st TCAM_Property = {
	{
		TCAM_MAC_Start_In_TCAM,			TCAM_VLAN_Start_In_TCAM,
		TCAM_TYPE_Start_In_TCAM,			TCAM_PTLv4_Start_In_TCAM,
		TCAM_PTLv6_Start_In_TCAM,			TCAM_SIPv4_Start_In_TCAM,
		TCAM_DIPv4_Start_In_TCAM, 			TCAM_SIPv6_Start_In_TCAM,
		TCAM_DIPv6_Start_In_TCAM,			TCAM_SPORT_Start_In_TCAM,
		TCAM_DPORT_Start_In_TCAM,			TCAM_Teredo_SPORT_Start_In_TCAM,
		TCAM_Teredo_DPORT_Start_In_TCAM,	TCAM_UDP_ESP_SPORT_Start_In_TCAM,
		TCAM_UDP_ESP_DPORT_Start_In_TCAM,	TCAM_OFFSET_Start_In_TCAM,
	},	
	{
		TCAM_MAC_Number_Of_Set,			TCAM_VLAN_Number_Of_Set,
		TCAM_TYPE_Number_Of_Set,			TCAM_PTLv4_Number_Of_Set,
		TCAM_PTLv6_Number_Of_Set,			TCAM_SIPv4_Number_Of_Set,
		TCAM_DIPv4_Number_Of_Set, 			TCAM_SIPv6_Number_Of_Set,
		TCAM_DIPv6_Number_Of_Set,			TCAM_SPORT_Number_Of_Set,
		TCAM_DPORT_Number_Of_Set,		TCAM_Teredo_SPORT_Number_Of_Set,
		TCAM_Teredo_DPORT_Number_Of_Set,	TCAM_UDP_ESP_SPORT_Number_Of_Set,
		TCAM_UDP_ESP_DPORT_Number_Of_Set,	TCAM_OFFSET_Number_Of_Set,
	},
	{
		TCAM_MAC_Entry_Per_Set,			TCAM_VLAN_Entry_Per_Set,
		TCAM_TYPE_Entry_Per_Set,			TCAM_PTLv4_Entry_Per_Set,
		TCAM_PTLv6_Entry_Per_Set,			TCAM_SIPv4_Entry_Per_Set,
		TCAM_DIPv4_Entry_Per_Set, 			TCAM_SIPv6_Entry_Per_Set,
		TCAM_DIPv6_Entry_Per_Set,			TCAM_SPORT_Entry_Per_Set,
		TCAM_DPORT_Entry_Per_Set,			TCAM_Teredo_SPORT_Entry_Per_Set,
		TCAM_Teredo_DPORT_Entry_Per_Set,	TCAM_UDP_ESP_SPORT_Entry_Per_Set,
		TCAM_UDP_ESP_DPORT_Entry_Per_Set,	TCAM_OFFSET_Entry_Per_Set,
	},
};

//Ret_Code_et TCAM_OCP_Write(struct rtl8168_private *tp, u16 DataBit, u16 DontCareBit, u8 Valid, u16 entry_number,
//                                                                                ONOFF_Switch_et data, ONOFF_Switch_et care, ONOFF_Switch_et valid);
//Ret_Code_et TCAM_WriteRule(struct rtl8168_private *tp, Rule_NO_et number, RuleFormat_et bit, ONOFF_Switch_et OnOff);

#if 0//PKTFilter_Test
Ret_Code_et TCAM_OCP_Write(u16 DataBit, u16 DontCareBit, u8 Valid, u16 entry_number,
										ONOFF_Switch_et data, ONOFF_Switch_et care, ONOFF_Switch_et valid);
Ret_Code_et TCAM_WriteRule(Rule_NO_et number, RuleFormat_et bit, ONOFF_Switch_et OnOff);
void PacketFilterInit(void);

void clearAllPFRule();
void setTCAMData(TCAM_Allocation* info);
Rule_NO_et setPFRule(TCAMRule* rule);
void RstSharePFRuleMem(TCAMRule* rstRule, TCAMRule* copyRule);
void SetOOBBasicRule();
void EnableIBIPv6ICMPPFRule();
void DisableIBIPv6ICMPPFRule();
void EnableOOBDHCPv6PFRule();
void DisableOOBDHCPv6PFRule();
#endif

/*----------------------------Software Define-------------------------------------*/


enum PFRuleIdx
{
	ArpFRule =0,
	OOBUnicastPFRule = 1, //Packets match OOB Mac Address, IPv4, TCP/UCP/ICMP will be accepted
	OOBIPv6PFRule = 2, //Packets match OOB Mac Address/Multicast address, IPv6 will be accepted
	OOBPortPFRule = 3, //Filter multicast/broadcast packets(Do not care ip version?)
	IBIPv4TCPPortPFRule = 1,
	IBIPv4UDPPortPFRule = 2,
	IBIPv6TCPPortPFRule = 3,
	IBIPv6UDPPortPFRule = 4,		
	IBIPv6ICMPPFRule = 5,		
	NumOfPFRule,
};

enum TCAMSetIdx
{
	TCAMMacIDSet = 0,
	TCAMVLANTagSet,
	TCAMTypeSet,
	TCAMIPv4PTLSet,
	TCAMIPv6PTLSet,
	TCAMDIPv4Set,
	TCAMDIPv6Set,
	TCAMDDPortSet,		
	NumOfTCAMSet,
};

enum IPv4ListIdx
{
	UniIPv4Addr = 0,
	LBIPv4Addr,
	GBIPv4Addr,
	MIPv4Addr,	
	NumOfIPv4Addr,
};

enum IPv6ListIdx
{
	UniIPv6Addr = 0,
	LLIPv6Addr,
	NumOfIPv6Addr,
};


enum EthTypeBitMap
{
	IPv4TypeBitMap = 1<<0,
	IPv6TypeBitMap = 1<<1,
	ARPTypeBitMap = 1<<2,
	VLANTypeBitMap = 1<<3,	
};

enum MacIDBitMap
{
	IBMacBitMap = 1<<0,
	OOBMacBitMap = 1<<1,
	Mac8021xBitMap = 1<<2,
	PTPv1MacBitMap = 1<<3,	
	PTPv2MacBitMap = 1<<3,	
};


enum IPv4AddrBitMap
{
	EnableUniIPv4Addr = 1<<0,
	EnableLBIPv4Addr = 1<<1,
	EnableGBIPv4Addr = 1<<2,
	EnableMIPv4Addr = 1<<3,
};

enum IPv6AddrBitMap
{
	EnableUniIPv6Addr = 1<<0,
	EnableLLLIPv6Addr = 1<<1,
	EnableMIPv6Addr = 1<<2,
};

enum IPv4PTLBitMap
{
	IPv4PTLTCP = 	1<<0,
	IPv4PTLUDP = 	1<<1,
	IPv4PTLICMP = 	1<<2,
	IPv4PTLAH =	1<<3,
	IPv4PTLESP =	1<<4,
	IPv4PTLIPV4IPV6 =1<<5,	
};

enum IPv6PTLBitMap
{
	IPv6PTLTCP =	1<<0,
	IPv6PTLUDP =	1<<1,
	IPv6PTLICMP =	1<<2,
	IPv6PTLAH = 1<<3,
	IPv6PTLESP =	1<<4,
	IPv6PTLHopByHop =1<<5,	
	IPv6PTLRoute =1<<6,
	IPv6PTLDest =1<<7,		
	IPv6PTLFrag =1<<8,

};

#endif

#undef DBG_reg
//#undef DEBUG_RTL8168
//#undef DEBUG_RTL8168_TX
//#undef DEBUG_RTL8168_RX

//#define DEBUG_RTL8168
//#define DEBUG_RTL8168_TX
//#define DEBUG_RTL8168_RX

#define drv_version "v0.1"
#define drv_date "01-08-2017"

/* Condensed operations for readability. */
#define currticks()	get_timer(0)

/* MAC address length*/
#define MAC_ADDR_LEN	6

/* max supported gigabit ethernet frame size -- must be at least (dev->mtu+14+4).*/
#define MAX_ETH_FRAME_SIZE	1536

#define PA2VA(vaddr)            ((u32) (vaddr) | 0x80000000)
#define VA2PA(vaddr)            ((u32) (vaddr) & ~0x80000000)

#define RX_BUF_SIZE 1536
#define R8168_TX_RING_BYTES (NUM_TX_DESC * sizeof(struct TxDesc))
#define R8168_RX_RING_BYTES (NUM_RX_DESC * sizeof(struct RxDesc))

#define NUM_TX_DESC	16	/* Number of Tx descriptor registers */
#define NUM_RX_DESC	16	/* Number of Rx descriptor registers */

#define TX_TIMEOUT  (6*HZ)

/* write/read MMIO register. Notice: {read,write}[wl] do the necessary swapping */
#define RTL_W8(reg, val8)	writeb ((val8), ioaddr + (reg))
#define RTL_W16(reg, val16)	writew ((val16), ioaddr + (reg))
#define RTL_W32(reg, val32)	writel ((val32), ioaddr + (reg))
#define RTL_R8(reg)		readb (ioaddr + (reg))
#define RTL_R16(reg)		readw (ioaddr + (reg))
#define RTL_R32(reg)		((unsigned long) readl (ioaddr + (reg)))

#define ETH_FRAME_LEN	MAX_ETH_FRAME_SIZE
#define ETH_ALEN	MAC_ADDR_LEN
#define ETH_ZLEN	60

#define AUTONEG_ENABLE		0x01
#define DUPLEX_HALF		0x00
#define DUPLEX_FULL		0x01
#define SPEED_10		10
#define SPEED_100		100
#define SPEED_1000		1000

#define bus_to_phys(addr)  (addr)	
//pci_mem_to_phys((pci_dev_t)dev->priv, (pci_addr_t)a)

//#define pci_mem_to_phys(dev, addr)  (addr)

#define _RXDESC_PER_CACHELINE (ARCH_ALIGN/sizeof(struct RxDesc)) 
#define ARCH_ALIGN 64



enum RTL8168_registers {
    MAC0            = 0x00,     /* Ethernet hardware address. */
    MAC4            = 0x04,
    MAR0            = 0x08,     /* Multicast filter. */
    TxDescStartAddrLow  = 0x24,   
    RxDescAddrLow = 0x28,   
    IntrMask        = 0x2C,
    IntrStatus      = 0x2E,
    TxPoll          = 0x30,
    ChipCmd         = 0x36,    
    TxConfig        = 0x40,
    RxConfig        = 0x44,    
    CPlusCmd        = 0x48,
};

enum RTL8168_register_content {
    /* InterruptStatusBits */
    SYSErr      = 0x8000,
    PCSTimeout  = 0x4000,
    SWInt       = 0x0100,
    TxDescUnavail   = 0x0080,
    RxFIFOOver  = 0x0040,
    LinkChg     = 0x0020,
    RxDescUnavail   = 0x0010,
    TxErr       = 0x0008,
    TxOK        = 0x0004,
    RxErr       = 0x0002,
    RxOK        = 0x0001,

    /* RxStatusDesc */
    RxRWT = (1 << 22),
    RxRES = (1 << 21),
    RxRUNT = (1 << 20),
    RxCRC = (1 << 19),

    /* ChipCmdBits */
    StopReq  = 0x80,
    CmdReset = 0x10,
    CmdRxEnb = 0x08,
    CmdTxEnb = 0x04,
    RxBufEmpty = 0x01,

    /* Cfg9346Bits */
    Cfg9346_Lock = 0x00,
    Cfg9346_Unlock = 0xC0,
    Cfg9346_EEDO = (1 << 0),
    Cfg9346_EEDI = (1 << 1),
    Cfg9346_EESK = (1 << 2),
    Cfg9346_EECS = (1 << 3),
    Cfg9346_EEM0 = (1 << 6),
    Cfg9346_EEM1 = (1 << 7),

    /* rx_mode_bits */
    AcceptErr = 0x20,
    AcceptRunt = 0x10,
    AcceptBroadcast = 0x08,
    AcceptMulticast = 0x04,
    AcceptMyPhys = 0x02,
    AcceptAllPhys = 0x01,

    /* Transmit Priority Polling*/
    HPQ = 0x80,
    NPQ = 0x40,
    FSWInt = 0x01,

    /* RxConfigBits */
    Reserved2_shift = 13,
    RxCfgDMAShift = 8,
    RxCfg_128_int_en = (1 << 15),
    RxCfg_fet_multi_en = (1 << 14),
    RxCfg_half_refetch = (1 << 13),
    RxCfg_9356SEL = (1 << 6),

    /* TxConfigBits */
    TxInterFrameGapShift = 24,
    TxDMAShift = 8, /* DMA burst value (0-7) is shift this many bits */
    TxMACLoopBack = (1 << 17),  /* MAC loopback */


    /* CPlusCmd */
    EnableBist  = (1 << 15),
    Macdbgo_oe  = (1 << 14),
    Normal_mode = (1 << 13),
    Force_halfdup   = (1 << 12),
    Force_rxflow_en = (1 << 11),
    Force_txflow_en = (1 << 10),
    Cxpl_dbg_sel    = (1 << 9),//This bit is reserved in RTL8168B
    ASF     = (1 << 8),//This bit is reserved in RTL8168C
    PktCntrDisable  = (1 << 7),
    RxVlan      = (1 << 6),
    RxChkSum    = (1 << 5),
    Macdbgo_sel = 0x001C,
    INTT_0      = 0x0000,
    INTT_1      = 0x0001,
    INTT_2      = 0x0002,
    INTT_3      = 0x0003,

    /* DBG_reg */
    Fix_Nak_1 = (1 << 4),
    Fix_Nak_2 = (1 << 3),
    DBGPIN_E2 = (1 << 0),

    /* DumpCounterCommand */
    CounterDump = 0x8, 

    /* MCU Command */
    Now_is_oob = (1 << 7),
    Txfifo_empty = (1 << 5),
    Rxfifo_empty = (1 << 4),

    /* GPIO */
    GPIO_en = (1 << 0),

};

enum _DescStatusBit {
    DescOwn     = (1 << 31), /* Descriptor is owned by NIC */
    RingEnd     = (1 << 30), /* End of descriptor ring */
    FirstFrag   = (1 << 29), /* First segment of a packet */
    LastFrag    = (1 << 28), /* Final segment of a packet */

    /* Tx private */
    /*------ offset 0 of tx descriptor ------*/
    LargeSend   = (1 << 27), /* TCP Large Send Offload (TSO) */
    LargeSend_DP = (1 << 16), /* TCP Large Send Offload (TSO) */
    MSSShift    = 16,        /* MSS value position */
    MSSMask     = 0x7FFU,    /* MSS value 11 bits */
    TxIPCS      = (1 << 18), /* Calculate IP checksum */
    TxUDPCS     = (1 << 17), /* Calculate UDP/IP checksum */
    TxTCPCS     = (1 << 16), /* Calculate TCP/IP checksum */
    TxVlanTag   = (1 << 17), /* Add VLAN tag */

    /*@@@@@@ offset 4 of tx descriptor => bits for RTL8168C/CP only     begin @@@@@@*/
    TxUDPCS_C   = (1 << 31), /* Calculate UDP/IP checksum */
    TxTCPCS_C   = (1 << 30), /* Calculate TCP/IP checksum */
    TxIPCS_C    = (1 << 29), /* Calculate IP checksum */
    /*@@@@@@ offset 4 of tx descriptor => bits for RTL8168C/CP only     end @@@@@@*/


    /* Rx private */
    /*------ offset 0 of rx descriptor ------*/
    PID1        = (1 << 18), /* Protocol ID bit 1/2 */
    PID0        = (1 << 17), /* Protocol ID bit 2/2 */

#define RxProtoUDP  (PID1)
#define RxProtoTCP  (PID0)
#define RxProtoIP   (PID1 | PID0)
#define RxProtoMask RxProtoIP

    RxIPF       = (1 << 16), /* IP checksum failed */
    RxUDPF      = (1 << 15), /* UDP/IP checksum failed */
    RxTCPF      = (1 << 14), /* TCP/IP checksum failed */
    RxVlanTag   = (1 << 16), /* VLAN tag available */

    /*@@@@@@ offset 0 of rx descriptor => bits for RTL8168C/CP only     begin @@@@@@*/
    RxUDPT      = (1 << 18),
    RxTCPT      = (1 << 17),
    /*@@@@@@ offset 0 of rx descriptor => bits for RTL8168C/CP only     end @@@@@@*/

    /*@@@@@@ offset 4 of rx descriptor => bits for RTL8168C/CP only     begin @@@@@@*/
    RxV6F       = (1 << 31),
    RxV4F       = (1 << 30),
    /*@@@@@@ offset 4 of rx descriptor => bits for RTL8168C/CP only     end @@@@@@*/
};

enum bits {
    BIT_0 = (1 << 0),
    BIT_1 = (1 << 1),
    BIT_2 = (1 << 2),
    BIT_3 = (1 << 3),
    BIT_4 = (1 << 4),
    BIT_5 = (1 << 5),
    BIT_6 = (1 << 6),
    BIT_7 = (1 << 7),
    BIT_8 = (1 << 8),
    BIT_9 = (1 << 9),
    BIT_10 = (1 << 10),
    BIT_11 = (1 << 11),
    BIT_12 = (1 << 12),
    BIT_13 = (1 << 13),
    BIT_14 = (1 << 14),
    BIT_15 = (1 << 15),
    BIT_16 = (1 << 16),
    BIT_17 = (1 << 17),
    BIT_18 = (1 << 18),
    BIT_19 = (1 << 19),
    BIT_20 = (1 << 20),
    BIT_21 = (1 << 21),
    BIT_22 = (1 << 22),
    BIT_23 = (1 << 23),
    BIT_24 = (1 << 24),
    BIT_25 = (1 << 25),
    BIT_26 = (1 << 26),
    BIT_27 = (1 << 27),
    BIT_28 = (1 << 28),
    BIT_29 = (1 << 29),
    BIT_30 = (1 << 30),
    BIT_31 = (1 << 31)
};

enum effuse {
    EFUSE_SUPPORT = 1,
    EFUSE_NOT_SUPPORT = 0,
};

struct TxDesc {
    u32 opts1;
    u32 opts2;
    u64 addr;
};

struct RxDesc {
    u32 opts1;
    u32 opts2;
    u64 addr;
};


struct rtl8168_private {
    struct eth_device *dev;
    volatile void * mmio_addr;	/* memory map physical address */
    pci_dev_t devno;
    int chipset;
    u32 mcfg;
    u32 cur_page;
    u32 cur_rx;	/* Index into the Rx descriptor buffer of next Rx pkt. */
    u32 cur_tx;	/* Index into the Tx descriptor buffer of next Rx pkt. */
    u32 dirty_rx;
    u32 dirty_tx;
    u8 *DescArrays;	/* Index of Descriptor buffer */
    struct TxDesc *TxDescArray;	/* Index of 256-alignment Tx Descriptor buffer */
    struct RxDesc *RxDescArray;	/* Index of 256-alignment Rx Descriptor buffer */
    u8 *RxBufferRings;	/* Index of Rx Buffer  */
    u8 *RxBufferRing[NUM_RX_DESC];	/* Index of Rx Buffer array */

    u8  efuse;
    u32 rx_buf_sz;
    u32 rtl8168_rx_config;

    u16 cp_cmd;
};

#define ALIGN_8                 (0x7)
#define ALIGN_16                (0xf)
#define ALIGN_32                (0x1f)
#define ALIGN_64                (0x3f)
#define ALIGN_256               (0xff)
/*
typedef struct _GDUMP_TALLY {
    u64 TxOK;
    u64 RxOK;
    u64 TxERR;
    u32 RxERR;
    u16 MissPkt;
    u16 FAE;
    u32 Tx1Col;
    u32 TxMCol;
    u64 RxOKPhy;
    u64 RxOKBrd;
    u32 RxOKMul;
    u16 TxAbt;
    u16 TxUndrn; //only possible on jumbo frames
}

GDUMP_TALLY, *PGDUMP_TALLY;

static u8 dummy[sizeof( GDUMP_TALLY ) + ALIGN_64];
*/


static Rule_NO_et 	Rule_NO = 0;
TCAM_Allocation TCAMMem[NumOfTCAMSet];
TCAMRule TCAMRuleMem[NumOfPFRule];



/*****************************************************************************
 * Author : Han Wang (HanWang@realtek.com)

 * Create date : 2011/03/23
 
 * DESCRIPTION: TCAM_OCP_Read
 
 *****************************************************************************/
Ret_Code_et TCAM_OCP_Read(struct rtl8168_private *tp, u32 entry_number, TCAM_ReadType_et type, TCAM_Entry_Setting_st *pstTCAM_Table)
{
	volatile void * ioaddr = tp->mmio_addr;
	volatile u32 reg_data = 0;
	
	if(entry_number >TCAM_Entry_Number)
		return RET_FAIL;

	/*TCAM_ACC_FLAG*/
	reg_data |= DWBIT31;
	/*RW_SEL*/
	reg_data &= ~DWBIT30;
	/*TCAM_WM : read type select*/
	if(type == TCAM_data)
		reg_data |= DWBIT13;
	else
		reg_data &= ~DWBIT13;
	reg_data |= DWBIT12;	
	/*TCAM_ADDR*/
	reg_data |= (entry_number&0x000001FF);
	
	RTL_W32(IO_TCAM_PORT,reg_data);
	while((DWBIT31&RTL_R32(IO_TCAM_PORT))== DWBIT31){};

	reg_data = 0;
	reg_data = RTL_R32(IO_TCAM_DOUT);
	
	if(type == TCAM_data)
		pstTCAM_Table->Value = 0x0000FFFF & reg_data;
	else
		pstTCAM_Table->DontCareBit = 0x0000FFFF & (reg_data>>16);

	//pstTCAM_Table->Valid = ((DWBIT16 & reg_data) >> 16);
	pstTCAM_Table->Valid = RTL_R32(IO_TCAM_VOUT)&0x01;

	return RET_OK;
}

Ret_Code_et TCAM_OCP_Write(struct rtl8168_private *tp, u16 DataBit, u16 DontCareBit, u8 Valid, u16 entry_number,
										ONOFF_Switch_et data, ONOFF_Switch_et care, ONOFF_Switch_et valid)
{
	volatile void * ioaddr = tp->mmio_addr;
	u32  reg_data = 0;
	TCAM_Entry_Setting_st stTCAM_Table={0};
	
	//patch solution
	TCAM_OCP_Read(tp, entry_number, TCAM_data, &stTCAM_Table);
	TCAM_OCP_Read(tp, entry_number, TCAM_care, &stTCAM_Table);
	
	reg_data |= (DontCareBit<<16)&0xFFFF0000;
	reg_data |= DataBit;
	RTL_W32(IO_TCAM_DATA,reg_data);
	
	reg_data = 0;
	/*TCAM_ACC_FLAG*/
	reg_data |= DWBIT31;
	/*RW_SEL*/
	reg_data |= DWBIT30;
	/*VALID BIT*/
	reg_data |= (Valid&0x01)<<16;
	/*TCAM_WM : valid*/	
	if(data == Switch_OFF)
		reg_data |= DWBIT15;
	/*TCAM_WM : care*/	
	if(care == Switch_OFF)
		reg_data |= DWBIT14;
	/*TCAM_WM : data*/
	if(valid == Switch_OFF)
		reg_data |= DWBIT13;
	/*TCAM_ADDR*/
	reg_data |= (entry_number&0x000001FF);
	
	RTL_W32(IO_TCAM_PORT, reg_data);
	while((DWBIT31&RTL_R32(IO_TCAM_PORT))== DWBIT31);

	return RET_OK;
}

Ret_Code_et TCAM_AccessEntry(struct rtl8168_private *tp, TCAM_Entry_Type_et Type, u16 Number, u16 Set, TCAM_RW_et RW, TCAM_Entry_Setting_st *value)
{
	u32 entry_number = 0;
	Ret_Code_et ret = RET_OK;
	
	if(Number >= TCAM_Property.Entry_Per_Set[Type])
		return RET_FAIL;
	if(Set >= TCAM_Property.Number_Of_Set[Type])
		return RET_FAIL;
	if(RW >= TCAM_RW_MAX)
		return RET_FAIL;
	if(value == NULL)
		return RET_FAIL;

	entry_number = TCAM_Property.Start_In_TCAM[Type] + (Set*TCAM_Property.Entry_Per_Set[Type])+Number;

	if(RW == TCAM_Read)
	{
		ret = TCAM_OCP_Read(tp, entry_number, TCAM_data, value);
		//ret = TCAM_OCP_Read(entry_number, TCAM_care, value);
	}
	else
		ret = TCAM_OCP_Write(tp, value->Value, value->DontCareBit, value->Valid, entry_number, Switch_ON, Switch_ON, Switch_ON);

	return ret;
}

void PacketFilterSettingMAC(struct rtl8168_private *tp, u8 MAC_Set, u8 MAC_Address[6], ONOFF_Switch_et Valid)
{
	TCAM_Entry_Setting_st  st_TCAM_Entry_Setting = {0,};
	u8 i = 0;

	for(i=0; i<TCAM_Property.Entry_Per_Set[TCAM_MAC]; i++)
	{
		st_TCAM_Entry_Setting.Value = MAC_Address[i*2]<<8|MAC_Address[i*2+1];
		st_TCAM_Entry_Setting.DontCareBit = 0;
		st_TCAM_Entry_Setting.Valid = Valid;
		TCAM_AccessEntry(tp ,TCAM_MAC, i, MAC_Set, TCAM_Write, &st_TCAM_Entry_Setting);
	}
}

void PacketFillDefault(struct rtl8168_private *tp)
{
	volatile void * ioaddr = tp->mmio_addr;
	u8 MAC_Address[6],i;
	
	//GetMacAddr_F(MAC_Address, eth0);
	
	for (i = 0; i < MAC_ADDR_LEN; i++)
        	MAC_Address[i] = RTL_R8(MAC0 + i);
	PacketFilterSettingMAC(tp,1, MAC_Address, Switch_ON);
	
	/*
	PacketFilterSettingType(0, 0x0800, Switch_ON);
	PacketFilterSettingType(1, 0x86DD, Switch_ON);
	PacketFilterSettingType(2, 0x0806, Switch_ON);
	PacketFilterSettingType(3, 0x8100, Switch_ON);	*/
}




Ret_Code_et PacketFilterRuleEn(struct rtl8168_private *tp, Rule_NO_et number, ONOFF_Switch_et OnOff)
{
	volatile void * ioaddr = tp->mmio_addr;
	u32 data = 0;
	
	if(number > RULE_NO_MAX)
	{
		return RET_FAIL;
	}

	if(OnOff == Switch_ON)
	{
		data = RTL_R32(IO_PKT_RULE_EN);
		data |= (u32)0x00000001 << number;
		data |= DWBIT31;
		RTL_W32(IO_PKT_RULE_EN,data);
		while((DWBIT31&RTL_R32(IO_PKT_RULE_EN))== DWBIT31){};
	}
	else
	{
		data = RTL_R32(IO_PKT_RULE_EN);
		data &= ~((u32)0x00000001 << number);
		data |= DWBIT31;
		RTL_W32(IO_PKT_RULE_EN,data);
		while((DWBIT31&RTL_R32(IO_PKT_RULE_EN))== DWBIT31){};
	}
	return RET_OK;
}

void TCAM_RuleActionSet(struct rtl8168_private *tp, TCAM_RuleActionSet_st* act)
{
	volatile void * ioaddr = tp->mmio_addr;
	u32 value = 0;
	u32 offset = 0;
	u32 ActValue = 0;

	ActValue = (act->IB | act->OOB<<1 | act->Drop<<2 | act->Meter_No<<3 | act->Meter_En<<6 | act->Gamming<<7);
	offset = (act->number/4)*4;
	value = RTL_R32(IO_PKT_RULE_ACT0+offset);
	value &= ~((u32)0x000000FF<<((act->number%4)*8));
	value |= ActValue<<((act->number%4)*8);
	RTL_W32(IO_PKT_RULE_ACT0+offset,value);
}



Ret_Code_et TCAM_WriteRule(struct rtl8168_private *tp, Rule_NO_et number, RuleFormat_et bit, ONOFF_Switch_et OnOff)
{
	volatile void * ioaddr = tp->mmio_addr;	
	u32 rule = 0;

	if(number > RULE_NO_MAX || bit >MAX_RULE_NUMBER)
		return RET_FAIL;

	rule = IO_PKT_RULE0;
	rule = rule + (sizeof(u32)*4*number);
	rule = rule + (sizeof(u32)*(bit>>5));

	if(OnOff == Switch_ON)
		{RTL_W32(rule,RTL_R32(rule) | (0x01<<(bit%32)));}
	else
		{RTL_W32(rule,RTL_R32(rule) & (0x01<<(bit%32)));}

	return RET_OK;
}

RuleFormat_et TCAM_GetRuleBit (TCAM_Entry_Type_et Type, u16 Set)
{
	switch(Type)
	{
		case TCAM_MAC:
				return MAC_0+Set;
		case TCAM_MARIB:
				return MARI;
		case TCAM_MAROOB:
				return MARO;
		case TCAM_VLAN:
				if(Set<=3)
					return VLAN_0+Set;
				else if(Set<=5)
					return VLAN_4to5;
				else if(Set<=10)
					return VLAN_6to10;
				else if(Set<=15)
					return VLAN_11to15;
				else
					return OUT_OF_RANGE;
		case TCAM_TYPE:
				if(Set<=7)
					return TYPE_0+Set;
				else if(Set<=11)
					return TYPE_8to11;
				else if(Set<=15)
					return TYPE_12to15;
				else
					return OUT_OF_RANGE;
		case TCAM_PTLv4:
		case TCAM_PTLv6:
				return PTL_0+Set;
		case TCAM_SIPv4:
		case TCAM_SIPv6:
				return SIP_0+Set;
		case TCAM_DIPv4:
		case TCAM_DIPv6:
				return DIP_0+Set;			
		case TCAM_SPORT:
				if(Set<=4)
					return SPORT_0to4;
				else if(Set<=9)
					return SPORT_5to9;
				else if(Set<=14)
					return SPORT_10to14;
				else if(Set<=19)
					return SPORT_15to19;
				else if(Set<=24)
					return SPORT_20to24;
				else if(Set<=29)
					return SPORT_25to29;
				else if(Set<=39)
					return SPORT_30to39;
				else if(Set<=49)
					return SPORT_40to49;
				else if(Set<=59)
					return SPORT_50to59;
				else if(Set<=69)
					return SPORT_60to69;
				else
					return OUT_OF_RANGE;
		case TCAM_DPORT:
				if(Set<=9)
					return DPORT_0to9;
				else if(Set<=19)
					return DPORT_10to19;
				else if(Set<=29)
					return DPORT_20to29;
				else if(Set<=39)
					return DPORT_30to39;
				else if(Set<=49)
					return DPORT_40to49;
				else if(Set<=59)
					return DPORT_50to59;
				else if(Set<=69)
					return DPORT_60to69;
				else if(Set<=79)
					return DPORT_70to79;
				else if(Set<=89)
					return DPORT_80to89;
				else if(Set<=99)
					return DPORT_90to99;
				else if(Set<=109)
					return DPORT_100to109;
				else if(Set<=127)
					return DPORT_110to127;
				else
					return OUT_OF_RANGE;
		case TCAM_Teredo_SPORT:			
		case TCAM_Teredo_DPORT:
		case TCAM_UDP_ESP_SPORT:
		case TCAM_UDP_ESP_DPORT:
			return 100;
		case TCAM_OFFSET:
				if(Set<=3)
					return OFFSET_0to3;
				else if(Set<=7)
					return OFFSET_4to7;
				else if(Set<=11)
					return OFFSET_8to11;
				else if(Set<=15)
					return OFFSET_12to15;
				else if(Set<=19)
					return OFFSET_16to19;
				else if(Set<=23)
					return OFFSET_20to23;
				else if(Set<=27)
					return OFFSET_24to27;
				else if(Set<=31)
					return OFFSET_28to31;
				else if(Set<=35)
					return OFFSET_32to35;
				else if(Set<=39)
					return OFFSET_36to39;
				else if(Set<=43)
					return OFFSET_40to43;
				else if(Set<=47)
					return OFFSET_44to47;
				else if(Set<=51)
					return OFFSET_48to51;
				else if(Set<=55)
					return OFFSET_52to55;
				else if(Set<=59)
					return OFFSET_56to59;
				else if(Set<=63)
					return OFFSET_60to63;				
				else
					return OUT_OF_RANGE;
		default:
			return OUT_OF_RANGE;
	}
}

void PacketFilterInit(struct rtl8168_private *tp)
{
	volatile void * ioaddr = tp->mmio_addr;
	u32 i = 0, j = 0;

	//TCAM vaild bit clear and all Leaky Bucket clear
	RTL_W32(IO_PKT_CLR,0x800000FF);
	//bsp_wait(100);

	//Fill default value
	PacketFillDefault(tp);
	
	//disable all rule to every rule
	for(i=0; i<RULE_NO_MAX; i++)
	{
		for(j=0; j<MAX_RULE_NUMBER; j++)
		{
			TCAM_WriteRule(tp, i, j, Switch_OFF);
		}
	}

	//Rule 0 use IB RCR setting and disable all rule
	RTL_W32(IO_PKT_RULE_EN, 0x10000000);
	while(DWBIT31 == (DWBIT31 & RTL_R32(IO_PKT_RULE_EN))){};

	//enable rule 0
	//PacketFilterRuleEn(RULE_NO_0, Switch_ON);
	//PacketEntryRuleTabRst();
}

Rule_NO_et PacketFilterSetRule(struct rtl8168_private *tp, TCAMRule* rule, Rule_NO_et rule_no)
{
	u32 i = 0;
	TCAM_RuleActionSet_st act = {0,};
	
	if(rule_no >= RULE_NO_MAX)
		return RULE_SETTING_FAIL;			
	
	if(rule->MARType & UNICAST_MAR)
	{
		for(i=0; i<16; i++)
		{
			if(rule->MACIDBitMap& (u16)0x01<<i)
				TCAM_WriteRule(tp, rule_no, TCAM_GetRuleBit(TCAM_MAC, i), Switch_ON);
		}
	}
	
	if(rule->MARType & MULCAST_MAR)
	{
		if(rule->IBMAR)
			TCAM_WriteRule(tp, rule_no, MARI, Switch_ON);
		if(rule->OOBMAR)
			TCAM_WriteRule(tp, rule_no, MARO, Switch_ON);
	}

	if(rule->MARType & BROADCAST_MAR)
		TCAM_WriteRule(tp, rule_no, BRD, Switch_ON);

	/*VLAN*/
	for(i=0; i<16; i++)
	{
		if(rule->VLANBitMap& (u16)0x01<<i)
			TCAM_WriteRule(tp, rule_no, TCAM_GetRuleBit(TCAM_VLAN, i), Switch_ON);
	}
	/*TYPE*/
	for(i=0; i<16; i++)
	{
		if(rule->TypeBitMap& (u16)0x01<<i)
			TCAM_WriteRule(tp, rule_no, TCAM_GetRuleBit(TCAM_TYPE, i), Switch_ON);
	}
	/*IPv4 Protocol*/
	for(i=0; i<16; i++)
	{
		if(rule->IPv4PTLBitMap& (u16)0x01<<i)
			TCAM_WriteRule(tp, rule_no, TCAM_GetRuleBit(TCAM_PTLv4, i), Switch_ON);
	}
	/*IPv6 Protocol*/
	for(i=0; i<16; i++)
	{
		if(rule->IPv6PTLBitMap& (u16)0x01<<i)
			TCAM_WriteRule(tp, rule_no, TCAM_GetRuleBit(TCAM_PTLv6, i), Switch_ON);
	}
	/*DIPv4*/
	for(i=0; i<16; i++)
	{
		if(rule->DIPv4BitMap& (u16)0x01<<i)
			TCAM_WriteRule(tp, rule_no, TCAM_GetRuleBit(TCAM_DIPv4, i), Switch_ON);
	}
	/*DIPv6*/
	for(i=0; i<16; i++)
	{
		if(rule->DIPv6BitMap& (u16)0x01<<i)
			TCAM_WriteRule(tp, rule_no, TCAM_GetRuleBit(TCAM_DIPv6, i), Switch_ON);
	}
	/*PORT*/
	for(i=0; i<32; i++)
	{
		if(rule->DPortBitMap& (u16)0x01<<i)
			TCAM_WriteRule(tp, rule_no, TCAM_GetRuleBit(TCAM_DPORT, i), Switch_ON);
	}

	/*set action*/
	if(rule->FRIBNotDrop == 0)
		act.IB = 1;	
	if(rule->FROOB)
		act.OOB = 1;
	act.number = rule_no;
	TCAM_RuleActionSet(tp, &act);

	/*enable rule*/
	PacketFilterRuleEn(tp, rule_no, Switch_ON);
	
	return rule_no;
}

//Clear all port filter rule
void clearAllPFRule(struct rtl8168_private *tp)
{
	volatile void * ioaddr = tp->mmio_addr;
	u32  i = 0, j = 0;
	TCAM_RuleActionSet_st act ={0,};
	
	//TCAM vaild bit clear and all Leaky Bucket clear
	RTL_W32(IO_PKT_CLR,0x800000FF);
	//yukuen bsp_wait(100);

	//disable all rule to every rule
	for(i=0; i<RULE_NO_MAX; i++)
	{
		for(j=0; j<MAX_RULE_NUMBER; j++)
		{
			TCAM_WriteRule(tp ,i, j, Switch_OFF);
		}

		act.number = i;
		TCAM_RuleActionSet(tp ,&act);
		
		PacketFilterRuleEn(tp ,i, Switch_OFF);
	}
	
	Rule_NO = 0;
}

void chgPFRule(struct rtl8168_private *tp, Rule_NO_et ruleNo, TCAMRule* rule)
{
	u32 j=0;
	//TCAM_RuleActionSet_st act ={0,};

	PacketFilterRuleEn(tp, ruleNo, Switch_OFF);
	for(j=0; j<MAX_RULE_NUMBER; j++)
	{
		TCAM_WriteRule(tp, ruleNo, j, Switch_OFF);
	}

	PacketFilterSetRule(tp, rule, ruleNo);
}

Rule_NO_et setPFRule(struct rtl8168_private *tp, TCAMRule* rule)
{	
	if(rule->isSet)
	{
		chgPFRule(tp, rule->ruleNO , rule);
		return rule->ruleNO;		
	}
	else
	{
		rule->isSet = 1;
		rule->ruleNO = PacketFilterSetRule(tp, rule, Rule_NO++);
		return rule->ruleNO;
	}
}

void RstSharePFRuleMem(TCAMRule* rstRule, TCAMRule* copyRule)
{
	unsigned char ruleNO = rstRule->ruleNO;
	unsigned char isSet = rstRule->isSet;

	memset(rstRule, 0, sizeof(TCAMRule));
	if(copyRule)
	{
		memcpy(rstRule, copyRule, sizeof(TCAMRule));
	}
	rstRule->ruleNO = ruleNO;
	rstRule->isSet = isSet;	
}


void PFMemInit(void)
{
	memset(TCAMMem, 0 ,sizeof(TCAMMem));
	memset(TCAMRuleMem, 0, sizeof(TCAMRuleMem));
	//Init TCAMMem type
	TCAMMem[TCAMMacIDSet].type = TCAM_MAC;
	TCAMMem[TCAMVLANTagSet].type = TCAM_VLAN;
	TCAMMem[TCAMTypeSet].type = TCAM_TYPE;
	TCAMMem[TCAMIPv4PTLSet].type = TCAM_PTLv4;
	TCAMMem[TCAMIPv6PTLSet].type = TCAM_PTLv6;
	TCAMMem[TCAMDIPv4Set].type = TCAM_DIPv4;
	TCAMMem[TCAMDIPv6Set].type = TCAM_DIPv6;
	TCAMMem[TCAMDDPortSet].type = TCAM_DPORT;

	TCAMMem[TCAMDIPv4Set].num = NumOfIPv4Addr;
	TCAMMem[TCAMDIPv4Set].data = (unsigned char*)malloc(NumOfIPv4Addr*IPv4_ADR_LEN);
	memset(TCAMMem[TCAMDIPv4Set].data, 0 ,sizeof(NumOfIPv4Addr*IPv4_ADR_LEN));
	
	TCAMMem[TCAMDIPv6Set].num = NumOfIPv6Addr;
	TCAMMem[TCAMDIPv6Set].data = (unsigned char*)malloc(NumOfIPv6Addr*IPv6_ADR_LEN);
	memset(TCAMMem[TCAMDIPv6Set].data, 0 ,sizeof(NumOfIPv6Addr*IPv6_ADR_LEN));	
	#if 0
	//dpconf->HostIP[intf].addr stroe with big endian
	dpconf->HostIP[eth0].addr = ntohl(dpconf->HostIP[eth0].addr);	
	//setIPAddress_F(dpconf->HostIP[intf].addr);
	setIPAddress_withoutFlash(dpconf->HostIP[eth0].addr, eth0);
	#endif
}

void SetOOBBasicRule(struct rtl8168_private *tp)
{
	RstSharePFRuleMem(&TCAMRuleMem[ArpFRule], 0);
	RstSharePFRuleMem(&TCAMRuleMem[OOBUnicastPFRule], 0);
	RstSharePFRuleMem(&TCAMRuleMem[OOBIPv6PFRule], 0);	
	//ARP rule
	TCAMRuleMem[ArpFRule].FROOB = 1;
	TCAMRuleMem[ArpFRule].FRIBNotDrop = 1;
	TCAMRuleMem[ArpFRule].MARType = UNICAST_MAR | BROADCAST_MAR;
	TCAMRuleMem[ArpFRule].MACIDBitMap = OOBMacBitMap;
	#if 0
	TCAMRuleMem[ArpFRule].TypeBitMap = ARPTypeBitMap;	
	TCAMRuleMem[ArpFRule].DIPv4BitMap = EnableUniIPv4Addr; 
	#endif
	TCAMRuleMem[ArpFRule].ruleNO = setPFRule(tp, &TCAMRuleMem[ArpFRule]);
	
	#if 0
	//Accept IPv4/IPv6 udp unicast packet
	TCAMRuleMem[OOBUnicastPFRule].FROOB = 1;
	TCAMRuleMem[OOBUnicastPFRule].FRIBNotDrop = 1;
	TCAMRuleMem[OOBUnicastPFRule].MARType = UNICAST_MAR;
	TCAMRuleMem[OOBUnicastPFRule].MACIDBitMap = OOBMacBitMap;	
	TCAMRuleMem[OOBUnicastPFRule].TypeBitMap = IPv4TypeBitMap | IPv6TypeBitMap;	
	TCAMRuleMem[OOBUnicastPFRule].DIPv4BitMap = EnableUniIPv4Addr | EnableUniIPv6Addr;
	TCAMRuleMem[OOBUnicastPFRule].IPv4PTLBitMap = IPv4PTLTCP | IPv4PTLUDP | IPv4PTLICMP | IPv6PTLUDP; 
	TCAMRuleMem[OOBUnicastPFRule].ruleNO = setPFRule_F(&TCAMRuleMem[OOBUnicastPFRule]);
	
	//Accept IPv6 tcp and icmp packet match OOB mac or multicast mac
	TCAMRuleMem[OOBIPv6PFRule].FROOB = 1;
	TCAMRuleMem[OOBIPv6PFRule].OOBMAR = 1;
	TCAMRuleMem[OOBIPv6PFRule].FRIBNotDrop = 1;
	TCAMRuleMem[OOBIPv6PFRule].MARType = UNICAST_MAR | MULCAST_MAR;
	TCAMRuleMem[OOBIPv6PFRule].MACIDBitMap = OOBMacBitMap;
	TCAMRuleMem[OOBIPv6PFRule].TypeBitMap = IPv6TypeBitMap; 
	TCAMRuleMem[OOBIPv6PFRule].IPv6PTLBitMap = IPv6PTLTCP | IPv6PTLICMP; 
	TCAMRuleMem[OOBIPv6PFRule].ruleNO = setPFRule_F(&TCAMRuleMem[OOBIPv6PFRule]);
	#endif	

}

void hwPFInit(struct rtl8168_private *tp)
{
  //  volatile void * ioaddr = tp->mmio_addr;

	clearAllPFRule(tp);	
	PacketFilterInit(tp);
	PFMemInit();
	SetOOBBasicRule(tp);	
}
/**************************************************************************
RECV - Receive a frame
***************************************************************************/
static int rtl8168oob_recv(struct eth_device *dev)
{
    /* return true if there's an ethernet packet ready to read */
    /* nic->packet should contain data on return */
    /* nic->packetlen should contain length of data */
    struct rtl8168_private *tp = (struct rtl8168_private *)dev->priv;
    volatile void * ioaddr = tp->mmio_addr;
    int cur_rx;
    int length = 0;
    u16 sts = RTL_R16(IntrStatus);
    int count =0;
    int i=0;
    u32 addr,size;
#ifdef DEBUG_RTL8168_RX
    printf ("%s\n", __FUNCTION__);
#endif

   // printf("IntrStatus %x \n",IntrStatus);

    RTL_W16(IntrStatus, sts);

    rmb();

    //if ((sts & (RxOK)))
        //printf("RxOK %x \n",sts);
    //if ((sts & (RxDescUnavail | RxErr | RxOK)) == 0)
    //    return 0;

    cur_rx = tp->cur_rx;

   // while(1) {
		addr=(u32)&tp->RxDescArray[cur_rx];
		addr &= ~(ARCH_ALIGN - 1);
		size = roundup(sizeof(struct RxDesc), ARCH_ALIGN);
		invalidate_dcache_range(addr, addr + size);
		
	//	printf ("RX cur_rx = %X opts1 = %X \n", cur_rx, le32_to_cpu(tp->RxDescArray[cur_rx].opts1));
		
        if ((le32_to_cpu(tp->RxDescArray[cur_rx].opts1) & DescOwn) == 0) {
		
            count = count +1;
          
            if (!(le32_to_cpu(tp->RxDescArray[cur_rx].opts1) & RxRES)) {
                length = (int) (le32_to_cpu(tp->RxDescArray[cur_rx].
                                            opts1) & 0x00003fff) - 4;


                invalidate_dcache_range((unsigned long)tp->RxBufferRing[cur_rx],(unsigned long)tp->RxBufferRing[cur_rx]+length);
	//	printf ("invalidate %x  length %x \n", tp->RxBufferRing[cur_rx],length);
            //    dmb();
            //    NetReceive(tp->RxBufferRing[cur_rx], length);
               net_process_received_packet(tp->RxBufferRing[cur_rx], length);     
             //   printf ("NetReceive\n");
            } else {
                puts("Error Rx");
            }            		
			/*
			* Free the current buffer, restart the engine and move forward
			* to the next buffer. Here we check if the whole cacheline of
			* descriptors was already processed and if so, we mark it free
			* as whole.
			*/		 		 
			size = _RXDESC_PER_CACHELINE - 1;
			if ((cur_rx & size) == size)		
			{	 		   
				for (i = cur_rx - size; i <= cur_rx ; i++) {
					if (i == NUM_RX_DESC - 1){
						tp->RxDescArray[i].opts1 =
							cpu_to_le32(DescOwn | RingEnd | tp->rx_buf_sz);	
					}
					else{
						tp->RxDescArray[i].opts1 =
							cpu_to_le32(DescOwn | tp->rx_buf_sz);					
					}
				}
				
				flush_cache((unsigned long)&tp->RxDescArray[cur_rx-size],ARCH_ALIGN);			
			}		
			cur_rx = (cur_rx + 1) % NUM_RX_DESC;
			tp->cur_rx = cur_rx;
			
		} else {
          //  printf ("NetReceive %d packets\n",count);
            return count;
        }  

    return (0);		/* initially as this is called to flush the input */
}

#define HZ 1000

#ifdef DEBUG_RTL8168_TX
static void print_packet( u8 * buf, int length )
{
    int i;
    int remainder;
    int lines;

    printf("Packet of length %d \n", length );

    lines = length / 16;
    remainder = length % 16;

    for ( i = 0; i < lines ; i ++ ) {
        int cur;

        for ( cur = 0; cur < 8; cur ++ ) {
            u8 a, b;

            a = *(buf ++ );
            b = *(buf ++ );
            printf("%02x%02x ", a, b );
        }
        printf("\n");
    }
    for ( i = 0; i < remainder/2 ; i++ ) {
        u8 a, b;

        a = *(buf ++ );
        b = *(buf ++ );
        printf("%02x%02x ", a, b );
    }
    printf("\n");
}
#endif
/**************************************************************************
SEND - Transmit a frame
***************************************************************************/
static int rtl8168oob_send(struct eth_device *dev, void *packet, int length)
{
    /* send the packet to destination */
    struct rtl8168_private *tp = (struct rtl8168_private *)dev->priv;
    volatile void * ioaddr = tp->mmio_addr;
    u32 to, len = length;
    int ret, cur_tx;
//    u32 addr,size;

#ifdef DEBUG_RTL8168_TX
    int stime = currticks();
    printf ("%s\n", __FUNCTION__);
    printf("sending %d bytes\n", len);
#endif

    cur_tx = tp->cur_tx;

#ifdef DEBUG_RTL8168_TX
    u8 *buf;
    buf = (u8 *) packet;
    printf("pointer %x\n",packet);

    print_packet (buf, length);
#endif

    tp->TxDescArray[cur_tx].addr = cpu_to_le64(VA2PA(bus_to_phys(packet)));

    if (cur_tx == (NUM_TX_DESC - 1)) {
        tp->TxDescArray[cur_tx].opts1 =
            cpu_to_le32((DescOwn | RingEnd | FirstFrag | LastFrag) |
                        ((len > ETH_ZLEN) ? len : ETH_ZLEN));
    } else {
        tp->TxDescArray[cur_tx].opts1 =
            cpu_to_le32((DescOwn | FirstFrag | LastFrag) |
                        ((len > ETH_ZLEN) ? len : ETH_ZLEN));
    }

    wmb();
//printf("Tx cur addr = %x\n",&tp->TxDescArray[cur_tx]);
    flush_cache((unsigned long)&tp->TxDescArray[cur_tx],sizeof(struct TxDesc));
    flush_cache((unsigned long)packet,length);
    //RTL_W8(TxPoll, NPQ);    /* set polling bit */
    RTL_W8(TxPoll,0x80);

    tp->cur_tx = (cur_tx+1) % NUM_TX_DESC;
    to = currticks() + TX_TIMEOUT;
/*
    addr=(u32)&tp->TxDescArray[cur_tx];
    addr &= ~(ARCH_ALIGN - 1);
    size = roundup(sizeof(struct TxDesc), ARCH_ALIGN);
 //   invalidate_dcache_range(addr, addr + size);
*/
    do {

      flush_cache((unsigned long)&tp->TxDescArray[cur_tx],
                   sizeof(struct TxDesc));
//invalidate_dcache_range((unsigned long)&tp->TxDescArray[cur_tx],(unsigned long)&tp->TxDescArray[cur_tx]+sizeof(struct TxDesc));
 //invalidate_dcache_range(addr, addr + size);
        RTL_W8(TxPoll, 0x80);
    } while ((le32_to_cpu(tp->TxDescArray[cur_tx].opts1) & DescOwn)
             && (currticks() < to));	/* wait */

    if (currticks() >= to) {
#ifdef DEBUG_RTL8168_TX
        puts ("tx timeout/error\n");
        printf ("%s elapsed time : %d\n", __FUNCTION__, currticks()-stime);
#endif
        ret = 0;
    } else {
#ifdef DEBUG_RTL8168_TX
        puts("tx done\n");
        printf ("%s elapsed time : %d\n", __FUNCTION__, currticks()-stime);
#endif
        ret = length;
    }

    /* Delay to make net console (nc) work properly */
    udelay(20);
    return ret;
}
/*
static void rtl8168_hw_set_rx_packet_filter(struct eth_device *dev)
{
    struct rtl8168_private *tp = (struct rtl8168_private *)dev->priv;
    volatile void * ioaddr = tp->mmio_addr;
    int rx_mode;
    u32 tmp = 0;

#ifdef DEBUG_RTL8168
    printf ("%s\n", __FUNCTION__);
#endif

    // IFF_ALLMULTI 
    // Too many to filter perfectly -- accept all multicasts. 
    rx_mode = RTL_R32(RxConfig);
    rx_mode |= AcceptBroadcast | AcceptMulticast | AcceptMyPhys;

    tp->rtl8168_rx_config = rtl_chip_info[tp->chipset].RCR_Cfg;
    tmp = tp->rtl8168_rx_config | rx_mode | (RTL_R32(RxConfig) & rtl_chip_info[tp->chipset].RxConfigMask);

    RTL_W32(RxConfig, tmp);

    RTL_W32(MAR0 + 0, 0xffffffff);
    RTL_W32(MAR0 + 4, 0xffffffff);
}

static void
rtl8168_rx_desc_offset0_init(struct rtl8168_private *tp, int own)
{
    int i = 0;
    int ownbit = 0;

    if (own)
        ownbit = DescOwn;

    for (i = 0; i < NUM_RX_DESC; i++) {
        if (i == (NUM_RX_DESC - 1))
            tp->RxDescArray[i].opts1 = cpu_to_le32((ownbit | RingEnd) | (unsigned long)tp->rx_buf_sz);
        else
            tp->RxDescArray[i].opts1 = cpu_to_le32(ownbit | (unsigned long)tp->rx_buf_sz);

        flush_cache((unsigned long)&tp->RxDescArray[i],sizeof(struct RxDesc));
    }
}
*/

/*
static void
rtl8168_tally_counter_addr_fill(struct rtl8168_private *tp)
{
    volatile void * ioaddr = tp->mmio_addr;
    unsigned long tally;

    tally = ((unsigned long)dummy + ALIGN_64) & ~ALIGN_64;

    if (!tally)
        return;

    RTL_W32(CounterAddrLow, cpu_to_le32(bus_to_phys(tally)));
    RTL_W32(CounterAddrHigh, 0);
}

static void
rtl8168_tally_counter_clear(struct rtl8168_private *tp)
{
    volatile void * ioaddr = tp->mmio_addr;
    unsigned long tally;

    if (tp->mcfg == CFG_METHOD_1 || tp->mcfg == CFG_METHOD_2 ||
        tp->mcfg == CFG_METHOD_3 )
        return;

    tally = ((unsigned long)dummy + ALIGN_64) & ~ALIGN_64;

    if (!tally)
        return;

    tally |= BIT_0;
    RTL_W32(CounterAddrHigh, 0);
    RTL_W32(CounterAddrLow, cpu_to_le32(bus_to_phys(tally)));
}
*/
static void
rtl8168_desc_addr_fill(struct rtl8168_private *tp)
{
    volatile void * ioaddr = tp->mmio_addr;


    RTL_W32(TxDescStartAddrLow, VA2PA(bus_to_phys(tp->TxDescArray)));   
    RTL_W32(RxDescAddrLow,VA2PA(bus_to_phys(tp->RxDescArray)));    


}

void rtl8168_init_ring_indexes(struct rtl8168_private *tp)
{
    tp->dirty_tx = 0;
    tp->dirty_rx = 0;
    tp->cur_tx = 0;
    tp->cur_rx = 0;
}

static inline void
rtl8168_mark_as_last_descriptor(struct RxDesc *desc)
{
    desc->opts1 |= cpu_to_le32(RingEnd);
}

static void
rtl8168_tx_desc_init(struct rtl8168_private *tp)
{
    int i = 0;

    memset(tp->TxDescArray, 0x0, NUM_TX_DESC * sizeof(struct TxDesc));

    for (i = 0; i < NUM_TX_DESC; i++) {
        if (i == (NUM_TX_DESC - 1))
            tp->TxDescArray[i].opts1 = cpu_to_le32(RingEnd);
	else 
	    tp->TxDescArray[i].opts1 = cpu_to_le32(0);

	flush_cache((unsigned long)&tp->TxDescArray[i],sizeof(struct TxDesc));
    }
    //flush_cache((unsigned long)&tp->TxDescArray,NUM_TX_DESC * sizeof(struct TxDesc));
}

static void
rtl8168_rx_desc_init(struct rtl8168_private *tp)
{
    memset(tp->RxDescArray, 0x0, NUM_RX_DESC * sizeof(struct RxDesc));
   
   flush_cache((unsigned long)&tp->RxDescArray,NUM_RX_DESC * sizeof(struct RxDesc));
}

static u32
rtl8168_rx_fill(struct rtl8168_private *tp,
                struct eth_device *dev,
                u32 start,
                u32 end)
{
    unsigned long rxb;
    u32 cur;

    rxb = (unsigned long)tp->RxBufferRings;
    rxb = (rxb + ALIGN_8) & ~ALIGN_8;

    for (cur = start; end - cur > 0; cur++) {
        if (cur == (NUM_RX_DESC - 1))
            tp->RxDescArray[cur].opts1 =
                cpu_to_le32((DescOwn | RingEnd) + tp->rx_buf_sz);
        else
            tp->RxDescArray[cur].opts1 =
                cpu_to_le32(DescOwn + tp->rx_buf_sz);

        tp->RxBufferRing[cur] = (u8 *)(rxb + cur * tp->rx_buf_sz);
        tp->RxDescArray[cur].addr =
            cpu_to_le64(VA2PA(bus_to_phys(tp->RxBufferRing[cur])));

        flush_cache((unsigned long)&tp->RxDescArray[cur],sizeof(struct RxDesc));
        flush_cache((unsigned long)tp->RxBufferRing[cur], tp->rx_buf_sz);
    }

    return cur - start;
}

static int
rtl8168_init_ring(struct eth_device *dev)
{
    struct rtl8168_private *tp = (struct rtl8168_private *)dev->priv;
#ifdef DEBUG_RTL8168
    int stime = currticks();
    printf ("%s\n", __FUNCTION__);
#endif

    rtl8168_init_ring_indexes(tp);

    rtl8168_tx_desc_init(tp);
    rtl8168_rx_desc_init(tp);

    rtl8168_rx_fill(tp, dev, 0, NUM_RX_DESC);

    rtl8168_mark_as_last_descriptor(tp->RxDescArray + NUM_RX_DESC - 1);

#ifdef DEBUG_RTL8168
    printf ("%s elapsed time : %d\n", __FUNCTION__, currticks()-stime);
#endif

    return 0;
}


static void rtl8168_down(struct eth_device *dev)
{
    struct rtl8168_private *tp = (struct rtl8168_private *)dev->priv;

    //rtl8168_hw_reset(dev);

    //rtl8168_hw_d3_para(dev);

    if (tp->RxBufferRings) {
        free(tp->RxBufferRings);
        tp->RxBufferRings = NULL;
    }

    if (tp->DescArrays) {
        free(tp->DescArrays);
        tp->DescArrays = NULL;
    }
}

static void rtl8168_get_mac_address(struct rtl8168_private *tp)
{
    struct eth_device *dev = tp->dev;
    volatile void * ioaddr = tp->mmio_addr;
    int i;
    int overwrite=1;

    uint8_t env_enetaddr[6];

    if (!eth_env_get_enetaddr("ethaddr", env_enetaddr)){
                    overwrite=0;
    }

    if(overwrite){
    // add valid mac
    RTL_W32(MAC0,  env_enetaddr[0]
		   |env_enetaddr[1]<<8
		   |env_enetaddr[2]<<16
		   |env_enetaddr[3]<<24);
    RTL_W16(MAC4,  env_enetaddr[4]
		   |env_enetaddr[5]<<8);
    }

    for (i = 0; i < MAC_ADDR_LEN; i++)
        dev->enetaddr[i] = RTL_R8(MAC0 + i);

    eth_env_set_enetaddr("ethaddr", dev->enetaddr);

#ifdef DEBUG_RTL8168
    printf("MAC Address");
    for (i = 0; i < MAC_ADDR_LEN; i++)
        printf(":%02x", dev->enetaddr[i]);
    putc('\n');
#endif
}
/**************************************************************************
INIT - setting up the ethernet interface
***************************************************************************/
static int rtl8168oob_init(struct eth_device *dev, bd_t *bis)
{
    struct rtl8168_private *tp = (struct rtl8168_private *)dev->priv;
    volatile void * ioaddr = tp->mmio_addr;
#ifdef DEBUG_RTL8168
    int stime = currticks();
    printf ("%s\n", __FUNCTION__);
#endif

    tp->DescArrays = (u8 *)malloc(R8168_TX_RING_BYTES +
                                  R8168_RX_RING_BYTES +
                                  ALIGN_256);
    if (!tp->DescArrays) {
        printf("Can not allocate memory of r8168\n");
        return -ENOMEM;
    }

    tp->RxBufferRings = (u8 *)memalign(64, NUM_RX_DESC * tp->rx_buf_sz + ALIGN_8);
    if (!tp->RxBufferRings) {
        printf("Can not allocate memory of r8168\n");
        free(tp->DescArrays);
        tp->DescArrays = 0;
        return -ENOMEM;
    }

    /* Tx Descriptors needs 256 bytes alignment; */
    tp->TxDescArray = (struct TxDesc *)((unsigned long)(tp->DescArrays +
                                        ALIGN_256) & ~ALIGN_256);

    /* Rx Descriptors needs 256 bytes alignment; */
    tp->RxDescArray = (struct RxDesc *)((u8 *)tp->TxDescArray +
                                        R8168_TX_RING_BYTES);

    rtl8168_get_mac_address(tp);
    hwPFInit(tp);


    RTL_W8(ChipCmd,0x0);

    rtl8168_init_ring(dev);
   	//Assign Tx and Rx descriptor address
    rtl8168_desc_addr_fill(tp);
    

    RTL_W16(CPlusCmd,0x0021);
    
    RTL_W16(RxConfig,0x028E);

    RTL_W16(IntrMask,0x0000);

    RTL_W16(IntrStatus,0xFFFF);
   
    RTL_W16(IntrMask,0xFFFF);
 
    while(RTL_R16(ChipCmd) & 0x300)
    {
    	printf ("waiting\n");
    }
    
 //tp->TxDescArray += 0x20000000;
//tp->RxDescArray += 0x20000000;
   
    RTL_W8(ChipCmd,0x0C);
       
    //rtl8168_tally_counter_clear(tp);
/*    
    rtl8168_hw_reset(dev);
 
    rtl8168_hw_phy_config(dev);

    rtl8168_hw_start(tp);
 */   
#ifdef DEBUG_RTL8168
    printf ("Default Set Speed To 100 Mbps\n");
#endif
   // rtl8168_set_speed_xmii(tp, AUTONEG_ENABLE, SPEED_1000, DUPLEX_FULL);

#ifdef DEBUG_RTL8168
    printf ("%s elapsed time : %d\n", __FUNCTION__, currticks()-stime);
#endif
    return 0;
}

/**************************************************************************
HALT - Turn off ethernet interface
***************************************************************************/
static void rtl8168oob_halt(struct eth_device *dev)
{
#ifdef DEBUG_RTL8168
    printf ("%s\n", __FUNCTION__);
#endif

    rtl8168_down(dev);
}

#define REG_BASE    0xBAF70000

int rtl8168oob_initialize(bd_t *bis)
{
    int card_number = 0;
    struct eth_device *dev;
    struct rtl8168_private *tp;
    u32 iobase;
    //int idx = 0;
    int i=0;

//   while (0)
    if(1) {
        /* Find RTL8168 */
        //if ((devno = pci_find_devices(supported, idx++)) < 0)
        //    break;

        //pci_read_config_dword(devno, PCI_BASE_ADDRESS_1, &iobase);
        //iobase &= ~0xf;
        iobase = REG_BASE;

        debug ("r8168: REALTEK RTL8168 @0x%x\n", iobase);

        dev = (struct eth_device *)malloc(sizeof *dev);
        if (!dev) {
            printf("Can not allocate memory of r8168\n");
            //    break;
        }
        memset(dev, 0, sizeof(*dev));

        tp = (struct rtl8168_private *)malloc(sizeof *tp);
        if (!tp) {
            printf("Can not allocate memory of r8168\n");
            free(dev);
            //  break;
        }
        memset(tp, 0, sizeof(*tp));
        tp->mmio_addr = (volatile void *)iobase;
        tp->dev = dev;
        tp->rx_buf_sz = RX_BUF_SIZE;

        /* Identify chip attached to board */
        //rtl8168_get_mac_version(tp, tp->mmio_addr);

        //rtl8168_print_mac_version(tp);
        printf("Realtek PCIe GBE Family Controller\n");

        sprintf(dev->name, "r8168#%d", card_number);
        //printf("dev->name=%s\n",dev->name);
        rtl8168_get_mac_address(tp);

        tp->chipset = i;

        dev->priv = (void *)tp;
        dev->iobase = (int)tp->mmio_addr;

        dev->init = rtl8168oob_init;
        dev->halt = rtl8168oob_halt;
        dev->send = rtl8168oob_send;
        dev->recv = rtl8168oob_recv;

        eth_register (dev);

        card_number++;
		
    }

    return card_number;
}
