#include <common.h>
#include <dm.h>
#include <fdtdec.h>
#include <asm/gpio.h>
#include <asm/io.h>
#include <dm/device-internal.h>
#include <dt-bindings/gpio/gpio.h>
#include <rtl8117.h>

#define TESTIO_ACT              2
#define DBG_ACT                 10
#define LADPS_EN                0
#define LINKOK_GPIO_EN          9
#define GPI_OE_REG              13

#define GPIO_EN                 0
#define GPIO_OUT_EN             1
#define GPIO_INPUT_VAL          2
#define GPIO_OUTPUT_VAL         3

#define GP_HIGH     1
#define GP_LOW      0
#define GP_DIROUT   1
#define GP_DIRIN    0

static inline void gpio_prepare(struct udevice *dev)
{
    u32 temp = 0;
    OOB_access_IB_reg(UMAC_PAD_CONT_REG, &temp, 0xf, OOB_READ_OP);
    temp &= ~((1<<DBG_ACT)|(1<<TESTIO_ACT));//set dbg_act = 0 & test_io_act = 0
    //Disable JTAG IN and OUT pins
    temp |= ((1 << 7) | (1 << 8));
    OOB_access_IB_reg(UMAC_PAD_CONT_REG, &temp, 0xf, OOB_WRITE_OP);
}

static inline void gpi_chage_mode(struct udevice *dev)
{
    u32 temp = 0;

    OOB_access_IB_reg(UMAC_MISC_1, &temp, 0xf, OOB_READ_OP);
    temp &= ~(1<<LINKOK_GPIO_EN);
    OOB_access_IB_reg(UMAC_MISC_1, &temp, 0xf, OOB_WRITE_OP);

    OOB_access_IB_reg(UMAC_CONFIG6_PMCH, &temp, 0xf, OOB_READ_OP);
    temp &= ~(1<<LADPS_EN);
    OOB_access_IB_reg(UMAC_CONFIG6_PMCH, &temp, 0xf, OOB_WRITE_OP);

    OOB_access_IB_reg(UMAC_PIN_OE_REG, &temp, 0xf, OOB_READ_OP);
    temp &= ~(1<<GPI_OE_REG);
    OOB_access_IB_reg(UMAC_PIN_OE_REG, &temp, 0xf, OOB_WRITE_OP);
}

static inline int __rtk_gpio_direction(struct udevice *dev, unsigned offset, bool out, int value)
{
    u32 CTL_REG=0, CTL_INT_REG=0, en_int_bit=0, sta_int_bit=0;
    int offset_base;
    u32 temp;
    debug("[%s] offset(%u) out(%u)", __FUNCTION__,offset,out);

    gpio_prepare(dev);

    if ((offset>=4) && (offset <= 24))
    {
        CTL_REG = GPIO_CTRL2_SET + (((offset-4)/8)<<2);
        offset_base = 4 + ((offset-4)/8)*8;
    }
    else if ((offset>=1) && (offset <= 3))
    {
        CTL_REG = GPIO_CTRL1_SET;
        offset_base = -1;
    }
    else if (offset==0)  //GPI pin
    {
        gpi_chage_mode(dev);
        CTL_REG = GPIO_CTRL1_SET;
        offset_base = 0;
    }

    temp = readl(OOB_MAC_BASE_ADDR + CTL_REG);
    temp &= (~(0x0000000F<<((offset-offset_base)*4+GPIO_EN)));
    temp |= (1<<((offset-offset_base)*4+GPIO_EN));
    if (out)
    {
        temp |= (1<<((offset-offset_base)*4+GPIO_OUT_EN));
        temp |= ((value!=0)?(1<<((offset-offset_base)*4+GPIO_INPUT_VAL)):0);
    }
    else
        temp &= (~(1<<((offset-offset_base)*4+GPIO_OUT_EN)));
    writel(temp, OOB_MAC_BASE_ADDR + CTL_REG);
    if (!out)
    {
#ifdef POSITIVE_DEGE_INTERRUPT
        if ((offset>=1) && (offset <= 7))
        {
            CTL_INT_REG = OOBMAC_EXTR_INT;
            en_int_bit = 20 + offset;
            sta_int_bit = 4 + offset;
        }
        else if ((offset>=8) && (offset <= 23))
        {
            CTL_INT_REG = GPIO_CTRL5_SET;
            en_int_bit = offset + 8;
            sta_int_bit = offset - 8;
        }
        else if (offset == 0)
        {
            CTL_INT_REG = OOBMAC_EXTR_INT;
            en_int_bit = 19;
            sta_int_bit = 3;
        }
#else
        if ((offset>=1) && (offset <= 24))
        {
            CTL_INT_REG = GPIO_CTRL7_SET + ((offset<15)?0:4);
            en_int_bit = 1 + offset + ((offset<15)?16:0);
            sta_int_bit = (1 + offset)%16;
        }
        else if (offset == 0)
        {
            CTL_INT_REG = GPIO_CTRL7_SET;
            en_int_bit = 16;
            sta_int_bit = 0;
        }
#endif
        temp = readl(OOB_MAC_BASE_ADDR + CTL_INT_REG);
        temp &= (~(1<<en_int_bit));
        temp |= ((1<<en_int_bit));
        writel(temp, OOB_MAC_BASE_ADDR + CTL_INT_REG);
        temp &= (~(1<<sta_int_bit));
        temp |= ((1<<sta_int_bit));
        writel(temp, OOB_MAC_BASE_ADDR + CTL_INT_REG);
    }

    debug("[%s] offset(%u)  addr(0x%08llx)  temp(0x%08x)", __FUNCTION__, offset,
                 (u64)((OOB_MAC_BASE_ADDR) + CTL_REG), temp);
    return 0;
}
static int gpio_rtl8117_direction_input(struct udevice *dev, unsigned offset)
{
    debug("[%s] offset(%u)", __FUNCTION__,offset);
    return __rtk_gpio_direction(dev, offset, GP_DIRIN, 0);
}

static int gpio_rtl8117_direction_output(struct udevice *dev, unsigned offset, int value)
{
    debug("[%s] offset(%u) value(%d)", __FUNCTION__,offset,value);
    return __rtk_gpio_direction(dev, offset, GP_DIROUT, value);
}
static inline int rtk_gpio_ops(struct udevice *dev, unsigned offset, int value, bool set)
{
    u32 temp=0;
    u32 CTL_REG=0;;
    int offset_base;
    debug("[%s] offset(%u) value(%u) set(%u)", __FUNCTION__,offset, value, set);

    if ((offset>=4) && (offset <= 24))
    {
        CTL_REG = GPIO_CTRL2_SET + (((offset-4)/8)<<2);
        offset_base = 4 + ((offset-4)/8)*8;
    }
    else if ((offset>=1) && (offset <= 3))
    {
        CTL_REG = GPIO_CTRL1_SET;
        offset_base = -1;
    }
    else if (offset==0)
    {
        CTL_REG = GPIO_CTRL1_SET;
        offset_base = 0;
    }

    temp = readl(OOB_MAC_BASE_ADDR + CTL_REG);
    if (set)
    {
        temp &= (~(1<<((offset-offset_base)*4+GPIO_INPUT_VAL)));;//clear ori value
        temp |= (value<<((offset-offset_base)*4+GPIO_INPUT_VAL));//set new value
        writel(temp, OOB_MAC_BASE_ADDR + CTL_REG);
    }
    else
        temp &= (1<<((offset-offset_base)*4+GPIO_OUTPUT_VAL));

    return (temp!=0)?1:0;
}

static int gpio_rtl8117_get_value(struct udevice *dev, unsigned offset)
{
    return rtk_gpio_ops(dev, offset, 0, 0);
}

static void gpio_rtl8117_set_value(struct udevice *dev, unsigned offset, int value)
{
    rtk_gpio_ops(dev, offset, value, 1);
}

static int gpio_rtl8117_xlate(struct udevice *dev, struct gpio_desc *desc,
			    struct ofnode_phandle_args *args)
{
    u32 pin;
    debug("[%s] args[0]=%d args[1]=%d args[2]=%d args[3]=%d", __FUNCTION__, args->args[0],args->args[1],args->args[2],args->args[3]);

    pin = args->args[0];
	desc->offset = args->args[0];
	desc->flags = args->args[1]?GPIOD_IS_OUT:GPIOD_IS_IN;
	desc->flags |= args->args[3] & GPIO_ACTIVE_LOW ? GPIOD_ACTIVE_LOW : 0;
	desc->value = args->args[2];

    return 0;
}

static const struct dm_gpio_ops gpio_rtl8117_ops = {
	.direction_input	= gpio_rtl8117_direction_input,
	.direction_output	= gpio_rtl8117_direction_output,
	.get_value		= gpio_rtl8117_get_value,
	.set_value		= gpio_rtl8117_set_value,
	.xlate = gpio_rtl8117_xlate,
};
static int gpio_rtl8117_probe(struct udevice *dev)
{
	int gpio_node;
	struct gpio_dev_priv *uc_priv = dev_get_uclass_priv(dev);

    gpio_node = fdt_path_offset(gd->fdt_blob, "/gpio");
    if (gpio_node < 0)
        debug("DTB file doesn't have gpio node.\n");
    else
    	uc_priv->gpio_count = fdtdec_get_int(gd->fdt_blob, gpio_node, "Realtek,gpio_numbers", 25);

	return 0;
}
static int gpio_rtl8117_bind(struct udevice *dev)
{
	return 0;
}

static const struct udevice_id gpio_rtl8117_ids[] = {
	{.compatible = "realtek,rtl8117-gpio"},
	{}
};

U_BOOT_DRIVER(gpio_rtl8117) = {
	.name	= "gpio_rtl8117",
	.id	= UCLASS_GPIO,
	.ops	= &gpio_rtl8117_ops,
	.probe	= gpio_rtl8117_probe,
	.bind	= gpio_rtl8117_bind,
};