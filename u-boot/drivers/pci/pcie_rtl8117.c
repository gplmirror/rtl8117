/*
 * (C) Copyright 2004 Tundra Semiconductor Corp.
 * Alex Bounine <alexandreb@tundra.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/*
 * PCI initialisation for the Tsi108 EMU board.
 */

#include <config.h>

#include <common.h>
#include <pci.h>
#include <asm/io.h>
#include <tsi108.h>
#if defined(CONFIG_OF_LIBFDT)
#include <libfdt.h>
#include <fdt_support.h>
#endif

#include <malloc.h>

#include "pcie_rtl8117_reg.h"
#include "pcie_rtl8117_core.h"

#ifndef CONFIG_SYS_PCI_MEMORY_BUS
#define CONFIG_SYS_PCI_MEMORY_BUS 0x80000000
#endif

#ifndef CONFIG_SYS_PCI_MEMORY_PHYS
#define CONFIG_SYS_PCI_MEMORY_PHYS 0x0
#endif

#ifndef CONFIG_SYS_PCI_MEMORY_SIZE
#define CONFIG_SYS_PCI_MEMORY_SIZE (2 * 1024 * 1024 * 1024UL) /* 2G */
#endif

struct pci_controller local_hose;

struct rtl8117_pci_controller {
//	struct pci_controller pci_controller;
//	struct resource io_res;
//	struct resource mem_res;
	rtk_pci_dev rtk_pci_dev;
};

static struct rtl8117_pci_controller rpc;

static int rtl8117_read_config_dword (struct pci_controller *hose,
				      pci_dev_t dev, int offset, u32 * value)
{
	u32 ret = 0;

	if (PCI_BUS(dev) != 0 || PCI_DEV(dev) != 1)
		return -1;

	ret = pcieh_cfg32_read(PCI_BUS(dev), PCI_DEV(dev), PCI_FUNC(dev), offset, value);
	if (ret != PH_SUCCESS) {
		return -1;
	}

	debug("rtl8117_read_config_dword reg = 0x%x, *pval = 0x%x\n", offset, *value);

	return 0;
}

static int rtl8117_write_config_dword (struct pci_controller *hose,
				       pci_dev_t dev, int offset, u32 value)
{
	u32 ret = 0;

	if (PCI_BUS(dev) != 0 || PCI_DEV(dev) != 1)
		return -1;

	debug("rtl8117_write_config_dword reg = 0x%x, value = 0x%x\n", offset, value);

	ret = pcieh_cfg32_write(PCI_BUS(dev), PCI_DEV(dev), PCI_FUNC(dev), offset, value);
	if (ret != PH_SUCCESS)
		return -1;

	return 0;
}

void pci_init_board (void)
{
	struct pci_controller *hose = (struct pci_controller *)&local_hose;

	hose->first_busno = 0;
	hose->last_busno = 0x1;

	rpc.rtk_pci_dev.pci_base = 0xbafa0000;
	rpc.rtk_pci_dev.oob_mac_base = 0xbaf70000;
	rpc.rtk_pci_dev.fun0_base_addr = 0xbaf10000;

	init_pci_setup(&rpc.rtk_pci_dev);
	pcieh_init(&rpc.rtk_pci_dev);

	pci_set_region (hose->regions + 0,
		       CONFIG_SYS_PCI_MEMORY_BUS,
		       CONFIG_SYS_PCI_MEMORY_PHYS,
		       CONFIG_SYS_PCI_MEMORY_SIZE, PCI_REGION_MEM | PCI_REGION_SYS_MEMORY);

	/* PCI memory space */
	pci_set_region (hose->regions + 1,
		       CONFIG_SYS_PCI_MEM_BUS,
		       CONFIG_SYS_PCI_MEM_PHYS, CONFIG_SYS_PCI_MEM_SIZE, PCI_REGION_MEM);

	hose->region_count = 2;

	pci_set_ops (hose,
		    pci_hose_read_config_byte_via_dword,
		    pci_hose_read_config_word_via_dword,
		    rtl8117_read_config_dword,
		    pci_hose_write_config_byte_via_dword,
		    pci_hose_write_config_word_via_dword,
		    rtl8117_write_config_dword);

	pci_register_hose (hose);

	hose->last_busno = pci_hose_scan (hose);

	/* see if we are a PCIe or PCI controller */
	debug ("Done PCI initialization\n");
	return;
}
