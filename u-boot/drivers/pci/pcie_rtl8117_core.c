#include <common.h>
#include <dm.h>
#include <errno.h>
#include <usb.h>
#include <malloc.h>
#include <memalign.h>
#include <phys2bus.h>
#include <usbroothubdes.h>
#include <wait_bit.h>
#include <asm/io.h>

#include "pcie_rtl8117_reg.h"
#include "pcie_rtl8117_core.h"

#if 0
#define default_IMR (BIT_0 | BIT_1 | BIT_2 | BIT_3 |BIT_13 | BIT_14 | BIT_16)
#else
#define default_IMR (BIT_0 | BIT_1 | BIT_2 | BIT_3)
#endif

#define disable_dev_IMR (BIT_13 | BIT_14 | BIT_16)

#define BUS_NUM_T	1 //total bus num,defalt define 1
#define DEV_NUM_T	1 //total device num,defalt define 1
#define FUN_NUM_T	2 //total function num,defalt defin 2

#define PCIE_TIME_OUT		1000000
#define PCIE_SCAN_TIME_OUT	5
#define PCIE_RESCAN_MAX		1

static rtk_pci_dev *this_pdev;

//static DEFINE_RAW_SPINLOCK(rtl8117_pci_lock);

static INT8U oobm_read_pci_info(void)
{
	return readb(OOBMAC_BASE_ADDR + PCI_INFO);
}

static void oobm_write_pci_info(INT8U value)
{
	writeb(value, OOBMAC_BASE_ADDR + PCI_INFO);
}

inline void pcieh_set_imr_with_devint(void)
{
	writel(default_IMR, WIFI_IMR);
}

inline void pcieh_set_imr_wo_devint(void)
{
	writel(disable_dev_IMR, WIFI_IMR);
}

//RC CFG RW
INT32U pcieh_rc32_read(volatile USHORT addr, volatile INT32U *value)
{
	volatile INT32U tmp = 0, rv = PH_ERROR_TIMEOUT;
	unsigned long flags;

	OS_ENTER_CRITICAL;

	writel(addr, RC_DBI_ADDR);
	writel(0x3, RC_DBI_CTRL_REG);

	for (tmp = 0; tmp < PCIE_TIME_OUT; tmp++) {
		if (!(readl(RC_DBI_CTRL_REG) & BIT_0)) {
			*value = readl(RC_DBI_RDATA);
			rv  = PH_SUCCESS;
			break;
		}
	}

	OS_EXIT_CRITICAL;

	return rv;
}

INT32U pcieh_rc32_write(volatile USHORT addr, volatile ULONG value)
{
	volatile INT32U tmp = 0, rv = 0;
	unsigned long flags;

	OS_ENTER_CRITICAL;

	writel(addr, RC_DBI_ADDR);
	writel(value, RC_DBI_WDATA);
	writel(0x7D, RC_DBI_CTRL_REG);

	for (tmp = 0; tmp < PCIE_TIME_OUT; tmp++) {
		if (!(readl(RC_DBI_CTRL_REG) & BIT_0)) {
			rv ++;
			break;
		}
	}

	OS_EXIT_CRITICAL;

	return 0;
}

static void  _2_4281_cfg(rtk_pci_dev* pdev)
{
	unsigned long flags;
	OS_ENTER_CRITICAL;

	writeb(0x0, WIFI_CFG0);
	writeb(0x3, BYP_MODE_CFG_REG);
	writeb(0x2, DUMMY_REG0);

	writeb(readb(ELBI_TRAN_CFG0) & 0xFE, ELBI_TRAN_CFG0); //set  LEBI_RTANS working at by 4281mode(0)
	writeb(readb(CDM_MBOX_CFG0) & 0xFE, CDM_MBOX_CFG0); //set CDM_MBOX working at 4281 mode(0)

	writeb(0xFF, SII_INT_CR);
	writeb(0x00, SII_MSG_BYP_ENABLE_REG);
	writeb(0x00, SII_MSG_SRC_SEL);

	pcieh_set_imr_wo_devint();	//set interrupt

	writeb(0x0, WIFI_DIRECTION_MODE_CR);

	pdev->Bypass_mode_wocfg_flag = 0;

	OS_EXIT_CRITICAL;
}

/*
control the PCIE host's pcie rst pin by 4281
0:pcie rst falling down;
1:pcie rst rising up;
*/
static void prst_control(INT8U value)
{
	if (!value) {
		writeb(readb(HOST_PAD_OUT_REG) & 0xFE, HOST_PAD_OUT_REG);
	}
	else if (value) {
		writeb(readb(HOST_PAD_OUT_REG) | BIT_0, HOST_PAD_OUT_REG);
	}
}

/*
control the PCIE host's pcie isolateb pin by 4281
0:isolateb falling down;
1:isolateb rising up;
*/
static void Iso_control(INT8U value)
{
	if (!value) {
		writeb(readb(HOST_PAD_OUT_REG) & 0xFD, HOST_PAD_OUT_REG);
	}
	else if (value) {
		writeb(readb(HOST_PAD_OUT_REG) | BIT_1, HOST_PAD_OUT_REG);
	}
}

//bit0:Power rst
//bit1:WIFIDASH_SOFT_RESET
static void pcieh_sw_reset(INT8U value)
{
	writel(value, PCIE_FUNO_SW_RESET);
	writel(0, PCIE_FUNO_SW_RESET);
}

static void fun0_init(void)
{
	writel(BIT_21|BIT_22, PCIE_FUN0_ISR);

	writeb(0x0, HOST_PAD_SRC_SEL_REG);
}

static INT32U rc_indi_cfg_init(void)
{
	volatile INT32U Rvalue, ph_result = PH_SUCCESS, Regdata;
	volatile USHORT ph_addr;

	pcieh_set_imr_wo_devint();	//set IMR of rc_indr_int_msk

	//enable device message interrupt
	Regdata = readl(SII_INT_CR);
	Regdata |= BIT_0;
	writel(Regdata, SII_INT_CR);

	//set rc IO base addr
	ph_addr = 0x1c;
	ph_result |= pcieh_rc32_read(ph_addr, &Rvalue);
	Rvalue = Rvalue >> 16;
	Rvalue = Rvalue << 16;
	Rvalue |= 0xD0D0;
	ph_result |= pcieh_rc32_write(ph_addr, Rvalue);
	ph_result |= pcieh_rc32_read(ph_addr, &Rvalue);

	//set RC IO enable memory eable bit
	ph_result |= pcieh_rc32_read(0x4, &Rvalue);
	ph_result |= pcieh_rc32_write(0x4, Rvalue | 0x7);

	return ph_result;
}


static INT32U device_scan(rtk_pci_dev* pdev)
{
	volatile INT32U i, j, k, tmpd, ph_result = PH_ERROR_NO_DEV;
	volatile INT8U pci_info;

	pdev->FunNo = 0;

	for (i = 0; i < BUS_NUM_T; i++) {
		for (j = 0; j < DEV_NUM_T; j++) {
			for (k = 0; k < FUN_NUM_T; k++) {
				pcieh_cfg32_read(i, j, k, 0, &tmpd);
				if (tmpd != 0xFFFFFFFF) {
					pdev->FunNo++;
					ph_result = PH_SUCCESS;
//					ph_result |= cfg_space_init(pdev, i, j, k);
					/*There has a slave device connect to PCIE host*/
					pci_info = oobm_read_pci_info();
					oobm_write_pci_info(pci_info | PCI_DEV_ON);
				}
			}
		}
	}

	return ph_result;
}

void pcieh_enable_power_seq(void)
{
	Iso_control(1);
	msleep(100); //each tick is 10ms, total delay 100ms
	prst_control(1);

}

void pcieh_disable_pwr_seq(void)
{
	prst_control(0);
	msleep(500); //each tick is 10ms, total delay 100ms
	Iso_control(0);
	msleep(500); //each tick is 10ms, total delay 100ms
}
EXPORT_SYMBOL(pcieh_disable_pwr_seq);

INT32U adapter_cfg_init(rtk_pci_dev *pdev)
{
	INT32U ph_result = PH_SUCCESS, i = 0, rescan_num = 0;

	_2_4281_cfg(pdev);
	fun0_init();

	writew(readw(OOBMAC_BASE_ADDR + OOBMAC_IMR) | BIT_6, OOBMAC_BASE_ADDR + OOBMAC_IMR);

init:
	pcieh_enable_power_seq();

	/*polling the L0 states*/
	do {
		i++;
		msleep(100);
	} while (!(readb(RC_MAC_STATUS_REG) & BIT_0) && i <= PCIE_SCAN_TIME_OUT);

	if (i > PCIE_SCAN_TIME_OUT) {
		if (rescan_num < PCIE_RESCAN_MAX) {
			pcieh_disable_pwr_seq();
			rescan_num++;
			i = 0;
			goto init;
		} else {
			printf("[PCIE HOST] polling L0 state failed\n");
			ph_result |= PH_ERROR_TIMEOUT;
		}
	} else {

		printf("[PCIE HOST] polling L0 state succeed\n");

		ph_result |= rc_indi_cfg_init();
		ph_result |= device_scan(pdev);
	}

	return ph_result;
}

INT32U pcieh_init(rtk_pci_dev *pdev)
{
	INT32U ph_result = PH_SUCCESS, pci_bus_scan_status = PH_SUCCESS;
	volatile INT8U pci_info;

	printf("[PCIE HOST] start the connection\r\n");

	pci_bus_scan_status = adapter_cfg_init(pdev);

	if (pci_bus_scan_status &
		(PH_ERROR_PAGEFULL | PH_ERROR_WRONGVALUE |PH_ERROR_NO_DEV | PH_ERROR_TIMEOUT))
	{
		ph_result = PH_ERROR_PCIELINK_FAIL;
		printf("[PCIE HOST] can't connection\r\n");
		goto error;
	}
	else if (pdev->Vendor_cfg[0][0] != 0xFFFFFFFF)
	{
		ph_result = 0;
		if (!ph_result) {
			pcieh_set_imr_with_devint();
			printf("[PCIE HOST] init done\r\n");
		} else {
			ph_result = PH_ERROR_NODRV;
			goto error;
		}
	}
	else
	{
		ph_result = PH_ERROR_UNKNOWN;
		goto error;
	}

	return ph_result;

error:
	/*set the device_on bit 0*/
	pci_info = oobm_read_pci_info();
	oobm_write_pci_info(pci_info & ~(PCI_DEV_ON));
	return ph_result;
}
EXPORT_SYMBOL(pcieh_init);

INT32U pcieh_exit(rtk_pci_dev *pdev)
{
#if 0
	prst_control(0);
	Iso_control(0);
	memset(pdev, 0, sizeof(rtk_pci_dev));
#endif
	return 0;
}

static void rc_ephy_init(void)
{
	//RC ephy parameter
	Rc_ephy_W(0x0, 0x584E);
	Rc_ephy_W(0x6, 0xF0F0);
	Rc_ephy_W(0xC, 0x219);
	Rc_ephy_W(0xD, 0xF64);
	Rc_ephy_W(0x1E, 0x08F5);
}

/***********************************************
Function: Addr2Byte_en
Return Value: other	: successful
			0		: Failure

This function expect the rertuen value:
0x0011, 0x0110, 0x1100 			for Word
0x0001, 0x0010, 0x0100, 0x1000 		for byte
************************************************/
static USHORT Addr2Byte_en(volatile USHORT addr, volatile USHORT byte_init)
{
	volatile USHORT byte_off, byte_en;

	byte_en = byte_init;
	byte_off = addr % 4;
	byte_en = byte_en << byte_off;

	if (byte_init == 0x3) {
		if (!((byte_en & 0x3) || (byte_en & 0x6) || (byte_en & 0xc)))
			byte_en = 0;
	} else if (byte_init == 0x1) {
		if (!((byte_en & BIT_0) || (byte_en & BIT_1) || (byte_en & BIT_2) || (byte_en & BIT_3)))
			byte_en = 0;
	}

	return byte_en;
}

static INT32U Value2Byte_en_Word(INT32U byte_en, INT32U value_temp, INT8U cmd)
{
	INT32U value = 0;

	switch (byte_en) {
	case 0x3:
		value = value_temp;
		break;
	case 0x6:
		if (cmd == R_CMD)
			value = value_temp >> 8;
		else if (cmd == W_CMD)
			value = value_temp <<8;
		break;
	case 0xc:
		if (cmd == R_CMD)
			value = value_temp >> 16;
		else if (cmd == W_CMD)
			value = value_temp << 16;
		break;
	default:
		break;
	}

	return value;
}

static INT32U Value2Byte_en_Byte(INT32U byte_en, INT32U value_temp, INT8U cmd)
{
	INT32U value = 0;

	switch (byte_en) {
	case 0x1:
		value = (INT8U)value_temp;
		break;
	case 0x2:
		if (cmd == R_CMD)
			value = value_temp >> 8;
		else if (cmd == W_CMD)
			value = value_temp << 8;
		break;
	case 0x4:
		if (cmd == R_CMD)
			value = value_temp >> 16;
		else if (cmd == W_CMD)
			value = value_temp << 16;
		break;
	case 0x8:
		if (cmd == R_CMD)
			value = value_temp >> 24;
		else if (cmd == W_CMD)
			value = value_temp << 24;
		break;
	default:
		break;
	}

	return value;
}

ULONG Send_TLP_Polling(
	volatile INT32U TLP_TPYE,	//TLP Type
	volatile INT32U RW,		//Read or Write
	volatile INT32U Base_addr_H,	//high Base addr
	volatile INT32U Base_addr_L,	//low base addr
	volatile INT32U offset,		//offset addr
	volatile INT32U first_byte_en,	//first byte enable
	volatile INT32U last_byte_en,	//last byte enalbe
	volatile INT32U LEN,		//length
	volatile INT32U Timeout,	//polling time out num
	volatile INT32U *value		//Read or write data ptr
	)
{
	volatile struct RcPage *hostpage,hostpaget;
	volatile struct CFG_ADDR *cfg_addr;
	volatile INT32U tmp = 0,*pg_value, addr , rv = PH_SUCCESS, pg_wr_ptr, Rhostpage;

//	OS_CPU_SR  cpu_sr = 0;

	unsigned long flags;

	OS_ENTER_CRITICAL;

	hostpage = &hostpaget;

	/*check page full*/
	do{
		tmp++;
	} while((readl(PAGE_STATUS_REG) & BIT_7) && (tmp < 10000));	//check page full

	if (tmp >= 10000) {
		//reset rc ephy and pcie

		dump_stack();


		OOBMACWriteIBReg(EN_CLK_REG1, 1, 1, 0);
		OOBMACWriteIBReg(EN_CLK_REG1, 1, 1, 1);

		writel(1, WIFIDASH_SOFT_RESET);
		writel(0, WIFIDASH_SOFT_RESET);

		rv = PH_ERROR_PAGEFULL;
		goto Send_TLP_Polling_EXIT;
	}

	pg_wr_ptr = (readl(WIFI_DASH_BASE_ADDR + RC_INDIRECT_CH_OFFSET) & 0x18) >> 3;

	/*get the page should be using now*/
	Rhostpage = WIFI_DASH_BASE_ADDR + RC_INDIRECT_CH_OFFSET+ PAGE_CTRL_OFFSET*(pg_wr_ptr + 1);

	/*get the pg_value offset*/
	pg_value = (INT32U *)(WIFI_DASH_BASE_ADDR + PAGE0_DATA_B3_B0 + PAGE_DATA_OFFSET*pg_wr_ptr);

	/*rst the value of pg_value and page*/

	writel(0, Rhostpage);
	writel(0, Rhostpage + 0x4);
	writel(0, (INT32U)pg_value);

	/*cfg  page*/
	hostpage->EP = 0;
	hostpage->LEN = LEN;
	hostpage->LAST_BE = last_byte_en;
	hostpage->FIRST_BE = first_byte_en;
	hostpage->ADDRH = Base_addr_H;

	switch (TLP_TPYE) {
	case CFG_TYPE:
		cfg_addr = (struct CFG_ADDR *)((INT8U *)hostpage + 8);
		addr = offset/4;
		hostpage->TLP_TYPE = CFG_TYPE;
		cfg_addr->bus = Base_addr_L >> 8;
		cfg_addr->dev = (Base_addr_L << 8) >> 11;
		cfg_addr->fun = (Base_addr_L << 13) >> 13;
		cfg_addr->reg_num = addr;
		break;
	case IO_TYPE:
		hostpage->TLP_TYPE = IO_TYPE;
		addr= Base_addr_L + offset;
		hostpage->ADDRL = addr;
		break;
	case MEM_TYPE:
		hostpage->TLP_TYPE = MEM_TYPE;
		addr = Base_addr_L + offset;
		hostpage->ADDRL = addr;
		break;
	default:
		rv = 4;
		goto Send_TLP_Polling_EXIT;
		break;
	}

	writel(*((INT32U *)hostpage), Rhostpage);
	writel(hostpage->ADDRL, Rhostpage + 0x8);
	writel(hostpage->ADDRH, Rhostpage + 0xC);

	if (RW == R_CMD) {
		hostpage->CMD = R_CMD;
		hostpage->OWN = 1;

		writel(*((INT32U *)hostpage + 1), Rhostpage + 0x4);

		for (tmp = 0; tmp < PCIE_TIME_OUT; tmp++) {
			if (!(readl(Rhostpage + 0x4) & BIT_31)) {
				*value = readl((INT32U)pg_value);

				writel(readl(Rhostpage + 0x4) | BIT_28, Rhostpage + 0x4);

				rv = PH_SUCCESS;
				goto Send_TLP_Polling_EXIT;
			}
		}
	} else if (RW == W_CMD) {
		hostpage->CMD = W_CMD;
		hostpage->OWN = 1;

		writel(*value, (INT32U)pg_value);
		writel(*((INT32U *)hostpage + 1), Rhostpage + 0x4);

		for (tmp = 0 ;tmp < PCIE_TIME_OUT; tmp++) {
			if (!(readl(Rhostpage + 0x4) & BIT_31)) {
				writel(readl(Rhostpage + 0x4) | BIT_28, Rhostpage + 0x4);//clear done bit
				rv = PH_SUCCESS;
				goto Send_TLP_Polling_EXIT;
			}
		}
	}
	rv |= PH_ERROR_TIMEOUT;
	dump_stack();


Send_TLP_Polling_EXIT:

	OS_EXIT_CRITICAL;
	return rv;
}

ULONG pcieh_cfg32_read(volatile USHORT bus, volatile USHORT dev,
								volatile USHORT fun, volatile USHORT addr, volatile INT32U *value)
{
	volatile INT32U Base_addr_L = 0, ph_result = 0;
	volatile INT32U IMRvalue;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF, WIFI_IMR);

	Base_addr_L = bus;
	Base_addr_L = (Base_addr_L << 5) + dev;
	Base_addr_L = (Base_addr_L<< 3) + fun;
	ph_result = Send_TLP_Polling(CFG_TYPE, R_CMD, 0, Base_addr_L, addr, 0b1111, 0, 1, PCIE_TIME_OUT, value);

	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}
EXPORT_SYMBOL(pcieh_cfg32_read);

ULONG pcieh_cfg32_write(volatile USHORT bus, volatile USHORT dev,
								volatile USHORT fun, volatile USHORT addr, volatile INT32U value)
{
	volatile INT32U Base_addr_L = 0, ph_result = 0;
	volatile INT32U IMRvalue;

	/*close imr of msg interrupt from device*/

	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	Base_addr_L = bus;
	Base_addr_L = (Base_addr_L << 5) + dev;
	Base_addr_L = (Base_addr_L << 3) + fun;
	ph_result = Send_TLP_Polling(CFG_TYPE, W_CMD, 0, Base_addr_L, addr, 0b1111, 0, 1, PCIE_TIME_OUT, &value);

	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}
EXPORT_SYMBOL(pcieh_cfg32_write);

ULONG pcieh_cfg16_read(volatile USHORT bus, volatile USHORT dev,
								volatile USHORT fun, volatile USHORT addr, volatile INT32U *value)
{
	volatile INT32U Base_addr_L = 0, byte_en, value_temp, ph_result = 0;
	volatile INT32U IMRvalue;

	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	byte_en = Addr2Byte_en(addr, 0x03);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	Base_addr_L = bus;
	Base_addr_L = (Base_addr_L << 5) + dev;
	Base_addr_L = (Base_addr_L << 3) + fun;

	ph_result = Send_TLP_Polling(CFG_TYPE, R_CMD, 0, Base_addr_L, addr, byte_en, 0, 1, PCIE_TIME_OUT, &value_temp);
	*value = (INT16U)Value2Byte_en_Word(byte_en, value_temp, R_CMD);

end:
	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}
EXPORT_SYMBOL(pcieh_cfg16_read);

ULONG pcieh_cfg16_write(volatile USHORT bus, volatile USHORT dev,
								volatile USHORT fun, volatile USHORT addr, volatile INT32U value)
{
	volatile INT32U Base_addr_L = 0, byte_en, value_temp, ph_result = 0;
	volatile INT32U IMRvalue;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	byte_en = Addr2Byte_en(addr, 0x03);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	Base_addr_L = bus;
	Base_addr_L = (Base_addr_L << 5) + dev;
	Base_addr_L = (Base_addr_L << 3) + fun;
	value_temp = value;

	value = Value2Byte_en_Word(byte_en, value_temp, W_CMD);
	ph_result = Send_TLP_Polling(CFG_TYPE, W_CMD, 0, Base_addr_L, addr, byte_en, 0, 1, PCIE_TIME_OUT, &value);

end:

	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}
EXPORT_SYMBOL(pcieh_cfg16_write);

ULONG pcieh_cfg8_read(volatile USHORT bus, volatile USHORT dev,
								volatile USHORT fun, volatile USHORT addr, volatile INT32U *value)
{
	volatile INT32U Base_addr_L = 0, byte_en, value_temp, ph_result = 0;
	volatile INT32U IMRvalue;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	byte_en = Addr2Byte_en(addr, 0x01);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	Base_addr_L = bus;
	Base_addr_L = (Base_addr_L << 5) + dev;
	Base_addr_L = (Base_addr_L << 3) + fun;

	ph_result = Send_TLP_Polling(CFG_TYPE, R_CMD, 0, Base_addr_L, addr, byte_en, 0, 1, PCIE_TIME_OUT, &value_temp);
	*value = (INT8U)Value2Byte_en_Byte(byte_en, value_temp, R_CMD);

end:
	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}
EXPORT_SYMBOL(pcieh_cfg8_read);

ULONG pcieh_cfg8_write(volatile USHORT bus, volatile USHORT dev,
							volatile USHORT fun, volatile USHORT addr, volatile INT32U value)
{
	volatile INT32U Base_addr_L = 0, byte_en, value_temp, ph_result = 0;
	volatile INT32U IMRvalue;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	byte_en = Addr2Byte_en(addr, 0x01);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	Base_addr_L = bus;
	Base_addr_L = (Base_addr_L << 5) + dev;
	Base_addr_L = (Base_addr_L << 3) + fun;
	value_temp = value;

	value = Value2Byte_en_Byte(byte_en, value_temp, W_CMD);
	ph_result = Send_TLP_Polling(CFG_TYPE, W_CMD, 0, Base_addr_L, addr, byte_en, 0, 1, PCIE_TIME_OUT, &value);

end:
	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}
EXPORT_SYMBOL(pcieh_cfg8_write);

ULONG pcieh_io32_read(volatile INT32U io_base, volatile USHORT io_addr, volatile INT32U *value)
{
	volatile INT32U IMRvalue, ph_result = 0;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	ph_result = Send_TLP_Polling(IO_TYPE, R_CMD, 0, io_base, io_addr, 0b1111, 0, 1, PCIE_TIME_OUT, value);

	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}

ULONG pcieh_io32_write(volatile INT32U io_base, volatile USHORT io_addr, volatile INT32U value)
{
	volatile INT32U IMRvalue,ph_result;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	ph_result = Send_TLP_Polling(IO_TYPE, W_CMD, 0, io_base, io_addr, 0b1111, 0, 1, PCIE_TIME_OUT, &value);

	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}

ULONG pcieh_io16_read(volatile INT32U io_base, volatile USHORT io_addr, volatile INT32U *value)
{
	volatile INT32U byte_en, value_temp, ph_result = 0;
	volatile INT32U IMRvalue;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	byte_en = Addr2Byte_en(io_addr, 0x03);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	ph_result = Send_TLP_Polling(IO_TYPE, R_CMD, 0, io_base, io_addr, byte_en, 0, 1, PCIE_TIME_OUT, &value_temp);
	*value = (INT16U)Value2Byte_en_Word(byte_en, value_temp, R_CMD);

end:
	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}

ULONG pcieh_io16_write(volatile INT32U io_base, volatile USHORT io_addr, volatile INT32U value)
{
	volatile INT32U byte_en, ph_result = 0;
	volatile INT32U IMRvalue;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	byte_en = Addr2Byte_en(io_addr, 0x03);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	value = Value2Byte_en_Word(byte_en, value, W_CMD);
	ph_result = Send_TLP_Polling(IO_TYPE, W_CMD, 0, io_base, io_addr, byte_en, 0, 1, PCIE_TIME_OUT, &value);

end:
	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}

ULONG pcieh_io8_read(volatile INT32U io_base, volatile USHORT io_addr, volatile INT32U *value)
{
	volatile INT32U byte_en, value_temp, ph_result = 0;
	volatile INT32U IMRvalue;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	byte_en = Addr2Byte_en(io_addr, 0x01);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	ph_result = Send_TLP_Polling(IO_TYPE, R_CMD, 0, io_base, io_addr, byte_en, 0, 1, PCIE_TIME_OUT, &value_temp);
	*value =(INT8U)Value2Byte_en_Byte(byte_en, value_temp, R_CMD);

end:
	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}

ULONG pcieh_io8_write(volatile INT32U io_base, volatile USHORT io_addr, volatile INT32U value)
{
	volatile INT32U byte_en, ph_result = 0;
	volatile INT32U IMRvalue;

	/*close imr of msg interrupt from device*/
	IMRvalue = readl(WIFI_IMR);
	writel(IMRvalue & 0xFFFFFFF0, WIFI_IMR);

	byte_en = Addr2Byte_en(io_addr, 0x01);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	value = Value2Byte_en_Byte(byte_en, value, W_CMD);
	ph_result = Send_TLP_Polling(IO_TYPE, W_CMD, 0, io_base, io_addr, byte_en, 0, 1, PCIE_TIME_OUT, &value);

end:
	writel(IMRvalue, WIFI_IMR);
	return ph_result;
}

u32 pcieh_mem32_read(volatile u32 Haddr, volatile u32 Laddr, volatile u32 *value)
{
	volatile INT32U ph_result;

	ph_result = Send_TLP_Polling(MEM_TYPE, R_CMD, Haddr, 0, Laddr, 0b1111, 0, 1, PCIE_TIME_OUT, value);

	return ph_result;
}
EXPORT_SYMBOL(pcieh_mem32_read);

u32 pcieh_mem32_write(volatile u32 Haddr, u32 Laddr, u32 value)
{
	volatile INT32U ph_result;

	ph_result = Send_TLP_Polling(MEM_TYPE, W_CMD, Haddr, 0, Laddr, 0b1111, 0, 1, PCIE_TIME_OUT, &value);

	return ph_result;
}
EXPORT_SYMBOL(pcieh_mem32_write);

u32 pcieh_mem16_read(volatile u32 Haddr, volatile u32 Laddr, volatile u32 *value)
{
	volatile INT32U byte_en, value_temp, ph_result = 0;

	byte_en = Addr2Byte_en(Laddr, 0x03);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	ph_result = Send_TLP_Polling(MEM_TYPE, R_CMD, Haddr, 0, Laddr, byte_en, 0, 1, PCIE_TIME_OUT, &value_temp);
	*value = (INT16U)Value2Byte_en_Word(byte_en, value_temp, R_CMD);

end:
	return ph_result;
}
EXPORT_SYMBOL(pcieh_mem16_read);

u32  pcieh_mem16_write(volatile u32 Haddr, volatile u32 Laddr, volatile u32 value)
{
	volatile INT32U byte_en, ph_result = 0;

	byte_en = Addr2Byte_en(Laddr, 0x03);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	value = Value2Byte_en_Word(byte_en, value, W_CMD);
	ph_result = Send_TLP_Polling(MEM_TYPE, W_CMD, Haddr, 0, Laddr, byte_en, 0, 1, PCIE_TIME_OUT, &value);

end:
	return ph_result;
}
EXPORT_SYMBOL(pcieh_mem16_write);

u32 pcieh_mem8_read(volatile u32 Haddr, volatile u32 Laddr, volatile u32 *value)
{
	volatile INT32U byte_en, value_temp, ph_result = 0;

	byte_en = Addr2Byte_en(Laddr, 0x01);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	ph_result = Send_TLP_Polling(MEM_TYPE, R_CMD, Haddr, 0, Laddr, byte_en, 0, 1, PCIE_TIME_OUT, &value_temp);
	*value =(INT8U)Value2Byte_en_Byte(byte_en, value_temp, R_CMD);

end:
	return ph_result;
}
EXPORT_SYMBOL(pcieh_mem8_read);

u32 pcieh_mem8_write(volatile u32 Haddr, volatile u32 Laddr, volatile u32 value)
{
	volatile INT32U byte_en, ph_result = 0;

	byte_en = Addr2Byte_en(Laddr, 0x01);
	if (!byte_en) {
		ph_result = PH_ERROR_WRONGVALUE;
		goto end;
	}

	value = Value2Byte_en_Byte(byte_en, value, W_CMD);
	ph_result = Send_TLP_Polling(MEM_TYPE, W_CMD, Haddr, 0, Laddr, byte_en, 0, 1, PCIE_TIME_OUT, &value);

end:
	return ph_result;
}
EXPORT_SYMBOL(pcieh_mem8_write);

/*
OOBMAC access IBMAC : DWORD alignment bit 31-0   OCP reg
value: relative to value of lowBit to highBit
*/
INT32U OOBMACReadIBReg(INT16U reg)
{
	writel(0x800f0000 | reg, OOBMAC_IB_ACC_SET);
	while((readl(OOBMAC_IB_ACC_SET) & 0x80000000));

	return readl(OOBMAC_IB_ACC_DATA);
}

void OOBMACWriteIBReg(INT16U reg, INT8U highBit, INT8U lowBit, INT32U value)
{
	INT32U reg_cmd = 0, reg_data = 0;
	INT32U oriValue = 0x00000000, inputValue = 0x00000000, maskValue = 0x00000000;
	INT8U i = 0;

	reg_cmd = OOBMAC_IB_ACC_SET;
	reg_data = OOBMAC_IB_ACC_DATA;

	oriValue = OOBMACReadIBReg(reg);

	for (i = lowBit; i <= highBit; i++)
		maskValue |= (1<<i);

	maskValue = ~maskValue;
	inputValue = (oriValue & maskValue) | (value << lowBit);

	writel(inputValue, reg_data);
	writel(0x808f0000 | reg, reg_cmd);

	while ((readl(reg_cmd) & 0x80000000));
}

void OOB2IB_W(INT16U reg, INT8U byte_en, INT32U value)
{
	writel(value, OOBMAC_IB_ACC_DATA);
	writel((0x80800000 | (byte_en << 16) | reg), OOBMAC_IB_ACC_SET);

	while ((readl(OOBMAC_IB_ACC_SET) & 0x80000000));
}

void Rc_ephy_W(INT32U reg, INT32U data)
{
	INT32U cmd = 0x80000000;

	cmd |= (reg << 16);
	cmd |= data;
	OOB2IB_W(EPHY_MDCMDIO_RC_DATA, 0xF, cmd);

	while(OOBMACReadIBReg(EPHY_MDCMDIO_RC_DATA) & BIT_31);
}

void init_pci_setup(rtk_pci_dev *pdev)
{
	this_pdev = pdev;

	pcieh_disable_pwr_seq();

	/*The pin contorl need init first or isolate pin control will cause HW abnormal*/
	OOB2IB_W(PIN_C, 0xF, OOBMACReadIBReg(PIN_C) | (BIT_16 | BIT_17 | BIT_18));

	pcieh_sw_reset(BIT_1);

	rc_ephy_init();
	msleep(100);
}
EXPORT_SYMBOL(init_pci_setup);
