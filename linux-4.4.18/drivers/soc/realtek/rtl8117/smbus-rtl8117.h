#ifndef _SMBUS_RTL8117_H_
#define _SMBUS_RTL8117_H_
#include <rtl8117_platform.h>
/*
SMBus Engine Register
Time: 2008.03.03 1300PM
*/
//physical address:0x1200_1000~0x1200_17FF
#define SMBUS_BASE_ADDR      0xBAF30000
#define SEIO_SMEn                0x0000
#define SEIO_SMPollEn            0x0002
#define SEIO_ISR                 0x0008
#define SEIO_IMR                 0x0009
#define SEIO_MIS_ERR             0x000B
#define SEIO_SlaveAddr           0x000C
#define SEIO_RxDesc              0x001C
#define SEIO_RxCurDesc           0x0020
#define SEIO_TxDesc              0x0024

#define SEIO_SCTimeOut           0x0028
#define SEIO_SampleTimingCtrl    0x0040
#define SEIO_PutDataTimingCtrl   0x0042
#define SEIO_SMBFallingSetting   0x0044
#define SEIO_SMBRisingSetting    0x0046
//2008.04.01 tomadd
#define SEIO_Status              0x0001
#define SEIO_ModeCmd             0x0004
#define SEIO_MTxDesc             0x0054
//2008.04.06 tomadd
#define SEIO_BusFree             0x002C
#define SEIO_HoldTimeRStart_1    0x002E
#define SEIO_RS_SetupTime_1      0x0030
#define SEIO_StopSetupTime_1     0x0032
#define SEIO_DataHoldTime        0x0034
#define SEIO_MasterClkLow        0x0038
#define SEIO_MasterClkHigh       0x003A
#define SEIO_MasterRBControl     0x003C
#define SEIO_MasterTDControl     0x003E
#define SEIO_SlaveTimingCtrl     0x0040
#define SEIO_SlaveLDTimingCtrl   0x0042
#define SEIO_SlaveSMBCFallTime   0x0044
#define SEIO_SlaveSMBCRiseTime   0x0046
#define SEIO_SlaveSMBDFallTime   0x0048
#define SEIO_SlaveSMBDRiseTime   0x004A
#define SEIO_HoldTimeRStart_2    0x004C
#define SEIO_RS_SetupTime_2      0x004E
#define SEIO_StopSetupTime_2     0x0050
#define SEIO_SMBCFallTime        0x0058
#define SEIO_SMBCRiseTime        0x0059
#define SEIO_SMBDFallTime        0x005A
#define SEIO_SMBDRiseTime        0x005B

//SMBus Engine -2
#define SEIO_SMEn2               0x0080
#define SEIO_Status2             0x0081
#define SEIO_SMPollEn2           0x0082
#define SEIO_ModeCmd2            0x0084
#define SEIO_ISR2                0x0088
#define SEIO_IMR2                0x0089
#define SEIO_SlaveAddr2          0x008C
#define SEIO_SlaveAddr3          0x0090
#define SEIO_SampleTimingCtrl2   0x00C0
#define SEIO_PutDataTimingCtrl2  0x00C2
#define SEIO_SMBFallingSetting2  0x00C4
#define SEIO_SMBRisingSetting2   0x00C6
#define SEIO_RxDesc2             0x009C
#define SEIO_RxCurDesc2          0x00A0
#define SEIO_BusFree2            0x00AC
#define SEIO_HoldTimeRStart_1S   0x00AE
#define SEIO_RS_SetupTime_1S     0x00B0
#define SEIO_StopSetupTime_1S    0x00B2
#define SEIO_DataHoldTime_S      0x00B4
#define SEIO_MasterClkLow_S      0x00B8
#define SEIO_MasterClkHigh_S     0x00BA
#define SEIO_MasterRBControl_S   0x00BC
#define SEIO_MasterTDControl_S   0x00BE
#define SEIO_SlaveTimingCtrl_S   0x00C0
#define SEIO_SlaveLDTimingCtrl_S 0x00C2
#define SEIO_SlaveSMBCFallTime_S 0x00C4
#define SEIO_SlaveSMBCRiseTime_S 0x00C6
#define SEIO_SlaveSMBDFallTime_S 0x00C8
#define SEIO_SlaveSMBDRiseTime_S 0x00CA
#define SEIO_HoldTimeRStart_2S   0x00CC
#define SEIO_RS_SetupTime_2S     0x00CE
#define SEIO_StopSetupTime_2S    0x00D0
//SMBus Engine -2 for internal test
#define SMBUS1_IOBASE            SMBUS_BASE_ADDR + 0x10000  // SMBUS Slave1 

#define MAC_IB_ACC_DATA           0x00A0
#define MAC_IB_ACC_SET            0x00A4

#define MAC_IDR0                 0x0000
#define MAC_IDR4                 0x0004
#define MAC_MAR0                 0x0008
#define MAC_MAR4                 0x000C

/* ----------------------------------------------------------------------------
define parameters-----------------------------
*/
#define BIG_ENDIAN          2
#define LITTLE_ENDIAN       1
#define BYTE_ORDER LITTLE_ENDIAN

//move to inet_types.h
//#define MAC_ADDR_LEN        6
#define ETH_PADDING_SIZE    2

//#define IPv4_ADR_LEN  4
//#define IPv6_ADR_LEN  16

#define ETHERNET_TYPE_IPv4  0x0800
#define ETHERNET_TYPE_IPv6  0x86DD
#define ETHERNET_TYPE_ARP   0x0806

#define IPv4 4
#define IPv6 6

#define TCP_HDR_SIZE    20
#define UDP_HDR_SIZE    8

#define IPv4_HDR_OFFSET     16
#define TCPv4_HDR_OFFSET    36
#define TCPv6_HDR_OFFSET    56

#define ETH_PKT_LEN     1516
#define ETH_PAYLOAD_LEN  1300

#define ETH_HDR_SIZE    16
#define MAX_PAGE_LINE_LEN 255
#define LEAST_PAGE_LINE_LEN 255

#define eth0    0
#define wlan0   1
#define MAX_INTF    2

//size of PKT(4) + ETH_HDR_SIZE(16) + IP max header size 60 + TCP max header size 60
//Teredo Tunnel ETH_HDR_SIZE(16) + IPv4 max header size 60 +UDP Header 8+ teredo tunnel 20 byte(min)?? + IPv6 Header size 40
#define PKT_HDR_SIZE 140
#define IP_PKT_HDR_SIZE 80
//UDP_HDR_SIZE  8
#define UDP_PKT_HDR_SIZE 140

#define MAXSENDPKTS         3
#define MAXRTTIMES      12
#define TIMEOUT             3
#define CONNECT_RETRY   3
#define SKT_DLY_TIME    20

#define off 0
#define on  1

#define NEED_PATCH 9
//#define MAX_RTR_SOLICITATION_DELAY 1

#define DHCPv4_Cli 68
#define DHCPv4_Srv 67

#define DHCPv6_Cli 546
#define DHCPv6_Srv 547

#define SNMPPort   0x00A2

#define DNS_Srv_PORT 53

#define BASIC_AUTH 1
#define DIGEST_AUTH 2
#define KERB_AUTH 3
#define MAX_PS_LEN 32 //provisioning server 
#define MAX_DN_LEN 64//domain name
#define MAX_HN_LEN 16 //host name
#define MAX_FQDN_LEN 32 //fqdn (amt like)
#define MAX_PW_LEN 32 //password
#define MAX_OU_LEN 64 //ou
#define MAX_OTP_LEN 32
#define PID_LEN 8
#define PPS_LEN 32

#define UNICAST_MAR 1
#define MULCAST_MAR 2
#define BROADCAST_MAR   4

#define MAC_ADDR_LEN    6

#define IPv4 4
#define IPv6 6

#define IP_PROTO_ICMP 0x01
#define IP_PROTO_ICMPv6 0x3A
#define IP_PROTO_UDP 0x11
#define IP_PROTO_TCP 0x06

#define IPv4_ADR_LEN 4
#define IPv6_ADR_LEN 16
#define PORT_ADR_LEN 2
#define TYPE_ADR_LEN 2
#define PTL_ADR_LEN 1

#define MAC_MAC_STATUS            0x0104
/*----------------------*/

typedef struct _pkidata
{
    u8 fqdn[MAX_FQDN_LEN];
    u8 dnssuf[MAX_FQDN_LEN];
    u8 certhash[6][20];
} PKIDATA;

typedef struct _pskdata
{
    u8 PID[PID_LEN];
    u8 PPS[PPS_LEN];
} PSKDATA;

typedef struct _UserInfo {
    char  name[16];
    char  passwd[16];
    u8  role;
    u8  opt;
    u16 crc16;
    u8  caption[23];
    u8  opaque;
    struct _UserInfo *next;
} UserInfo;


typedef struct _IPAddress
{
    u32 addr;
} IPAddress;
typedef struct _dpconf
{
    UserInfo admin;
    //u32 wdt;
    IPAddress HostIP[MAX_INTF];
    IPAddress SubnetMask[MAX_INTF];
    IPAddress GateWayIP[MAX_INTF];
    IPAddress DNSIP[MAX_INTF];
    IPAddress ConsoleIP[MAX_INTF];

    u32 asfon:1, chipset: 3, numsent: 4, hbtime : 8, polltime : 6, asfpoll: 1, lspoll: 1, arpaddr : 8 ;
    u32 IPv4Enable:MAX_INTF, IPv6Enable:MAX_INTF, DHCPv4Enable:MAX_INTF, DHCPv6Enable:MAX_INTF, EchoService:1, 
    Security:3, httpService:1, httpsService:1, wsmanService:1, isDHCPv4Enable: MAX_INTF;
    u32  MatchSubnetMaskValue[MAX_INTF];
    //u8 DNSMAR[MAC_ADDR_LEN];

    u8 srcMacAddr[MAX_INTF][MAC_ADDR_LEN];
    u16 efusever;
    u32 fwMajorVer;
    u32 fwMinorVer;
    u32 fwExtraVer;
    u32 fwBuildVer;
    u32 builddate;

    u8 HostIPv6[MAX_INTF][IPv6_ADR_LEN];
    u8 IPv6Prefix[MAX_INTF][IPv6_ADR_LEN];
    u8 IPv6DNS[MAX_INTF][IPv6_ADR_LEN];
    u8 IPv6GateWay[MAX_INTF][IPv6_ADR_LEN];
    u8 IPv6PrefixLen[MAX_INTF];
    u8 BMCPoll:1, BMCPollTime: 7;
    u8 DBGMacAddr[MAC_ADDR_LEN];
    IPAddress DBGIP;
    u16 DASHPort;
    u16 DASHTLSPort;
    u8  HostName[16];
    u8  Profiles[12];
    u8 DomainName[MAX_DN_LEN];
    u16 ProvisioningSetup:1, ProvisioningMode:1, ProvisioningState: 2, PKIEnabled:1, PSKEnabled:1, dummy1:10;
    u16 ProvisionServerPort;
    IPAddress ProvisionServerIP;
    u8 ProvisionServerName[MAX_PS_LEN];
    struct _pkidata pkidata;
    struct _pskdata pskdata;
    u8 OTP[MAX_OTP_LEN];
    u8 OUString[MAX_OU_LEN];
    u8 UUID[20];
    u32 ipmode;
    u8  aesmsg[20]; //runtime usage, to get key from setup binary
    u32 efusekey;
    u32 bios:8, biostype: 8,  vendor:8, pldmtype: 4, pldmmultitx: 1, pldmdbg:1, enpldmsnr :1, pldmreset:1;
    u8  restart;    //inform dash to reget the in-band data
    u8 eap;
    u8 usbotg:1, ehcipatch:1, usbrsvd:6;
    u8 useTeredo:1, teredoEnable:1, teredorsvd:6;
    u8 teredoSrv[32];
    //PLDMRCC means the slave address of Remote Control and PLDM are the same
    u32 pldmsnr:8, numofsnr:8, pldmfromasf: 1, pldmpec: 1, snrpec: 1, pldmrsvd: 13;
    u8  pldmslaveaddr;
    u8  pldmsrvd[3];
    u32 *eaptls;
    unsigned char eappro;
    unsigned char enctype;
    unsigned char enckey[16];
    u16 counter;      //+briankuo , 20140909 : Add for recording reset times
} DPCONF;

enum { PLDM_BLOCK_READ = 0,
       PLDM_BLOCK_WRITE,
     };
typedef struct
{
#if 0
    struct
    {
        unsigned int length  : 7;
        unsigned int index   : 8;
        unsigned int eor     : 1;
        unsigned int rsvd    : 8;
        unsigned int rsvd2   : 7;
        unsigned int own     : 1;
    } st;
#endif
    u32 opts;
    u8* buf_addr;

} volatile smbrxdesc_t;

typedef struct
{
    struct
    {
        unsigned int length  : 7;
        unsigned int index   : 8;
        unsigned int eor     : 1;
        unsigned int rsvd    : 8;
        unsigned int rsvd2   : 7;
        //INT32U mctp    :  1;
        unsigned int own     : 1;
    } st;
    unsigned char * tx_buf_addr;

} volatile smbtxdesc_t;

#define MAX_INDEX 64
struct smbus_rtl8117 {
    //void __iomem *base;
    u32 base;
    smbtxdesc_t * txdesc;
    smbtxdesc_t * txmdesc;
    smbrxdesc_t * rxdesc;
    void * rx_buf[MAX_INDEX];
    void * tx_buf[MAX_INDEX];
    dma_addr_t txphyaddr;
    dma_addr_t txmphyaddr;
    dma_addr_t rxphyaddr;
    struct device       *dev;
    //struct tasklet_struct smbios_task;
    //void (*handle_pldm)(u8 *addr);
    //void (*smbus_process_pldm)(u8 *addr);
    struct work_struct smb_work;
    //struct work_struct pldmsnr_work;
    //struct work_struct evt_work;
    //struct mutex mutex;
    //spinlock_t slock;
    struct list_head smb_list;
    //struct list_head *pldmsnr_list;
    //struct list_head *evt_list;
    u32 smbrxdescnum;
    u32 smbrxsize;
    //DPCONF *dpconf;
    //pldm_t *pldmdataptr;
    //pldm_res_t *pldmresponse[7];
    void * smbtxbuffer;
    dma_addr_t smbtxbuf_map;
    spinlock_t smbus_spinlock;
    u32 mmap_page_addr;
};

typedef struct
{
    u8 slaveaddr;
    u8 cmd;
    u8 length;
    u8 sslaveaddr;
    u8 hdrversion:1, rsvd: 5, mctpstop:1, mctpstart:1;
    u8 dstept;
    u8 srcept;
    u8 msgtag:3, to:1, pktseq:2, msgstop:1, msgstart:1;
    u8 msgtype:7, ic: 1;
    u8 iid: 5, rsvd2: 1, datagram: 1, rd: 1;
    u8 pldmtype:6, hdrver: 2;
    u8 pldmcmd;
    u8 pldmcode;
    u8 val[57];
    u8 val2[2];
} pldm_res_t;

typedef struct
{
    u8  *ptr[3];
    u32 len[3];
    u32 tag[3];
    u8  TBL1:1, TBL2:1, TBL3:1, pldmerror:1, RSVD: 4;
    u8  index;
    u8  valid;
    u8  dirty;
    u8  *xferptr[3];
    u32 numwrite[4];
    u32 xferlen[3];
    u32 xfertag[3];
    u32 chksum[3];
} pldm_t;

typedef struct _asf_ctldata
{
    u8 function;
    u8 slaveaddr;
    u8 cmd;
    u8 data;
} asf_ctldata_t;

typedef struct _asf_alert
{
    u8   assert;
    u8   deassert;
    u8   exactmatch;
    u8   logtype:4, status:4;
    u8   address;
    u8   command;
    u8   datamsk;
    u8   cmpvalue;
    u8   evtsnrtype;
    u8   evttype;
    u8   evtoffset;
    u8   evtsrctype;
    u8   evtseverity;
    u8   snrnum;
    u8   entity;
    u8   instance;
} asf_alert_t;

typedef struct _legacy_snr
{
    u8  addr;
    u8  command;
    u8  addrplus;
    u8  type;
    u8  sensorindex;
} legacy_snr_t;


typedef struct _asf_config
{
    u8           maxwdt;
    u8           minsnrpoll;
    u16          systemid;
    u8           IANA[4];
    u32          pec;
    asf_ctldata_t   asf_rctl[4];
    u8           RMCPCap[7];
    u8           RMCPCmpCode;
    u8           RMCPIANA[4];
    u8           RMCPSpecialCmd;
    u8           RMCPSpecialCmdArgs[2];
    u8           RMCPBootOptions[2];
    u8           RMCPOEMArgs[2];
    u8           numofsnr;
    legacy_snr_t    legacysnr[16];
    u8           numofalerts;
    asf_alert_t     asfalerts[16];
} asf_config_t;


struct master_smbus_parameters
{
    unsigned char msg_type;
    unsigned char cmd;
    bool pldmfromasf;
    bool pldmpec;
    bool asfpec;
    unsigned char poll_type;
    unsigned char * bufaddr;
    unsigned char length;
};

struct slave_smbus_info
{
    unsigned char * buf;
    unsigned int len;
};

struct smbus_arp_info
{
    unsigned int index;
    unsigned int value;
};


typedef struct _sensor
{
    unsigned char  name[32];
    unsigned char  offset[2];
    unsigned char  pollindex: 4, exist:4;
    unsigned char  event: 2, fault:6;
    int value;
    unsigned char  state;
    unsigned char  prestate;
    unsigned char  type:7, signvalue:1;
    unsigned char  index;
    unsigned int LNC;
    unsigned int UNC;
    unsigned int LC;
    unsigned int UC;
    unsigned int LF;
    unsigned int UF;
} sensor_t;

typedef struct
{
    unsigned char  pollflag;
    unsigned char  expired;
    unsigned char  bootopt[11];
    unsigned char  MsgType;
    unsigned char  LSPFlag;
    unsigned char  ASFFlag;
    unsigned char  PollTimeOut;
    unsigned char  PollType;
    unsigned char  Status[28];
    unsigned char  Flag[56];
    unsigned char  Boottime;
    unsigned char  SensorIndex;
    unsigned char  LSensor[8];
    //at most support 8 legacy sensors
    sensor_t *sensor;
    unsigned char  IBRmtl;
    unsigned char  IBRmtlCmd;
} smbiosrmcpdata;

struct rx_data_list {
    struct list_head head;
    u8 * addr;
    u8 len;
    dma_addr_t mapping;
};

typedef enum _msendtype {
    REMOTE_CONTROL = 0,
    ASF_SENSOR_POLL,
    LS_POLL,
    BMC_POLL,
    PLDM_RESPONSE,
    PLDM_REQUEST,
} msendtype;

typedef enum _pldm_base_code
{
    PLDM_SUCCESS,
    PLDM_ERROR,
    PLDM_ERROR_INVALID_DATA,
    PLDM_ERROR_INVALID_LENGTH,
    PLDM_ERROR_NOT_READY,
    PLDM_ERROR_UNSUPPORTED_PLDM_CMD,
    PLDM_INVALID_PLDM_TYPE = 0x20,
} PLDM_BASE_CODE;

//PLDM DSP 0246
enum {
    GetSMBIOSStructureTableMetadata = 1,
    SetSMBIOSStructureTableMetadata,
    GetSMBIOSStructureTable,
    SetSMBIOSStructureTable,
    GetSMBIOSStructureByType,
    GetSMBIOSStructureByHandle,
};

enum {
    NO_SMBIOS_STRUCTURE_TABLE_METADATA = 0x83,
};

//PLDM DSP 0247
enum {
    GetBIOSTable = 1,
    SetBIOSTable,
    UpdateBIOSTable,
    GetBIOSTableTags,
    SetBIOSTableTags,
    AcceptBIOSAttributesPendingValues,
    SetBIOSAttributeCurrentValue,
    GetBIOSAttributeCurrentValueByHandle,
    GetBIOSAttributePendingValueByHandle,
    GetBIOSAttributeCurrentValueByType,
    GetBIOSAttributePendingValueByType,
    GetDateTime,
    SetDateTime,
    GetBIOSStringTableStringType,
    SetBIOSStringTableStringType,
    BIOS_TABLE_TAG_UNAVAILABLE = 0x86,
};

enum {
    PLDM_BIOS_TABLE_UNAVAILABLE = 0x83,
    PLDM_BIOS_TABLE_TAG_UNAVAILABLE = 0x86,
    PLDM_UNKNOWN = 0xFF,
};

enum {
    BIOSEnumeration = 0x00,
    BIOSString = 0x01,
    BIOSPassword = 0x02,
    BIOSBootConfigSetting = 0x04,
};

typedef struct _SMBIOS_Table_EP
{
    unsigned char anchorStr[4];
    char chkSum;
    char EPLen;
    char majorVer;
    char minorVer;
    unsigned short MaxStSize;
    char EPReVer;
    char formattedArea[5];
    char ianchorStr[5];
    char ichkSum;
    unsigned short StTableLen;
    unsigned int StTableAddress;
    unsigned short StNum;
    unsigned int SMBIOSNum;
} SMBIOS_Table_EP;

enum {SMBSSTATE=0, SMBGSTATE, SMARP, SMSSEND, SMMSEND, SMBSMEN, SMBOFF1, MMAP, CLAERBOOTOPT, INBAND_CHECK, IODRVAP_EXIST, SINGLE_IP, TEST} ;

enum {S_S0 = 0, S_S1, S_S2, S_S3, S_S4, S_S5, S_BIOS, S_UNKNOWN = 0x0E};

#define MAC_BASE_ADDR        0xBAF70000


#define MAC_FVID                  0x0120
#define MAC_IBREG                 0x0124
#define MAC_OOBREG                0x0128
#define MAC_BIOSREG               0x012C
#define MAC_OOB_LANWAKE 0x013C
#define MAC_HOSTNAME              0x0140
#define MAC_LAN_WAKE              0x0154
#define MAC_ACPI                  0x0160
#define MAC_SYNC1                 0x0170
#define MAC_SWISR                 0x0180
#define MAC_ROMVER                0x019C
#define MAC_NO_CLEAR              0x01DC
//in-band remote control
#define IB_Reset     0x01
#define IB_Sleep     0x02
#define IB_Hibernate 0x03
#define IB_Shutdown  0x04

//RMCP Message Type
#define RMCP_Reset   0x10
#define RMCP_PWR_On  0x11
#define RMCP_PWR_Off 0x12
#define RMCP_PWR_CR  0x13
#define RMCP_HIBER   0x14
#define RMCP_STANDBY 0x15

#define RMCP_TEST    0x20
#define RMCP_PONG    0x40
#define RMCP_CAP_RES 0x41
#define RMCP_SST_RES 0x42
#define RMCP_OPS_RES 0x43
#define RMCP_CLS_RES 0x44
#define RMCP_PING    0x80
#define RMCP_CAP_REQ 0x81
#define RMCP_SST_REQ 0x82
#define RMCP_OPS_REQ 0x83
#define RMCP_CLS_REQ 0x84
#define RMCP_RAKP_M1 0xC0
#define RMCP_RAKP_M2 0xC1
#define RMCP_RAKP_M3 0xC2

//VA/VB, bsp_bits_set is 8 bits access
//VC is 32 bits access

#define BIT_IN_BAND          0x0C
#define BIT_RSVD             0x0B
//HwFunCtr
#define BIT_AAB              0x17

#define BIT_FILTERMACIP      0x16
#define BIT_AATCP_UDPB       0x15
#define BIT_OOBFILTERDISABLE 0x14
#define BIT_FILTERRMCP       0x13

#define BIT_PCIEBDGRESET     0x12
#define BIT_PHYCLR           0x11
#define BIT_PCIEBDGEN        0x10

//Cfg
#define BIT_POWER_ON_RESET   0x1D
#define BIT_SINGLE_IP        0x1C

#define BIT_LANWAKE_IB_DIS   0x01
#define BIT_OOB_LANWAKE      0x00

#define BIT_ISOLATE          0x00
#define BIT_AUTOLOAD         0x01
#define BIT_LINKOK           0x02
#define BIT_TP10             0x03
#define BIT_TP100            0x04
#define BIT_TP1000           0x05

//For MAC_STATUS 0x10E
#define BIT_FLASH_LOCK       0x00

//For IBREG 0x124
#define BIT_DRIVERRDY        0x00
#define BIT_APRDY            0x01
#define BIT_FWMODE           0x0A

//For OOBREG 0x128
#define BIT_DASHEN           0x00
#define BIT_TLSEN            0x01
#define BIT_FIRMWARERDY      0x02
#define BIT_OOBRESET         0x03

//For OOBREG 0x13c
#define BIT_OOBLANWAKE         0x00

//For BIOSREG 0x12C
#define BIT_SYSSH            0x00

//FOR ACPI 0x160
#define BIT_ACPI             0x00

//For SYNC1 0x170
#define BIT_RMTCTL           0x00

//For GPIO control 0x500
#define BIT_GPO_EN           0x04
#define BIT_GPO_OE           0x05
#define BIT_GPO_I            0x06
#define BIT_GPO_C            0x07

//For 0x700
#define BIT_AES_KEY          0x00

enum {
    POLL_NONE = 0,
    POLL_LS,
    POLL_ASF_STATUS,
    POLL_ASF_PEND,
    POLL_ASF_DATA,
    POLL_SENSOR_DETECT,
    POLL_SENSOR,
    POLL_STOP
};
enum {ASF_RESET = 0, ASF_POWER_OFF, ASF_POWER_ON, ASF_POWER_CR};
static inline void bsp_bits_set(u16 offset, u32 value, u8 bits, u8 width)
{
    u16 count = 0;
    u32 valmask = 0;

    for (count = 0; count < width; count++)
        valmask |= (1 << (count+bits));

    //only clear zero value
    value = (valmask & (value << bits));
    valmask ^= value;

    //always clear first, but only for the zero value
    if((offset % 4) == 0 )
    {
        rtl_writel(rtl_readl(MAC_BASE_ADDR + offset)&(~valmask), MAC_BASE_ADDR + offset);
        rtl_writel(rtl_readl(MAC_BASE_ADDR + offset)|(value), MAC_BASE_ADDR + offset);
    }
    else if ((offset %2) == 0)
    {
        rtl_writew(rtl_readw(MAC_BASE_ADDR + offset)&(~valmask), MAC_BASE_ADDR + offset);
        rtl_writew(rtl_readw(MAC_BASE_ADDR + offset)|(value), MAC_BASE_ADDR + offset);
    }
    else
    {
        rtl_writeb(rtl_readb(MAC_BASE_ADDR + offset)&(~valmask), MAC_BASE_ADDR + offset);
        rtl_writeb(rtl_readb(MAC_BASE_ADDR + offset)|(value), MAC_BASE_ADDR + offset);
    }
}

static inline u32 bsp_bits_get(u16 offset, u8 field, u8 width)
{
    u32 valmask = 0;
    u32 count = 0;

    u32 value;

    if((offset % 4) == 0)
        value = rtl_readl(MAC_BASE_ADDR + offset);//REG32(MAC_BASE_ADDR + offset);
    else if((offset % 2) == 0)
        value = rtl_readw(MAC_BASE_ADDR + offset);
    else
        value = rtl_readb(MAC_BASE_ADDR + offset);

    for (count = 0; count < width; count++)
        valmask |= ( 1 << (count+field));

    value = ((value & valmask) >> field);

    return value;
}

static inline u8 bsp_get_sstate(void)
{
    u32 state;
#if 0
    u32 state, acpi;

//CONFIG_VERSION >= IC_VERSION_EP_RevA
    //isolate pin 1 equals S0
    if(bsp_bits_get(MAC_MAC_STATUS, BIT_ISOLATE, 1))
        state = S_S0;
    else
        state = S_S5;

    acpi = bsp_bits_get(MAC_ACPI, BIT_ACPI, 8);

    //isolate will reflect the S5 only
    //if console tool or others change the state to S3 or S4
    //the state chould be S3 or S4 instead of S5
    if(state == S_S5 && (acpi == S_S3 || acpi == S_S4))
        state = acpi;
#else

    state = bsp_bits_get(MAC_ACPI, BIT_ACPI, 8);

    if (state == S_BIOS) //bios would set S0 to 0x06, patch it
        state = S_S0;
    else if (state > S_BIOS)
        state = S_UNKNOWN;
#endif

    return state;
}

#if 0
typedef struct _UserInfo {
    char  name[16];
    char  passwd[16];
    u8  role;
    u8  opt;
    u16 crc16;
    u8  caption[23];
    u8  opaque;
    struct _UserInfo *next;
} UserInfo;

typedef struct _IPAddress
{
    u32 addr;
} IPAddress;

typedef struct _pkidata
{
    u8 fqdn[MAX_FQDN_LEN];
    u8 dnssuf[MAX_FQDN_LEN];
    u8 certhash[6][20];
} PKIDATA;

typedef struct _pskdata
{
    u8 PID[PID_LEN];
    u8 PPS[PPS_LEN];
} PSKDATA;

typedef struct
{
    unsigned long total[2];     /*!< number of bytes processed  */
    unsigned long state[4];     /*!< intermediate digest state  */
    unsigned char buffer[64];   /*!< data block being processed */

    unsigned char ipad[64];     /*!< HMAC: inner padding        */
    unsigned char opad[64];     /*!< HMAC: outer padding        */
}
md5_context;

typedef struct
{
    unsigned long total[2];     /*!< number of bytes processed  */
    unsigned long state[5];     /*!< intermediate digest state  */
    unsigned char buffer[64];   /*!< data block being processed */

    unsigned char ipad[64];     /*!< HMAC: inner padding        */
    unsigned char opad[64];     /*!< HMAC: outer padding        */
}
sha1_context;

typedef struct _EAPRECLAYER
{
    unsigned char ct;
    unsigned short size;
} EAPRECLAYER;

typedef struct _EAPTLS
{
    unsigned char eap;
    unsigned char eapid;
    char *identity;
    char *password;
    unsigned int totallen;
    unsigned char *buf;
    unsigned char *sendbuf;
    unsigned int sendbuflen;
    unsigned char *recvbuf;
    unsigned int recvbuflen;
    int keylen;                         /*!<  symmetric key length    */
    int minlen;                         /*!<  min. ciphertext length  */
    int ivlen;                          /*!<  IV length               */
    int maclen;                         /*!<  MAC length              */
    md5_context fin_md5;               /*!<  Finished MD5 checksum   */
    sha1_context fin_sha1;              /*!<  Finished SHA-1 checksum */
    unsigned char clientRandom[32];     /* client's random sequence */
    unsigned char serverRandom[32];     /* server's random sequence */
    unsigned char iv_enc[16];           /*!<  IV (encryption)         */
    unsigned char iv_dec[16];           /*!<  IV (decryption)         */
    unsigned char mac_enc[32];          /*!<  MAC (encryption)        */
    unsigned char mac_dec[32];          /*!<  MAC (decryption)        */
    unsigned long ctx_enc[128];         /*!<  encryption context      */
    unsigned long ctx_dec[128];         /*!<  decryption context      */
    unsigned char sessionID[32];
    unsigned short cipher;              /*!< chosen cipher      */
    x509_cert *server_cert;               /*!<  peer X.509 cert chain   */
    x509_cert *cli_cert;                /*!<  own X.509 certificate   */
    int major_ver;          /*!< max. major version from client   */
    int minor_ver;          /*!< max. minor version from client   */
    unsigned char out_ctr[8];
    unsigned char in_ctr[8];
    unsigned char master[48];
    EAPRECLAYER rec;
    unsigned char *keymaterial;
} EAPTLS;

typedef struct _dpconf
{
    UserInfo admin;
    u32 wdt;
    IPAddress HostIP[MAX_INTF];
    IPAddress SubnetMask[MAX_INTF];
    IPAddress GateWayIP[MAX_INTF];
    IPAddress DNSIP[MAX_INTF];
    IPAddress ConsoleIP[MAX_INTF];

    u32 asfon:1, chipset: 3, numsent: 4, hbtime : 8, polltime : 6, asfpoll: 1, lspoll: 1, arpaddr : 8 ;
    u32 IPv4Enable:MAX_INTF, IPv6Enable:MAX_INTF, DHCPv4Enable:MAX_INTF, DHCPv6Enable:MAX_INTF, EchoService:1, 
    Security:3, httpService:1, httpsService:1, wsmanService:1, isDHCPv4Enable: MAX_INTF;
    u32  MatchSubnetMaskValue[MAX_INTF];
    //u8 DNSMAR[MAC_ADDR_LEN];

    u8 srcMacAddr[MAX_INTF][MAC_ADDR_LEN];
    u16 efusever;
    u32 fwMajorVer;
    u32 fwMinorVer;
    u32 fwExtraVer;
    u32 fwBuildVer;
    u32 builddate;

    u8 HostIPv6[MAX_INTF][IPv6_ADR_LEN];
    u8 IPv6Prefix[MAX_INTF][IPv6_ADR_LEN];
    u8 IPv6DNS[MAX_INTF][IPv6_ADR_LEN];
    u8 IPv6GateWay[MAX_INTF][IPv6_ADR_LEN];
    u8 IPv6PrefixLen[MAX_INTF];
    u8 BMCPoll:1, BMCPollTime: 7;
    u8 DBGMacAddr[MAC_ADDR_LEN];
    IPAddress DBGIP;
    u16 DASHPort;
    u16 DASHTLSPort;
    u8  HostName[16];
    u8  Profiles[12];
    u8 DomainName[MAX_DN_LEN];
    u16 ProvisioningSetup:1, ProvisioningMode:1, ProvisioningState: 2, PKIEnabled:1, PSKEnabled:1, dummy1:10;
    u16 ProvisionServerPort;
    IPAddress ProvisionServerIP;
    u8 ProvisionServerName[MAX_PS_LEN];
    struct _pkidata pkidata;
    struct _pskdata pskdata;
    u8 OTP[MAX_OTP_LEN];
    u8 OUString[MAX_OU_LEN];
    u8 UUID[20];
    u32 ipmode;
    u8  aesmsg[20]; //runtime usage, to get key from setup binary
    u32 efusekey;
    u32 bios:8, biostype: 8,  vendor:8, pldmtype: 4, pldmmultitx: 1, pldmdbg:1, enpldmsnr :1, pldmreset:1;
    u8  restart;    //inform dash to reget the in-band data
    u8 eap;
    u8 usbotg:1, ehcipatch:1, usbrsvd:6;
    u8 useTeredo:1, teredoEnable:1, teredorsvd:6;
    u8 teredoSrv[32];
    //PLDMRCC means the slave address of Remote Control and PLDM are the same
    u32 pldmsnr:8, numofsnr:8, pldmfromasf: 1, pldmpec: 1, snrpec: 1, pldmrsvd: 13;
    u8  pldmslaveaddr;
    u8  pldmsrvd[3];
    struct _EAPTLS *eaptls;
    unsigned char eappro;
    unsigned char enctype;
    unsigned char enckey[16];
    //u8 DomainRealm[MAX_DN_LEN];
#if 0
    //mail_wakeup module by eccheng
#ifdef CONFIG_MAIL_WAKEUP_ENABLED
    u8 mail_wake_up; //on/off {on : != 0, off : = 0}
    u8 email_server[MWU_MAX_STR]; //[max.length=128] {IP, Domain Name}//POP3(110) and POP3S(995)
    u8 smtp_SSL; //enable {disable (port=110), enable (port=995)}
    u8 email_acount[MWU_MAX_STR]; //[max.length=128]
    u8 email_passwd[MWU_MAX_STR]; //[max.length=128]
    u8 match_address[MWU_MAX_STR]; //(from) [max.length=128]
    u8 match_subject[MWU_MAX_STR]; //[max.length=128]
    u8 match_keyword[MWU_MAX_STR]; //[max.length=128]only check email content s first 1024 bytes
    u8 Activate_condition; //{condition code = 1..13 }, now only support 7,12,13
    u8 mailPeriod;// (second) {default:3 second}
    //System Variables
    struct mailwakeup_ts ts_base;
#endif

    u16 counter;      //+briankuo , 20140909 : Add for recording reset times
#ifdef CONFIG_DOORBELL_CHECK_ENABLED
    u16 ostmr_unit;
#endif
#if CONFIG_WIFI_ENABLED
    WIFICfg wificfg;
#endif
#endif
} DPCONF;

typedef struct _eventdata
{
    u32  timestamp;
    u8   Event_Sensor_Type;
    u8   Event_Type;
    u8   Event_Offset;
    u8   Event_Source_Type;
    u8   Event_Severity;
    u8   Sensor_Device;
    u8   Sensor_Number;
    u8   Entity;
    u8   Entity_Instance;
    u8   EventData[8];
    u8   alertnum :6, sent: 2;
    u16  timeout;
    u16  seqnum;
    u8   interval;
    u8   watchdog :2, logtype : 6;
    struct _eventdata *next;
}
#endif

extern struct smbus_rtl8117 *sp;

/* disable interrupt */
static inline void smbus_disable_irq(void)
{
    rtl_writeb(0x00, sp->base + SEIO_IMR);
}

/* enable interrupt */
static inline void smbus_enable_irq(void)
{
    rtl_writeb(0x13, sp->base + SEIO_IMR);
}

/* read interrupt status */
static inline u8 smbus_read_isr(void)
{
    return rtl_readb(sp->base + SEIO_ISR);
}

/* clear interrupt status */
static inline void smbus_clear_isr(u8 irq)
{
    rtl_writeb(irq & 0xEF, sp->base + SEIO_ISR);
}

unsigned char SMBus_Prepare_RmtCtrl(unsigned char MsgType, unsigned char force);


#endif
