#include <linux/types.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/dma-mapping.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/platform_device.h>
#include <rtl8117_platform.h>
#include <linux/of_gpio.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/ioctl.h>
#include <linux/kernel.h>
#include <linux/err.h>
#include <linux/signal.h>
#include <linux/of_irq.h>
#include <linux/blkdev.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <asm/unaligned.h>
#include <linux/kthread.h>
#include <net/sock.h>
#include <linux/skbuff.h>
#include <linux/time.h>
#include <linux/netdevice.h>
#include <linux/ip.h>
#include <linux/in.h>
#include <linux/delay.h>
#include <linux/un.h>
#include <linux/unistd.h>
#include <linux/wait.h>
#include <linux/ctype.h>
#include <asm/unistd.h>
#include <asm/string.h>
#include <net/tcp.h>
#include <net/inet_connection_sock.h>
#include <net/request_sock.h>
#include <rtl_tls.h>
#include "rtl8117-asf.h"
#include "smbus-rtl8117.h"


#define RMCP_ASFV1_UDP_PORT  623
#define RMCP_ASFV2_UDP_PORT  664

int ksocket_send(struct socket *sock, struct sockaddr_in *addr, unsigned char *buf, int len)
{
    struct msghdr msg;
    struct iovec iov;
    mm_segment_t oldfs;
    int size = 0;
    int err;
    if (sock->sk==NULL)
        return 0;
    err = import_single_range(WRITE, buf, len, &iov, &msg.msg_iter);
    if (unlikely(err))
        return err;
    msg.msg_flags = 0;
    msg.msg_name = addr;
    msg.msg_namelen = sizeof(struct sockaddr_in);
    msg.msg_control = NULL;
    msg.msg_controllen = 0;
    msg.msg_control = NULL;
    oldfs = get_fs();
    set_fs(KERNEL_DS);
    size = sock_sendmsg(sock,&msg);
    set_fs(oldfs);
    return size;
}
int ksocket_receive(struct socket* sock, struct sockaddr_in* addr, unsigned char* buf, int len)
{
    struct msghdr msg;
    struct iovec iov;
    mm_segment_t oldfs;
    int size = 0;
    int err;
    if (sock==NULL) return 0;
    err = import_single_range(READ, buf, len, &iov, &msg.msg_iter);
    if (unlikely(err))
        return err;
    msg.msg_flags = 0;
    msg.msg_name = addr;
    msg.msg_namelen = sizeof(struct sockaddr_in);
    msg.msg_control = NULL;
    msg.msg_controllen = 0;
    msg.msg_iocb = NULL;
    msg.msg_control = NULL;
    oldfs = get_fs();
    set_fs(KERNEL_DS);
    size = sock_recvmsg(sock,&msg,iov_iter_count(&msg.msg_iter), MSG_DONTWAIT);
    set_fs(oldfs);
    return size;
}

const u8 Status[]=
{
    0x00, 0x01, 0x02, 0x03,
    0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0a, 0x0b,
    0x0c, 0x0d, 0x0e, 0x0f
};

//static const INT8U RMCP_IANA[] = {0x00, 0x00 , 0x11, 0xBE};
u8 KeyA[SIK_KEY_SIZE]= {0x10,0x67,0xb4,0xd0,0xd5,0x2b,0xc5,0x0c,0x3a,0xaf,0x66,0x69,0xb7,0x34,0x45,0x99,0x28,0xf4,0xf9,0xcc};
u8 KeyO[SIK_KEY_SIZE]= {0x98,0x73,0xf0,0x5b,0x1c,0x67,0xcc,0x50,0xd1,0x1e,0x2b,0xb1,0x10,0x0d,0xbb,0xc3,0x8d,0xd7,0x08,0xe2};
u8 KeyG[SIK_KEY_SIZE]= {0xde,0xef,0x16,0x3b,0xfb,0x8c,0x30,0x50,0x6b,0x3b,0xf1,0x13,0x77,0x35,0x4d,0x9c,0x55,0xbd,0xeb,0x87};

u8 Slide[WindowSIZE] = {0};
struct sockaddr_in cliaddrv1, cliaddrv2;
int tls_fd;
u8 Remote_Control = 0;
extern volatile DPCONF * dpconf;
extern asf_config_t *asfconfig;
extern smbiosrmcpdata *smbiosrmcpdataptr;

struct socket *asfv1_socket;
struct socket *asfv2_socket;
u8  SIK[SIK_KEY_SIZE];

void sha1_hmac( unsigned char *key, int keylen,
                unsigned char *input, int ilen,
                unsigned char output[20] )
{
    if (keylen == 20)
    {
        struct hmac_sha1_param *hsparam = kzalloc(sizeof(struct hmac_sha1_param), GFP_KERNEL);
        hsparam->key = key;
        hsparam->keylen = keylen;
        hsparam->data = input;
        hsparam->len = ilen;
        hsparam->out = output;
        hmacsha1(hsparam);
        kfree(hsparam);
    }
    else if (keylen == 24)
    {
        struct hmac_sha1_param *hsparam = kzalloc(sizeof(struct hmac_sha1_param), GFP_KERNEL);
        hsparam->key = key;
        hsparam->keylen = keylen;
        hsparam->data = input;
        hsparam->len = ilen;
        hsparam->out = output;
        hmacsha1_24(hsparam);
        kfree(hsparam);
    }
}

u8 sw_check(u32 serial, u8 Reset)
{
    static u32 slidestart = 0;
    static u32 slideindex;
    u32 index;

    if (Reset == 1)
    {
        slidestart = 0;
        slideindex = 0;
        for (index = 0; index < WindowSIZE; index++)
            Slide[index] = 0;
    }
    //sliding window
    else {
        if (slidestart == 0 && Slide[0] == 0) //initial condition
        {
            slidestart = serial;
            slideindex = 0;
        }

        index = serial - slidestart;
        if (index < 0 || index > 31)
            return 1; //error

        if (Slide[(index+slideindex)%WindowSIZE] == 1)
            return 1; //duplicate message
        else
            Slide[(slideindex+index)%WindowSIZE] = 1;

        if (Slide[slideindex] == 1) //slide the window
        {
            for (index = 0; index < WindowSIZE; index++)
            {
                if (Slide[(slideindex+index)%WindowSIZE] == 1)
                    Slide[(slideindex+index)%WindowSIZE] = 0;
                else if (Slide[(slideindex+index)%WindowSIZE] == 0) //find a non-zero start
                {
                    slideindex = (slideindex+index)% WindowSIZE;
                    slidestart += index;
                    break;
                }
            }
        }
    }
    return 0;
}

u8 *strip_rsp(RMCPCB *rmcp, RSPHdr *rsphdr)
{
    rmcp->session_seq = htonl(rsphdr->seq_num);
    rmcp->rsp_hdr = rsphdr;
    return ((u8*) rsphdr + sizeof(RSPHdr));
}

void rmcpackSend(RMCPCB * rmcp, unsigned char *rx_buf)
{
    unsigned char *tx_buf = NULL;
    RMCPHdr    *rmcphdr;
    RSPHdr     *rsphdr = NULL;
    RSPTrailer *rsptrailer;
    struct sockaddr_in *cliaddr;
    unsigned int len = 0;
    struct socket * skt = (rmcp->version == 1)?asfv1_socket:asfv2_socket;

    cliaddr = &cliaddrv1;
    tx_buf = kzalloc(128, GFP_KERNEL);
    rmcphdr = (RMCPHdr *)tx_buf;

    if (rmcp->version == 2)
    {
        rsphdr = (RSPHdr *)rmcphdr;
        memcpy(rsphdr, rmcp->rsp_hdr, sizeof(RSPHdr));
        if (rmcp->state && rmcp->rmcp_data->MsgType != RMCP_RAKP_M3)
        {
            rmcp->session_seq++;
            rsphdr->seq_num = htonl(rmcp->session_seq);
        }
        len += sizeof(RSPHdr);
        rmcphdr = (RMCPHdr *)((u8 *)rmcphdr + sizeof(RSPHdr));
        cliaddr = &cliaddrv2;
    }

    memcpy(rmcphdr, rmcp->rmcp_hdr, sizeof(RMCPHdr));
    rmcphdr->com |= 0x80;

    len += sizeof(RMCPHdr);

    //add RSP Trailer, only when a session has been created
    if (rmcp->state == 1 && rmcp->rmcp_data->MsgType != RMCP_RAKP_M3 && rsphdr)
    {
        rsptrailer = (RSPTrailer *)(tx_buf + len);
        rsptrailer->padding     = 0x0000;
        rsptrailer->pad_len     = 0x02;
        rsptrailer->next_header = 0x06;
        len += sizeof(RSPTrailer);
        sha1_hmac(SIK, SIK_KEY_SIZE, (u8 *) rsphdr , len-SIK_KEY_SIZE_USE, rsptrailer->integrity);
    }
    ksocket_send(skt, cliaddr, tx_buf, len);
}

void RMCP_Response(RMCPCB * rmcp, unsigned char *rx_buf)
{
    u16 sha_len = 0;
    u8 *PreData = kzalloc(128, GFP_KERNEL);
    u8 *RandomSeed = kzalloc(SIK_KEY_SIZE, GFP_KERNEL);
    u8 index = 0;
    struct sockaddr_in *cliaddr;
    RMCPHdr    *rmcphdr;
    RSPHdr     *rsphdr = NULL;
    RMCPData   *rmcpdata;
    RSPTrailer *rsptrailer;
    u8      *vardata;
    unsigned int len = 0;
    unsigned char *tx_buf;
    struct socket * skt = (rmcp->version == 1)?asfv1_socket:asfv2_socket;

    cliaddr = &cliaddrv1;
    memset(RandomSeed, 0, SIK_KEY_SIZE);

    tx_buf = kzalloc(128, GFP_KERNEL);
    rmcphdr = (RMCPHdr *)tx_buf;

    if (rmcp->version == 2)
    {
        rsphdr = (RSPHdr *) rmcphdr;
        memcpy(rsphdr, rmcp->rsp_hdr, sizeof(RSPHdr));
        if (rmcp->state)
        {
            rmcp->session_seq++;
            rsphdr->seq_num = htonl(rmcp->session_seq);
        }
        len += sizeof(RSPHdr);
        rmcphdr = (RMCPHdr *) ((u8 *) rmcphdr + sizeof(RSPHdr));
        cliaddr = &cliaddrv2;
    }

    //copy rmcp header from the received packet
    memcpy(rmcphdr, rmcp->rmcp_hdr, sizeof(RMCPHdr));
    if (rmcphdr->seq_num != 0xff)
        rmcphdr->seq_num++;
    len += sizeof(RMCPHdr);

    rmcpdata = (RMCPData *)((u8 *)rmcphdr + sizeof(RMCPHdr));

    memcpy(rmcpdata, rmcp->rmcp_data, sizeof(RMCPData));
    len += sizeof(RMCPData);
    vardata = (u8 *)(rmcpdata) + sizeof(RMCPData);

    switch (rmcp->rmcp_data->MsgType)
    {
    case RMCP_PING:
        rmcpdata->Data_Length = 0x10;
        rmcpdata->MsgType = RMCP_PONG;
        memset(vardata, 0 , 4);    //IANA
        memset(vardata+4, 0, 4);   //OEM
        *(vardata+8) = 0x01;       //ASF Version 1.0
        if (rmcphdr->seq_num != 0xff)
            *(vardata+9) = 0x80;       //Supported Inteartions
        else
            *(vardata+9) = 0xA0;
        //DASH should be 0xA0
        //memset(vardata+10, 0 , 6);
        *(vardata+10) = (dpconf->fwMajorVer << 4) + (dpconf->fwMinorVer);
        *(vardata+11) = dpconf->fwExtraVer;
        *((u32 *)(vardata+12)) = dpconf->fwBuildVer;
        break;

    case RMCP_OPS_REQ:
        //keep needed field
        memcpy(rmcp->MgtSID, rmcp->var_data, sizeof(rmcp->MgtSID));

        rmcpdata->Data_Length = 0x1C;
        rmcpdata->MsgType = RMCP_OPS_RES;

        //should check status, if error only return the first 8 bytes
        //RMCP Data content
        *vardata = rmcp->status;
        memset(vardata+1, 0, 3);     //reserved
        memcpy(vardata+4, rmcp->MgtSID, sizeof(rmcp->MgtSID));
        *(u32 *)(vardata+8) = htonl(rmcp->client_sid);
        memcpy(vardata+12, rmcp->var_data+4, 16);
        //Authentication Payload, Integrity Payload
        break;

    case RMCP_RAKP_M1:
        if (rmcp->role == ASF_Operator)
            rmcp->key = KeyO;
        else
            rmcp->key = KeyA;

        rmcpdata->Data_Length = 0x38;
        rmcpdata->MsgType = RMCP_RAKP_M2;

        //RMCP Data content
        *vardata = rmcp->status;
        memset(vardata+1, 0, 3);     //reserved
        memcpy(vardata+4, rmcp->MgtSID, sizeof(rmcp->MgtSID));

        //Client Random number
        while (index < SIK_KEY_SIZE)
        {
            (*((int *)(RandomSeed + index))) = read_rand();
            printk("random is 0x%02x-0x%02x.\n", RandomSeed[index], RandomSeed[index+1]);
            index += 4;

        }
        memset(PreData, 0 , 8);
        sha1_hmac(RandomSeed, SIK_KEY_SIZE, PreData, 8, vardata+8);
        memcpy(rmcp->CltRandom, (vardata+8), 16);

        //Client Global UID, should from SMBIOS implementation
        rmb();
        memcpy(vardata+24, (const void *)dpconf->UUID, 16);

        //Generate Integrity Check Value
        memcpy(PreData, rmcp->MgtSID, sizeof(rmcp->MgtSID));
        //memcpy(PreData+4, &rmcp->client_sid, sizeof(rmcp->client_sid));
        *(u32 *) (PreData+4) = htonl(rmcp->client_sid);
        memcpy(PreData+8,  (rmcp->var_data+4) , sizeof(rmcp->MgtRandom));
        memcpy(PreData+24, rmcp->CltRandom ,  sizeof(rmcp->CltRandom));
        rmb();
        memcpy(PreData+40, (const void *)dpconf->UUID, 16);

        PreData[56] = rmcp->role;
        PreData[57] = rmcp->name_length;
        memcpy(&PreData[58], rmcp->username, rmcp->name_length);

        sha_len = 58 + rmcp->name_length;
        sha1_hmac(rmcp->key, SIK_KEY_SIZE, PreData, sha_len, vardata+40);
        break;

    case RMCP_CAP_REQ:
        rmcpdata->Data_Length = 0x10;
        rmcpdata->MsgType = RMCP_CAP_RES;
        memset(vardata, 0 , 4);    //IANA
        memset(vardata+4, 0, 4);   //OEM
        memcpy(vardata+8, &asfconfig->RMCPCap[4], 3);
        memcpy(vardata+11, &asfconfig->RMCPCap[0], 4);
        *(vardata+15) = 0;
        break;

    case RMCP_SST_REQ:
        rmcpdata->Data_Length = 0x04;
        rmcpdata->MsgType = RMCP_SST_RES;
        *vardata     = bsp_get_sstate();
        //smbiosrmcpdataptr->sstate; //default is unknow state
        *(vardata+1) = smbiosrmcpdataptr->expired; //watchdog timeout
        memset(vardata+2, 0 , 2);
        break;

    case RMCP_CLS_REQ:
        rmcpdata->Data_Length = 0x01;
        rmcpdata->MsgType = RMCP_CLS_RES;
        *vardata = rmcp->status;
        *(vardata+1) = 0; //padding
        if (Remote_Control == 2)
            Remote_Control = 1;
        break;

    default:
        break;
    }

    len += rmcpdata->Data_Length;

    //add RSP Trailer, only when a session has been created
    if (rmcp->state == 1 && rsphdr)
    {
        if (rmcp->rmcp_data->MsgType == RMCP_CLS_REQ)
            len--;  //alignment issue

        rsptrailer = (RSPTrailer *)(tx_buf + len);
        if (rmcp->rmcp_data->MsgType == RMCP_CLS_REQ)
        {
            rsptrailer->pad_len = 0x01;
            rmcp->state = 0;
        }
        else
        {
            rsptrailer->padding = 0x0000;
            rsptrailer->pad_len = 0x02;
        }
        rsptrailer->next_header = 0x06;
        len += sizeof(RSPTrailer);
        sha1_hmac(SIK, SIK_KEY_SIZE, (u8 *)rsphdr, len-SIK_KEY_SIZE_USE, rsptrailer->integrity);
    }

    kfree(RandomSeed);
    kfree(PreData);
    ksocket_send(skt, cliaddr, tx_buf, len);
}

u8 check_rmcp_hdr(RMCPCB *rmcp)
{
    RMCPHdr *rmcphdr = rmcp->rmcp_hdr;

    if (rmcphdr->version != RMCP_HdrVersion)
        return 1;

    if (rmcphdr->seq_num  == 0xff && rmcphdr->com != RMCP_ClassofACK)
        rmcp->type = RMCP_SEND_RES;

    //update the sliding window index for version 2.0
    if (rmcphdr->com == RMCP_ClassofACK)
    {
        return 1; //bypass ACK
    }
    else if (rmcphdr->com != RMCP_ClassofMessage)
        return 1;

    return 0;
}

u8 check_rmcp_data(RMCPCB *rmcp)
{
    RMCPData *rmcpdata = rmcp->rmcp_data;
    u8 *vardata = rmcp->var_data;
    u8 sha_len;
    u8 *PreData = kzalloc(128, GFP_KERNEL);
    u8 *SHAData = kzalloc(SIK_KEY_SIZE, GFP_KERNEL);

    if (rmcp->version == 1)
    {
        if (rmcpdata->Data_Length != 0)
        {
            kfree(PreData);
            kfree(SHAData);
            return 1;
        }
    }
    else
    {
        if (rmcpdata->MsgType == RMCP_OPS_REQ)
        {
            rmcp->status = Status[0];
            if (*(vardata+4) != Payload_Type_AA || *(vardata+7) != 0x08 || *(vardata+8) != 0x01 || *(vardata+12) != Payload_Type_IA || *(vardata+15) != 0x08 || *(vardata+16) != 0x01 || *(vardata+19) != 0x00)
            {
                kfree(PreData);
                kfree(SHAData);
                return 4; //error
            }
        }
        else if (rmcpdata->MsgType == RMCP_RAKP_M1)
        {
            if (htonl(*(u32 *) vardata) != rmcp->client_sid)
                rmcp->status = Status[3];
            else if (*(vardata+20) > ASF_Administrator)
                rmcp->status = Status[10];
            else if (*(vardata+20) != ASF_Administrator && *(vardata+20) != ASF_Operator)
                rmcp->status = Status[11];
            else
                rmcp->status = Status[0];

            //keep needed field
            memcpy(rmcp->MgtRandom, (vardata+4), sizeof(rmcp->MgtRandom));
            rmcp->role = *(vardata + 20);           //User Role
            rmcp->name_length = *(vardata + 23);    //User Name Length
            memcpy(rmcp->username, (vardata+24), rmcp->name_length);
            if (rmcp->name_length > 16)
                rmcp->name_length = 16;

            if (rmcp->role == ASF_Operator)
                rmcp->key = KeyO;
            else
                rmcp->key = KeyA;
        }
        else if (rmcpdata->MsgType == RMCP_RAKP_M3)
        {
            rmcp->type   = RMCP_SEND_ACK;
            rmcp->state  = 1;
            rmcp->status = *vardata;

            sw_check(0,1); //Reset sliding window index

            if (htonl(*((u32 *) vardata + 1)) != rmcp->client_sid)
                rmcp->status = Status[3];

            memcpy(PreData, rmcp->CltRandom, sizeof(rmcp->CltRandom));
            memcpy(PreData+16, rmcp->MgtSID, sizeof(rmcp->MgtSID));
            PreData[20] = rmcp->role;
            PreData[21] = rmcp->name_length;
            memcpy(PreData+22, rmcp->username, rmcp->name_length);
            sha_len = 22 + rmcp->name_length;

            sha1_hmac(rmcp->key, SIK_KEY_SIZE, PreData, sha_len, SHAData);
            if (memcmp(rmcp->var_data+8, SHAData, SIK_KEY_SIZE_USE))
            {
                kfree(PreData);
                kfree(SHAData);
                return 1; //error
            }

            //SIK generation
            memcpy(PreData, rmcp->MgtRandom, sizeof(rmcp->MgtRandom));
            memcpy(PreData+16, rmcp->CltRandom, sizeof(rmcp->CltRandom));
            PreData[32] = rmcp->role;
            PreData[33] = rmcp->name_length;
            memcpy(PreData+34, rmcp->username, rmcp->name_length);
            sha_len = 34 + rmcp->name_length;
            sha1_hmac(KeyG, SIK_KEY_SIZE, PreData, sha_len, SHAData);
            memcpy(SIK, SHAData, sizeof(SIK));
        }
        else if (rmcp->state && ((rmcpdata->MsgType & 0x10) == 0x10))
        {
            rmcp->type = RMCP_SEND_ACK;
            if (rmcpdata->Data_Length > 0)
                memcpy(smbiosrmcpdataptr->bootopt, vardata, sizeof(smbiosrmcpdataptr->bootopt));
            else
                memset(smbiosrmcpdataptr->bootopt, 0, sizeof(smbiosrmcpdataptr->bootopt));

            smbiosrmcpdataptr->MsgType = rmcpdata->MsgType;
            if (!memcmp(vardata+4, "DPVEMP", 6))
                Remote_Control = 2;
            else
                Remote_Control = 1;
        }
    }

    kfree(PreData);
    kfree(SHAData);
    return 0;
}

void rmcpInput(RMCPCB * rmcp, unsigned char * rx_buf)
{
    rmcp->type = (RMCP_SEND_ACK | RMCP_SEND_RES);
    if (rmcp->version == 2)
        rx_buf = strip_rsp(rmcp, (RSPHdr *) rx_buf);
    if (rmcp->state && sw_check(rmcp->session_seq,0))
    {
        printk("rmcpcb->state && sw_check(rmcpcb->session_seq,0) dismatch xxx.\n");
        return;
    }
    rmcp->rmcp_hdr  = (RMCPHdr *) rx_buf;
    rmcp->rmcp_data = (RMCPData *) (rx_buf + sizeof(RMCPHdr));
    rmcp->var_data  = (u8 *) rmcp->rmcp_data + sizeof(RMCPData);

    if (check_rmcp_hdr(rmcp))
    {
        return ;
    }

    if (check_rmcp_data(rmcp))
    {
        return ;
    }

    if ((rmcp->type & RMCP_SEND_ACK) == RMCP_SEND_ACK)
    {
        rmcpackSend(rmcp, rx_buf);
    }

    if ((rmcp->type & RMCP_SEND_RES) == RMCP_SEND_RES)
    {
        RMCP_Response(rmcp, rx_buf);
    }

    //do the remote control at the last to prevent sending packet fail
    //during the loop back
    if (Remote_Control == 1)
    {
        Remote_Control = 0;
        //ioctl(smbus_fd, SMMSEND, &smbiosrmcpdataptr->MsgType);
        SMBus_Prepare_RmtCtrl(smbiosrmcpdataptr->MsgType, 0);
    }
}

static DEFINE_MUTEX(nobkl);
static void ksocket_start(int id)
{
    RMCPCB * rmcpcb=NULL;
    int clientlen;
    int rx_len;
    unsigned char rx_buf[1024];
    struct socket_wq *wq;
    struct socket *skt;
    struct sockaddr_in* addr;
    DECLARE_WAITQUEUE(wait,current);

    clientlen = sizeof(cliaddrv2);
    skt = (id==1)? asfv1_socket: asfv2_socket;
    addr = (id==1)? &cliaddrv1: &cliaddrv2;

    mutex_lock(&nobkl);
    {
        current->flags |= PF_NOFREEZE;
        allow_signal(SIGKILL|SIGSTOP);
    }
    mutex_unlock(&nobkl);
    if (rmcpcb == NULL)
    {
        rmcpcb = (RMCPCB *)kzalloc(sizeof(RMCPCB), GFP_KERNEL);
        //memset(rmcpcb, 0, sizeof(RMCPCB));
        rmcpcb->client_sid = 0x01;
    }
    mutex_lock(&nobkl);
    rmcpcb->version = id;
    mutex_unlock(&nobkl);
    while (1)
    {
        mutex_lock(&nobkl);
        memset(rx_buf, 0, sizeof(rx_buf));
        rx_len = ksocket_receive(skt, addr, rx_buf, sizeof(rx_buf));
        dpconf->ConsoleIP[0].addr = addr->sin_addr.s_addr;
        if (rx_len > 0)
        {
            rx_buf[rx_len] = 0;
            rmcpInput(rmcpcb, rx_buf);
        }
        mutex_unlock(&nobkl);
        wq = rcu_dereference(skt->sk->sk_wq);
        add_wait_queue(&wq->wait, &wait);
        __set_current_state(TASK_INTERRUPTIBLE);
        schedule_timeout(1*HZ/100);
        __set_current_state(TASK_RUNNING);
        remove_wait_queue(&wq->wait, &wait);
    }
    kfree(rmcpcb);
}

static int __init asf_init(void)
{
    struct sockaddr_in sin;
    int error;
    int port = 0;
    int i;
    unsigned char name[12];
    for (i=0; i<2; i++)
    {
        if (i==0)
        {
            port = RMCP_ASFV1_UDP_PORT;
            memcpy(name, "asfv1_udp", sizeof(name));
        }
        else
        {
            port = RMCP_ASFV2_UDP_PORT;
            memcpy(name, "asfv2_udp", sizeof(name));
        }
        if (i==0)
            error = sock_create_kern(&init_net, PF_INET, SOCK_DGRAM, IPPROTO_UDP, &asfv1_socket);
        else
            error = sock_create_kern(&init_net, PF_INET, SOCK_DGRAM, IPPROTO_UDP, &asfv2_socket);
        if (unlikely(error<0))
        {
            printk(KERN_ERR "cannot create asfv1 socket");
            return -1;
        }

        sin.sin_addr.s_addr = htonl(INADDR_ANY);
        sin.sin_family = AF_INET;
        sin.sin_port = htons(port);

        if (i==0)
            error = kernel_bind(asfv1_socket, (struct sockaddr *)&sin, sizeof(sin));
        else
            error = kernel_bind(asfv2_socket, (struct sockaddr *)&sin, sizeof(sin));
        if (unlikely(error<0))
        {
            printk(KERN_ERR "cannot bind socket, error code: %d.\n", error);
            return -1;
        }
        kthread_run((void *)ksocket_start, i+1, name);
    }

    return 0;
}
late_initcall(asf_init);

