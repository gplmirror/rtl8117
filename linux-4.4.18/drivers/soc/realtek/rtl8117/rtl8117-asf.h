#ifndef __RTL8117_ASF__
#define __RTL8117_ASF__

#define WindowSIZE             0x20
//RMCP Header
#define RMCP_V1_PORT             0x26f
#define RMCP_V2_PORT             0x298
#define RMCP_HdrVersion          0x06
#define RMCP_HdrRsvd             0x00
#define RMCP_ClassofMessage      0x06
#define RMCP_ClassofACK          0x86


//RMCP Data
#define RMCP_DatRsvd      0x00
#define Payload_Type_AA   0x01
#define Payload_Type_IA   0x02

//role
#define ASF_Administrator 0x01
#define ASF_Operator      0x00

#define SIK_KEY_SIZE      0x14
#define SIK_KEY_SIZE_USE  0x0C

#define RMCP_OFFSET       (ETH_HDR_SIZE + IP_HLEN + UDP_HDR_SIZE)

//dash profile
#define DASH_PWR_On    "2"
#define DASH_Sleep     "4"
#define DASH_Reset     "5"
#define DASH_Hibernate "7"
#define DASH_Shutdown  "8"
#define DASH_PWR_CR    "9"

//in-band remote control
#define IB_Reset     0x01
#define IB_Sleep     0x02
#define IB_Hibernate 0x03
#define IB_Shutdown  0x04

//RMCP Message Type
#define RMCP_Reset   0x10
#define RMCP_PWR_On  0x11
#define RMCP_PWR_Off 0x12
#define RMCP_PWR_CR  0x13
#define RMCP_HIBER   0x14
#define RMCP_STANDBY 0x15

#define RMCP_TEST    0x20
#define RMCP_PONG    0x40
#define RMCP_CAP_RES 0x41
#define RMCP_SST_RES 0x42
#define RMCP_OPS_RES 0x43
#define RMCP_CLS_RES 0x44
#define RMCP_PING    0x80
#define RMCP_CAP_REQ 0x81
#define RMCP_SST_REQ 0x82
#define RMCP_OPS_REQ 0x83
#define RMCP_CLS_REQ 0x84
#define RMCP_RAKP_M1 0xC0
#define RMCP_RAKP_M2 0xC1
#define RMCP_RAKP_M3 0xC2
//remote control 0310

#define RMCP_SEND_NONE    0x00
#define RMCP_SEND_ACK     0x01
#define RMCP_SEND_RES     0x10

//structure for rmcp state, rmcp control block
typedef struct
{
    u32 session_id;  //session id
    u32 seq_num;  //sequence number
} RSPHdr;

typedef struct
{
    u16 padding;
    u8  pad_len;
    u8  next_header;
    u8  integrity[SIK_KEY_SIZE_USE];
} RSPTrailer;

typedef struct
{
    u8 version;
    u8 reserved;
    u8 seq_num;
    u8 com; //class of message
} RMCPHdr;

typedef struct
{
    u8  IANA[4];
    u8  MsgType;
    u8  MsgTag;
    u8  rsvd;
    u8  Data_Length;
} RMCPData;

typedef struct
{
    u8    version;
    u8    state;
    u8    type;
    u8    seq_num;
    u8    status;
    u8    role;
    u8    name_length;
    u8    rsvd;
    u32   session_seq;
    u32   client_sid;
    RSPHdr   *rsp_hdr;
    RMCPHdr  *rmcp_hdr;
    RMCPData *rmcp_data;
    u8    *var_data;
    u8    *key;
    u8    MgtSID[4];
    u8    CltRandom[16];
    u8    MgtRandom[16];
    u8    username[16];
} RMCPCB;
//#include "rtl_tls.h"

//extern long read_rand(void);

//int asf_init(void);
#endif
