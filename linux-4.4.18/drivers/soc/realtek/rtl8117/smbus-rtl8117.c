#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/module.h>
#include <linux/suspend.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <linux/interrupt.h>
#include <linux/pci.h>
#include <linux/io.h>
#include <linux/list.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <linux/timekeeping.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include "smbus-rtl8117.h"

const unsigned char RTKUDID[18] = { 0x11, 0x41, 0x08, 0x10, 0xEC, 0x81, 0x68, 0x00, 0x04, \
                                    0x10, 0xEC, 0x81, 0x68, 0x00, 0x00, 0x00, 0x00, 0xB1
                                  };
const asf_ctldata_t asf_ctldata[4] = {
    {0x00, 0x88, 0x00, 0x03}, {0x01, 0x88, 0x00, 0x02},
    {0x02, 0x88, 0x00, 0x01}, {0x03, 0x88, 0x00, 0x04}
};

#define CONF_FLASH_ADDR 0xA2020000
pldm_res_t *pldmresponse[7];
pldm_t *pldmdataptr;
DPCONF * dpconf;
asf_config_t *asfconfig;
smbiosrmcpdata *smbiosrmcpdataptr;
static unsigned int SMBRxDescIndex = 0;
static unsigned int remote_control= 0;
struct smbus_rtl8117 *sp;
struct sock *nl_sk = NULL;
struct nlmsghdr *nlh=NULL;
unsigned int pid = 0;
//unsigned char bootopt[11];
unsigned int IC_VERSION = 0;
#define NETLINK_SMBUS 30

unsigned char master_check(void)
{
    unsigned char loop_count = 0;
    unsigned char ret = 0;

    //0 means idle
    //while (((REG8(SMBUS_BASE_ADDR+SEIO_Status)&0x07)) && loop_count < 100)
    while ((rtl_readb(sp->base + SEIO_Status)&0x07) && (loop_count < 100))
    {
        udelay(1);
        loop_count++;
    }

    if (loop_count == 100)
        ret = 1;
    return ret;
}

unsigned char master_smbus_send(struct master_smbus_parameters *param)
{
    //no need to copy from bufaddr, since bufaddr is the smbtx buf addr
    unsigned char status, ret;
    unsigned char count = 0;
    smbrxdesc_t *txmdesc = (smbrxdesc_t *)sp->txmdesc;

    status = smbus_read_isr();
    while ((status & 0x02))
    {
        udelay(1);
        if (count++ > 100) {
            return 1;
        }
    }

    ret = master_check();
    if (ret == 0)
    {
        if((param->msg_type == PLDM_RESPONSE) && !dpconf->pldmfromasf)
        {
            if(dpconf->pldmpec)
                rtl_writeb(rtl_readb(sp->base + SEIO_SMEn)|0x02, sp->base + SEIO_SMEn);
            else
                rtl_writeb(rtl_readb(sp->base + SEIO_SMEn)&0xFD, sp->base + SEIO_SMEn);
        }
        //using global pec setting
        else
        {
            if(asfconfig->pec)
                rtl_writeb(rtl_readb(sp->base + SEIO_SMEn)|0x02, sp->base + SEIO_SMEn);
            else
                rtl_writeb(rtl_readb(sp->base + SEIO_SMEn)&0xFD, sp->base + SEIO_SMEn);
        }

        //check again the status to avoid slave send happened at the same time
        if (!(rtl_readb(sp->base + SEIO_Status)&0x07))
        {
            if (param->msg_type == BMC_POLL)
                bsp_bits_set(MAC_BIOSREG, 1, BIT_SYSSH, 1);
            //if (param->msg_type == BMC_POLL && (bsp_get_sstate() != S_S0 || param->poll_type == POLL_STOP) )
            if (param->msg_type == BMC_POLL && (bsp_get_sstate() != S_S0))
            {
                bsp_bits_set(MAC_BIOSREG, 0, BIT_SYSSH, 1);
                ret = 1;
            }
            else
            {
                txmdesc->buf_addr = (u8 *)cpu_to_le32(dma_map_single(sp->dev, param->bufaddr, param->length, DMA_TO_DEVICE));
                txmdesc->opts = cpu_to_le32(param->length + 0x80000000);
                rtl_writeb(param->cmd, sp->base + SEIO_ModeCmd);
                rtl_writeb(0x10, sp->base + SEIO_SMPollEn);
                dma_unmap_single(sp->dev, (dma_addr_t)txmdesc->buf_addr, param->length, DMA_TO_DEVICE);
            }
        }
    }
    return ret;
}

void slave_smbus_send(unsigned char *bufaddr, unsigned char length)
{
    //no need to copy from bufaddr, since bufaddr is the smbtx buf addr
    smbtxdesc_t *txdesc = sp->txdesc;
    //unsigned char *buf=NULL;
    if (bufaddr != NULL)
    {
        txdesc->tx_buf_addr = (u8 *)cpu_to_le32(dma_map_single(sp->dev, bufaddr, length, DMA_TO_DEVICE));
    }
    else
    {
        txdesc->tx_buf_addr = (u8 *)cpu_to_le32(sp->smbtxbuf_map);
    }
    //buf = txdesc->tx_buf_addr;
    txdesc->st.own = cpu_to_le32(1);
    txdesc->st.length = cpu_to_le32(length);
    rtl_writeb(0x01, sp->base + SEIO_SMPollEn);
    dma_unmap_single(sp->dev, (dma_addr_t)txdesc->tx_buf_addr, length, DMA_TO_DEVICE);
    //return buf;
}

void SMBus_Prepare_BootOptions(void)
{
    //smbtxdesc_t *txdesc = sp->txdesc;
    u32 *tmp;
    u8 * bptr = kzalloc(0x10 , GFP_KERNEL);
    bptr = (u8 *)(((uint32_t)bptr+3)&(~3));
    tmp = bptr;
    if (*(smbiosrmcpdataptr->bootopt+2) == 0x11)
    {
        *tmp = 0x00101602 + sizeof(smbiosrmcpdataptr->bootopt);
        //*bptr++ = sizeof(smbiosrmcpdataptr->bootopt) + 2;
        //*bptr++ = 0x16; //subcmd-return boot option
    }
    else
    {
        *tmp = 0x00101702;
        //*bptr++ = 0x02;
        //*bptr++ = 0x17; //subcmd-no return boot option
    }
    u8 * t = bptr;

    //*bptr++ = 0x10; //VersionNumner
    if (*(smbiosrmcpdataptr->bootopt+2) == 0x11)
    {
        memcpy(bptr, smbiosrmcpdataptr->bootopt, sizeof(smbiosrmcpdataptr->bootopt));
        slave_smbus_send(bptr, sizeof(smbiosrmcpdataptr->bootopt)+3);
    }
    else
    {
        slave_smbus_send(bptr, 3);
    }

    kfree(bptr);
}

void handle_pldm_br_ami(unsigned char *addr, unsigned int len)
{
    SMBIOS_Table_EP *smbiosmeta ;
    if ((len >=11) && addr[1] == 0x0f && addr[10] == 0x03 &&
            (addr[7] & 0x80) == 0x80 && addr[4] == 0x01)//add one item for fujitsu PLDM @20140515
    {
        switch (addr[11])
        {
        case GetBIOSTableTags:
            pldmdataptr->pldmerror = 0;
            if (pldmdataptr->valid)
            {
                pldmdataptr->index = 5;
                memcpy(&pldmresponse[pldmdataptr->index]->val[0], &pldmdataptr->tag[0], 3*sizeof(pldmdataptr->tag[0]));
            }
            else
            {
                pldmdataptr->index = 4;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_BIOS_TABLE_TAG_UNAVAILABLE;
                pldmresponse[pldmdataptr->index]->length = 0x0A;
            }
            break;
        case GetBIOSTable:
            if (addr[17] == 0x03 && pldmdataptr->dirty)
            {
                pldmdataptr->index = 0;
                //pldmdataptr->dirty = 0;
            }
            else
            {
                pldmdataptr->index = 4;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_BIOS_TABLE_UNAVAILABLE;
                pldmresponse[pldmdataptr->index]->length = 0x0A;
                //may only end here
                smbiosrmcpdataptr->PollType = POLL_NONE;
            }
            break;
        case AcceptBIOSAttributesPendingValues:
            pldmdataptr->dirty = 0;
        default:
            //default length
            pldmdataptr->index = 4;
            pldmresponse[pldmdataptr->index]->length = 0x0A;
            pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
            break;
        }
        pldmresponse[pldmdataptr->index]->pldmtype = addr[10];
        pldmresponse[pldmdataptr->index]->pldmcmd = addr[11];
    }
    //only AMI implements DSP0246
    else if ((len >=11) && addr[1] == 0x0f && addr[10] == 0x01 &&
             (addr[7] & 0x80) == 0x80 && addr[4] == 0x01)//add one item for fujitsu PLDM @20140515
    {
        switch (addr[11])
        {
        case GetSMBIOSStructureTableMetadata:
            pldmdataptr->pldmerror = 0;
            pldmdataptr->index = 5;
            smbiosmeta = (SMBIOS_Table_EP *)(CONF_FLASH_ADDR + 0x6000 + 0x10);
            memcpy(&pldmresponse[pldmdataptr->index]->val[0], &smbiosmeta->majorVer , 4);
            memcpy(&pldmresponse[pldmdataptr->index]->val[4], &smbiosmeta->StTableLen , 2);
            memcpy(&pldmresponse[pldmdataptr->index]->val[6], &smbiosmeta->StNum , 2);
            memcpy(&pldmresponse[pldmdataptr->index]->val[8], smbiosmeta , 4);
            break;
        case SetSMBIOSStructureTable:
            //smbiosptr = (unsigned char *) (SMBIOS_ROM_START_ADDR + SMBIOS_DATA_OFFSET);
            //memcpy(smbiosptr, flash_buf + 0xB000+ SMBIOS_DATA_OFFSET, 4096);
            pldmdataptr->index = 4;
            pldmresponse[pldmdataptr->index]->length = 0x0A;
            pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
            break;
        case SetSMBIOSStructureTableMetadata:
            pldmdataptr->index = 4;
            pldmresponse[pldmdataptr->index]->length = 0x0A;
            pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;

            //dirty[SMBIOSTBL].length = *(unsigned short *) (addr+16) + SMBIOS_DATA_OFFSET + SMBIOS_HEADER_SIZE;
            //memset(timestampdataptr + dirty[SMBIOSTBL].length, 0 , MAX_SMBIOS_SIZE-dirty[SMBIOSTBL].length);
            break;
        default:
            //default length
            pldmdataptr->index = 4;
            pldmresponse[pldmdataptr->index]->length = 0x0A;
            pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
            break;
        }
        pldmresponse[pldmdataptr->index]->pldmtype = addr[10];
        pldmresponse[pldmdataptr->index]->pldmcmd = addr[11];
    }
}

void SMBus_Get_UDID(unsigned char *addr)
{
    memcpy(addr, RTKUDID, 18);
    if(IC_VERSION > 1)
        addr[2] += 1;
    if (IC_VERSION > 4)
        addr += 1;
    addr[17] =  dpconf->arpaddr + 1;
    slave_smbus_send(addr, addr[0]+3);
}

void smbus_arp(unsigned char *addr)
{
    static unsigned char arpindex = 0;
    unsigned char *bptr;
    bptr = kzalloc(0x80, GFP_KERNEL);
    bptr = (u8 *)(((uint32_t)bptr+3)&(~3));

    //addr[1] is smbus comand
    if (addr[2] == 0xC3 && addr[1] == 0x03)
        SMBus_Get_UDID(bptr);
    else if (addr[1] == 0x01) //prepare arp
        arpindex = 0;
    else if (addr[1] == 0x04)
    {
        dpconf->arpaddr = addr[19];
        rtl_writeb(addr[19], SMBUS_BASE_ADDR + SEIO_SlaveAddr + arpindex++);
    }

    //assing value for easily handling in TASK
    addr[0] = dpconf->arpaddr;
    addr[2] = dpconf->arpaddr + 1;

    kfree(bptr);
}

void smbus_init(void);
static irqreturn_t rtk_smbus_interrupt(int irq, void *dev_instance)
{
    int handled = 0;
    unsigned char status;
    unsigned char v2;
    unsigned char *addr;
    unsigned char len;
    struct device *dev = dev_instance;
    struct smbus_rtl8117 *sp = dev_get_drvdata(dev);
    int res=0;
    struct sk_buff *skb;
    struct nlmsghdr *nlh_new=NULL;
    smbrxdesc_t *smbrxdesc = sp->rxdesc + SMBRxDescIndex;

    smbus_disable_irq();
    status = smbus_read_isr();
    smbus_clear_isr(status);
    v2 = rtl_readb(sp->base + 0x01);
    if(status & 0x10)
    {
        pr_err("ERROR, RDU!\n");
        rtl_writeb(0x00, sp->base + SEIO_SMEn);
        rtl_writeb(0x00, sp->base + SEIO_Status);
        smbus_init();
        rtl_writeb(0x10, sp->base + SEIO_Status);
    }
    while (!(smbrxdesc->opts & 0x80000000))
    {
        len = smbrxdesc->opts & 0x0000007F;
        addr = (u8 *)smbrxdesc->buf_addr + 0x80000000;
        if (addr[0] == 0xC2)
            smbus_arp(addr);
        if ((len == 3) && (addr[1] == 0x03)&& (addr[0]==dpconf->arpaddr) && (addr[2]==dpconf->arpaddr+1))
            SMBus_Prepare_BootOptions();

        else if ((len == 3) && (addr[1] == 0x0f) && (addr[0]==dpconf->arpaddr) && (addr[2]==dpconf->arpaddr+1))
        {
            if(pldmdataptr->index > 5)
                pldmdataptr->index = 4;
            slave_smbus_send(&pldmresponse[pldmdataptr->index]->length, pldmresponse[pldmdataptr->index]->length + 1);
            if(!(v2 & 0x20))
                pldmdataptr->index++;
        }
        else if (nl_sk && pid)
        {

            if ((addr[0]==dpconf->arpaddr) && (addr[2]!= dpconf->arpaddr+1))
                handle_pldm_br_ami(addr, len);
            skb = nlmsg_new(len, 0);
            if (!skb)
                printk(KERN_ERR "net_link: allocate failed, len is %d.\n", len);
            nlh_new = nlmsg_put(skb,0,0,NLMSG_DONE,len,0);
            if (!nlh_new)
                printk(KERN_ERR "nlh is NULL!\n\n");
            NETLINK_CB(skb).portid = 0; /* from kernel */
            NETLINK_CB(skb).dst_group = 0; /* not in mcast group */
            memcpy(NLMSG_DATA(nlh_new), addr, len);
            nlh_new->nlmsg_len = len;
            res = netlink_unicast(nl_sk, skb, pid, MSG_DONTWAIT);
            if (res < 0)
                printk(KERN_ERR "Error(%d) while smbus send back to user.\n", res);
        }

        smbrxdesc->opts = cpu_to_le32(0x80000080 + (SMBRxDescIndex<<7));
        if (SMBRxDescIndex == sp->smbrxdescnum-1)
            smbrxdesc->opts = cpu_to_le32(smbrxdesc->opts | 0x8000);
        SMBRxDescIndex = (SMBRxDescIndex + 1)% (sp->smbrxdescnum);
        smbrxdesc = sp->rxdesc + SMBRxDescIndex;
    }

    handled = 1;
    smbus_enable_irq();
    return IRQ_RETVAL(handled);
}

int bsp_IODrvAP_exist(void)
{
    if (!bsp_bits_get(MAC_IBREG, BIT_APRDY, 1))
        return 0;
    else
        return 1;
}

unsigned char SMBus_Prepare_RmtCtrl(unsigned char MsgType, unsigned char force)
{
    unsigned char *bptr = NULL;
    unsigned char ret = 0;
    static unsigned char enter = 0;
    unsigned char state;
    struct master_smbus_parameters *new_pa;
    //prevent re-entry, possibly caused by OSTimeDly
    if (enter)
        return 2;
    enter = 1;

    state = bsp_get_sstate();

    if (dpconf->vendor == 0)
    {
        if ((MsgType == RMCP_PWR_Off || MsgType == RMCP_Reset || MsgType == RMCP_PWR_CR) && (state == S_S3 || state == S_S4 || state == S_S5))
            return 2;

        else if (MsgType == RMCP_PWR_On && state == S_S0)
            return 2;

        else if (state == S_UNKNOWN)
            return 2;
    }

    bptr = kzalloc(3+4, GFP_KERNEL);
    bptr = (u8 *)(((uint32_t)bptr+3)&(~3));
    //in band only exists in S0
    //graceful shutdown through client tool
    if (bsp_IODrvAP_exist() && !force)
    {
        //printk("IB Exist\n");
        //printk("0x11 is %x\n", rtl_readb(0xBAF70000 + 0x11));
        if (MsgType == RMCP_PWR_Off)
            bsp_bits_set(MAC_SYNC1, IB_Shutdown, BIT_RMTCTL, 3);
        else if (MsgType == RMCP_HIBER) //hibernate
            bsp_bits_set(MAC_SYNC1, IB_Hibernate, BIT_RMTCTL, 3);
        else if (MsgType == RMCP_STANDBY)
            bsp_bits_set(MAC_SYNC1, IB_Sleep, BIT_RMTCTL, 3);
        else
            bsp_bits_set(MAC_SYNC1, IB_Reset, BIT_RMTCTL, 3);

        //timeout is 10 secs
        //OSTimeDly(10*OS_TICKS_PER_SEC);
        smbiosrmcpdataptr->IBRmtl = 1;
        smbiosrmcpdataptr->IBRmtlCmd = MsgType;
        enter = 0;
        return ret;

    }

    //in-band does not exist, but issue a hibernate or standby
    switch(MsgType)
    {
    case RMCP_Reset:
        MsgType = ASF_RESET;
        break;

    case RMCP_PWR_On:
        MsgType = ASF_POWER_ON;
        break;

    case RMCP_PWR_Off:
    case RMCP_HIBER:
    case RMCP_STANDBY:
        MsgType = ASF_POWER_OFF;
        break;

    case RMCP_PWR_CR:
        MsgType = ASF_POWER_CR;
        break;
    }

    memcpy(bptr, &asf_ctldata[MsgType].slaveaddr, 3);
    new_pa = kzalloc(sizeof(struct master_smbus_parameters), GFP_KERNEL);
    new_pa->length = 3;
    new_pa->cmd = 0x04;
    new_pa->bufaddr = bptr;
    new_pa->msg_type = REMOTE_CONTROL;
    ret = master_smbus_send(new_pa);
    //alwyas clear the bits used to inform IB
    bsp_bits_set(MAC_SYNC1, 0x0, BIT_RMTCTL, 3);
    enter = 0;
    if (ret) ret = 2;
    return ret;
}

static void rtk_nl_recv_msg(struct sk_buff *skb)
{
    nlh = (struct nlmsghdr *)skb->data;
    pid = nlh->nlmsg_pid;
}

void kernel_mmap_user(void)
{
    int i = 0;
    //u8 * virt_addr;
    uint32_t page_addr = 0x80040000;
    unsigned char * timestampdataptr = NULL;

    memset((u8 *)page_addr, 0x0, PAGE_SIZE);
    //virt_addr = (u8 *)page_addr;
    dpconf = (DPCONF *)page_addr;
#ifdef CONFIG_BLOCK_READ
    pldmresponse[0] = (pldm_res_t *)((uint32_t)page_addr + 2 + 0x100);
#else
    pldmresponse[0] = (pldm_res_t *)((uint32_t)page_addr+ 0x100);
#endif
    pldmresponse[1] = (pldm_res_t *)(pldmresponse[0] + 1);
    pldmresponse[2] = (pldm_res_t *)(pldmresponse[0] + 2);
#ifdef CONFIG_BLOCK_READ
    pldmresponse[3] = (pldm_res_t *)((uint32_t)page_addr + 0x300 + 2+ 0x100);
#else
    pldmresponse[3] = (pldm_res_t *)((uint32_t)page_addr + 0x300+ 0x100);
#endif
    pldmresponse[4] = (pldm_res_t *)(pldmresponse[3] + 1);
    pldmresponse[5] = (pldm_res_t *)(pldmresponse[3] + 2);
    pldmresponse[6] = (pldm_res_t *)(pldmresponse[3] + 3);
    pldmdataptr = (pldm_t *)((uint32_t)page_addr + 0x800);

    asfconfig = (asf_config_t *)((uint32_t)page_addr + 0xb00);
    memcpy(asfconfig->asf_rctl, asf_ctldata, sizeof(asf_ctldata));
    for(i=0; i < 4; i++)
        asfconfig->asf_rctl[i].slaveaddr &= 0xFE;
    smbiosrmcpdataptr = (smbiosrmcpdata *)((uint32_t)page_addr + 0xc00);
    timestampdataptr = (unsigned char *)((uint32_t)page_addr + 0xe00);
    sp->mmap_page_addr = page_addr;
}

static long smbusdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg_)
{
    void __user *arg = (void __user *)arg_;
    //int __user *p = arg;
    //struct slave_smbus_info *sei = kzalloc(sizeof(struct slave_smbus_info), GFP_KERNEL);
    //struct smbus_arp_info *sai = NULL;
    //struct master_smbus_parameters *param = NULL;
    unsigned char state, acpi;
    //unsigned char *buf = NULL;
    //int i = 0;
    uint32_t page_addr;
    switch (cmd) {
    case SMBOFF1:
        state = rtl_readb(SMBUS_BASE_ADDR + 0x01);
        copy_to_user(arg, &state, 1);
        break;
    case SMBSMEN:
        copy_from_user(&state, arg, 1);
        rtl_writeb(state, SMBUS_BASE_ADDR + SEIO_SMEn);
        break;
    case SMBSSTATE:
        bsp_bits_set(MAC_ACPI, arg_, BIT_ACPI, 8);
        break;
    case SMBGSTATE:
        if(bsp_bits_get(MAC_MAC_STATUS, BIT_ISOLATE, 1))
            state = S_S0;
        else
            state = S_S5;
        acpi = bsp_bits_get(MAC_ACPI, BIT_ACPI, 8);
        //isolate will reflect the S5 only
        //if console tool or others change the state to S3 or S4
        //the state chould be S3 or S4 instead of S5
        if(state == S_S5 && (acpi == S_S3 || acpi == S_S4))
            state = acpi;
        copy_to_user(arg, &state, 1);
        break;
    case SMMSEND:
        copy_from_user(&state, arg, 1);
        SMBus_Prepare_RmtCtrl(state, 0);
        break;
    case CLAERBOOTOPT:
        memset(smbiosrmcpdataptr->bootopt, 0, sizeof(smbiosrmcpdataptr->bootopt));
        break;
    case MMAP:
        page_addr = VA2PA(sp->mmap_page_addr);
        copy_to_user(arg, &page_addr, 4);
        break;
    case INBAND_CHECK:
        state = bsp_bits_get(MAC_IBREG, BIT_DRIVERRDY, 1);
        if (state)
            state = 1;
        copy_to_user(arg, &state, 1);
        break;
    case IODRVAP_EXIST:
        state = bsp_bits_get(MAC_IBREG, BIT_APRDY, 1);
        if (state)
            state = 1;
        copy_to_user(arg, &state, 1);
        break;
    case SINGLE_IP:
        state = bsp_bits_get(MAC_OOBREG, BIT_SINGLE_IP, 1);
        copy_to_user(arg, &state, 1);
        break;
    case TEST:
        //printk("dpconf->UUID is 0x%x-0x%x-0x%x-0x%x\n", dpconf->UUID[0], dpconf->UUID[1], dpconf->UUID[2], dpconf->UUID[3]);
    default:
        return -EINVAL;
    }
    return 0;
}

static const struct file_operations smbusdev_fops = {
    .owner = THIS_MODULE,
    .open = simple_open,
    .unlocked_ioctl = smbusdev_ioctl,
#ifdef CONFIG_COMPAT
    //.compat_ioctl = smbusdev_compat_ioctl,
#endif /* CONFIG_COMPAT */
};

static struct miscdevice smbusdev = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "smbusdev",
    .fops = &smbusdev_fops,
    .mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH,
};

static int __init smbusdev_register(void)
{
    int rc;

    rc = misc_register(&smbusdev);
    if (unlikely(rc)) {
        pr_err("registration of /dev/smbusdev failed\n");
        return rc;
    }

    return 0;
}

void smbus_init(void)
{
    u8 freq_div = 0x01, freq_slave_div=0x06;
    SMBRxDescIndex = 0;

    rtl_writeb(0xCD, sp->base + SEIO_SMEn);
    rtl_writeb(0xC0, sp->base + SEIO_SMPollEn);
    rtl_writel(0x08186a00, sp->base + SEIO_SCTimeOut);
    rtl_writel(0xc8c8c8c8, sp->base + SEIO_SlaveAddr);
    rtl_writew((0x03E8 / freq_div), sp->base + SEIO_BusFree);
    rtl_writew((0x03E8 / freq_div), sp->base + SEIO_HoldTimeRStart_1);
    rtl_writew((0x03E8 / freq_div), sp->base + SEIO_RS_SetupTime_1);
    rtl_writew((0x03E8 / freq_div), sp->base + SEIO_StopSetupTime_1);
    rtl_writew((0x03E / freq_div), sp->base + SEIO_DataHoldTime);
    rtl_writew((0x0768 / freq_div), sp->base + SEIO_MasterClkLow);
    rtl_writew((0x0768 / freq_div), sp->base + SEIO_MasterClkHigh);
    rtl_writew((0x03B4 / freq_div), sp->base + SEIO_MasterRBControl);
    rtl_writew((0x03B4 / freq_div), sp->base + SEIO_MasterTDControl);
    rtl_writew((0x03B4 / freq_slave_div), sp->base + SEIO_SlaveTimingCtrl);
    rtl_writew((0x003F / freq_slave_div), sp->base + SEIO_SlaveLDTimingCtrl);
    rtl_writew((0x0030 / freq_slave_div), sp->base + SEIO_SlaveSMBCFallTime);
    rtl_writew((0x0090 / freq_slave_div), sp->base + SEIO_SlaveSMBCRiseTime);
    rtl_writew((0x0030 / freq_slave_div), sp->base + SEIO_SlaveSMBDFallTime);
    rtl_writew((0x0090 / freq_slave_div), sp->base + SEIO_SlaveSMBDRiseTime);
    rtl_writew((0x00FE / freq_div), sp->base + SEIO_HoldTimeRStart_2);
    rtl_writew((0x00FE / freq_div), sp->base + SEIO_RS_SetupTime_2);
    rtl_writew((0x00FE / freq_div), sp->base + SEIO_StopSetupTime_2);
    rtl_writeb(0x02, sp->base + SEIO_SMBCFallTime);
    rtl_writeb(0x02, sp->base + SEIO_SMBCRiseTime);
    rtl_writeb(0x02, sp->base + SEIO_SMBDFallTime);
    rtl_writeb(0x02, sp->base + SEIO_SMBDRiseTime);
}

static ssize_t rtl8117_smbus_rc_read(struct file *file, char __user *buf, size_t count, loff_t *ppos)
{
    ssize_t len = 0;
    char str[128];
    unsigned char val = remote_control;

    len = sprintf(str, "%x\n", val);
    copy_to_user(buf, str, len);
    if (*ppos == 0)
        *ppos += len;
    else
        len = 0;
    return len;
}

static ssize_t rtl8117_smbus_rc_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos)
{
    char tmp[32];
    int num;
    unsigned int val;
    if (buf && !copy_from_user(tmp, buf, count)) {
        num = sscanf(tmp, "%x", &val);
    }
    remote_control = val;
    SMBus_Prepare_RmtCtrl(val, 0);
    return count;
}

static int rtl8117_smbus_rc_open(struct inode *inode, struct file *file)
{
    return single_open(file, NULL, PDE_DATA(inode));
}

unsigned int ReadIBReg(unsigned short reg)
{
    rtl_writel((0x800f0000 | reg), MAC_BASE_ADDR + MAC_IB_ACC_SET);
    while(rtl_readl(MAC_BASE_ADDR + MAC_IB_ACC_SET) & 0x80000000)
        ;
    return rtl_readl(MAC_BASE_ADDR + MAC_IB_ACC_DATA);
}

void SetMacAddr(unsigned char *addr)
{
    unsigned int v = 0;

    memcpy(&v, addr, 4);
    rtl_writel(v, MAC_BASE_ADDR + MAC_IDR0);
    memcpy(&v, addr + 4, 4);
    rtl_writel(v & 0x0000FFFF, MAC_BASE_ADDR + MAC_IDR4);
}


void get_mac_addr(void)
{
    unsigned int macaddrval;
    macaddrval = ReadIBReg(0xC000);
    memcpy(dpconf->srcMacAddr[eth0], &macaddrval, 4);
    macaddrval = ReadIBReg(0xC004);
    memcpy(dpconf->srcMacAddr[eth0]+4, &macaddrval, 2);
    SetMacAddr(dpconf->srcMacAddr[eth0]);
}

static const struct file_operations smbus_rc_fops =
{
    .owner = THIS_MODULE,
    .open = rtl8117_smbus_rc_open,
    .read = rtl8117_smbus_rc_read,
    .write = rtl8117_smbus_rc_write,
    .release    = single_release,
};
#if 1
DPCONF dpdefault = {

    //default efuse aes key encrypted (0x01 0x23...)
    .admin = {{0x31, 0x97, 0x6F, 0x6E, 0x55, 0x9D, 0x34, 0x4C, 0xCE, 0xFB, 0xFA, 0x4E, 0x90, 0x5C, 0xF2, 0xC1}
        ,{0x62, 0x60, 0x0E, 0x7C, 0x06, 0x8B, 0x2D, 0x86, 0x8C, 0xCA, 0x2E, 0xBE, 0x80, 0x65, 0x03, 0x3F}
        ,0xA0, 0x5B, 0xA034
        ,{0xA9, 0x67, 0xB2, 0xEC, 0x57, 0xAE, 0x7B, 0x17, 0x56, 0x52, 0x7A, 0x61, 0x88, 0x02, 0x33, 0xE0, 0xFB, 0x4D, 0x01, 0x97, 0x7B, 0xDf, 0x3C}
        , 0xDD, (struct _UserInfo *)0x95ECB8A8
    },
    .HostIP[eth0].addr = 0x0A00A8C0,
    .SubnetMask[eth0].addr = 0x00FFFFFF,
    .GateWayIP[eth0].addr = 0xFE00A8C0,
    .DNSIP[eth0].addr = 0x0100A8C0,
    .ConsoleIP[eth0].addr = 0x0300A8C0,
    .asfon = 0,
    .lspoll = 0,
    .asfpoll = 0,
    .DHCPv4Enable = 0,//1<<eth0,
    .EchoService = 1,
    .httpService = 1,
    .httpsService = 1,
    .wsmanService = 1,
    //.ProvisioningState = PROVISIONED,
    //.chipset = CONFIG_CHIPSET,
    .numsent = 3,
    .hbtime = 10,
    .polltime = 3,
    .arpaddr = 0xC8,
    .IPv4Enable = 1<<eth0 | 1<<wlan0,
    .IPv6Enable = 0<<eth0,
    .DHCPv6Enable = 0<<eth0,
    //.Security = DIGEST_AUTH,
    //.rsvd3 = 0,
    .MatchSubnetMaskValue[eth0] = 0,
    .MatchSubnetMaskValue[wlan0] = 0,
    //.DHCPv4State = 0,
    //.GateWayMAR = "",
    //.DNSMAR = "",
    .srcMacAddr[eth0] = {0x00,0x00, 0x00, 0x00, 0x00, 0x00},
    .srcMacAddr[wlan0] = {0x00,0x00, 0x00, 0x00, 0x00, 0x00},
};
#endif

//extern int asf_init(void);

static int rtk_smbus_probe(struct platform_device *pdev)
{
    //SMBRxDescIndex = 0;
    static struct proc_dir_entry * smbus_proc_dir = NULL;
    static struct proc_dir_entry * smbus_rc_proc_entry = NULL;
    static char * smbus_dir_name = "smbus";
    static char * smbus_rc_entry_name = "remote_control";
    const u32 *prop;
    int size;
    int irq;
    int i = 0;
    dma_addr_t mapping;
    void * tmp_buf = NULL;
    struct netlink_kernel_cfg cfg = {
        .input = rtk_nl_recv_msg,
    };

    sp = devm_kzalloc(&pdev->dev, sizeof(struct smbus_rtl8117), GFP_KERNEL);
    kernel_mmap_user();
    //memcpy(dpconf, CONF_FLASH_ADDR + 0x1000, sizeof(DPCONF));
    memcpy(dpconf, &dpdefault, sizeof(DPCONF));
#ifdef CONFIG_IC_VERSION_FP_RevA
    IC_VERSION = 15;
#endif
#ifdef CONFIG_BLOCK_READ
    dpconf->pldmtype = PLDM_BLOCK_READ;
#else
    dpconf->pldmtype = 1;
#endif
#if defined(CONFIG_BIOS_AMI)
    dpconf->bios = 0;
#elif defined(CONFIG_BIOS_PHOENIX)
    dpconf->bios = 1;
#elif defined(CONFIG_BIOS_DELL)
    dpconf->bios = 2;
#endif

    dpconf->pldmslaveaddr = 0x88;
#if defined(CONFIG_CHIPSET_INTEL)
    dpconf->chipset = 0;
#elif defined(CONFIG_CHIPSET_AMD)
    dpconf->chipset = 1;
#elif defined(CONFIG_CHIPSET_NVIDIA)
    dpconf->chipset = 2;
#elif defined(CONFIG_CHIPSET_HP)
    dpconf->chipset = 3;
#elif defined(CONFIG_CHIPSET_LENOVO)
    dpconf->chipset = 4;
#elif defined(CONFIG_CHIPSET_AMDSOC)
    dpconf->chipset = 5;
#endif
    dpconf->arpaddr = 0xC8;
#if defined(CONFIG_VENDOR_FSC)
    dpconf->vendor = 0;
#elif defined(CONFIG_VENDOR_PEGATRON)
    dpconf->vendor = 1;
#elif defined(CONFIG_VENDOR_EVERLIGHT)
    dpconf->vendor = 2;
#elif defined(CONFIG_VENDOR_GENERIC)
    dpconf->vendor = 3;
#elif defined(CONFIG_VENDOR_SAMSUNG)
    dpconf->vendor = 4;
#elif defined(CONFIG_VENDOR_DELL)
    dpconf->vendor = 5;
#endif
    //printk("chipset is %d, vendor is %d.\n", dpconf->chipset, dpconf->vendor);
#if 0
    dpconf->pldmfromasf = 0;
    //UserInfo tmp;
    /*	if (IC_VERSION > 6)
        {
            tmp = {0xA0, 0x82, 0x1A, 0x5A, 0xCA, 0x77, 0x27, 0xC3, 0xD1, 0x4C, 0xD1, 0x1F, 0x99,
            0xAF, 0x7B, 0xEF, 0x7D, 0xE1, 0xE7, 0x48, 0xE3, 0xE5, 0x03, 0xBE, 0x41, 0x79, 0x10, 0xAC, 0x00,
            0x34, 0x04, 0x40, 0x90, 0xD0, 0x4CC5, 0xD2, 0xA9, 0xE8, 0x3A, 0xF3, 0xD5, 0x94, 0xB7, 0xAE,
            0x95, 0x08, 0x83, 0x25, 0x27, 0x63, 0xA3, 0x31, 0xEC, 0x0F, 0x8A, 0xE9, 0x9F, 0x35, 0x84, 0x47F6873C};
    		dpconf->admin = tmp;
        }
    	else

        {
            tmp = {{0x31, 0x97, 0x6F, 0x6E, 0x55, 0x9D, 0x34, 0x4C, 0xCE, 0xFB, 0xFA, 0x4E, 0x90, 0x5C, 0xF2, 0xC1}
            ,{0x62, 0x60, 0x0E, 0x7C, 0x06, 0x8B, 0x2D, 0x86, 0x8C, 0xCA, 0x2E, 0xBE, 0x80, 0x65, 0x03, 0x3F}
            ,0xA0, 0x5B, 0xA034
            ,{0xA9, 0x67, 0xB2, 0xEC, 0x57, 0xAE, 0x7B, 0x17, 0x56, 0x52, 0x7A, 0x61, 0x88, 0x02, 0x33, 0xE0, 0xFB, 0x4D, 0x01, 0x97, 0x7B, 0xDf, 0x3C}
            , 0xDD, (struct _UserInfo *)0x95ECB8A8};
    		dpconf->admin = tmp;
        }
    */
    dpconf->admin.name[16]= {0x31, 0x97, 0x6F, 0x6E, 0x55, 0x9D, 0x34, 0x4C, 0xCE, 0xFB, 0xFA, 0x4E, 0x90, 0x5C, 0xF2, 0xC1};
    dpconf->HostIP[0].addr = 0x0A00A8C0;
    dpconf->SubnetMask->addr = 0x00FFFFFF;
    dpconf->GateWayIP->addr = 0xFE00A8C0;
    dpconf->DNSIP->addr = 0x0100A8C0;
    dpconf->ConsoleIP->addr = 0x0300A8C0;
    dpconf->asfon = 0;
    dpconf->lspoll = 0;
    dpconf->asfpoll = 0;
    dpconf->DHCPv4Enable = 0;//1<<eth0,
    dpconf->EchoService = 1;
    dpconf->httpService = 1;
    dpconf->httpsService = 1;
    dpconf->wsmanService = 1;
    //dpconf->ProvisioningState = PROVISIONED,
    //dpconf->chipset = CONFIG_CHIPSET,
    dpconf->numsent = 3;
    dpconf->hbtime = 10;
    dpconf->polltime = 3;
    dpconf->arpaddr = 0xC8;
    dpconf->IPv4Enable = 1<<eth0 | 1<<wlan0;
    dpconf->IPv6Enable = 0<<eth0;
    dpconf->DHCPv6Enable = 0<<eth0;
    memcpy(dpconf->srcMacAddr[eth0],0, 6);
    memcpy(dpconf->srcMacAddr[wlan0],0, 6);
    //dpconf->srcMacAddr[eth0] = {0x00,0x00, 0x00, 0x00, 0x00, 0x00},
    //dpconf->srcMacAddr[wlan0] = {0x00,0x00, 0x00, 0x00, 0x00, 0x00},
#endif
    if(dpconf->pldmtype == PLDM_BLOCK_READ)
    {
        sp->smbrxsize = 36;
        sp->smbrxdescnum = 64;
        sp->smbrxdescnum = 256;
    }
    else
    {
        sp->smbrxsize = 76;
        sp->smbrxdescnum = 16;
    }
    prop = of_get_property(pdev->dev.of_node, "reg", &size);
    if ((prop) && (size))
        sp->base = of_iomap(pdev->dev.of_node, 0);

    irq = irq_of_parse_and_map(pdev->dev.of_node, 0);
    sp->txdesc = dma_alloc_coherent(&pdev->dev, sizeof(smbrxdesc_t ),
                                    &sp->txphyaddr, GFP_KERNEL);
    sp->txmdesc = dma_alloc_coherent(&pdev->dev, sizeof(smbrxdesc_t ),
                                     &sp->txmphyaddr, GFP_KERNEL);
    sp->rxdesc = dma_alloc_coherent(&pdev->dev, sizeof(smbrxdesc_t )*sp->smbrxdescnum,
                                    &sp->rxphyaddr, GFP_KERNEL);
    smbus_init();
    sp->smbtxbuffer = (u8 *)dma_alloc_coherent(&pdev->dev, 0x80, &sp->smbtxbuf_map, GFP_KERNEL) + 0x80000000;
    for (i=0; i<sp->smbrxdescnum; i++)
    {
        tmp_buf = kzalloc(sp->smbrxsize+4, GFP_KERNEL);
        tmp_buf = (void *)(((u32)tmp_buf+3)&(~0x3));
        mapping = dma_map_single(&pdev->dev, tmp_buf, sp->smbrxsize, DMA_FROM_DEVICE);
        sp->rxdesc[i].buf_addr = (u8 *)cpu_to_le32(mapping);
        sp->rxdesc[i].opts = cpu_to_le32(0x80000000 + (i<<7) + sp->smbrxsize);
        if (i==sp->smbrxdescnum-1)
            sp->rxdesc[i].opts = cpu_to_le32(sp->rxdesc[i].opts | 0x8000);
    }
    spin_lock_init(&sp->smbus_spinlock);

    nl_sk = netlink_kernel_create(&init_net, NETLINK_SMBUS, &cfg);
    if (!nl_sk) {
        printk(KERN_ERR "Error creating socket.\n");
        return -10;
    }

    smbusdev_register();
    platform_set_drvdata(pdev, sp);

    rtl_writel(sp->rxphyaddr, sp->base + SEIO_RxDesc);
    rtl_writel(sp->rxphyaddr, sp->base + SEIO_RxCurDesc);
    rtl_writel(sp->txphyaddr, sp->base + SEIO_TxDesc);
    rtl_writel(sp->txmphyaddr, sp->base + SEIO_MTxDesc);

    smbus_disable_irq();
    smbus_clear_isr(smbus_read_isr());
    request_irq(irq, rtk_smbus_interrupt, 0, "rtk-smbus", &(pdev->dev));
    smbus_enable_irq();

    if (!smbus_proc_dir)
    {
        smbus_proc_dir = proc_mkdir(smbus_dir_name, NULL);
        if (!smbus_proc_dir)
        {
            printk(KERN_ERR,"Create directory \"%s\" failed.\n", smbus_dir_name);
            return -EPERM;
        }
    }
    if (!smbus_rc_proc_entry)
    {
        smbus_rc_proc_entry = proc_create_data(smbus_rc_entry_name, 0666, smbus_proc_dir, &smbus_rc_fops, NULL);
        if (!smbus_rc_proc_entry)
        {
            printk(KERN_ERR, "Create file \"%s\"\" failed.\n", smbus_rc_entry_name);
            return -EPERM;
        }
    }

    get_mac_addr();

    //asf_init();
    return 0;
}
static void rtk_smbus_shutdown(struct platform_device *pdev)
{
    //dma_addr_t mapping;
    //void * tmp_buf = NULL;
    int i = 0;
    for (i=0; i<sp->smbrxdescnum; i++)
    {
        dma_unmap_single(&pdev->dev, (dma_addr_t)sp->rxdesc[i].buf_addr, sp->smbrxsize, DMA_FROM_DEVICE);
        kfree((const void *)((u32)sp->rxdesc[i].buf_addr|0x80000000));
    }
    dma_free_coherent(&pdev->dev, sizeof(smbrxdesc_t )*sp->smbrxdescnum,
                      sp->rxdesc, sp->rxphyaddr);
    dma_free_coherent(&pdev->dev, sizeof(smbrxdesc_t ),
                      sp->txdesc, sp->txphyaddr);
    dma_free_coherent(&pdev->dev, sizeof(smbrxdesc_t ),
                      sp->txmdesc, sp->txmphyaddr);
    devm_kfree(&pdev->dev, sp);
}

static struct of_device_id rtk_smbus_ids[] = {
    { .compatible = "realtek,rtl8117-smbus" },
    { /* Sentinel */ },
};
MODULE_DEVICE_TABLE(of, rtk_smbus_ids);

static struct platform_driver realtek_smbus_platdrv = {
    .driver = {
        .name   = "rtl8117-smbus",
        .owner  = THIS_MODULE,
        .of_match_table = rtk_smbus_ids,
    },
    .probe      = rtk_smbus_probe,
    .shutdown   = rtk_smbus_shutdown,
};

static int __init rtk_smbus_init(void)
{
    int rc;

    rc = platform_driver_register(&realtek_smbus_platdrv);
    //asf_init();
    return rc;
}
module_init(rtk_smbus_init);

static void __exit rtk_smbus_exit(void)
{
    platform_driver_unregister(&realtek_smbus_platdrv);
}
module_exit(rtk_smbus_exit);
