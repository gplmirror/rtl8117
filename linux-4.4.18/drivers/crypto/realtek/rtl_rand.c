#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/module.h>
#include <linux/suspend.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <linux/interrupt.h>
#include <linux/pci.h>
#include <linux/io.h>
#include <linux/list.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <linux/timekeeping.h>
#include <linux/miscdevice.h>
#include <crypto/internal/hash.h>
#include <crypto/sha.h>
#include <rtl8117_platform.h>

static u32 TRNG_BASE;
//#define TRNG_BASE             0xBB900000
#define TRNG_ANALOG			 	0x14
#define TRNG_RETURN3			 	0x2C
#define TRNG_RESULTR			 	0x38
#define TRNG_RETURN4			 	0x30
#define TRNG_POW_BIT				0x01
#define TRNG_RDY_BIT				0x01

static long
trngdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg_)
{
    void __user *arg = (void __user *)arg_;
    int __user *p = arg;

    u32 ret;
    switch (cmd) {
    case 0:
        ret = rtl_readl(TRNG_BASE + TRNG_RESULTR);
        ret &= 0x7fffffff;
        copy_to_user(arg, &ret, 4);
        break;
    default:
        break;
    }
    return 0;
}

u32 read_rand(void)
{
    volatile u32 data;
    data = rtl_readl(TRNG_BASE + TRNG_RESULTR);
    data &= 0x7fffffff;
    return data;
}
EXPORT_SYMBOL(read_rand);

static const struct file_operations trngdev_fops = {
    .owner = THIS_MODULE,
    //.open = tlsdev_open,
    .open = simple_open,
    //.release = tlsdev_release,
    .unlocked_ioctl = trngdev_ioctl,
//#ifdef CONFIG_COMPAT
//    .compat_ioctl = cryptodev_compat_ioctl,
//#endif /* CONFIG_COMPAT */
    //.poll = cryptodev_poll,
};

static struct miscdevice trngdev = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "trng",
    .fops = &trngdev_fops,
    .mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH,
};

static int __init trngdev_register(void)
{
    int rc;

    rc = misc_register(&trngdev);
    if (unlikely(rc)) {
        pr_err("registration of /dev/trng failed\n");
        return rc;
    }

    return 0;
}

static int trng_probe(struct platform_device *pdev)
{
    const u32 *prop;
    unsigned int size;

    prop = of_get_property(pdev->dev.of_node, "reg", &size);
    if ((prop) && (size))
        TRNG_BASE = (u32)of_iomap(pdev->dev.of_node, 0);

    rtl_writel(rtl_readl(TRNG_BASE + TRNG_ANALOG)|TRNG_POW_BIT, TRNG_BASE + TRNG_ANALOG);
    while ((rtl_readl(TRNG_BASE + TRNG_RETURN3)&TRNG_RDY_BIT) == 0)
    {
        udelay(2);
    }
    trngdev_register();
    return 0;
}

static const struct of_device_id rtl_trng_ids[] = {
    { .compatible = "realtek,rtl8117-trng", },
    {},
};
MODULE_DEVICE_TABLE(of, rtl_trng_ids);

static struct platform_driver rtl_trng_driver = {
    .probe      = trng_probe,
    //.remove     = tls_remove,
    //.suspend    = mcp_suspend,
    //.resume     = mcp_resume,
    .driver     = {
        .name   = "rtl_trng",
        //.bus = &platform_bus_type,
        .owner  = THIS_MODULE,
        .of_match_table = of_match_ptr(rtl_trng_ids),
    },
};

static int __init rtl_trng_init(void)
{
    return platform_driver_register(&rtl_trng_driver);
}

static void __exit rtl_trng_exit(void)
{

    platform_driver_unregister(&rtl_trng_driver);
}

module_init(rtl_trng_init);
module_exit(rtl_trng_exit);
MODULE_DESCRIPTION("Realtek random number generator.");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("vera_xu@realsil.com.cn");
