#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/module.h>
#include <linux/suspend.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <linux/interrupt.h>
#include <linux/pci.h>
#include <linux/io.h>
#include <linux/list.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <linux/timekeeping.h>
#include <linux/miscdevice.h>
#include <crypto/internal/hash.h>
#include <crypto/sha.h>
#include <rtl8117_platform.h>
#include <rtl_tls.h>

static u32 TLS_BASE;

struct tls_dev {
    struct platform_device *spdev;
    struct mutex mutex;
    u8 * TLSdesc_baseaddr;
};
struct tls_dev *tdev;

static struct platform_device *spdev;

void tls_set(u8 *keyaddr, u8 *payloadaddr, u8 alsel, u16 length)
{
    tlsdesc_t *tlsdesc = (tlsdesc_t *) tdev->TLSdesc_baseaddr;
    tlsdesc->offset1.cmd.length = length;
    tlsdesc->offset1.cmd.alsel = (alsel & 0x0f);

//#if CONFIG_VERSION >= IC_VERSION_FP_RevA
    tlsdesc->offset1.cmd.key_size = ((alsel & 0xf0)>>4);
//#endif

    if (keyaddr != NULL)
        tlsdesc->key_addr = (u8 *) VA2PA(keyaddr);
    tlsdesc->payload_addr = (u8 *) VA2PA(payloadaddr);

    tlsdesc->offset1.cmd.own = 1; //set own bit
    //printk("tlsdesc->offset1.cmd is 0x%x.\n", tlsdesc->offset1.cmd);
    //udelay(5);
    //printk("TLS_BASE is 0x%x, val is 0x%x.\n", TLS_BASE, readb(TLS_BASE + TLS_POLL));
    rtl_writeb(0x80, TLS_BASE + TLS_POLL);
    //printk("after set polling bit.\n");
}

int tls_valid(void)
{
    //tlsdesc_t *tlsdesc = (tlsdesc_t *) TLSdescStartAddr;
    tlsdesc_t *tlsdesc = (tlsdesc_t *)tdev->TLSdesc_baseaddr;
    return tlsdesc->offset1.cmd.valid;
}

int tls_poll(void)
{
    //tlsdesc_t *tlsdesc = (tlsdesc_t *) TLSdescStartAddr;
    tlsdesc_t *tlsdesc = (tlsdesc_t *)tdev->TLSdesc_baseaddr;
    while (1)
    {
        if (tlsdesc->offset1.cmd.own == 0)
            break;
    }

    return tlsdesc->offset1.cmd.length;
}

void aes_cbc(char *key, int mode, int length, unsigned char *iv, unsigned char *input, unsigned char *output)
{
    aesHwKey *hk = 0;
    //unsigned char* inData;
    //unsigned char* pData;
    //unsigned char* pOutput = output;
    //char enc_key[16];

//#if CONFIG_VERSION <= IC_VERSION_DP_RevF
//    if ( mode == AES_ENCRYPT )
//#else
    if ( (mode&0xf) == AES_CBC_ENCRYPT )
//#endif
    {
        hk = kzalloc(sizeof(aesHwKey), GFP_KERNEL);
        memset(hk, 0 , sizeof(aesHwKey));
        memcpy(hk->AES_Key, (unsigned char*)key, 16);
        //inData = malloc(length);
        //memcpy(inData, input, length);
        //pData = inData;

        while (1)
        {
            memcpy(hk->IV, iv, 16);
            //AES data must grater than 16 byte
            if (length >= 1600 && (length - 1600) > 16)
            {
//#if CONFIG_VERSION <= IC_VERSION_DP_RevF
                //tls_set((u8 *) hk, input, 0x01,1600);
//#else
                mutex_lock(&tdev->mutex);
                tls_set((u8 *) hk, input, AES128_CBC_ENCRYPT,1600);
//#endif

                if (tls_poll() == -1)
                    ;
                mutex_unlock(&tdev->mutex);
                if (output != input)
                {
                    memcpy(output, input, 1600);
                }

                input += 1600;
                output += 1600;
                length -= 1600;
                memcpy( iv, input - 16, 16);
            }
            else
            {
//#if CONFIG_VERSION <= IC_VERSION_DP_RevF
//               tls_set((u8 *) hk, input, 0x01, length);
//#else
                mutex_lock(&tdev->mutex);
                tls_set((u8 *) hk, input, AES128_CBC_ENCRYPT, length);
//#endif

                if (tls_poll() == -1)
                    ;
                mutex_unlock(&tdev->mutex);
                if (output != input)
                {
                    memcpy(output, input, length);
                }

                input += length;
                output += length;
                length -= length;
                memcpy( iv, input - 16, 16);
                break;
            }
        }
        kfree(hk);
        //free(inData);
    }
    else
    {
        hk = kzalloc(sizeof(aesHwKey), GFP_KERNEL);
        memset(hk, 0 , sizeof(aesHwKey));
        memcpy(hk->AES_Key, (unsigned char*)key, 16);
        //inData = malloc(length);
        //memcpy(inData, input, length);
        //pData = inData;
        //memcpy(inData, input, length);

        while (1)
        {
            memcpy(hk->IV, iv, 16);
            if (length >= 1600 && (length - 1600) > 16)
            {
                memcpy( iv, input + 1600 - 16, 16);
//#if CONFIG_VERSION <= IC_VERSION_DP_RevF
//                tls_set((u8 *) hk, input, 0x02,1600);
//#else
                mutex_lock(&tdev->mutex);
                tls_set((u8 *) hk, input, AES128_CBC_DECRYPT,1600);
//#endif
                if (tls_poll() == -1)
                    ;
                mutex_unlock(&tdev->mutex);
                memcpy(output, input, 1600);
                input += 1600;
                output += 1600;
                length -= 1600;
            }
            else
            {
                memcpy( iv, input + length - 16, 16);
//#if CONFIG_VERSION <= IC_VERSION_DP_RevF
//                tls_set((u8 *) hk, input, 0x02, length);
//#else
                mutex_lock(&tdev->mutex);
                tls_set((u8 *) hk, input, AES128_CBC_DECRYPT, length);
//#endif

                if (tls_poll() == -1)
                    ;
                mutex_unlock(&tdev->mutex);
                memcpy(output, input, length);
                input += length;
                output += length;
                length -= length;
                break;
            }
        }

        kfree(hk);
        //free(inData);
    }
}



void hmacsha1(struct hmac_sha1_param *hs_param)
{
    dma_addr_t key_mapping, data_mapping;
    u8 * key = NULL;
    u8 *data = NULL;
    key = dma_alloc_coherent(&spdev->dev, hs_param->keylen+0x40, &key_mapping, GFP_KERNEL);
    data = dma_alloc_coherent(&spdev->dev, hs_param->len, &data_mapping, GFP_KERNEL);
    memcpy(key, hs_param->key, hs_param->keylen);
    memcpy(data, hs_param->data, hs_param->len);
    mutex_lock(&tdev->mutex);
    tls_set(key, data, SHA1_HMAC_20, hs_param->len);
    if(tls_poll() == -1)
    {
        printk("tls failed.\n");
    }
    else
        memcpy(hs_param->out, key+0x40, 20);
    mutex_unlock(&tdev->mutex);
    //memcpy(hs_param->key+0x40, key+0x40, 20);
    dma_free_coherent(&spdev->dev, hs_param->keylen+0x40, key, key_mapping);
    dma_free_coherent(&spdev->dev, hs_param->len, data, data_mapping);
}
EXPORT_SYMBOL(hmacsha1);
void hmacsha1_24(struct hmac_sha1_param *hs_param)
{
    dma_addr_t key_mapping, data_mapping;
    u8 * key = NULL;
    u8 *data = NULL;
    key = dma_alloc_coherent(&spdev->dev, hs_param->keylen+0x40, &key_mapping, GFP_KERNEL);
    data = dma_alloc_coherent(&spdev->dev, hs_param->len, &data_mapping, GFP_KERNEL);
    memcpy(key, hs_param->key, hs_param->keylen);
    memcpy(data, hs_param->data, hs_param->len);
    mutex_lock(&tdev->mutex);
    tls_set(key, data, SHA1_HMAC_20, hs_param->len);
    if(tls_poll() == -1)
    {
        printk("tls failed.\n");
    }
    else
        memcpy(hs_param->out, key, 20);
    mutex_unlock(&tdev->mutex);
    dma_free_coherent(&spdev->dev, hs_param->keylen+0x40, key, key_mapping);
    dma_free_coherent(&spdev->dev, hs_param->len, data, data_mapping);
}
EXPORT_SYMBOL(hmacsha1_24);
enum { HMACSHA1=1, HMACSHA1_24, AES_CBC};
static long
tlsdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg_)
{
    //printk("tlsdev_ioctl.\n");
    void __user *arg = (void __user *)arg_;
    //int __user *p = arg;
#if 0
    struct session_op sop;
    struct kernel_crypt_op kcop;
    struct kernel_crypt_auth_op kcaop;
    struct crypt_priv *pcr = filp->private_data;
    struct fcrypt *fcr;
    struct session_info_op siop;
    uint32_t ses;
    int ret, fd;

    if (unlikely(!pcr))
        BUG();

    fcr = &pcr->fcrypt;
#endif
    struct hmac_sha1_param *hs_param = kzalloc(sizeof(struct hmac_sha1_param), GFP_KERNEL);
    struct aes_cbc_param *ac_param = kzalloc(sizeof(struct aes_cbc_param), GFP_KERNEL);
    dma_addr_t key_mapping, data_mapping, iv_mapping, output_mapping;
    u8 * key = NULL;
    u8 *data = NULL;
    u8 * output = NULL;
    u8 *iv = NULL;
    //key = dma_alloc_coherent(&spdev->dev, 20+0x40, &key_mapping, GFP_KERNEL);
    switch (cmd) {
    case HMACSHA1:
        copy_from_user(hs_param, arg, sizeof(*hs_param));
        key = dma_alloc_coherent(&spdev->dev, hs_param->keylen+0x40, &key_mapping, GFP_KERNEL);
        data = dma_alloc_coherent(&spdev->dev, hs_param->len, &data_mapping, GFP_KERNEL);
        copy_from_user(key, hs_param->key, hs_param->keylen);
        copy_from_user(data, hs_param->data, hs_param->len);
        //printk("key is %c.\n", key[0]);
        tls_set(key, data, SHA1_HMAC_20, hs_param->len);
        if(tls_poll() == -1)
        {
            printk("tls failed.\n");
        }
        else
        {
            //printk("tls pass.\n");
            copy_to_user(hs_param->key+0x40, key+0x40, 20);
        }
        dma_free_coherent(&spdev->dev, hs_param->keylen+0x40, key, key_mapping);
        dma_free_coherent(&spdev->dev, hs_param->len, data, data_mapping);
        break;
    case HMACSHA1_24:
        copy_from_user(hs_param, arg, sizeof(*hs_param));
        key = dma_alloc_coherent(&spdev->dev, hs_param->keylen, &key_mapping, GFP_KERNEL);
        data = dma_alloc_coherent(&spdev->dev, hs_param->len, &data_mapping, GFP_KERNEL);
        copy_from_user(key, hs_param->key, hs_param->keylen);
        copy_from_user(data, hs_param->data, hs_param->len);
        //printk("key is %c.\n", key[0]);
        tls_set(key, data, SHA1_HMAC_20, hs_param->len);
        if(tls_poll() == -1)
        {
            printk("tls failed.\n");
        }
        else
        {
            //printk("tls pass.\n");
            copy_to_user(hs_param->key, key, 20);
        }
        dma_free_coherent(&spdev->dev, hs_param->keylen, key, key_mapping);
        dma_free_coherent(&spdev->dev, hs_param->len, data, data_mapping);
        break;
    case AES_CBC:
        copy_from_user(ac_param, arg, sizeof(*ac_param));
        key = dma_alloc_coherent(&spdev->dev, 16, &key_mapping, GFP_KERNEL);
        data = dma_alloc_coherent(&spdev->dev, ac_param->len, &data_mapping, GFP_KERNEL);
        output = dma_alloc_coherent(&spdev->dev, ac_param->len, &output_mapping, GFP_KERNEL);
        iv = dma_alloc_coherent(&spdev->dev, 16, &iv_mapping, GFP_KERNEL);
        copy_from_user(key, ac_param->key, 16);
        copy_from_user(iv, ac_param->iv, 16);
        copy_from_user(data, ac_param->input, ac_param->len);
        copy_from_user(output, ac_param->output, ac_param->len);
        aes_cbc(key, ac_param->mode, ac_param->len, iv, data, output);
    default:
        break;
    }
    return 0;

}

static const struct file_operations tlsdev_fops = {
    .owner = THIS_MODULE,
    //.open = tlsdev_open,
    .open = simple_open,
    //.release = tlsdev_release,
    .unlocked_ioctl = tlsdev_ioctl,
//#ifdef CONFIG_COMPAT
//    .compat_ioctl = cryptodev_compat_ioctl,
//#endif /* CONFIG_COMPAT */
    //.poll = cryptodev_poll,
};

static struct miscdevice tlsdev = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "tls",
    .fops = &tlsdev_fops,
    .mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH,
};

static int __init tlsdev_register(void)
{
    int rc;

    rc = misc_register(&tlsdev);
    if (unlikely(rc)) {
        pr_err("registration of /dev/tlsdev failed\n");
        return rc;
    }

    return 0;
}


static int tls_probe(struct platform_device *pdev)
{
    const u32 *prop;
    tlsdesc_t *tlsdesc;
    unsigned int size;
    dma_addr_t mapping;
    tdev = kzalloc(sizeof(struct tls_dev), GFP_KERNEL);
    tdev->spdev = pdev;
    mutex_init(&tdev->mutex);

    prop = of_get_property(pdev->dev.of_node, "reg", &size);
    if ((prop) && (size))
        TLS_BASE = (u32)of_iomap(pdev->dev.of_node, 0);
    tdev->TLSdesc_baseaddr = dma_alloc_coherent(&pdev->dev, sizeof(tlsdesc_t), &mapping, GFP_KERNEL);
    tlsdesc = (tlsdesc_t *) tdev->TLSdesc_baseaddr;
    tlsdesc->offset1.word = 0x40000000; //only set eor
    rtl_writel(mapping, TLS_BASE + TLS_DESC);
    tlsdev_register();
    return 0;
}

static const struct of_device_id rtl_tls_ids[] = {
    { .compatible = "realtek,rtl8117-tls", },
    {},
};
MODULE_DEVICE_TABLE(of, rtl_tls_ids);

static struct platform_driver rtl_tls_driver = {
    .probe      = tls_probe,
    //.remove     = tls_remove,
    //.suspend    = mcp_suspend,
    //.resume     = mcp_resume,
    .driver     = {
        .name   = "rtl_tls",
        //.bus = &platform_bus_type,
        .owner  = THIS_MODULE,
        .of_match_table = of_match_ptr(rtl_tls_ids),
    },
};

static int __init rtl_tls_init(void)
{
    return platform_driver_register(&rtl_tls_driver);
}

static void __exit rtl_tls_exit(void)
{

    platform_driver_unregister(&rtl_tls_driver);
}

module_init(rtl_tls_init);
module_exit(rtl_tls_exit);
MODULE_DESCRIPTION("Realtek TLS hw acceleration support.");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("vera_xu@realsil.com.cn");
