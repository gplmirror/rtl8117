#ifndef __RTL8117_TLS__
#define __RTL8117_TLS__

#define SHA1_HMAC_20 8
#define MD5_HASH 5

#define TLSdescStartAddr 0x80040280
#define TLS_TEST_DATA 0x80040000
#define TLS_TEST_KEY 0x80040100

#define TLS_DESC                   0x00
#define TLS_POLL                   0x04
#define TLS_IMR                    0x05
#define TLS_STATUS                 0x06

#define AES_128     0x10
#define AES_192     0x20
#define AES_256     0x30
#define AES_CBC_ENCRYPT     0
#define AES_CBC_DECRYPT     1

#define AES128_CBC_ENCRYPT  (AES_128 | AES_CBC_ENCRYPT)
#define AES128_CBC_DECRYPT  (AES_128 | AES_CBC_DECRYPT)
#define AES192_CBC_ENCRYPT  (AES_192 | AES_CBC_ENCRYPT)
#define AES192_CBC_DECRYPT  (AES_192 | AES_CBC_DECRYPT)
#define AES256_CBC_ENCRYPT  (AES_256 | AES_CBC_ENCRYPT)
#define AES256_CBC_DECRYPT  (AES_256 | AES_CBC_DECRYPT)
typedef struct _aesHwKey
{
//#if CONFIG_VERSION  >= IC_VERSION_FP_RevA
    unsigned char AES_Key[32];
//#else
    //unsigned char AES_Key[16];
//#endif
    unsigned char IV[16];
} aesHwKey;

typedef struct
{
    union
    {
        u32 word;
        struct
        {
            u32 length: 15, rsvd:8, key_size:2, alsel:4, valid:1, eor:1, own:1;
        } cmd;
    } offset1;

    u32 RSVD;
    u8  *key_addr;
    u8  *payload_addr;
} volatile tlsdesc_t;

struct hmac_sha1_param
{
    u8 * key;
    u8 * data;
    u8 * out;
    u32 keylen;
    u32 len;
};

struct aes_cbc_param
{
    u8 * key;
    u8 * input;
    u8 *output;
    u8 * iv;
    u8 mode;
    u32 len;
};

u32 read_rand(void);
void hmacsha1(struct hmac_sha1_param *hs_param);
void hmacsha1_24(struct hmac_sha1_param *hs_param);
#endif
