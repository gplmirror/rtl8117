struct VgaInfo
{
    u16 FB0_Vresol, FB0_Hresol, FB0_BPP, FB0_VBnum, FB0_HBnum, FB1_Vresol, FB1_Hresol, FB1_BPP, FB1_VBnum, FB1_HBnum;
};
struct VncInfo
{
    unsigned char NewVNCCli:1, HwVNCEnable:1, Enable:1;
};