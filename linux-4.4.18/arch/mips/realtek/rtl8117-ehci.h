#ifndef _RTL8117_EHCI_H_
#define _RTL8117_EHCI_H_
#include <linux/net.h>


#if 0
#define __DEV__         1
#define dev_msg(fmt, ...)               printk(KERN_INFO fmt, ##__VA_ARGS__)
#define dev_cond_msg(cond, fmt, ...)    if(cond) printk(KERN_INFO fmt, ##__VA_ARGS__)
#else
#define __DEV__         0
#define dev_msg(...)
#define dev_cond_msg(...)
#endif


#define CTLOUTdescNumber 4
#define CTLINdescNumber  1
#define OUTdescNumber 1
#define INdescNumber 4

#define MAX_BULK_LEN 4096

#define READ6 0x08
#define WRITE6 0x0a

#define INQUIRY                                 0x12
#define READ_FORMAT_CAPACITIES                  0x23
#define READ10                                  0x28
#define WRITE10                                 0x2A
#define MODE_SENSE6                             0x1A
#define REQUEST_SENSE                           0x03
#define MODE_SELECT6                            0x15
#define MODE_SELECT10                           0x55
#define MODE_SENSE10                            0x5A
#define READ_CAPACITY                           0x25
#define START_STOP_UNIT                         0x1B
#define TEST_UNIT_READY                         0x00
#define READ_TOC                                0x43
#define READ_IO_BLOCK                           0x66
#define PREVENT_ALLOW_MEDIUM_REMOVAL            0x1E
#define GET_EVENT_STATUS_NOTIFICATION           0x4A
#define READ_DISC_INFORMATION                   0x51
#define GET_CONFIGURATION                       0x46
#define ERASE12                                 0xAC
#define READ_DVD_STRUCTURE                      0xAD
#define REPORT_KEY                              0xA4
#define SYNC_CACHE                              0x35
#define READ_TRACK_INFORMATION                  0x52
#define SERVICE_ACTION_IN                         0x9E
#define SECURITY_PROTOCOL_IN                    0xA2
#define IdeGetMaxLun                            0xFE

// MSC Test
#define READ12                                  0xA8
#define WRITE12                                 0xAA
#define WRITEANDVERIFY                          0x2E
#define VERIFY                                  0x2F

//Sense key
#define SK_NOT_READY                            0x02
#define SK_MEDIUM_ERROR                         0x03
#define SK_HARDWARE_ERROR                       0x04
#define SK_ILLEGAL_REQUEST                      0x05
#define SK_UNIT_ATTENTION                       0x06
#define SK_DATA_PROTECT                         0x07
#define ASC_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE  0x21
#define ASCQ_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE 0x00
#define ASC_MEDIUM_NOT_PRESENT                  0x3a
#define ASCQ_MEDIUM_NOT_PRESENT                 0x00
#define ASC_PERIPHERAL_DEVICE_WRITE_FAULT       0x03
#define ASCQ_PERIPHERAL_DEVICE_WRITE_FAULT      0x00
#define ASC_UNRECOVERED_READ_ERROR              0x11
#define ASCQ_UNRECOVERED_READ_ERROR             0x00
#define ASC_WRITE_PROTECTED                     0x27
#define ASCQ_WRITE_PROTECTED                    0x00
#define ep0_mps                                 64

#define USB_REQ_SYNCH_FRAME     0x0C
#define USB_GET_IDLE_REQUEST      0x02

#define USB_PR_CBI	0x00		/* Control/Bulk/Interrupt */
#define USB_PR_CB	0x01		/* Control/Bulk w/o interrupt */
#define USB_PR_BULK	0x50		/* bulk only */
#define USB_PR_UAS	0x62		/* USB Attached SCSI */

#define USB_PR_USBAT	0x80		/* SCM-ATAPI bridge */
#define USB_PR_EUSB_SDDR09	0x81	/* SCM-SCSI bridge for SDDR-09 */
#define USB_PR_SDDR55	0x82		/* SDDR-55 (made up) */
#define USB_PR_DPCM_USB	0xf0		/* Combination CB/SDDR09 */
#define USB_PR_FREECOM	0xf1		/* Freecom */
#define USB_PR_DATAFAB	0xf2		/* Datafab chipsets */
#define USB_PR_JUMPSHOT	0xf3		/* Lexar Jumpshot */
#define USB_PR_ALAUDA	0xf4		/* Alauda chipsets */
#define USB_PR_KARMA	0xf5		/* Rio Karma */
#define USB_PR_DEVICE	0xff		/* Use device's value */

#define USB_SC_RBC	0x01		/* Typically, flash devices */
#define USB_SC_8020	0x02		/* CD-ROM */
#define USB_SC_QIC	0x03		/* QIC-157 Tapes */
#define USB_SC_UFI	0x04		/* Floppy */
#define USB_SC_8070	0x05		/* Removable media */
#define USB_SC_SCSI	0x06		/* Transparent */
#define USB_SC_LOCKABLE	0x07		/* Password-protected */

struct command_block_wrapper {
    u32   dCBWSignature;
    u32   dCBWTag;
    u32   dCBWDataTransferLength;
    u8    bmCBWFlags;
    u8    bCBWLUN : 4, Reserved :4 ;
    u8    bCBWCBLength : 5, Reserved2: 3;
    u8    rbc[16];
};

/* CSW data structure */
struct command_status_wrapper {
    u32 dCSWSignature;
    u32 dCSWTag;
    u32 dCSWDataResidue;
    u8  bCSWStatus;
};
#define IN_USE_REDIRECT 1
#define IN_USE_HID  2
#define BULK_BUF_SIZE 0x10000


struct rtl8117_ehci_core {
    struct platform_device *pdev;

    /* control out desc */
    u8* ctl_outdesc_addr;
    dma_addr_t ctl_outdesc_phy;
    u8 ctl_outdesc_index;

    /* control in desc */
    u8* ctl_indesc_addr;
    dma_addr_t ctl_indesc_phy;
    u8 ctl_indesc_index;

    /* bulk out desc */
    u8* bulk_outdesc_addr;
    dma_addr_t bulk_outdesc_phy;
    u8 bulk_outdesc_index;

    /* bulk in desc */
    u8* bulk_indesc_addr;
    dma_addr_t bulk_indesc_phy;
    u8 bulk_indesc_index;

    /* control out buffer */
    u8* ctl_outbuf_addr;
    dma_addr_t ctl_outbuf_phy;

    /* bulk out buffer */
    u8* bulk_outbuf_addr;
    dma_addr_t bulk_outbuf_phy;
#ifdef CONFIG_USB_REDIRECTION
    u8* inter_inbuf1_addr;
    dma_addr_t inter_inbuf1_phy;

    u8* inter_inbuf2_addr;
    dma_addr_t inter_inbuf2_phy;
#endif

    u32 imr;
    u8 bulk_state;
};


// CBW or data from host to device will go through this function
enum{
    EHCI_STATE_BULK_CBW,        // wait bulk cbw
    EHCI_STATE_BULL_IN,         // wait data from device to host
    EHCI_STATE_BULK_OUT,        // wait data from host to device
};

#define WW_FIRST        0x0001UL
#define WW_LAST         0x0002UL

struct write_work {
    struct work_struct data_write_schedule;
    unsigned int write_work_data;
    void *payload;
    struct command_status_wrapper csw;
    unsigned int flags;
};

struct rtl8117_ehci_device {
    struct usb_device *udev;

    struct delayed_work schedule;

    struct usb_ctrlrequest request;
    struct command_block_wrapper data_read_cbw;
    struct command_status_wrapper data_read_csw;

    u8 data_read_buf[BULK_BUF_SIZE];

    struct command_block_wrapper data_write_cbw;
    u8 data_write_buf[BULK_BUF_SIZE];

    struct delayed_work data_read_schedule;

    u8 control_buf[256];

    struct file *filp;
    struct inode *inode;

    loff_t      file_length;
    loff_t      num_sectors;

    unsigned int    blkbits;    /* Bits of logical block size of bound block device */
    unsigned int    blksize;    /* logical block size of bound block device */
    u8      usb_type;
    struct mutex    mutex;
    struct mutex    port1_mutex;	/* Mutex for port1 critical section */

#ifdef CONFIG_USB_REDIRECTION
    u8      usb_state;
    u8      portnum:3, hid_state:3, IN_stall:2;
    u8      kb_hid_enabled:2, mouse_hid_enabled:2, ep0_status:4;
    u8      in_use_redirect:3, in_use_hid:3, last_use:2;
    u8      msd_address;
    u8      hid_address;
    u32     block_nr;
#endif
};

/**
 * States of EP0.
 */
typedef enum ep0_state
{
    EP0_DISCONNECT,     /* no host */
    EP0_IDLE,
    EP0_IN_DATA_STATUS,
    EP0_OUT_DATA_STATUS,
    EP0_STATUS,
    EP0_STALL,
    EP0_CBI,
} ep0state_e;

typedef struct
{
    u32 length: 16, rsvd:4, devaddr:7, stall:1, ls:1, fs:1, eor:1, own:1;
    dma_addr_t bufaddr;
} volatile outindesc_r;

static inline void rtl8117_ehci_writeb(u8 val, u32 addr)
{
    writeb(val, (volatile void __iomem *)addr);
}

static inline void rtl8117_ehci_writew(u16 val, u32 addr)
{
    writew(val, (volatile void __iomem *)addr);
}

static inline void rtl8117_ehci_writel(u32 val, u32 addr)
{
    writel(val, (volatile void __iomem *)addr);
}

static inline u32 ehci_readl(u32 addr)
{
    return readl((volatile void __iomem *)addr);
}

static inline u8 ehci_readb(u32 addr)
{
    return readb((volatile void __iomem *)addr);
}


/* rtl8117-ehci*/
bool rtl8117_ehci_intr_handler(void);

void rtl8117_ehci_set_otg_power(bool on);
void rtl8117_set_ehci_enable(bool on);

void rtl8117_ehci_poweron_request(void);

/* rtl8117-ehci-core */
int rlt8117_ehci_core_init(void *platform_pdev);

void rtl8117_ehci_intep_enabled(u8 portnum);
void rtl8117_ehci_intep_disabled(u8 portnum);

void rtl8117_ehci_ep0_start_transfer(u16 len, u8 *addr, u8 is_in, bool stall);
void rtl8117_ehci_ep_start_transfer(u32 len, u8 *addr, bool stall);
void activate_bulkout_desc(int index, u32 len, u8 *addr);

/* rtl8117-ehci-device */
int rtl8117_ehci_device_init(void);

void rtl8117_ehci_control_request(struct usb_ctrlrequest *request);
void rtl8117_ehci_bulkout_request(void* ptr, u32 length);

int rtl8117_ehci_open_file(char *path);
int rtl8117_ehci_close_file(void);
void handle_ep0(void);
#ifdef CONFIG_USB_REDIRECTION
#define DEVICE_REPORT_TYPE              0x22
int send_netlink_socket(unsigned char *addr, unsigned int len);

void netlink_create(void);
void Wt_IBIO(u8 phyaddr,u32 Wt_Data);
void VGA_pre_Initial(void);
void VGA_initial(void);
void VGA_enable(void);
void VGA_disable(void);
void vnc_init(void);
u32 Rd_IBIO(u8 phyaddr);
int EHCI_RST(void);
void rtl8117_ehci_init(void);
void rtl8117_ehci_init_bulk(void);
void rtl8117_ehci_init_hid(void);
void show_register(int);

void enable_vnc_usb_dev(void);
void disable_vnc_usb_dev(void);
void rtkehci_interep_transfer(u8 len, u8 *addr, u8 is_in, u8 portnum);
#define VGA_IOBASE		0xbafb0000	// VGA Slave
#define WIFI_DASH_IOBASE 0xbafa0000

#define BIT_ISOLATE          0x00
#define MAC_MAC_STATUS            0x0104

#define VGA_IMR			0x52C	// VGA Slave IMR
#define VGA_ISR			0x530	// VGA Slave ISR
#define STD_reg_ISR0		0x540     //VGA STD reg ISR0 0xFFFFFFFF
#define STD_reg_ISR1		0x544     //VGA STD reg ISR1 0xFFFFFFFF
#define STD_reg_ISR2		0x548     //VGA STD reg ISR2 0x0000FFFF
#define STD_reg_IMR0		0x534     //VGA STD reg IMR0 0xFFFFFFFF
#define STD_reg_IMR1		0x538     //VGA STD reg IMR1 0xFFFFFFFF
#define STD_reg_IMR2		0x53C     //VGA STD reg IMR2 0x0000FFFF

//frame buffer maxima
#define VBlock_maxnum 	0x11
#define HBlock_maxnum 	0x1E
#define InBlock_maxnum 	0x40
#define VGA_ROM_SPI_base	0x02000000 //SW modify


//frame buffer register
//FB0 : legacy frame buffer
#define FB0_resol		0x56C // bit[11:0] : hori-resolution , bit[23:12] : veri-resol,  bit[31:24] : byte per pixel
#define FB0_CTRL 		0x11C
#define FB0_VaddrBase 	0x120
#define FB0_HaddrBase 	0x164
#define FB0_LaddrBase	 0x1DC
#define FB0_BGN			0x2DC
#define FB0_BFN_base	0x2E0
#define FB0_BFN_timer	0x320
#define FB0_HBN_BPP		0x100
#define FB0_DDR_base	0x08000000 //SW modify


//FB1 : BAR frame buffer
#define FB1_resol		0x570 // bit[11:0] : hori-resolution , bit[23:12] : veri-resol,  bit[31:24] : byte per pixel
#define FB1_CTRL 		0x324
#define FB1_VaddrBase 	0x328
#define FB1_HaddrBase 	0x36C
#define FB1_LaddrBase	0x3E4
#define FB1_BGN			0x4E4
#define FB1_BFN_base	0x4E8
#define FB1_BFN_timer	0x528
#define FB1_HBN_BPP 	0x108
#define FB1_DDR_base	0x08000000 //SW modify


#define VGA_dummy2 0x574
/*
VGA_dummy2 :
bit[0] : set to test VGA
bit[1] : VGA_ini ok flag
bit[2] : diff flag ckeck test flag
bit[19:16] : fb0/fb1block timer
*/
#define VGA_dummy3 0x578

#define is_IN 1

//For OOBREG 0x128
#define BIT_DASHEN           0x00
#define BIT_TLSEN            0x01
#define BIT_FIRMWARERDY      0x02
#define BIT_OOBRESET         0x03
#define BIT_VGAEN	         0x04


#define VALID_PORTSC		0x1005



static inline void bsp_bits_set(u16 offset, u32 value, u8 bits, u8 width)
{
    u16 count = 0;
    u32 valmask = 0;

    for (count = 0; count < width; count++)
        valmask |= (1 << (count+bits));

    //only clear zero value
    value = (valmask & (value << bits));
    valmask ^= value;

    //always clear first, but only for the zero value
    if((offset % 4) == 0 )
    {
        //OOB_MAC_BASE_ADDR
        writel(readl(OOB_MAC_BASE_ADDR+offset)&(~valmask), OOB_MAC_BASE_ADDR+offset);
        writel(readl(OOB_MAC_BASE_ADDR+offset)|value, OOB_MAC_BASE_ADDR+offset);
    }
    else if ((offset %2) == 0)
    {
        writew(readw(OOB_MAC_BASE_ADDR+offset)&(~valmask), OOB_MAC_BASE_ADDR+offset);
        writew(readw(OOB_MAC_BASE_ADDR+offset)|value, OOB_MAC_BASE_ADDR+offset);
    }
    else
    {
        writeb(readb(OOB_MAC_BASE_ADDR+offset)&(~valmask), OOB_MAC_BASE_ADDR+offset);
        writeb(readb(OOB_MAC_BASE_ADDR+offset)|value, OOB_MAC_BASE_ADDR+offset);
    }
}

static inline u32 bsp_bits_get(u16 offset, u8 field, u8 width)
{
    u32 valmask = 0;
    u32 count = 0;

    u32 value;

    if((offset % 4) == 0)
        value = readl(OOB_MAC_BASE_ADDR + offset);
    else if((offset % 2) == 0)
        value = readw(OOB_MAC_BASE_ADDR + offset);
    else
        value = readb(OOB_MAC_BASE_ADDR + offset);

    for (count = 0; count < width; count++)
        valmask |= ( 1 << (count+field));

    value = ((value & valmask) >> field);

    return value;
}

/* clear interrupt status register */
static inline void ehci_clear_isr(void)
{
    rtl8117_ehci_writel(ehci_readl(EHCI_ISR) & 0xFFFFFFF7, EHCI_ISR);
}

/* disable interrupt */
static inline void ehci_disable_irq(void)
{
    rtl8117_ehci_writel(0x00000000, EHCI_IMR);
}

/* enable interrupt */
static inline void ehci_enable_irq(void)
{
#ifndef CONFIG_USB_REDIRECTION
    rtl8117_ehci_writel(0xE00003F7, EHCI_IMR);
#else
    rtl8117_ehci_writel(0xE00003F7, EHCI_IMR);
#endif
}

typedef struct _qhdr
{
    unsigned int cmd;
    unsigned int length;
    unsigned char  tip[4];
    unsigned short option;
    unsigned short port;
    unsigned char contype;
} QHdr;
int ktcp_start(QHdr *qhdr);
int ktcp_send(struct socket *sock,char *buf,int len);
int ktcp_recv(struct socket *sock, unsigned char *buf, int len);
void ktcp_stop(void);
#endif
enum {DISABLED= 0, DISCONNECTED, LISTENED, ACCEPTED, ENABLED, CONNECTED};
#define BIT_DISABLE_LINK_CHG 0x04
#define MAC_OOBREG                0x0128
enum{
    LCB_USB = 0,
    LCB_KVM,
};
extern unsigned int Link_change_blocker;
static inline void disable_link_change(int source){
    Link_change_blocker |= (1<<source);

    if(Link_change_blocker)
        bsp_bits_set(MAC_OOBREG, 1, BIT_DISABLE_LINK_CHG, 1);
}

static inline void enable_link_change(int source){
    Link_change_blocker &= ~(1<<source);

    if(!Link_change_blocker)
        bsp_bits_set(MAC_OOBREG, 0, BIT_DISABLE_LINK_CHG, 1);
}
#endif
