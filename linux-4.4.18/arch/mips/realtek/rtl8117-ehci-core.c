#include <linux/types.h>
#include <linux/pci.h>
#include <linux/io.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/usb.h>
#include <rtl8117_platform.h>
#include <vnc-8117.h>
#include "rtl8117-ehci.h"

#ifdef CONFIG_USB_REDIRECTION
extern bool usb_redirect;
u8 INT1DescINIndex=0,INT2DescINIndex=0;
#define INTIN1desNumber					1	//Maximal INT IN 1descriptor number
#define INTIN2desNumber					1	//Maximal INT IN 2 descriptor number
#endif
extern struct rtl8117_ehci_device rtl8117_ehci_dev;
extern struct VncInfo *VNCInfo;
extern unsigned char * VNCUSBbuf;
extern void show_register(int);
struct rtl8117_ehci_core ehci_core;


atomic_t bulkout_bh_bv = {0};         /* bit vector of tracking free bulkout descriptors
                                         which have entered button half */
spinlock_t bulkout_lock = __SPIN_LOCK_UNLOCKED(bulkout_lock);


//OOB writes IB IO
void Wt_IBIO(u8 phyaddr,u32 Wt_Data)
{
    u32  temp_count;
    u8 offset;
    offset = phyaddr;

    rtl8117_ehci_writel(Wt_Data, OOBACTDATA);
    rtl8117_ehci_writel(0x8000F000|offset, OOBACTADDR);

    temp_count=0;
    while((ehci_readl(OOBACTADDR)>>31)==1 && temp_count<1000)
        temp_count++;
}

//OOB writes IB PCIE configuration space
void Wt_PCIECS(u8 phyaddr,u32 Wt_Data)
{
    u8  temp_count;
    u8 offset;
    offset = phyaddr;

    rtl8117_ehci_writel(Wt_Data, DBIACTDATA);
    rtl8117_ehci_writel(0x8000F000|offset, DBIACTADDR);

    temp_count=0;

    while((ehci_readl(DBIACTADDR)>>31)==1&&temp_count<1000)
    {
        temp_count++;
    };
}
#ifdef CONFIG_USB_REDIRECTION
void rtl8117_ehci_init_hid(void)
{
    u8 i;
    outindesc_r *intin1desc =NULL;
    outindesc_r *intin2desc = NULL;

    printk(KERN_INFO "[EHCI] enter rtl8117_ehci_init_hid\n");

    intin1desc =  (outindesc_r *)(ehci_core.inter_inbuf1_addr);
    for (i = 0; i < INTIN1desNumber; i++) {
        /* prevent hardware overwrite each other */
        if (i == INTIN1desNumber-1)
            intin1desc->eor = 1;	/* last descriptor */
        else
            intin1desc->eor = 0;
        intin1desc->own = 0;
        intin1desc++;
    }
    rtl8117_ehci_writel(ehci_core.inter_inbuf1_phy, INTINDESCADDR1);

    intin2desc = (outindesc_r *)(ehci_core.inter_inbuf2_addr);
    for (i = 0; i < INTIN2desNumber; i++) {
        if (i == INTIN2desNumber-1)
            intin2desc->eor = 1;	/* last descriptor */
        else
            intin2desc->eor = 0;
        intin2desc->own = 0;
        intin2desc++;
    }
    rtl8117_ehci_writel(ehci_core.inter_inbuf2_phy, INTINDESCADDR2);

    rtl8117_ehci_writel(0x40000000, TIMEOUTCFG);
    rtl8117_ehci_writel(0x00000054, ENDPOINT_REG);
}

void rtl8117_ehci_init_bulk(void)
{
    outindesc_r *outdesc = (outindesc_r *)(ehci_core.bulk_outdesc_addr);
    outindesc_r *indesc = (outindesc_r *)(ehci_core.bulk_indesc_addr);
    int i;

    printk(KERN_INFO "[EHCI] enter rtl8117_ehci_init_bulk\n");


    ehci_core.bulk_state = EHCI_STATE_BULK_CBW;

    /* for bulk out descriptors, OUTdescNumber=4 */
    bulkout_bh_bv.counter = 0x00UL;
    for (i = 0; i < OUTdescNumber; i++) {
        outdesc->length = MAX_BULK_LEN;
        outdesc->bufaddr = ehci_core.bulk_outbuf_phy + (MAX_BULK_LEN * i);

        if (i == OUTdescNumber-1)
            outdesc ->eor = 1;//last descriptor
        else
            outdesc ->eor = 0;

        outdesc->own = 1;

        outdesc++;
    }

    /* for bulk IN descriptors, INdescNumber=4 */
    for (i = 0; i < INdescNumber; i++) {
        /* only set the eor */
        if (i == INdescNumber-1)
            indesc->eor  = 1;
        else
            indesc->eor  = 0;
        indesc->own = 0;
        indesc++;
    }

    /* assign out descriptor address to register */
    rtl8117_ehci_writel(ehci_core.bulk_outdesc_phy, OUTDesc_Addr);

    /*assign in descriptor address to register */
    rtl8117_ehci_writel(ehci_core.bulk_indesc_phy, INDesc_Addr);
}
#endif

void rtl8117_ehci_init(void)
{
    outindesc_r *ctloutdesc = (outindesc_r *)(ehci_core.ctl_outdesc_addr);
    outindesc_r *ctlindesc = (outindesc_r *)(ehci_core.ctl_indesc_addr);
    int i;

    printk(KERN_INFO "[EHCI] enter rtl8117_ehci_init\n");


    rtl8117_ehci_writel(0x00000000, EHCI_IMR);

    /* Reset Index */
    ehci_core.ctl_outdesc_index=0;
    ehci_core.ctl_indesc_index=0;
    ehci_core.bulk_outdesc_index=0;
    ehci_core.bulk_indesc_index=0;
    INT1DescINIndex=0;
    INT2DescINIndex=0;

    rtl8117_ehci_dev.ep0_status = EP0_IDLE;
    rtl8117_ehci_dev.hid_state = DISABLED;

    /* there are 3 downstream port, for 3 port modify*/
    Wt_IBIO(HCSPARAMS,0x00000003);

    rtl8117_ehci_writel(0x00000000, EHCICONFIG);
    rtl8117_ehci_writel(0x0002001B, EHCICONFIG);


    /* Reset Control Index */
    ehci_core.ctl_outdesc_index=0;
    ehci_core.ctl_indesc_index=0;

    /* for control out descriptor */
    for(i = 0; i < CTLOUTdescNumber; i++) {
	ctloutdesc->length = 0x40;
	ctloutdesc->bufaddr = ehci_core.ctl_outbuf_phy + i*0x40;

	if(i == CTLOUTdescNumber-1)
	    ctloutdesc->eor = 1;
	else
	    ctloutdesc->eor = 0;

	ctloutdesc->own = 1;
	ctloutdesc++;
    }

    /* for control in descriptor */
    for(i = 0; i < CTLINdescNumber; i++) {
	//only set the eor
	if (i==CTLINdescNumber-1)
	    ctlindesc->eor = 1;
	else
	    ctlindesc->eor = 0;
	ctlindesc++;
    }


    /* assign control out descriptor address to register */
    rtl8117_ehci_writel(ehci_core.ctl_outdesc_phy, CTLOUTDesc_Addr);

    /*assign control in descriptor address to register */
    rtl8117_ehci_writel(ehci_core.ctl_indesc_phy, CTLINDesc_Addr);


#ifdef CONFIG_USB_REDIRECTION
    ehci_core.imr = 0xE0030FF7;
#else
    ehci_core.imr = 0x60000FF7;
#endif

    rtl8117_ehci_writel(ehci_core.imr, EHCI_IMR);

    /* enable OUT transaction state mechine */
    rtl8117_ehci_writel(ehci_readl(EHCICONFIG) | 0x00010000, EHCICONFIG);

    printk(KERN_INFO "[EHCI] rtl8117_ehci_init done\n");
}

int rlt8117_ehci_core_init(void *platform_pdev)
{
    struct platform_device *pdev = (struct platform_device *)platform_pdev;
    struct device *d = &pdev->dev;

    printk(KERN_INFO "[EHCI] enter ehci_usb_enabled\n");

    ehci_core.pdev = pdev;
    ehci_core.ctl_outdesc_addr = dma_alloc_coherent(d, 0x200, &ehci_core.ctl_outdesc_phy, GFP_ATOMIC);
    if (!ehci_core.ctl_outdesc_addr) {
        printk(KERN_CRIT "[EHCI] ctl_outdesc_addr is null\n");
        return -1;
    }

    ehci_core.ctl_indesc_addr = (ehci_core.ctl_outdesc_addr + 0x040);
    ehci_core.bulk_outdesc_addr = (ehci_core.ctl_outdesc_addr + 0x080);
    ehci_core.bulk_indesc_addr = (ehci_core.ctl_outdesc_addr + 0x0C0);

    ehci_core.ctl_indesc_phy = (ehci_core.ctl_outdesc_phy + 0x040);
    ehci_core.bulk_outdesc_phy = (ehci_core.ctl_outdesc_phy + 0x080);
    ehci_core.bulk_indesc_phy = (ehci_core.ctl_outdesc_phy + 0x0C0);

    ehci_core.ctl_outbuf_addr = kmalloc(0x40*CTLOUTdescNumber, GFP_ATOMIC);
    if (!ehci_core.ctl_outbuf_addr) {
        printk(KERN_CRIT "[EHCI] ctl_outbuf_addr is null\n");
        return -1;
    }

    ehci_core.ctl_outbuf_phy = dma_map_single(d, ehci_core.ctl_outbuf_addr, 0x40*CTLOUTdescNumber, DMA_FROM_DEVICE);
    if (!ehci_core.ctl_outbuf_phy) {
        printk(KERN_CRIT "[EHCI] ctl_outbuf_phy is null\n");
        return -1;
    }

    ehci_core.bulk_outbuf_addr = dma_alloc_coherent(d, MAX_BULK_LEN*OUTdescNumber, &ehci_core.bulk_outbuf_phy, GFP_ATOMIC);
    if (!ehci_core.bulk_outbuf_addr) {
        printk(KERN_CRIT "[EHCI] bulk_outbuf_addr is null\n");
        return -1;
    }
#ifdef CONFIG_USB_REDIRECTION
    ehci_core.inter_inbuf1_addr = dma_alloc_coherent(d, 0x10*INTIN1desNumber, &ehci_core.inter_inbuf1_phy, GFP_ATOMIC);
    if (!ehci_core.inter_inbuf1_addr) {
        printk(KERN_CRIT "[EHCI] inter_inbuf1_addr is null\n");
        return -1;
    }
    ehci_core.inter_inbuf2_addr = dma_alloc_coherent(d, 0x10*INTIN2desNumber, &ehci_core.inter_inbuf2_phy, GFP_ATOMIC);
    if (!ehci_core.inter_inbuf2_addr) {
        printk(KERN_CRIT "[EHCI] inter_inbuf2_addr is null\n");
        return -1;
    }

#endif

    rtl8117_ehci_init();
    return 0;
}

// ehci engine reset function
int EHCI_RST(void)
{
    u32 ehcirst,count;

    count=0;
    ehcirst = 0x00800000;

    rtl8117_ehci_writel(ehcirst, EHCICONFIG);

    do
    {
        rtl8117_ehci_writel(ehcirst, EHCICONFIG);
        ehcirst = ehci_readl(EHCICONFIG);

        if (++count > 1000)
        {
            return 1;
        }
    }
    while ( !(ehcirst & 0x00800000) );	/* ehci firmware reset */
    return 0;
}

void ehci_usb_disabled(void)
{
    Wt_IBIO(PORTSC, 0x1000);
    Wt_IBIO(PORTSC+4, 0x1000);
    //Wt_IBIO(PORTSC+8, 0x1000);
    //reset ehci to avoid cmdsts always 0x80 @usb disconnect
    //EHCI_RST();
    mdelay(1000);
}

/* reset usb state for recover */
void ehci_usb_recovery(void)
{
    printk("[EHCI] invalid operations or status, try recovery\n");

    EHCI_RST();
    rtl8117_ehci_init();

    if (rtl8117_ehci_dev.in_use_hid) {
        rtl8117_ehci_dev.hid_address= 0;
        rtl8117_ehci_dev.kb_hid_enabled = false;
        rtl8117_ehci_dev.mouse_hid_enabled = false;
	Wt_IBIO(PORTSC+4, 0x1000);
    }

    if (rtl8117_ehci_dev.in_use_redirect){
        rtl8117_ehci_dev.msd_address= 0;
	Wt_IBIO(PORTSC, 0x1000);
    }

    if (rtl8117_ehci_dev.in_use_hid) {
	Wt_IBIO(PORTSC+0x04, 0x1001);
	rtl8117_ehci_init_hid();
    }

    if (rtl8117_ehci_dev.in_use_redirect) {
	Wt_IBIO(PORTSC, 0x1001);
	rtl8117_ehci_init_bulk();
    }
}

u32 Rd_IBIO(u8 phyaddr)
{
    u32  temp_count;
    u8  offset;
    //default error code
    u32  Rd_Data = 0xDEADBEAF;
    offset = phyaddr;

    rtl8117_ehci_writel(0x0000F000|offset, OOBACTADDR);
    temp_count=0;

    while((ehci_readl(OOBACTADDR)>>31)==0 && temp_count<1000)
    {
        Rd_Data = ehci_readl(OOBACTDATA);
        temp_count++;
    };

    return ehci_readl(OOBACTDATA);
}

//EHCI Bulk IN transfer
void rtl8117_ehci_ep_start_transfer(u32 len, u8 *addr, bool stall)
{
    struct device *d = &ehci_core.pdev->dev;

    outindesc_r *indesc = (outindesc_r *)(ehci_core.bulk_indesc_addr)+ehci_core.bulk_indesc_index;

    if (Rd_IBIO(PORTSC) != VALID_PORTSC || !rtl8117_ehci_dev.in_use_redirect) {
	printk("[EHCI] drop Bulk-In transfer during disconnecting\n");
	return;
    }

    {
        //if already stalled, just return
        while((ehci_readl(CMDSTS) & 0x00000080))
            ;

        indesc->length = len; // buffer size = 4kbytes
        indesc->ls  = 1;
        indesc->fs  = 0;//first segement

        if (stall) {
            indesc->stall = 1;
        }
        else {
            indesc->bufaddr = virt_to_phys(addr);
            if (len)
                dma_sync_single_for_device(d, indesc->bufaddr, len, DMA_TO_DEVICE);
            indesc->stall = 0;
        }

        indesc->own = 1;

        rtl8117_ehci_writel(ehci_readl(CMDSTS) | 0x00000080, CMDSTS);
        ehci_core.bulk_indesc_index = ( ehci_core.bulk_indesc_index + 1 ) % INdescNumber;
    }
}


#define __upolling_timeout(cond, to, freq)           \
    do{\
        int _cnt = 0;\
        while(cond){\
            if(_cnt++ < (to)){\
                udelay(freq);\
            }else{\
                return;\
            }\
        }\
    }while(0)

//EHCI control IN transfer
void rtl8117_ehci_ep0_start_transfer(u16 len, u8 *addr, u8 is_in, bool stall)
{
    struct device *d = &ehci_core.pdev->dev;

    //u8 *ctloutaddr,*ctlinaddr;
    outindesc_r *ctlindesc = (outindesc_r *)(ehci_core.ctl_indesc_addr)+ehci_core.ctl_indesc_index;
    outindesc_r *ctloutdesc;

    ///u32 tcounter=0;
    if (is_in)
    {
        ehci_core.ctl_indesc_index = ( ehci_core.ctl_indesc_index + 1 ) % CTLINdescNumber;
        ctlindesc->bufaddr = virt_to_phys((void*)addr);
        dma_sync_single_for_device(d, ctlindesc->bufaddr, len?len:0x40, DMA_TO_DEVICE);

        // 3 sec timeout, NOTE: doing printk/delay here will block the transfer
        __upolling_timeout((ehci_readl(CMDSTS)&0x00000001)!=0, 3000000, 1);

        if(stall == 1)
        {
            ctlindesc->stall = 1;//set stall
            rtl8117_ehci_dev.IN_stall = 0;
        }
        else
            ctlindesc->stall = 0;//clear stall

        ctlindesc->ls = 1;//last segment
        ctlindesc->length = len;
        ctlindesc->own= 1;

        //command status register
        //bit0: control in descriptor polling queue
        rtl8117_ehci_writel(ehci_readl(CMDSTS) | 0x00000001, CMDSTS);

        // 3 sec timeout
        __upolling_timeout((ehci_readl(CMDSTS)&0x00000001)!=0, 3000000, 1);

	rtl8117_ehci_dev.ep0_status = EP0_IDLE;
    }else{
        printk("control out desc idx:%d\n", ehci_core.ctl_outdesc_index);

        // outdesc bufaddr has been init in ehci_init
        ehci_core.ctl_outdesc_index = ( ehci_core.ctl_outdesc_index + 1 ) % CTLOUTdescNumber;
        ctloutdesc = (outindesc_r *)(ehci_core.ctl_outdesc_addr)+ehci_core.ctl_outdesc_index;

        ctloutdesc->length = 0x40;
        ctloutdesc->own = 1;
        rtl8117_ehci_writel(0x00000002, EHCI_ISR);
    }
}

//recycle control out descriptor
void recycle_ctloutdesc(outindesc_r *ctloutdesc)
{
    ctloutdesc->length = 0x40;
    ctloutdesc->own = 1;
    ehci_core.ctl_outdesc_index = (ehci_core.ctl_outdesc_index + 1) % (CTLOUTdescNumber);
    ctloutdesc = (outindesc_r *)ehci_core.ctl_outdesc_addr + ehci_core.ctl_outdesc_index;
}

#ifdef CONFIG_USB_REDIRECTION
void rtkehci_interep_transfer(u8 len, u8 *addr, u8 is_in, u8 portnum)
{
    outindesc_r *intindesc ;
//    INT32U tcounter=0;
    u32 count = 0;
    if (portnum == 1)
    {
        intindesc = (outindesc_r *)(ehci_core.inter_inbuf1_addr)+INT1DescINIndex;
        INT1DescINIndex = ( INT1DescINIndex + 1 ) % INTIN1desNumber;
    }
    else if (portnum == 2)
    {
        intindesc = (outindesc_r *)(ehci_core.inter_inbuf2_addr)+INT2DescINIndex;
        INT2DescINIndex = ( INT2DescINIndex + 1 ) % INTIN2desNumber;
    }

    if (is_in)
    {
        while(((ehci_readl(CMDSTS)&(0x00000001<<portnum))!=0))
        {
            //prevent to cmdsts no change @poweroff state tomadd 2016/11/15
            if(!bsp_bits_get(MAC_MAC_STATUS, BIT_ISOLATE, 1))
            {
                return;
            }

            if (count < 300)
            {
                mdelay(10);
                count++;
            }
            else
            {
                printk("rtkehci_interep_transfer timeout, port:%hhu, portsc1:0x%x\n",
		       portnum, Rd_IBIO(PORTSC+0x04));
                return;
            }
        }

        //while(intindesc->own)
        //{}
        //printk("exit from while loop.\n");

        intindesc->length = len;
        intindesc->ls = 1;
        intindesc->fs  = 0;//first segement

        if(rtl8117_ehci_dev.IN_stall)
        {
            intindesc->stall = 1;//set stall
        }
        else
        {
            intindesc->bufaddr = (dma_addr_t) VA2PA(addr);
            intindesc->stall = 0;//clear stall
        }

        intindesc->own= 1;
        rtl8117_ehci_writel(ehci_readl(CMDSTS)|(0x00000001<<portnum), CMDSTS);
    }
}

#endif


inline static
void __activate_bulkout_desc(int index, u32 len, u8 *addr)
{
    outindesc_r *outdesc= (outindesc_r *)(ehci_core.bulk_outdesc_addr)+index;

    outdesc->length = len;
    outdesc->own = 1;
    udelay(5);

    dev_msg("\tactivate outdesc index:%d, curr:%d, bv:%x, own:%d-%d-%d-%d, busy:%d\n",
            index, ehci_core.bulk_outdesc_index,
            bulkout_bh_bv.counter,
            ((outindesc_r *) (ehci_core.bulk_outdesc_addr)+3)->own,
            ((outindesc_r *) (ehci_core.bulk_outdesc_addr)+2)->own,
            ((outindesc_r *) (ehci_core.bulk_outdesc_addr)+1)->own,
            ((outindesc_r *) (ehci_core.bulk_outdesc_addr))->own,
            bulkout_bh_bv.counter & (0x01<<ehci_core.bulk_outdesc_index)
          );
}

void activate_bulkout_desc(int index, u32 len, u8 *addr)
{
    spin_lock(&bulkout_lock);
    bulkout_bh_bv.counter &= ~(0x01<<index);
    __activate_bulkout_desc(index, len, addr);

    /*
       Enable and clear bulkout desc unavailable interrupt({EHCI_IMR|EHCI_ISR}:3),
       the interrupt should be triggered if bulk out transaction done.
     */
    rtl8117_ehci_writel(ehci_readl(EHCI_IMR)|0x00000008, EHCI_IMR);
    rtl8117_ehci_writel(0x00000008, EHCI_ISR);
    spin_unlock(&bulkout_lock);
}


#define PORT_RST_INTERVAL	8
#define TRACKING_PORT		2
#define PORT_RETRY		3
#define RESET_RETRY		3

static
int check_port_error(int port)
{
    static u8 prst[TRACKING_PORT] = {0};
    static u8 major_err = 0;
    static struct timeval prev_prst[TRACKING_PORT] = {{0}};
    struct timeval now = {0};


    do_gettimeofday(&now);

    printk("check port error: now:%ld:%ld, prev:%ld:%ld, port:%d, prst:%d, major:%d\n",
	   now.tv_sec, now.tv_usec,
	   prev_prst[port].tv_sec, prev_prst[port].tv_usec,
	   port, prst[port], major_err);

    if ((now.tv_sec - prev_prst[port].tv_sec) < PORT_RST_INTERVAL) {
	if (++prst[port] >= PORT_RETRY) {
	    prst[port] = 0;

	    if (++major_err >= RESET_RETRY) {
		major_err = 0;
		return 1;
	    } else {
		Wt_IBIO(PORTSC + (0x04 * port), 0x1000);
		mdelay(5);
		Wt_IBIO(PORTSC + (0x04 * port), 0x1001);
	    }
	}
    } else {
	prst[port] = 0;
	major_err = 0;
    }

    prev_prst[port] = now;
    return 0;
}


bool rtl8117_ehci_intr_handler(void)
{
    static u32 cnt=0;
    u32 OUTSTS;
    u32 packetlen;

    int IMR_val;
    struct device *d = &ehci_core.pdev->dev;

    //bulk out descriptor address
    outindesc_r *outdesc = (outindesc_r *) (ehci_core.bulk_outdesc_addr)+ehci_core.bulk_outdesc_index;

    //control out descriptor address
    outindesc_r *ctloutdesc;

    //disable IMR
    rtl8117_ehci_writel(0x00000000, EHCI_IMR);
    OUTSTS = ehci_readl(EHCI_ISR);

    //clear ISR
    rtl8117_ehci_writel(OUTSTS & 0xFFFFFFF7, EHCI_ISR);

    IMR_val = ehci_core.imr;


    dev_msg("Enter EHCI interrupt handler\n");
    cnt++;

#ifdef CONFIG_USB_REDIRECTION
    if (OUTSTS & 0x00010000)
    {
            //printk("keyboard int !!!\n");
            if((rtl8117_ehci_dev.hid_state != DISABLED) && (rtl8117_ehci_dev.hid_state != DISCONNECTED))
            {
                rtkehci_interep_transfer(8, VNCUSBbuf, 1, 1);
            }
    }
#endif

    //host controller reset done interrupt
    if(OUTSTS & 0x00000080)
    {
        //printk(KERN_INFO "[EHCI] reset ehci done\n");
        //show_register();
        rtl8117_ehci_writeb(0, DEVICE_ADDRESS);
        rtl8117_ehci_writeb(0, DEVICE_ADDRESS+1);
        rtl8117_ehci_writeb(0, DEVICE_ADDRESS+2);

        /* to fix UEFI issue */
        if ((rtl8117_ehci_dev.usb_state != DISCONNECTED) && (rtl8117_ehci_dev.usb_state != DISABLED))
            Wt_IBIO(PORTSC, 0x1000);
        if ((rtl8117_ehci_dev.hid_state != DISCONNECTED) && (rtl8117_ehci_dev.hid_state != DISABLED))
            Wt_IBIO(PORTSC+0x04, 0x1000);
        rtl8117_ehci_dev.msd_address= 0;
        rtl8117_ehci_dev.hid_address= 0;
        rtl8117_ehci_dev.kb_hid_enabled = false;
        rtl8117_ehci_dev.mouse_hid_enabled = false;
        printk("[EHCI] interrupt handler reset ehci\n");
        EHCI_RST();
        rtl8117_ehci_init();

        if ((rtl8117_ehci_dev.usb_state != DISCONNECTED) &&
	    (rtl8117_ehci_dev.usb_state != DISABLED)) {
            //printk("usb_state is not DISCONNECTED and DISABLED.\n");
	    rtl8117_ehci_init_bulk();
            Wt_IBIO(PORTSC, 0x1001);
        }
        if(VNCInfo->HwVNCEnable == 1)
        {
	    rtl8117_ehci_init_hid();
            Wt_IBIO(PORTSC+0x04, 0x1001);
        }
        //show_register();
    }

    //host port 0 reset interrupt
    if(OUTSTS & 0x00000200)
    {
	printk("[EHCI] port 0 reset: %x, in use:%d\n",
	       Rd_IBIO(PORTSC), rtl8117_ehci_dev.in_use_redirect);

	rtl8117_ehci_writeb(0, (DEVICE_ADDRESS));
	rtl8117_ehci_dev.msd_address = 0;

	if(check_port_error(0))
	    goto error;
    }

    //host port 0 reset done interrupt
    if(OUTSTS & 0x00000100)
    {
	printk("[EHCI] port 0 reset done: %x\n", Rd_IBIO(PORTSC));
    }

    //host port 1 reset interrupt
    if(OUTSTS & 0x00000800)
    {
	printk("[EHCI] port 1 reset: %x, state:%d:%d, in use:%d\n",
	       Rd_IBIO(PORTSC + 0x04),
	       rtl8117_ehci_dev.kb_hid_enabled,
	       rtl8117_ehci_dev.mouse_hid_enabled,
	       rtl8117_ehci_dev.in_use_hid);

	rtl8117_ehci_writeb(0, (DEVICE_ADDRESS + 1));
	rtl8117_ehci_writeb(0, (DEVICE_ADDRESS + 2));
	rtl8117_ehci_dev.hid_address = 0;
        rtl8117_ehci_dev.kb_hid_enabled = false;
        rtl8117_ehci_dev.mouse_hid_enabled = false;

	if(check_port_error(1))
	    goto error;
    }

    //host port 1 reset done interrupt
    if(OUTSTS & 0x00000400)
    {
	printk("[EHCI] port 1 reset done: %x\n", Rd_IBIO(PORTSC + 0x04));
    }

    if((OUTSTS & 0x0000003) == 0x0000003)
    {
	printk("*** %u:CTRL DESC AMBIGUITY at status %d!!\n", cnt,
	       rtl8117_ehci_dev.ep0_status);
    }

    //Bit0 =1, setup token
    if(OUTSTS & 0x00000001)
    {
	ctloutdesc = (outindesc_r *)(ehci_core.ctl_outdesc_addr) + ehci_core.ctl_outdesc_index;

	printk("%u>> setup token, portsc0:%x, portsc1:%x, devaddr:%x, DEVICE ADDR:%06x, ctrl idx:%d, own [%d:%d:%d:%d]\n",
	       cnt,
	       Rd_IBIO(PORTSC),
	       Rd_IBIO(PORTSC+0x04),
	       ctloutdesc->devaddr,
	       ehci_readl(DEVICE_ADDRESS),
	       ehci_core.ctl_outdesc_index,
	       ((outindesc_r *)(ehci_core.ctl_outdesc_addr))->own,
	       ((outindesc_r *)(ehci_core.ctl_outdesc_addr)+1)->own,
	       ((outindesc_r *)(ehci_core.ctl_outdesc_addr)+2)->own,
	       ((outindesc_r *)(ehci_core.ctl_outdesc_addr)+3)->own
	       );

	if(rtl8117_ehci_dev.ep0_status != EP0_IDLE){
	    goto error;
	}

        // Firmware needs to check out descriptor own bit = 0 or 1
        if(!(ctloutdesc->own))
        {
            dma_rmb();

            dma_sync_single_for_cpu(d, ctloutdesc->bufaddr, 0x40, DMA_FROM_DEVICE);

	    /* determine portnum */
	    if (ctloutdesc->devaddr) {
		if ((rtl8117_ehci_dev.msd_address != 0)
		    && (ctloutdesc->devaddr == rtl8117_ehci_dev.msd_address)) {
		    rtl8117_ehci_dev.portnum  = 0;
		} else if ((rtl8117_ehci_dev.hid_address != 0)
			   && (ctloutdesc->devaddr ==
			       rtl8117_ehci_dev.hid_address)){
		    rtl8117_ehci_dev.portnum  = 1;
		} else {
		    printk("*** devaddr: 0x%x, hidaddr: 0x%x, msdaddr: 0x%x, in_use:%d:%d\n",
			   ctloutdesc->devaddr,
			   rtl8117_ehci_dev.hid_address,
			   rtl8117_ehci_dev.msd_address,
			   rtl8117_ehci_dev.in_use_redirect,
			   rtl8117_ehci_dev.in_use_hid
			   );
		    rtl8117_ehci_dev.portnum = 2;
		}
	    } else {
		if (rtl8117_ehci_dev.in_use_redirect &&
		    rtl8117_ehci_dev.msd_address == 0 &&
		    Rd_IBIO(PORTSC) == VALID_PORTSC) {
		    rtl8117_ehci_dev.portnum  = 0;
		} else if (rtl8117_ehci_dev.in_use_hid &&
			   rtl8117_ehci_dev.hid_address == 0 &&
			   Rd_IBIO(PORTSC + 0x04) == VALID_PORTSC) {
		    rtl8117_ehci_dev.portnum  = 1;
		} else {
		    printk("*** devaddr: 0x%x, hidaddr: 0x%x, msdaddr: 0x%x, in_use:%d:%d\n",
			   ctloutdesc->devaddr,
			   rtl8117_ehci_dev.hid_address,
			   rtl8117_ehci_dev.msd_address,
			   rtl8117_ehci_dev.in_use_redirect,
			   rtl8117_ehci_dev.in_use_hid
			   );
		    rtl8117_ehci_dev.portnum = 2;
		}
	    }

	    /* workaround for PORTSC change and Setup token race */
	    if (rtl8117_ehci_dev.portnum != 2) {
		rtl8117_ehci_control_request((struct usb_ctrlrequest *)(phys_to_virt(ctloutdesc->bufaddr)));
	    } else {
		goto error;
	    }

            dma_sync_single_for_device(d, ctloutdesc->bufaddr, 0x40, DMA_TO_DEVICE);

            recycle_ctloutdesc(ctloutdesc);
        }
    }

    //Bit1 =1,  control out
#if 1
    if(OUTSTS & 0x00000002)
    {
	ctloutdesc = (outindesc_r *)(ehci_core.ctl_outdesc_addr) + ehci_core.ctl_outdesc_index;

        //REG32(EHCI_ISR)=REG32(EHCI_ISR)&0x00000002;
        // Firmware needs to check out descriptor own bit = 0 or 1
        if(!(ctloutdesc->own))
        {
            //packetlen = 0x40-ctloutdesc->stoi.length;
            // or call IN transaction function.
            //if(packetlen == 0x00)
            //	setup_phase();//parsing usb2.0 chap 9 pattern...

            // recycle outdescriptor
            //ctloutdesc->stoi.length = 0x40; // write back to total bytes to transfer
            //ctloutdesc->stoi.own = 1;             // wrtie back to ownbit
            handle_ep0();
            recycle_ctloutdesc(ctloutdesc);
        }
    }
#endif

    //Bit4 =1, bulk out transaction, host to device(platform to 8117)
    if(OUTSTS & 0x00000010)
    {
        dma_rmb();

        dev_msg("outdesc index:%d, own:%d-%d-%d-%d, free size:%d\n",
                ehci_core.bulk_outdesc_index,
                ((outindesc_r *) (ehci_core.bulk_outdesc_addr)+3)->own,
                ((outindesc_r *) (ehci_core.bulk_outdesc_addr)+2)->own,
                ((outindesc_r *) (ehci_core.bulk_outdesc_addr)+1)->own,
                ((outindesc_r *) (ehci_core.bulk_outdesc_addr))->own,
                outdesc->length
              );

        if(!(outdesc->own)) {

            // remove the bulkout unavailable isr
            OUTSTS &= ~0x00000008;

            // keep tracking bottom-half tasks
            bulkout_bh_bv.counter |= (0x01<<ehci_core.bulk_outdesc_index);

            dma_sync_single_for_cpu(d, outdesc->bufaddr, MAX_BULK_LEN, DMA_FROM_DEVICE);
            packetlen = MAX_BULK_LEN - outdesc->length;
            rtl8117_ehci_bulkout_request(phys_to_virt(outdesc->bufaddr), packetlen);

            /* next Bulk out index */
            ehci_core.bulk_outdesc_index++;
            if(ehci_core.bulk_outdesc_index >= OUTdescNumber)
                ehci_core.bulk_outdesc_index = 0;
        }else{
            //dev_msg("\tbulkout desc own bit status mismatch, index:%d, bv:%x, own:%d-%d-%d-%d, free size:%d-%d-%d-%d\n",
            dev_msg("\tbulkout desc own bit status mismatch, index:%d, bv:%x, own:%d, free size:%d\n",
                    ehci_core.bulk_outdesc_index,
                    bulkout_bh_bv.counter,
                    //((outindesc_r *) (ehci_core.bulk_outdesc_addr)+3)->own,
                    //((outindesc_r *) (ehci_core.bulk_outdesc_addr)+2)->own,
                    //((outindesc_r *) (ehci_core.bulk_outdesc_addr)+1)->own,
                    ((outindesc_r *) (ehci_core.bulk_outdesc_addr))->own,
                    //((outindesc_r *) (ehci_core.bulk_outdesc_addr)+3)->length,
                    //((outindesc_r *) (ehci_core.bulk_outdesc_addr)+2)->length,
                    //((outindesc_r *) (ehci_core.bulk_outdesc_addr)+1)->length,
                    ((outindesc_r *) (ehci_core.bulk_outdesc_addr))->length
                  );
        }
    }

    //Bit2: control out descriptor unavailable
    //Bit3: bulk out descriptor unavailable
    //Bit5: bulk in descriptor unavailable
    //Bit6: control in descriptor unavailable
    if(OUTSTS & 0x00000004)
    {
        //process control out descriptor unavailable
    }

    if(OUTSTS & 0x00000008)
    {
        //process bulk out descriptor unavailable

        dev_msg("bulkout desc is unavailable: idx:%d, bv:%x, own:%d, busy:%d, free size:%d\n",
                ehci_core.bulk_outdesc_index,
                bulkout_bh_bv.counter,
                ((outindesc_r *) (ehci_core.bulk_outdesc_addr))->own,
                bulkout_bh_bv.counter & (0x01<<ehci_core.bulk_outdesc_index),
                ((outindesc_r *) (ehci_core.bulk_outdesc_addr))->length
               );

        if(outdesc->own){
            IMR_val &= ~0x00000008;

            dev_msg("\tbulkout desc unavailable when own bit is set, free:%d\n",
                    outdesc->length);
        }else{
            if((bulkout_bh_bv.counter & (0x01<<ehci_core.bulk_outdesc_index))){
                IMR_val &= ~0x00000008;

                dev_msg("\t** bulkout desc is not yet finished, disable bulkout interrupt %x, config:%x\n",
                        IMR_val, ehci_readl(EHCICONFIG));
            }else{
                if(!outdesc->own){
                    outdesc->length = MAX_BULK_LEN;
                    outdesc->own = 1;
                    dev_msg("\t** set bulkout desc onw bit\n");
                }
            }
        }
    }

    if(OUTSTS & 0x00000020)
    {
        //REG32(EHCI_ISR)=REG32(EHCI_ISR)&0x00000020;
        //process bulk in descriptor unavailable
    }

    if(OUTSTS & 0x00000040)
    {
        //REG32(EHCI_ISR)=REG32(EHCI_ISR)&0x00000040;
        //process control in descriptor unavailable
    }

    rtl8117_ehci_writel(IMR_val, EHCI_IMR);

    dev_msg("Quit EHCI interrupt handler\n");

    return 1;


error:
    ehci_usb_recovery();
    rtl8117_ehci_writel(IMR_val, EHCI_IMR);

    dev_msg("Quit EHCI interrupt handler\n");

    return 1;
}

void rtl8117_ehci_intep_enabled(u8 portnum)
{
    rtl8117_ehci_writeb(0, (DEVICE_ADDRESS + 1));
    rtl8117_ehci_writeb(0, (DEVICE_ADDRESS + 2));

    rtl8117_ehci_init_hid();

    if(portnum == 1) {
        Wt_IBIO(PORTSC+0x04, 0x1001);//port1
    }

    if(portnum == 2) {
        Wt_IBIO(PORTSC+0x08, 0x1001);//port2
    }
    rtl8117_ehci_dev.portnum = portnum;
}

void rtl8117_ehci_intep_disabled(u8 portnum)
{
    if(portnum == 1)
        Wt_IBIO(PORTSC+0x04, 0x1000);//port1
    if(portnum == 2)
        Wt_IBIO(PORTSC+0x08, 0x1000);//port2
    rtl8117_ehci_dev.portnum = 0;

    rtl8117_ehci_writeb(0, (DEVICE_ADDRESS + 1));
    rtl8117_ehci_writeb(0, (DEVICE_ADDRESS + 2));

//    mdelay(10);
}
