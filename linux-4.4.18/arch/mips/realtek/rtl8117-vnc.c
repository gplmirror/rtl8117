#include <linux/types.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/dma-mapping.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/platform_device.h>
#include <rtl8117_platform.h>
#include <linux/of_gpio.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/ioctl.h>
#include <rtl8117_platform.h>
#include <linux/usb.h>
#include <net/sock.h>
#include <net/tcp.h>
#include <linux/kthread.h>
#include <net/inet_connection_sock.h>
#include <net/request_sock.h>
#include <linux/timekeeping.h>
//#include <linux/net.h>
#include <vnc-8117.h>
#include "rtl8117-ehci.h"
#include "../pci/pcie-rtl8117-core.h"
//#include "rtl8117-vnc.h"

//unsigned int Link_change_blocker = 0;
char *data = NULL;
//#define USE_DMA_DMEM        0
#define MAC_BASE_ADDR        0xB2000000
#define MAC_HOSTNAME              0x0140

#define VGA_IOBASE		0xbafb0000	// VGA Slave
#define MAX_SIZE (PAGE_SIZE * 2)   /* max size mmaped to userspace */
#define DEVICE_NAME "mchar"
#define  CLASS_NAME "mogu"

#define MAC_OOBREG 0x0128

#define RMCP_Reset   0x10
#define RMCP_PWR_On  0x11
#define RMCP_PWR_Off 0x12
#define RMCP_PWR_CR  0x13
#define RMCP_HIBER   0x14
#define RMCP_STANDBY 0x15

//#define FB_BASE_ADDR 0x09800000
#define VNCPROTOCOLVER "RFB 003.00R\n"

#define VNC_AUT_NONE 1
#define VNC_AUT_VNC 2
#define VNC_AUT_ALG VNC_AUT_NONE
#define VNC_CHALLENGE_LEN 16
#define VNC_KEY_RECONNECT 1

#define VGA_FB_SIZE 0x00400000
struct socket* vnc_csock;
struct task_struct *fb_update;
static struct class*  class;
static struct device*  device;
static int major;
static char *sh_mem = NULL;

//u32 fb_phy_addr;
//void __iomem *fb_virt_addr;

struct vnc_service
{
    int running;
    struct socket *listen_socket;
    struct task_struct *thread;
    struct task_struct *accept_worker;
} *vnc_ser;

static DEFINE_MUTEX(mchar_mutex);
//static DEFINE_MUTEX(vnc_lock);
void vnc_server_in_kernel(void);
static void try_alloc_fb(void);
static void release_fb(void);
u8 plug_out_count=0;
extern bool usb_redirect;
extern struct rtl8117_ehci_device rtl8117_ehci_dev;
int hid_count = 0;

struct rtl8117_vga_private {
    struct device *dev;
    u8  * fb_virt_addr;
    dma_addr_t fb_mapping;
};
struct rtl8117_vga_private *priv;
extern unsigned char SMBus_Prepare_RmtCtrl(unsigned char MsgType, unsigned char force);
enum _VNCclientStates
{
    PtlVerState = 0x00,
    SecurityState = 0x01,
    ClientInitState = 0x02,
    ClientAuthState = 0x03,
    ServerInitState = 0x10,
    ClientToSrvMsgState = 0x11
} VNCclientStates;


typedef struct _PIXEL_FORMAT
{
    unsigned char bit_per_pixel;
    unsigned char depth;
    unsigned char big_endian_flag;
    unsigned char true_color_flag;
    unsigned short red_max;
    unsigned short green_max;
    unsigned short blue_max;
    unsigned char red_shift;
    unsigned char green_shift;
    unsigned char blue_shift;
    unsigned char padding[3];
} PIXEL_FORMAT;

struct _framebufferPara
{
    unsigned short width;
    unsigned short height;
    PIXEL_FORMAT pf;
    unsigned int name_len;
} FramebufferPara;

struct _framebufferUpdate
{
    unsigned char msg_type;
    unsigned char padding;
    unsigned short numRect;
    unsigned short x;
    unsigned short y;
    unsigned short width;
    unsigned short height;
    unsigned int encoding_type;
} framebufferUpdate;


struct _VNCHIDLastState
{
    short diffx;
    short diffy;
    unsigned short x;
    unsigned short y;
} VNCHIDLastState;

/* see [RFC 6143] */
enum _VNKeysym
{
    VNC_KEY_BACKSPACE           = 0xFF08,
    VNC_KEY_TAB                 = 0xFF09,
    VNC_KEY_ENTER               = 0xFF0D,
    VNC_KEY_ESCAPE              = 0xFF1B,
    VNC_KEY_INSERT              = 0xFF63,
    VNC_KEY_DELETE              = 0xFFFF,
    VNC_KEY_HOME                = 0xFF50,
    VNC_KEY_LEFT_ARROW          = 0xFF51,
    VNC_KEY_UP_ARROW            = 0xFF52,
    VNC_KEY_RIGHT_ARROW         = 0xFF53,
    VNC_KEY_DOWN_ARROW          = 0xFF54,
    VNC_KEY_PAGE_UP             = 0xFF55,
    VNC_KEY_PAGE_DOWN           = 0xFF56,
    VNC_KEY_END                 = 0xFF57,
    VNC_KEY_F1                  = 0xFFBE,
    VNC_KEY_F2                  = 0xFFBF,
    VNC_KEY_F3                  = 0xFFC0,
    VNC_KEY_F4                  = 0xFFC1,
    VNC_KEY_F5                  = 0xFFC2,
    VNC_KEY_F6                  = 0xFFC3,
    VNC_KEY_F7                  = 0xFFC4,
    VNC_KEY_F8                  = 0xFFC5,
    VNC_KEY_F9                  = 0xFFC6,
    VNC_KEY_F10                 = 0xFFC7,
    VNC_KEY_F11                 = 0xFFC8,
    VNC_KEY_F12                 = 0xFFC9,
    VNC_KEY_LEFT_SHIFT          = 0xFFE1,
    VNC_KEY_RIGHT_SHIFT         = 0xFFE2,
    VNC_KEY_LEFT_CTRL           = 0xFFE3,
    VNC_KEY_RIGHT_CTRL          = 0xFFE4,
    VNC_KEY_LEFT_GUI            = 0xFFE7,
    VNC_KEY_RIGHT_GUI           = 0xFFE8,
    VNC_KEY_LEFT_ALT            = 0xFFE9,
    VNC_KEY_RIGHT_ALT           = 0xFFEA,

    /* number pad */
    VNC_KEY_NUM_ENTER           = 0xFF8D,
    VNC_KEY_NUM_0               = 0xFFB0,
    VNC_KEY_NUM_1               = 0xFFB1,
    VNC_KEY_NUM_2               = 0xFFB2,
    VNC_KEY_NUM_3               = 0xFFB3,
    VNC_KEY_NUM_4               = 0xFFB4,
    VNC_KEY_NUM_5               = 0xFFB5,
    VNC_KEY_NUM_6               = 0xFFB6,
    VNC_KEY_NUM_7               = 0xFFB7,
    VNC_KEY_NUM_8               = 0xFFB8,
    VNC_KEY_NUM_9               = 0xFFB9,
    VNC_KEY_NUM_SLASH           = 0xFFAF,
    VNC_KEY_NUM_STAR            = 0xFFAA,
    VNC_KEY_NUM_DASH            = 0xFFAD,
    VNC_KEY_NUM_PLUS            = 0xFFAB,
    VNC_KEY_NUM_DOT             = 0xFFAE,
} VNKeysym;

#define VNC_KEY_NUM_PAD_OFFSET  0xFF80

#define KEY_MODIFIER_LEFT_CTRL      0x01
#define KEY_MODIFIER_LEFT_SHIFT      0x02
#define KEY_MODIFIER_LEFT_ALT      0x04
#define KEY_MODIFIER_LEFT_GUI      0x08
#define KEY_MODIFIER_RIGHT_CTRL   0x010
#define KEY_MODIFIER_RIGHT_SHIFT   0x020
#define KEY_MODIFIER_RIGHT_ALT      0x040
#define KEY_MODIFIER_RIGHT_GUI      0x080

/* ASCII to USB HID translate */
static const char ascii_hid_map[128] = {
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,     // 0x00-0x0f
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,     // 0x10-0x1f
    0x2c, 0x1e, 0x34, 0x20, 0x21, 0x22, 0x24, 0x34,     // 0x20-0x27, {' ', '!', '"', '#', '$', '%', '&', '''}
    0x26, 0x27, 0x25, 0x2e, 0x36, 0x2d, 0x37, 0x38,     // 0x28-0x2f, {'(', ')', '*', '+', ',', '-', '.', '/'}
    0x27, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24,     // 0x30-0x37, {'0', '1', '2', '3', '4', '5', '6', '7'}
    0x25, 0x26, 0x33, 0x33, 0x36, 0x2e, 0x37, 0x38,     // 0x38-0x3f, {'8', '9', ':', ';', '<', '=', '>', '?'}
    0x1f, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a,     // 0x40-0x47, {'@', 'A', 'B', 'C', 'D', 'E', 'F', 'G'}
    0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12,     // 0x48-0x4f, {'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'}
    0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a,     // 0x50-0x57, {'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W'}
    0x1b, 0x1c, 0x1d, 0x2f, 0x31, 0x30, 0x23, 0x2d,     // 0x58-0x5f, {'X', 'Y', 'Z', '[', '\', ']', '^', '_'}
    0x35, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a,     // 0x60-0x67, {'`', 'a', 'b', 'c', 'd', 'e', 'f', 'g'}
    0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12,     // 0x68-0x6f, {'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'}
    0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a,     // 0x70-0x77, {'p', 'q', 'r', 's', 't', 'u', 'v', 'w'}
    0x1b, 0x1c, 0x1d, 0x2f, 0x31, 0x30, 0x35, 0x4c,     // 0x78-0x7f, {'x', 'y', 'z', '{', '|', '}', '~', DEL}
};

#define ALL_SHIFT_MODIFIER      (KEY_MODIFIER_RIGHT_SHIFT | KEY_MODIFIER_LEFT_SHIFT)
#define AUTO_SHIFT              KEY_MODIFIER_LEFT_SHIFT // auto-append shift for upper char and symbol
#define COMPOSE_KEY_MASK        (~ALL_SHIFT_MODIFIER)


typedef struct _HIDKeyReport
{
    char mod;
    char rsv;
    char key[6];
} HIDKeyReport;

HIDKeyReport *hid_kb;

#define KEY_REPORT_MAX    6
#define KEY_REPORT_MASK   0x3f
static unsigned char __key_report_bv__ = 0x00;
static unsigned char __actual_shift__ = 0x00;

typedef struct _VNCPointEvent
{
    unsigned char msg_type;
    unsigned char button_mask;
    unsigned short  x;
    unsigned short	y;
} VNCPointEvent;

typedef struct _VNCKeyEvent
{
    unsigned char msg_type;
    unsigned char down_flag;
    unsigned char padding[2];
    unsigned int key;
} VNCKeyEvent;

//rtk tom report format
typedef struct _HIDMouse_Report
{
    char rID;
    char buttons;
    unsigned short x;
    unsigned short y;
    char wheel;
} HIDMouse_Report;


unsigned char * VNCUSBbuf;
struct VgaInfo *VGAInfo;
struct VncInfo *VNCInfo;

void update_frame_buffer(void);
void vga_send(struct socket *sock, char* addr, int x ,int y);
#if 0
void show_ddr(void)
{
    u32 * tmp;
    int i = 0;
    tmp = (unsigned int *)(fb_virt_addr);
    printk("frame buffer1 content:\n");
    for (i=0; i<100; i++)
    {
        printk("0x%x 0x%x 0x%x 0x%x\n", *(tmp+4*i), *(tmp+4*i+1), *(tmp+4*i+2), *(tmp+4*i+3));
    }
#if 0
    printk("frame buffer1 content:\n");
    for (i=0; i<8; i++)
    {
        memset(tmp+i*4, 0, 0x10);
    }
    for (i=0; i<8; i++)
    {
        printk("0x%x 0x%x 0x%x 0x%x\n", *(tmp+4*i), *(tmp+4*i+1), *(tmp+4*i+2), *(tmp+4*i+3));
    }
    printk("frame buffer1 content:\n");
    for (i=0; i<8; i++)
    {
        memset(tmp+i*4, 0xFFFFFFFF, 0x10);
    }
    for (i=0; i<8; i++)
    {
        printk("0x%x 0x%x 0x%x 0x%x\n", *(tmp+4*i), *(tmp+4*i+1), *(tmp+4*i+2), *(tmp+4*i+3));
    }
#endif
}
#endif
/*  executed once the device is closed or releaseed by userspace
 *  @param inodep: pointer to struct inode
 *  @param filep: pointer to struct file
 */
static int mchar_release(struct inode *inodep, struct file *filep)
{
    mutex_unlock(&mchar_mutex);
    pr_info("mchar: Device successfully closed\n");

    return 0;
}

/* executed once the device is opened.
 *
 */
static int mchar_open(struct inode *inodep, struct file *filep)
{
    int ret = 0;

    if(!mutex_trylock(&mchar_mutex)) {
        pr_alert("mchar: device busy!\n");
        ret = -EBUSY;
        goto out;
    }

    pr_info("mchar: Device opened\n");

out:
    return ret;
}

/*  mmap handler to map kernel space to user space
 *
 */
static int mchar_mmap(struct file *filp, struct vm_area_struct *vma)
{
    int ret = 0;
    struct page *page = NULL;
    unsigned long size = (unsigned long)(vma->vm_end - vma->vm_start);
    printk("%s.\n", __func__);
    printk("sh_mem is 0x%p.\n", sh_mem);
    if (size > MAX_SIZE) {
        ret = -EINVAL;
        goto out;
    }

    page = virt_to_page((unsigned long)sh_mem + (vma->vm_pgoff << PAGE_SHIFT));
    ret = remap_pfn_range(vma, vma->vm_start, page_to_pfn(page), size, vma->vm_page_prot);
    if (ret != 0) {
        goto out;
    }

out:
    return ret;
}

static ssize_t mchar_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
    int ret;

    if (len > MAX_SIZE) {
        pr_info("read overflow!\n");
        ret = -EFAULT;
        goto out;
    }

    if (copy_to_user(buffer, sh_mem, len) == 0) {
        pr_info("mchar: copy %u char to the user\n", len);
        ret = len;
    } else {
        ret =  -EFAULT;
    }

out:
    return ret;
}

static ssize_t mchar_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{
    int ret;

    if (copy_from_user(sh_mem, buffer, len)) {
        pr_err("mchar: write fault!\n");
        ret = -EFAULT;
        goto out;
    }
    pr_info("mchar: copy %d char from the user\n", len);
    ret = len;

out:
    return ret;
}

static const struct file_operations mchar_fops = {
    .open = mchar_open,
    .read = mchar_read,
    .write = mchar_write,
    .release = mchar_release,
    .mmap = mchar_mmap,
    /*.unlocked_ioctl = mchar_ioctl,*/
    .owner = THIS_MODULE,
};

#define bit_test(bv, b)         ((bv) & (0x01 << (b)))
#define bit_set(bv, b)          do{(bv) |= (0x01 << (b));}while(0)
#define bit_unset(bv, b)        do{(bv) &= ~(0x01 << (b));}while(0)

static inline
void __clear_keyboard_buf(void)
{
    __key_report_bv__ = 0x00;
    memset(hid_kb->key, 0, KEY_REPORT_MAX);
}

static inline
void __reset_keyboard_state(void)
{
    __actual_shift__ = 0x00;
    __clear_keyboard_buf();
}

static
char __push_key(char key)
{
    int i, empty = -1;

//    printk("push 0x%02hhx, map:%x, actual shift:%x\n",
//            key, __key_report_bv__,  __actual_shift__);
    if(__key_report_bv__ == KEY_REPORT_MASK)
        return -1;

    /* search key buffer */
    for(i=0; i<KEY_REPORT_MAX; i++) {
        if(hid_kb->key[i] == key)
            return i;

        if(empty == -1 && !bit_test(__key_report_bv__, i))
            empty = i;
    }

    if(empty >= 0) {
        bit_set(__key_report_bv__, empty);
        hid_kb->key[empty] = key;
    }

    return empty;
};

static
char __pop_key(char key)
{
    int i;

//    printk("pop 0x%02hhx, map:%x, actual shift:%x, mod:%x\n",
//            key, __key_report_bv__,  __actual_shift__, hid_kb->mod);
    if(__key_report_bv__ == 0) {
        return -1;
    }

    for(i=0; i<KEY_REPORT_MAX; i++) {
        if(hid_kb->key[i] == key) {
            hid_kb->key[i] = 0x00;
            bit_unset(__key_report_bv__, i);

            return i;
        }
    }

    return -1;
};

static
int __need_shift(char key)
{
    if(key >= 0x41 && key <= 0x5a)
        return 1;

    switch(key) {
    case '~':
    case '!':
    case '@':
    case '#':
    case '$':
    case '%':
    case '^':
    case '&':
    case '*':
    case '(':
    case ')':
    case '_':
    case '+':
    case '{':
    case '}':
    case '|':
    case ':':
    case '"':
    case '<':
    case '>':
    case '?':
        return 1;
    }

    return 0;
}

void vnc_key_down(unsigned int key)
{
    //0-0x20??
    // ASCII code
    if(key >=0x20 && key <= 0x7f) {
        if(key == 0x20) {       //SPACE
//#if VNC_KEY_RECONNECT//Press "SPACE" to re-connect usb hid dev
            plug_out_count++;
//#if CONFIG_VNC_KM_ENABLED
            //if(plug_out_count==2)
            //disable_vnc_usb_dev();
//#endif
            //printf("SPACE = 0x%x\n",plug_out_count);
            //printk("SPACE = 0x%x\n",plug_out_count);
//#endif
        }

        /* for a compose key(ALT/CTRL is set), don't care about the auto-append shift */
        if(!(hid_kb->mod & COMPOSE_KEY_MASK)) {
            if(__need_shift(key))
                hid_kb->mod |= AUTO_SHIFT;
            else
                hid_kb->mod &= ~ALL_SHIFT_MODIFIER;
        }

        __push_key(ascii_hid_map[key]);
    }else{
        switch(key){
            case VNC_KEY_LEFT_SHIFT:
                __actual_shift__ |= KEY_MODIFIER_LEFT_SHIFT;
                __clear_keyboard_buf();
                hid_kb->mod |= KEY_MODIFIER_LEFT_SHIFT;
                break;
            case VNC_KEY_RIGHT_SHIFT:
                __actual_shift__ |= KEY_MODIFIER_RIGHT_SHIFT;
                __clear_keyboard_buf();
                hid_kb->mod |= KEY_MODIFIER_RIGHT_SHIFT;
                break;
            case VNC_KEY_LEFT_CTRL:
                __clear_keyboard_buf();
                hid_kb->mod |= KEY_MODIFIER_LEFT_CTRL;
                break;
            case VNC_KEY_RIGHT_CTRL:
                __clear_keyboard_buf();
                hid_kb->mod |= KEY_MODIFIER_RIGHT_CTRL;
                break;
            case VNC_KEY_LEFT_GUI:
                __clear_keyboard_buf();
                hid_kb->mod |= KEY_MODIFIER_LEFT_GUI;
                break;
            case VNC_KEY_RIGHT_GUI:
                __clear_keyboard_buf();
                hid_kb->mod |= KEY_MODIFIER_RIGHT_GUI;
                break;
            case VNC_KEY_LEFT_ALT:
                __clear_keyboard_buf();
                hid_kb->mod |= KEY_MODIFIER_LEFT_ALT;
                break;
            case VNC_KEY_RIGHT_ALT:
                __clear_keyboard_buf();
                hid_kb->mod |= KEY_MODIFIER_RIGHT_ALT;
                break;

#define _CASE(KEY, CODE)    \
            case KEY:\
                     __push_key(CODE);\
            break

            _CASE(VNC_KEY_BACKSPACE, 0x2a);
            _CASE(VNC_KEY_TAB, 0x2b);
            _CASE(VNC_KEY_ENTER, 0x28);
            _CASE(VNC_KEY_ESCAPE, 0x29);
            _CASE(VNC_KEY_INSERT, 0x49);
            _CASE(VNC_KEY_DELETE, 0x4c);
            _CASE(VNC_KEY_HOME, 0x4a);
            _CASE(VNC_KEY_LEFT_ARROW, 0x50);
            _CASE(VNC_KEY_UP_ARROW, 0x52);
            _CASE(VNC_KEY_RIGHT_ARROW, 0x4f);
            _CASE(VNC_KEY_DOWN_ARROW, 0x51);
            _CASE(VNC_KEY_PAGE_UP, 0x4b);
            _CASE(VNC_KEY_PAGE_DOWN, 0x4e);
            _CASE(VNC_KEY_END, 0x4d);
            _CASE(VNC_KEY_F1, 0x3a);
            _CASE(VNC_KEY_F2, 0x3b);
            _CASE(VNC_KEY_F3, 0x3c);
            _CASE(VNC_KEY_F4, 0x3d);
            _CASE(VNC_KEY_F5, 0x3e);
            _CASE(VNC_KEY_F6, 0x3f);
            _CASE(VNC_KEY_F7, 0x40);
            _CASE(VNC_KEY_F8, 0x41);
            _CASE(VNC_KEY_F9, 0x42);
            _CASE(VNC_KEY_F10, 0x43);
            _CASE(VNC_KEY_F11, 0x44);
            _CASE(VNC_KEY_F12, 0x45);

            /* number pad */
            _CASE(VNC_KEY_NUM_ENTER, 0x28);
            _CASE(VNC_KEY_NUM_0, ascii_hid_map[VNC_KEY_NUM_0 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_1, ascii_hid_map[VNC_KEY_NUM_1 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_2, ascii_hid_map[VNC_KEY_NUM_2 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_3, ascii_hid_map[VNC_KEY_NUM_3 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_4, ascii_hid_map[VNC_KEY_NUM_4 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_5, ascii_hid_map[VNC_KEY_NUM_5 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_6, ascii_hid_map[VNC_KEY_NUM_6 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_7, ascii_hid_map[VNC_KEY_NUM_7 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_8, ascii_hid_map[VNC_KEY_NUM_8 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_9, ascii_hid_map[VNC_KEY_NUM_9 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_SLASH, ascii_hid_map[VNC_KEY_NUM_SLASH - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_DASH, ascii_hid_map[VNC_KEY_NUM_DASH - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_DOT, ascii_hid_map[VNC_KEY_NUM_DOT - VNC_KEY_NUM_PAD_OFFSET]);

        case VNC_KEY_NUM_STAR:
            hid_kb->mod |= AUTO_SHIFT;
            __push_key(ascii_hid_map[VNC_KEY_NUM_STAR - VNC_KEY_NUM_PAD_OFFSET]);
            break;
        case VNC_KEY_NUM_PLUS:
            hid_kb->mod |= AUTO_SHIFT;
            __push_key(ascii_hid_map[VNC_KEY_NUM_PLUS - VNC_KEY_NUM_PAD_OFFSET]);
            break;
#undef _CASE
        }
    }
}

void vnc_key_up(unsigned int key)
{
    if(key >=0x20 && key <= 0x7f) {
        __pop_key(ascii_hid_map[key]);
    }else{
        switch(key){
            case VNC_KEY_LEFT_SHIFT:
                __actual_shift__ &= ~KEY_MODIFIER_LEFT_SHIFT;
                __clear_keyboard_buf();
                hid_kb->mod &= ~KEY_MODIFIER_LEFT_SHIFT;
                break;
            case VNC_KEY_RIGHT_SHIFT:
                __actual_shift__ &= ~KEY_MODIFIER_RIGHT_SHIFT;
                __clear_keyboard_buf();
                hid_kb->mod &= ~KEY_MODIFIER_RIGHT_SHIFT;
                break;
            case VNC_KEY_LEFT_CTRL:
                __clear_keyboard_buf();
                hid_kb->mod &= ~KEY_MODIFIER_LEFT_CTRL;
                break;
            case VNC_KEY_RIGHT_CTRL:
                __clear_keyboard_buf();
                hid_kb->mod &= ~KEY_MODIFIER_RIGHT_CTRL;
                break;
            case VNC_KEY_LEFT_GUI:
                __clear_keyboard_buf();
                hid_kb->mod &= ~KEY_MODIFIER_LEFT_GUI;
                break;
            case VNC_KEY_RIGHT_GUI:
                __clear_keyboard_buf();
                hid_kb->mod &= ~KEY_MODIFIER_RIGHT_GUI;
                break;
            case VNC_KEY_LEFT_ALT:
                __clear_keyboard_buf();
                hid_kb->mod &= ~KEY_MODIFIER_LEFT_ALT;
                break;
            case VNC_KEY_RIGHT_ALT:
                hid_kb->mod &= ~KEY_MODIFIER_RIGHT_ALT;
                break;

#define _CASE(KEY, CODE)    \
            case KEY:\
                     __pop_key(CODE);\
            break

            _CASE(VNC_KEY_BACKSPACE, 0x2a);
            _CASE(VNC_KEY_TAB, 0x2b);
            _CASE(VNC_KEY_ENTER, 0x28);
            _CASE(VNC_KEY_ESCAPE, 0x29);
            _CASE(VNC_KEY_INSERT, 0x49);
            _CASE(VNC_KEY_DELETE, 0x4c);
            _CASE(VNC_KEY_HOME, 0x4a);
            _CASE(VNC_KEY_LEFT_ARROW, 0x50);
            _CASE(VNC_KEY_UP_ARROW, 0x52);
            _CASE(VNC_KEY_RIGHT_ARROW, 0x4f);
            _CASE(VNC_KEY_DOWN_ARROW, 0x51);
            _CASE(VNC_KEY_PAGE_UP, 0x4b);
            _CASE(VNC_KEY_PAGE_DOWN, 0x4e);
            _CASE(VNC_KEY_END, 0x4d);
            _CASE(VNC_KEY_F1, 0x3a);
            _CASE(VNC_KEY_F2, 0x3b);
            _CASE(VNC_KEY_F3, 0x3c);
            _CASE(VNC_KEY_F4, 0x3d);
            _CASE(VNC_KEY_F5, 0x3e);
            _CASE(VNC_KEY_F6, 0x3f);
            _CASE(VNC_KEY_F7, 0x40);
            _CASE(VNC_KEY_F8, 0x41);
            _CASE(VNC_KEY_F9, 0x42);
            _CASE(VNC_KEY_F10, 0x43);
            _CASE(VNC_KEY_F11, 0x44);
            _CASE(VNC_KEY_F12, 0x45);

            /* number pad */
            _CASE(VNC_KEY_NUM_ENTER, 0x28);
            _CASE(VNC_KEY_NUM_0, ascii_hid_map[VNC_KEY_NUM_0 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_1, ascii_hid_map[VNC_KEY_NUM_1 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_2, ascii_hid_map[VNC_KEY_NUM_2 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_3, ascii_hid_map[VNC_KEY_NUM_3 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_4, ascii_hid_map[VNC_KEY_NUM_4 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_5, ascii_hid_map[VNC_KEY_NUM_5 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_6, ascii_hid_map[VNC_KEY_NUM_6 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_7, ascii_hid_map[VNC_KEY_NUM_7 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_8, ascii_hid_map[VNC_KEY_NUM_8 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_9, ascii_hid_map[VNC_KEY_NUM_9 - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_SLASH, ascii_hid_map[VNC_KEY_NUM_SLASH - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_DASH, ascii_hid_map[VNC_KEY_NUM_DASH - VNC_KEY_NUM_PAD_OFFSET]);
            _CASE(VNC_KEY_NUM_DOT, ascii_hid_map[VNC_KEY_NUM_DOT - VNC_KEY_NUM_PAD_OFFSET]);

        case VNC_KEY_NUM_STAR:
            hid_kb->mod = ((hid_kb->mod & ~AUTO_SHIFT) | __actual_shift__);
            __pop_key(ascii_hid_map[VNC_KEY_NUM_STAR - VNC_KEY_NUM_PAD_OFFSET]);
            break;
        case VNC_KEY_NUM_PLUS:
            hid_kb->mod = ((hid_kb->mod & ~AUTO_SHIFT) | __actual_shift__);
            __pop_key(ascii_hid_map[VNC_KEY_NUM_PLUS - VNC_KEY_NUM_PAD_OFFSET]);
            break;
#undef _CASE
        }
    }
}

void VNCKeyBoardInput(u8* rx_buf)
{
    VNCKeyEvent	*ke;

    if(!rtl8117_ehci_dev.kb_hid_enabled
       || rtl8117_ehci_dev.hid_address == 0
       || !bsp_bits_get(MAC_MAC_STATUS, BIT_ISOLATE, 1)
       || (Rd_IBIO(PORTSC+0x04) != VALID_PORTSC)
       || (Rd_IBIO(0x20)&0x01) == 0x00)
        return;

    ke = (VNCKeyEvent*)rx_buf;
    ke->key = ntohl(ke->key);

//    printk("KeyEvent: %02x %02x %08x\n",
//          ke->msg_type, ke->down_flag, ke->key);

    if(ke->down_flag)
    {
        vnc_key_down(ke->key);
    }
    else
    {
        vnc_key_up(ke->key);
    }

//    printk("KeyReport %02x %02x %02x %02x %02x %02x %02x %02x\n",
//            hid_kb->mod, hid_kb->rsv,
//            hid_kb->key[0], hid_kb->key[1],
//            hid_kb->key[1], hid_kb->key[3],
//            hid_kb->key[3], hid_kb->key[5]
//            );
    rtkehci_interep_transfer(8 , VNCUSBbuf, is_IN, 1);
}

void VNCMouseInput(u8* rx_buf)
{
    VNCPointEvent *pe;
    HIDMouse_Report hmr;
    //int i = 0;

    if(!rtl8117_ehci_dev.mouse_hid_enabled
       || rtl8117_ehci_dev.hid_address == 0
       || !bsp_bits_get(MAC_MAC_STATUS, BIT_ISOLATE, 1)
       || (Rd_IBIO(PORTSC+0x04) != VALID_PORTSC)
       || (Rd_IBIO(0x20)&0x01) == 0x00)
        return;

    pe = (VNCPointEvent*)rx_buf;
    pe->x = ntohs(pe->x);
    pe->y = ntohs(pe->y);
    memset(&hmr, 0, sizeof(hmr));
    hmr.buttons = pe->button_mask;

    if(hmr.buttons == 4)
        hmr.buttons = 2;
    else if(hmr.buttons == 2)
        hmr.buttons = 4;

    hmr.rID = 1;
#define MOUSE_PHY_AXIS 1024
    if(VGAInfo->FB1_Hresol != 1024)
    {
        //Physical max is 1023(device_report_type1)
        hmr.x = pe->x*MOUSE_PHY_AXIS/800;
        hmr.y = pe->y*MOUSE_PHY_AXIS/600;
    }
    else
    {
        hmr.x = pe->x;
        hmr.y = (pe->y*MOUSE_PHY_AXIS)/VGAInfo->FB1_Vresol;
    }
    memcpy(VNCUSBbuf+8, &hmr, 7);
    rtkehci_interep_transfer(7 , VNCUSBbuf+8, 1, 2);
}


enum {KEYBOARD=0, MOUSE, FRAME_BUFFER, VGAINFO, KEEP_NOT_ALIVE, SET_MAC_ADDR, GET_MAC_ADDR, ENABLE_USB, VGA_ENABLE, VGA_DISBALE, MMAP_VNC, SET_SOCKETOPT, GET_CHG_RES, GET_BGN_REG, GET_BFN_REG, RE_INIT, DISABLE_USB, RELEASE_FB};
static long vncdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg_)
{
    void __user *arg = (void __user *)arg_;
    //int __user *p = arg;
    u8 *rx_buf = NULL;
    int fd;
    int i = 0;
    //int cnt = 0;
    //u32 tmp;
    u32 BGN,max_BGN,VblockNum,HblockNum;
    u32  FB1_BGN_reg,FB1_BFN_reg[17];
    u8 dummy;
    u8 old_val;
    //int val;

    rx_buf = kzalloc(16, GFP_KERNEL);

    switch (cmd) {
    case KEYBOARD:
        copy_from_user(rx_buf, arg, 8);
        VNCKeyBoardInput(rx_buf);
        break;
    case MOUSE:
        copy_from_user(rx_buf, arg, 8);
        VNCMouseInput(rx_buf);
        break;
    case FRAME_BUFFER:
        //printk("kernel start update frame buffer.\n");
        copy_from_user(&fd, arg, 4);
        //update_frame_buffer(fd);
        break;
    case VGAINFO:
        copy_to_user(arg, &VGAInfo, sizeof(VGAInfo));
        break;
    case KEEP_NOT_ALIVE:
        Wt_IBIO(PORTSC, 0x1000);
        VGA_disable();
        rtl8117_ehci_intep_disabled(1);
        break;
    case SET_MAC_ADDR:
        copy_from_user(rx_buf, arg, 16);
        for (i=0; i<4; i++)
            rtl8117_ehci_writel(*((u32 *)rx_buf), MAC_BASE_ADDR + MAC_HOSTNAME + 4*i);
        break;
    case GET_MAC_ADDR:
        for (i=0; i<16; i++)
            rx_buf[i] = ehci_readb(MAC_BASE_ADDR + MAC_HOSTNAME + i);
        copy_to_user(arg, rx_buf, 16);
        break;
    case ENABLE_USB:
        usb_redirect = 1;
        enable_vnc_usb_dev();
        break;
    case VGA_ENABLE:
        if (!usb_redirect)
            usb_redirect = 1;
        rtl8117_ehci_dev.in_use_hid = 1;
        old_val = rtl8117_ehci_dev.last_use;
        rtl8117_ehci_dev.last_use = 2;
        VNCInfo->HwVNCEnable =  1;
        if(!bsp_bits_get(MAC_MAC_STATUS, BIT_ISOLATE, 1))
        {
            SMBus_Prepare_RmtCtrl(RMCP_PWR_On, 0);
            mdelay(10);
            VGA_enable();
	    disable_vnc_usb_dev();
            enable_vnc_usb_dev();
        }
        else
        {
            SMBus_Prepare_RmtCtrl(RMCP_Reset, 0);
            mdelay(10);
            VGA_enable();
	    disable_vnc_usb_dev();
            enable_vnc_usb_dev();
        }
#if 0
#if 0
        for (i=0; i<6; i++)
        {
            tmp = 0x0003F010+i*4;
            OOB_access_IB_reg(0xE830, &tmp, 0xf, OOB_WRITE_OP);
            do {
                OOB_access_IB_reg(0xE830, &tmp, 0xf, OOB_READ_OP);
                if ((tmp&0x80000000) == 0x00)
                    break;
                if (cnt<100)
                    break;
                cnt++;
            } while(1);
            OOB_access_IB_reg(0xE82C, &tmp, 0xf, OOB_READ_OP);
            printk("pci ram base addr %d: 0x%x.\n", i, tmp);
        }
#endif
        cnt = 0;
        tmp = 0x0003F030;
        OOB_access_IB_reg(0xE830, &tmp, 0xf, OOB_WRITE_OP);
        do {
            OOB_access_IB_reg(0xE830, &tmp, 0xf, OOB_READ_OP);
            if ((tmp&0x80000000) == 0x00)
                break;
            if (cnt<100)
                break;
            cnt++;
        } while(1);
        OOB_access_IB_reg(0xE82C, &tmp, 0xf, OOB_READ_OP);
        printk("pci cfg 0x30 - 0x%x.\n", tmp);
#endif
        VNCInfo->NewVNCCli = 0;
        copy_to_user(arg, &priv->fb_mapping, 4);
        break;
    case VGA_DISBALE:
        VGA_disable();
        disable_vnc_usb_dev();
        SMBus_Prepare_RmtCtrl(RMCP_Reset, 0);
        VNCInfo->HwVNCEnable = 0;

        rtl8117_ehci_dev.in_use_hid = 0;
        rtl8117_ehci_dev.last_use = old_val;
        if ((!rtl8117_ehci_dev.in_use_redirect) && (!rtl8117_ehci_dev.in_use_hid))
            usb_redirect = 0;
        break;
    case MMAP_VNC:
        i = virt_to_phys(sh_mem);
        copy_to_user(arg, &i, 4);
        break;
    case SET_SOCKETOPT:
        break;
    case GET_CHG_RES:
        dummy = ehci_readb(VGA_IOBASE+VGA_dummy2);
        copy_to_user(arg, &dummy,  1);
        break;
    case GET_BFN_REG:
#define CEILING(x, u)   (((x) + ((u)-1)) / (u))

        HblockNum = CEILING(VGAInfo->FB1_Hresol, 0x40);
        VblockNum = CEILING(VGAInfo->FB1_Vresol, 0x40);
        max_BGN = CEILING(HblockNum*VblockNum, 0x20);

        FB1_BGN_reg = ehci_readl(VGA_IOBASE+FB1_BGN);
        for(BGN = 0; BGN < max_BGN; BGN++)
        {
            if ( ((0x1<<BGN)&(FB1_BGN_reg)) == (0x1<<BGN) )
            {
                //Read block change bit from reg and restore to memory
                //clear difference flag
                FB1_BFN_reg[BGN] = ehci_readl( VGA_IOBASE+FB1_BFN_base+(BGN*4));
                rtl8117_ehci_writel(FB1_BFN_reg[BGN], VGA_IOBASE+FB1_BFN_base+(BGN*4));
            }
            else
                FB1_BFN_reg[BGN] = 0x0;
        }
        copy_to_user(arg, FB1_BFN_reg, sizeof(FB1_BFN_reg));
        break;
    case GET_BGN_REG:
        FB1_BGN_reg = ehci_readl(VGA_IOBASE+FB1_BGN);
        copy_to_user(arg, &FB1_BGN_reg, 4);
        break;
    case RE_INIT:
        VGA_initial();
        rtl8117_ehci_writeb(0x67, VGA_IOBASE+VGA_dummy2+1);
        rtl8117_ehci_writeb(0x7, VGA_IOBASE+VGA_dummy2);
        break;
    case DISABLE_USB:
        usb_redirect = 0;
        disable_vnc_usb_dev();
        break;
    case RELEASE_FB:
        release_fb();
        break;
    default:
        return -EINVAL;
    }
    if (rx_buf)
        kfree(rx_buf);
    return 0;
}


static const struct file_operations vncdev_fops = {
    .owner = THIS_MODULE,
    .open = simple_open,
    .unlocked_ioctl = vncdev_ioctl,
};

static struct miscdevice vncdev = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "vnc",
    .fops = &vncdev_fops,
    .mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH,
};

static int __init vncdev_register(void)
{
    int rc;

    rc = misc_register(&vncdev);
    if (unlikely(rc)) {
        pr_err("registration of /dev/vnc failed\n");
        return rc;
    }

    return 0;
}


void mchar_register(void)
{
    VNCInfo = (struct VncInfo *)0x80050000;
    VGAInfo = (struct VgaInfo *)0x80050100;
    sh_mem = (char *)VNCInfo;
    major = register_chrdev(0, DEVICE_NAME, &mchar_fops);

    if (major < 0) {
        pr_info("mchar: fail to register major number!");
        //ret = major;
        goto out;
    }

    class = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(class)) {
        unregister_chrdev(major, DEVICE_NAME);
        pr_info("mchar: failed to register device class");
        //ret = PTR_ERR(class);
        goto out;
    }
    device = device_create(class, NULL, MKDEV(major, 0), NULL, DEVICE_NAME);
    if (IS_ERR(device)) {
        class_destroy(class);
        unregister_chrdev(major, DEVICE_NAME);
        //ret = PTR_ERR(device);
        goto out;
    }
out:
    return;
}

void vnc_init(void)
{
    memset(VNCUSBbuf, 0, 64);
    vncdev_register();
    mchar_register();
}

void VGA_pre_Initial(void)
{
    rtl8117_ehci_writeb(1, (VGA_IOBASE+VGA_dummy2+2));
    rtl8117_ehci_writel(VGA_ROM_SPI_base, (VGA_IOBASE+0x568));	//Set ROM base addr
    //rtl8117_ehci_writel(fb_phy_addr, (VGA_IOBASE+0x558));		//set FB0 base addr
    //rtl8117_ehci_writel(fb_phy_addr, (VGA_IOBASE+0x55C));		//set FB1 base addr
}

void set_frame_Block(u16 vpixelnum, u16 hpixelnum, u8 byteperpixel, u8 fb)
{

    u8 vBlocknum,hBlocknum,vex_pixel,hex_pixel,i;
    u32 block_byte_offset;
    u16 FB_CTRL=0,FB_VaddrBase=0,FB_HaddrBase=0,FB_LaddrBase=0;
    u32 base_addr;
    base_addr = 0x00000000;

    if(fb==0)
    {
        FB_CTRL = FB0_CTRL;
        FB_VaddrBase = FB0_VaddrBase;
        FB_HaddrBase = FB0_HaddrBase;
        FB_LaddrBase = FB0_LaddrBase;
    }
    else if(fb==1)
    {
        FB_CTRL = FB1_CTRL;
        FB_VaddrBase = FB1_VaddrBase;
        FB_HaddrBase = FB1_HaddrBase;
        FB_LaddrBase = FB1_LaddrBase;
    }

    vBlocknum = vpixelnum/0x40;
    hBlocknum = hpixelnum/0x40;
    vex_pixel = vpixelnum%0x40;
    hex_pixel = hpixelnum%0x40;
    if(vex_pixel != 0)
        vBlocknum++;
    if(hex_pixel != 0)
        hBlocknum++;
    if(fb==0)
    {
        VGAInfo->FB0_HBnum = hBlocknum;
        VGAInfo->FB0_VBnum = vBlocknum;
    }
    else if(fb==1)
    {
        VGAInfo->FB1_HBnum = hBlocknum;
        VGAInfo->FB1_VBnum = vBlocknum;
    }
    // write FB1 VGA control register :
    //block extension pixel and number of pixel for vertical or horizontal

    rtl8117_ehci_writel(hBlocknum|(vBlocknum<<8)|(hex_pixel<<16)|(vex_pixel<<24), VGA_IOBASE+FB_CTRL);
    //set vertical block address to HW -----
    block_byte_offset=(hpixelnum*byteperpixel*0x40);
    for(i=0; i<VBlock_maxnum; i++)
    {
        if(i<vBlocknum)
            rtl8117_ehci_writel(base_addr+(i*block_byte_offset), VGA_IOBASE+FB_VaddrBase+(i*0x4));
        else
            rtl8117_ehci_writel(0xFFFFFFFF, VGA_IOBASE+FB_VaddrBase+(i*0x4));
    }

    //set Horizontal block address to HW
    block_byte_offset=(0x40*byteperpixel);
    rtl8117_ehci_writel(base_addr, VGA_IOBASE+FB_HaddrBase);
    for(i=1; i<HBlock_maxnum; i++)
    {
        if(i<hBlocknum)
            rtl8117_ehci_writel(base_addr+(i*block_byte_offset), VGA_IOBASE+FB_HaddrBase+(i*0x4));
        else
            rtl8117_ehci_writel(0xFFFFFFFF, VGA_IOBASE+FB_HaddrBase+(i*0x4));
    }

    //set in block Line address to HW
    block_byte_offset=(hpixelnum*byteperpixel);
    for(i=0; i<InBlock_maxnum; i++)
        rtl8117_ehci_writel(base_addr+(i*block_byte_offset), VGA_IOBASE+FB_LaddrBase+(i*0x4));
}

void VGA_initial(void)
{
    u32 FB0_resolution,FB1_resolution;
    u8 k;
    static u8 vgaInit1st = 0;
    //set vga imr & clear isr
    //set STD reg imr & clear isr
    //read the resolution and set frame block reg
    if(vgaInit1st)
    {
        FB0_resolution=ehci_readl(VGA_IOBASE+FB0_resol);
        FB1_resolution=ehci_readl(VGA_IOBASE+FB1_resol);
    }
    else
    {
        //800*600*4
        FB0_resolution=0x04000000 | 0x258000 | 0x320 ;
        FB1_resolution=0x04000000 | 0x258000 | 0x320 ;
        vgaInit1st = 1;
    }

    // set fb0 frame block reg
    if(FB0_resolution!=0)
    {
        VGAInfo->FB0_Hresol=FB0_resolution&0x00000FFF;
        VGAInfo->FB0_Vresol=(FB0_resolution&0x00FFF000)>>12;
        VGAInfo->FB0_BPP=(FB0_resolution&0xFF000000)>>24;
        set_frame_Block(VGAInfo->FB0_Vresol, VGAInfo->FB0_Hresol, VGAInfo->FB0_BPP, 0);
        rtl8117_ehci_writel((VGAInfo->FB0_HBnum<<16|VGAInfo->FB0_BPP|0x00000000), VGA_IOBASE+FB0_HBN_BPP);
    }
    if(FB1_resolution!=0)
    {
        // set fb1 frame block reg
        VGAInfo->FB1_Hresol=FB1_resolution&0x00000FFF;
        VGAInfo->FB1_Vresol=(FB1_resolution&0x00FFF000)>>12;
        VGAInfo->FB1_BPP=(FB1_resolution&0xFF000000)>>24;
        set_frame_Block(VGAInfo->FB1_Vresol, VGAInfo->FB1_Hresol, VGAInfo->FB1_BPP, 1);
        rtl8117_ehci_writel((VGAInfo->FB1_HBnum<<16|VGAInfo->FB1_BPP|0x00000000), VGA_IOBASE+FB1_HBN_BPP);
    }
    //clear difference flag-----------------------------------------------

    // clear fb0 diff flag
    rtl8117_ehci_writel(0x0000FFFF, VGA_IOBASE+FB0_BGN);

    for(k=0; k<16; k++)
    {
        rtl8117_ehci_writel(0xFFFFFFFF, VGA_IOBASE+FB0_BFN_base+(k*4));
    }

    // clear fb1 diff flag
    rtl8117_ehci_writel(0x0000FFFF, VGA_IOBASE+FB1_BGN);

    for(k=0; k<16; k++)
    {
        rtl8117_ehci_writel(0xFFFFFFFF, VGA_IOBASE+FB1_BFN_base+(k*4));
    }
}
//extern int EHCI_RST(void);
void enable_vnc_usb_dev(void)
{
    if (mutex_trylock(&rtl8117_ehci_dev.port1_mutex)) {
	if(rtl8117_ehci_dev.hid_state == DISABLED) {
	    rtl8117_ehci_dev.hid_state = ENABLED;
	    __reset_keyboard_state();
	    rtl8117_ehci_intep_enabled(1);
	}

	mutex_unlock(&rtl8117_ehci_dev.port1_mutex);
    } else {
	printk("*** remote HID enabling...fail\n");
    }
}

void disable_vnc_usb_dev(void)
{
    if (mutex_trylock(&rtl8117_ehci_dev.port1_mutex)) {
	if(rtl8117_ehci_dev.hid_state == ENABLED) {
	    __reset_keyboard_state();
	    rtl8117_ehci_dev.kb_hid_enabled = false;
	    rtl8117_ehci_dev.mouse_hid_enabled = false;
	    rtl8117_ehci_intep_disabled(1);
	    rtl8117_ehci_dev.hid_state = DISABLED;
	}

	mutex_unlock(&rtl8117_ehci_dev.port1_mutex);
    } else {
	printk("*** remote HID disabling...fail\n");
    }
}


static
void try_alloc_fb(void)
{
    if (!priv->fb_mapping)
    {
        priv->fb_virt_addr =dma_alloc_coherent(priv->dev, VGA_FB_SIZE, &priv->fb_mapping, GFP_KERNEL|GFP_ONLY_VGA_FB_CMA);
        rtl8117_ehci_writel(priv->fb_mapping, (VGA_IOBASE+0x55C));       //set FB1 base addr
    }
}

static
void release_fb(void)
{
    if (priv->fb_mapping)
    {
        rtl8117_ehci_writel(0, (VGA_IOBASE+0x55C));       //set FB1 base addr
        dma_free_coherent(priv->dev, VGA_FB_SIZE, priv->fb_virt_addr, priv->fb_mapping);
        priv->fb_mapping = 0;
    }
}

void VGA_enable(void)
{
    //disable_link_change(LCB_KVM);
    try_alloc_fb();
    VGA_initial();
    rtl8117_ehci_writeb(0x67, VGA_IOBASE+VGA_dummy2+1);
}

void VGA_disable(void)
{
    u32 tmp = 0xFFFFFFFF;
    OOB_access_IB_reg(0xE82C, &tmp, 0xf, OOB_WRITE_OP);
    tmp = 0x8003F000;
    OOB_access_IB_reg(0xE830, &tmp, 0xf, OOB_WRITE_OP);
    //enable_link_change(LCB_KVM);
#if 0
    release_fb();
#endif
}

int rtk_vga_probe(struct platform_device *pdev)
{
    struct device *d = &pdev->dev;
    dma_addr_t vnc_usb_buf_phy;

    priv = kzalloc(sizeof(struct rtl8117_vga_private), GFP_ATOMIC|GFP_KERNEL);
    priv->dev = d;

    //fb_phy_addr = 0x9800000;
    //fb_virt_addr = (void __iomem *)0xa9800000;
    try_alloc_fb();

    /* FIXME: check the result of frame buffer allocation */

    VGA_pre_Initial();
    VNCUSBbuf = dma_alloc_coherent(d, 64, &vnc_usb_buf_phy, GFP_ATOMIC);
    hid_kb = (HIDKeyReport*)VNCUSBbuf;
    vnc_init();
    return 0;
}

int rtk_vga_remove(struct platform_device *pdev)
{
    release_fb();
    return 0;
}

static struct of_device_id rtk_vga_ids[] = {
    { .compatible = "realtek,rtl8117-vga" },
    { /* Sentinel */ },
};
MODULE_DEVICE_TABLE(of, rtk_vga_ids);

static struct platform_driver realtek_vga_platdrv = {
    .driver = {
        .name   = "rtl8117-vga",
        .owner  = THIS_MODULE,
        .of_match_table = rtk_vga_ids,
    },
    .probe      = rtk_vga_probe,
    .remove   = rtk_vga_remove,
};
static int __init rtk_vga_init(void)
{
    return platform_driver_register(&realtek_vga_platdrv);
}
module_init(rtk_vga_init);

static void __exit rtk_vga_exit(void)
{
    platform_driver_unregister(&realtek_vga_platdrv);
}
module_exit(rtk_vga_exit);
