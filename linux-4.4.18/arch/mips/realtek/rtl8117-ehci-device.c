/*
 *  Copyright (c) 2014 Realtek Semiconductor Corp. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 */
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/signal.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/usb/cdc.h>
#include <linux/blkdev.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <asm/unaligned.h>
#include <linux/kthread.h>
#include <linux/usb/storage.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <linux/semaphore.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/time.h>
#include <linux/netdevice.h>
#include <linux/ip.h>
#include <linux/in.h>
#include <linux/mutex.h>
#include <linux/delay.h>
#include <linux/un.h>
#include <linux/unistd.h>
#include <linux/wait.h>
#include <linux/ctype.h>
#include <asm/unistd.h>
#include <net/tcp.h>
#include <net/inet_connection_sock.h>
#include <net/request_sock.h>
#include <rtl8117_platform.h>
#include "rtl8117-ehci.h"
//#include "rtl8117-vnc.h"

#define MAX_RETRY_CNT 3
void show_register(int);
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");
extern bool usb_redirect;
//unsigned char usb_state;
//unsigned char usb_type;
unsigned char redir_reboot = 0;
struct write_work t_work[OUTdescNumber];

extern struct rtl8117_ehci_core ehci_core;
#if __DEV__
extern atomic_t bulkout_bh_bv;
#endif

#ifdef CONFIG_USB_REDIRECTION
//u8 device_addr0 = 0;
extern bool ehci_enabled;
//extern bool usb_redirect;
static int __init ehcidev_register(void);
extern void enable_vnc_usb_dev(void);
static struct socket *csock;

struct ktcp_service
{
    int running;
    struct socket *listen_socket;
    struct task_struct *thread;
    struct task_struct *accept_worker;
};

struct ktcp_service *ktcp_svc=NULL;
static DEFINE_MUTEX(nobkl);

u8 device_report_type0[62] = { 0x05, 0x01, 0x09, 0x06, 0xA1, 0x01, 0x05, 0x07, 0x19, 0xE0, 0x29, 0xE7, 0x15, 0x00, 0x25, 0x01, 0x75, 0x01, 0x95, 0x08, 0x81, 0x02, 0x95, 0x01, 0x75, 0x08, 0x81, 0x01, 0x95, 0x03, 0x75, 0x01, 0x05, 0x08, 0x19, 0x01, 0x29, 0x03, 0x91, 0x02, 0x95, 0x05, 0x75, 0x01, 0x91, 0x01, 0x95, 0x06, 0x75, 0x08, 0x26, 0xFF, 0x00, 0x05, 0x07, 0x19, 0x00, 0x29, 0x91, 0x81, 0x00, 0xC0	};
u8 device_report_type1[] =
{   0x05, 0x01, /* Usage Page (Generic Desktop)             */
    0x09, 0x02, /* Usage (Mouse)                            */
    0xA1, 0x01, /* Collection (Application)                 */
    0x09, 0x01, /*  Usage (Pointer)                         */
    0xA1, 0x00, /*  Collection (Physical)                   */
    0x85, 0x01,  /*   Added Report ID ... 0x01 must precede the sent data*/
    0x05, 0x09, /*      Usage Page (Buttons)                */
    0x19, 0x01, /*      Usage Minimum (01)                  */
    0x29, 0x03, /*      Usage Maximum (03)                  */
    0x15, 0x00, /*      Logical Minimum (0)                 */
    0x25, 0x01, /*      Logical Maximum (1)                 */
    0x95, 0x03, /*      Report Count (3)                    */
    0x75, 0x01, /*      Report Size (1)                     */
    0x81, 0x02, /*      Input (Data, Variable, Absolute)    */
    0x95, 0x01, /*      Report Count (1)                    */
    0x75, 0x05, /*      Report Size (5)                     */
    0x81, 0x01, /*      Input (Constant)    ;5 bit padding  */
    0x05, 0x01, /*      Usage Page (Generic Desktop)        */
    0x09, 0x30, /*      Usage (X)                           */
    0x09, 0x31, /*      Usage (Y)                           */
    0x35, 0x00,  /*     Physical minimum  = 0 */
    0x46, 0xff,0x03, /*Physical max = 1023*/
    0x15, 0x00, /*      Logical Minimum (0)              */
    0x26, 0xff,0x03, /*  logical Maximum = 1023            */
    0x75, 0x10, /*      Report Size (16)                     */
    0x95, 0x02, /*      Report Count (2)                    */
    0x81, 0x02, /*      Input (Data, Variable, 06-Relative, 02-Absolute)    */
    0x09, 0x38, /*    Wheel                         */
    0x35, 0x00,  /*     Physical minimum  = 0 */
    0x45, 0x00,  /*     Physical maximum  = 0 */
    0x15, 0x81, /*      Logical Minimum (-127)              */
    0x25, 0x7F, /*      Logical Maximum (127)               */
    0x75, 0x08, /*      Report Size (8)                     */
    0x95, 0x01, /*      Report Count (1)                    */
    0x81, 0x06, /*      Input (Data, Variable, 06-Relative, 02-Absolute)    */
    0xC0, 0xC0/*         End Collection ,End Collection      */ ,
};

#endif

enum {
    FSG_STRING_INTERFACE
};

#define CBW_SIZE 31
#define CSW_SIZE 13



const struct usb_device_descriptor msg_device_desc = {
    .bLength =		sizeof(msg_device_desc),
    .bDescriptorType =	USB_DT_DEVICE,
    .bcdUSB =		0x0200,
    .bDeviceClass =		USB_CLASS_PER_INTERFACE,
    .bDeviceSubClass =      0,
    .bDeviceProtocol =      0,
    .bMaxPacketSize0 =      0x40,

    .idVendor =		0x0BDA,//Realtek Semiconductor Corp.
    .idProduct =	0x8168,
    .bcdDevice =            0x0200,
    .iManufacturer =	0x1,
    .iProduct =		0x2,
    .iSerialNumber =	0x3,
    .bNumConfigurations =	1,
};
const u8 descriptors_cbi_hid[59] = {
    0x09, 0x02, 0x3B/*len*/, 0x00, 0x02/*int_num*/, 0x01, 0x00, 0xA0, 0x3C,
    0x09, 0x04, 0x00, 0x00, 0x01, 0x03, 0x01, 0x01, 0x00, 0x09, 0x21, 0x11, 0x01, 0x00, 0x01, 0x22, 0x3E, 0x00, 0x07, 0x05, 0x84/*ep4*/, 0x03, 0x08, 0x00, 0x0A,
    0x09, 0x04, 0x01, 0x00, 0x01, 0x03, 0x01, 0x02, 0x00, 0x09, 0x21, 0x11, 0x01, 0x00, 0x01, 0x22, /*0x82*/0x4a, 0x00, 0x07, 0x05, 0x85/*ep5*/, 0x03, 0x07, 0x00, 0x0A
};


static struct usb_config_descriptor msg_config_desc = {
    .bLength = USB_DT_CONFIG_SIZE,
    .bDescriptorType = USB_DT_CONFIG,
    .wTotalLength = 32,
    .bNumInterfaces = 1,
    .bConfigurationValue = 1,
    .iConfiguration = 0,
    //.bmAttributes = USB_CONFIG_ATT_ONE,
    .bmAttributes = 0xC0,
    .bMaxPower = 0x32,
};

struct usb_interface_descriptor fsg_intf_desc = {
    .bLength =		USB_DT_INTERFACE_SIZE,
    .bDescriptorType =	USB_DT_INTERFACE,

    .bInterfaceNumber = 0,
    .bAlternateSetting = 0,

    .bNumEndpoints =	2,		/* Adjusted during fsg_bind() */
    .bInterfaceClass =	USB_CLASS_MASS_STORAGE,
    .bInterfaceSubClass =	USB_SC_SCSI,	/* Adjusted during fsg_bind() */
    .bInterfaceProtocol =	USB_PR_BULK,	/* Adjusted during fsg_bind() */
    .iInterface =		FSG_STRING_INTERFACE,
};

struct usb_endpoint_descriptor fsg_fs_bulk_in_desc = {
    .bLength =		USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =	USB_DT_ENDPOINT,

    .bEndpointAddress =	USB_DIR_IN | 0x1,
    .bmAttributes =		USB_ENDPOINT_XFER_BULK,
    /* wMaxPacketSize set by autoconfiguration */

    .wMaxPacketSize = 0x200,
    .bInterval = 0x0,
};

struct usb_endpoint_descriptor fsg_fs_bulk_out_desc = {
    .bLength =		USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =	USB_DT_ENDPOINT,

    .bEndpointAddress =	USB_DIR_OUT | 0x2,
    .bmAttributes =		USB_ENDPOINT_XFER_BULK,
    /* wMaxPacketSize set by autoconfiguration */

    .wMaxPacketSize = 0x200,
    .bInterval = 0x0,
};

struct usb_descriptor_header *fsg_fs_function[] = {
    (struct usb_descriptor_header *) &fsg_intf_desc,
    (struct usb_descriptor_header *) &fsg_fs_bulk_in_desc,
    (struct usb_descriptor_header *) &fsg_fs_bulk_out_desc,
    NULL,
};

struct usb_endpoint_descriptor fsg_hs_bulk_in_desc = {
    .bLength =		USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =	USB_DT_ENDPOINT,

    /* bEndpointAddress copied from fs_bulk_in_desc during fsg_bind() */
    .bmAttributes =		USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize =	cpu_to_le16(512),
};

struct usb_endpoint_descriptor fsg_hs_bulk_out_desc = {
    .bLength =		USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =	USB_DT_ENDPOINT,

    /* bEndpointAddress copied from fs_bulk_out_desc during fsg_bind() */
    .bmAttributes =		USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize =	cpu_to_le16(512),
    .bInterval =		1,	/* NAK every 1 uframe */
};

struct usb_descriptor_header *fsg_hs_function[] = {
    (struct usb_descriptor_header *) &fsg_intf_desc,
    (struct usb_descriptor_header *) &fsg_hs_bulk_in_desc,
    (struct usb_descriptor_header *) &fsg_hs_bulk_out_desc,
    NULL,
};

struct usb_endpoint_descriptor fsg_ss_bulk_in_desc = {
    .bLength =		USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =	USB_DT_ENDPOINT,

    /* bEndpointAddress copied from fs_bulk_in_desc during fsg_bind() */
    .bmAttributes =		USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize =	cpu_to_le16(1024),
};

struct usb_ss_ep_comp_descriptor fsg_ss_bulk_in_comp_desc = {
    .bLength =		sizeof(fsg_ss_bulk_in_comp_desc),
    .bDescriptorType =	USB_DT_SS_ENDPOINT_COMP,

    /*.bMaxBurst =		DYNAMIC, */
};

struct usb_endpoint_descriptor fsg_ss_bulk_out_desc = {
    .bLength =		USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =	USB_DT_ENDPOINT,

    /* bEndpointAddress copied from fs_bulk_out_desc during fsg_bind() */
    .bmAttributes =		USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize =	cpu_to_le16(1024),
};

struct usb_ss_ep_comp_descriptor fsg_ss_bulk_out_comp_desc = {
    .bLength =		sizeof(fsg_ss_bulk_in_comp_desc),
    .bDescriptorType =	USB_DT_SS_ENDPOINT_COMP,

    /*.bMaxBurst =		DYNAMIC, */
};

struct usb_descriptor_header *fsg_ss_function[] = {
    (struct usb_descriptor_header *) &fsg_intf_desc,
    (struct usb_descriptor_header *) &fsg_ss_bulk_in_desc,
    (struct usb_descriptor_header *) &fsg_ss_bulk_in_comp_desc,
    (struct usb_descriptor_header *) &fsg_ss_bulk_out_desc,
    (struct usb_descriptor_header *) &fsg_ss_bulk_out_comp_desc,
    NULL,
};

const u8 str0[] = { 0x04, 0x03, 0x09, 0x04 };
const u8 str1[] = { 0x10 , 0x03, 'R', 0x00, 'e', 0x00, 'a', 0x00, 'l', 0x00, 't', 0x00, 'e', 0x00, 'k', 0x00 };
const u8 str2[] = { 0x20 , 0x03, 'U', 0x00, 'S', 0x00, 'B', 0x00, ' ', 0x00, 'R', 0x00, 'E', 0x00, 'D', 0x00, 'I', 0x00, 'R', 0x00, 'E', 0x00, 'C', 0x00, 'T', 0x00, 'I', 0x00, 'O', 0x00, 'N', 0x00 }    ;
//const u8 str3[] = { 0x1C , 0x03, '0', 0x00, '3', 0x00, '5', 0x00, '7', 0x00, '8', 0x00, '0', 0x00, '2', 0x00, '1', 0x00, '1', 0x00, '5', 0x00, '9', 0x00, '4', 0x00, '6', 0x00 };
const u8 str3[][32] = {{ 0x1C , 0x03, '0', 0x00, '3', 0x00, '5', 0x00, '7', 0x00, '8', 0x00, '0', 0x00, '2', 0x00, '1', 0x00, '1', 0x00, '5', 0x00, '9', 0x00, '4', 0x00, '6', 0x00 },
    { 0x1A , 0x03, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00, 0x23, 0x00 },
    {0x1C, 0x03, 'R', 0x00, 'T', 0x00, 'L', 0x00, '8', 0x00, '1', 0x00, '1', 0x00, '1', 0x00, 'D', 0x00, 'P', 0x00, 'D', 0x00, 'A', 0x00, 'S', 0x00, 'H', 0x00}
};

//const u8 inquiry_data[] = { 0x00, 0x80, 0x04, 0x02, 0x20, 0x00, 0x00, 0x00, 'R', 'e', 'a', 'l', 't', 'e', 'k', ' ', 'P', 'h', 'o', 't', 'o', '2', 'h', 'o', 'm', 'e', ' ', 'D', 'I', 'S', 'K', ' ', '2', '.', '0', '0'};
const u8 inquiry_data[3][36] = {{ 0x00, 0x80, 0x04, 0x02, 0x20, 0x00, 0x00, 0x00, 'R', 'e', 'a', 'l', 't', 'e', 'k', ' ', 'U', 'S', 'B', ' ', 'D', 'I', 'S', 'K', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '2', '.', '0', '0'},
    { 0x05, 0x80, 0x00, 0x32, 0x5B, 0x00, 0x00, 0x00, 'R', 'e', 'a', 'l', 't', 'e', 'k', ' ', 'U', 'S', 'B', ' ', 'C', 'D', '-', 'R', 'O', 'M', ' ', ' ', ' ', ' ', ' ', ' ', '2', '.', '0', '0'},
    { 0x00, 0x80, 0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 'R', 'e', 'a', 'l', 't', 'e', 'k', ' ', 'F', 'L', 'O', 'P', 'P', 'Y', ' ', 'D', 'R', 'I', 'V', 'E', ' ', ' ', ' ', ' ', '2', '.', '0', '0'}
};
u8 const mode_sense[18] = {0x00, 0x26, 0x94, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x1E, 0x01, 0xF4, 0x02, 0x12, 0x02, 0x00, 0x00, 0x50 };
//INT8U const disc_info[42] =  { 0x00, 0x28, 0x0E, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x01, 0x01};
u8 const disc_info[34] =  { 0x00, 0x20, 0x0E, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

u8 const disc_sense[40] = { 0x00, 0x20, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2A, 0x18, 0x3F, 0x00, 0x71, 0x73, 0x29, 0x23, 0x2B, 0x48, 0x01, 0x00, 0x01, 0x00, 0x2B, 0x48, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00 };



struct rtl8117_ehci_device rtl8117_ehci_dev;

void stop_usb_redirection(u8 val, bool is_cancel)
{
    static u8 enter = 0;
    struct write_work *ww = NULL;
    int i = 0;
    outindesc_r *indesc = (outindesc_r *)(ehci_core.bulk_indesc_addr) +
	ehci_core.bulk_indesc_index;

    printk("stop usb is called, in_use:%d, enter:%d\n",
	   rtl8117_ehci_dev.in_use_redirect, enter);

    mutex_lock(&rtl8117_ehci_dev.mutex);

    if (enter || !rtl8117_ehci_dev.in_use_redirect)
    {
        mutex_unlock(&rtl8117_ehci_dev.mutex);
        return;
    }

    enter = 1;
    mutex_unlock(&rtl8117_ehci_dev.mutex);

    rtl8117_ehci_dev.in_use_redirect = 0;

    if ((ktcp_svc!=NULL) && (ktcp_svc->running))
    {
        if (is_cancel)
        {
            cancel_delayed_work_sync(&rtl8117_ehci_dev.data_read_schedule);
            for (i=0; i<OUTdescNumber; i++)
            {
                ww = &t_work[i];
                cancel_work_sync(&ww->data_write_schedule);
            }
        }

        if (ehci_readl(CMDSTS) & 0x00000080) {
	    printk("[EHCI] stop USB while having pending Bulk-In\n");
	    indesc->own = 0;
	}

	/* waiting CSW transfer, workaound for rotten CSW */
	msleep(10);

        Wt_IBIO(PORTSC, 0x1000);
        rtl8117_ehci_writeb(0, DEVICE_ADDRESS);
        //ehci_patch();
        mdelay(1);
        ktcp_stop();
    }
    else
    {
        mutex_lock(&rtl8117_ehci_dev.mutex);
        enter = 0;
        mutex_unlock(&rtl8117_ehci_dev.mutex);
        return;
    }

    show_register(0);

    rtl8117_ehci_dev.last_use = val;
    if (!rtl8117_ehci_dev.in_use_hid)
        usb_redirect = 0;
    mutex_lock(&rtl8117_ehci_dev.mutex);
    enter = 0;
    mutex_unlock(&rtl8117_ehci_dev.mutex);

    printk("after stop usb: in_use_hid:%d, kb:%d, mouse:%d\n",
	   rtl8117_ehci_dev.in_use_hid,
	   rtl8117_ehci_dev.kb_hid_enabled,
	   rtl8117_ehci_dev.mouse_hid_enabled
	   );
}

int rtl8117_ehci_close_file(void)
{
    if (rtl8117_ehci_dev.filp) {
        vfs_fsync(rtl8117_ehci_dev.filp, 1);
        filp_close(rtl8117_ehci_dev.filp, NULL);

        rtl8117_ehci_dev.blksize = 0;
        rtl8117_ehci_dev.blkbits = 0;
        rtl8117_ehci_dev.filp = NULL;
        rtl8117_ehci_dev.file_length = 0;
        rtl8117_ehci_dev.num_sectors = 0;
    }

    return 0;
}

int rtl8117_ehci_open_file(char *path)
{
    struct file			*filp = NULL;
    int				rc = -EINVAL;
    struct inode			*inode = NULL;
    loff_t				size;
    loff_t				num_sectors;
    loff_t				min_sectors;
    unsigned int			blkbits;
    unsigned int			blksize;

    filp = filp_open(path, O_RDWR | O_LARGEFILE, 0);
    if (IS_ERR(filp)) {
        printk(KERN_CRIT "[EHCI] unable to open backing file: %s, filp = %d\n", path, (int)PTR_ERR(filp));
        dump_stack();
        return PTR_ERR(filp);
    }

    inode = file_inode(filp);
    if ((!S_ISREG(inode->i_mode) && !S_ISBLK(inode->i_mode))) {
        printk(KERN_CRIT "[EHCI] invalid file type: %s\n", path);
        dump_stack();
        goto out;
    }

    size = i_size_read(inode->i_mapping->host);
    if (size < 0) {
        printk(KERN_CRIT "[EHCI] unable to find file size: %s\n", path);
        rc = (int) size;
        goto out;
    }

    if (inode->i_bdev) {
        blksize = bdev_logical_block_size(inode->i_bdev);
        blkbits = blksize_bits(blksize);
    } else {
        blksize = 512;
        blkbits = 9;
    }

    num_sectors = size >> blkbits; /* File size in logic-block-size blocks */
    min_sectors = 1;

    if (num_sectors < min_sectors) {
        printk(KERN_CRIT "[EHCI] file too small: %s\n", path);
        rc = -ETOOSMALL;
        goto out;
    }

    rtl8117_ehci_dev.blksize = blksize;
    rtl8117_ehci_dev.blkbits = blkbits;
    rtl8117_ehci_dev.filp = filp;
    rtl8117_ehci_dev.file_length = size;
    rtl8117_ehci_dev.num_sectors = num_sectors;
    rtl8117_ehci_dev.usb_type = 0;
    rtl8117_ehci_dev.hid_state = 0;
    return 0;

out:
    fput(filp);
    return rc;
}

void handle_ep0(void)
{
    if (rtl8117_ehci_dev.ep0_status == EP0_IN_DATA_STATUS)
    {
	printk("\thandle ep0 at IN status, ctrlout idx:%d\n", ehci_core.ctl_outdesc_index);
        rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, false, false);
    }
    else if (rtl8117_ehci_dev.ep0_status == EP0_OUT_DATA_STATUS)
    {
	printk("\thandle ep0 at OUT status, ctrlout idx:%d\n", ehci_core.ctl_outdesc_index);
        rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
    } else {
	printk("*** handle ep0 at wrong status:%d, portsc1:0x%x\n",
	       ehci_core.ctl_outdesc_index, Rd_IBIO(PORTSC+0x04));
    }
}

#define _UREQNAME(name)		[name] = #name
static const char* usb_req_name[32] = {
    [0 ... 31] = "NA",
    _UREQNAME(USB_REQ_SET_ADDRESS),
    _UREQNAME(USB_REQ_SET_CONFIGURATION),
    _UREQNAME(USB_REQ_GET_DESCRIPTOR),
    _UREQNAME(USB_REQ_CLEAR_FEATURE),
    _UREQNAME(USB_REQ_GET_INTERFACE),
    _UREQNAME(USB_REQ_SET_INTERFACE),
    _UREQNAME(USB_REQ_GET_CONFIGURATION),
    _UREQNAME(USB_GET_IDLE_REQUEST),
    _UREQNAME(USB_REQ_SET_FEATURE),
};

#if 0
static void rtl8117_control_work_func_t(struct work_struct *work)
{
    struct usb_ctrlrequest *request = &rtl8117_ehci_dev.request;
#else
static void rtl8117_control_work_func_t(void)
{
    struct usb_ctrlrequest *request = &rtl8117_ehci_dev.request;
#endif
    u8 desctype, descindex;
    u8 length = request->wLength;
    u8 type = 0;
    u8 desc_length = 0;
    u8 *dbg = (u8*)request;
    u32 portsc_val;

    printk("%x:%s to port %d, %s size %d at status:%d[%d/%d]\n",
	   request->bRequest,
	   request->bRequest<32?usb_req_name[request->bRequest]:"NA",
	   rtl8117_ehci_dev.portnum,
	   (request->bRequestType >> 7)?"IN":"OUT",
	   request->wLength,
	   rtl8117_ehci_dev.ep0_status,
	   EP0_IN_DATA_STATUS, EP0_OUT_DATA_STATUS);

    portsc_val = Rd_IBIO(PORTSC+(0x04)*rtl8117_ehci_dev.portnum);

    printk("Setup packet %02x %02x %02x %02x %02x %02x %02x %02x\n",
	   *dbg, *(dbg+1), *(dbg+2), *(dbg+3), *(dbg+4), *(dbg+5), *(dbg+6),
	   *(dbg+7));

    if(portsc_val != VALID_PORTSC){
	printk("setup to port %d with invalid portsc:%x\n",
	       rtl8117_ehci_dev.portnum, portsc_val);
	goto do_nothing;
    }

    if ((request->bRequestType >> 7) == 1)
        rtl8117_ehci_dev.ep0_status = EP0_IN_DATA_STATUS;       // to host
    else
	rtl8117_ehci_dev.ep0_status = EP0_OUT_DATA_STATUS;      // to device

    switch ( request->bRequest ) {
    case USB_REQ_SET_ADDRESS:
        if (!usb_redirect)
        {
            rtl8117_ehci_writeb(request->wValue, DEVICE_ADDRESS);
        }
        else
        {
	    if (rtl8117_ehci_dev.portnum == 0) {
		printk("[EHCI] set msdaddr %u\n", request->wValue);

		rtl8117_ehci_writeb(request->wValue, DEVICE_ADDRESS);
		rtl8117_ehci_dev.msd_address = request->wValue;
		rtl8117_ehci_dev.usb_state = ENABLED;
	    } else if (rtl8117_ehci_dev.portnum == 1) {
		if(rtl8117_ehci_dev.hid_address &&
		   (rtl8117_ehci_dev.kb_hid_enabled ||
		    rtl8117_ehci_dev.mouse_hid_enabled))
		    printk("[EHCI] set hidaddr when already enabled, %u=>%u, DEVICE ADDR:%06x\n",
			   rtl8117_ehci_dev.hid_address, request->wValue,
			   ehci_readl(DEVICE_ADDRESS)
			  );
		else
		    printk("[EHCI] set hidaddr %u\n", request->wValue);

		rtl8117_ehci_writeb(request->wValue, DEVICE_ADDRESS+1);
		rtl8117_ehci_writeb(request->wValue, DEVICE_ADDRESS+2);
		rtl8117_ehci_dev.hid_address = request->wValue;
	    } else {
		printk("[EHCI] msdaddr:%d, hidaddr:%d set address when both non-zero, DEVICE ADDR:%06x\n",
		       rtl8117_ehci_dev.msd_address,
		       rtl8117_ehci_dev.hid_address,
		       ehci_readl(DEVICE_ADDRESS)
		       );
	    }
        }
        memset(rtl8117_ehci_dev.control_buf, 0, 64);
        rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
        return;

    case USB_REQ_SET_CONFIGURATION:
#if 0
        if (!usb_redirect)
        {
            memset(rtl8117_ehci_dev.control_buf, 0, 64);
            rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
        }
        else
        {
            if ((rtl8117_ehci_dev.in_use_redirect==1) && (rtl8117_ehci_dev.in_use_hid==0))
            {
                memset(rtl8117_ehci_dev.control_buf, 0, 64);
                rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
            }
            else
            {
                type = (request->bRequestType>>5)&0x3;
                if (type == 0)
                {
                    memset(rtl8117_ehci_dev.control_buf, 0, 64);
                    rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
                }
                else
                {
                    rtl8117_ehci_dev.ep0_status = EP0_OUT_DATA_STATUS;
                    rtl8117_ehci_ep0_start_transfer(request->wLength, NULL, false, false);
                }
            }
        }
#else
	/*
	   if no data segment, return status, otherwise, leave the response to
	   transaction handler.
	 */
	if(request->wLength == 0)
	    goto early_ack;
#endif

        return;

    case USB_REQ_GET_DESCRIPTOR:
        desctype = request->wValue >> 8;
        descindex = (request->wValue & 0xFF);

        if (desctype == USB_DT_DEVICE) {
            memcpy(rtl8117_ehci_dev.control_buf, &msg_device_desc, 18);
            //if ((rtl8117_ehci_dev.portnum == 1) && (usb_redirect) && (rtl8117_ehci_dev.last_use == 2))
            if ((rtl8117_ehci_dev.portnum == 1) && (usb_redirect))
            {
                //printk("((rtl8117_ehci_dev.portnum == 1) && (usb_redirect) && (rtl8117_ehci_dev.last_use == 2))\n");
                rtl8117_ehci_dev.control_buf[16] = 0x00;
                rtl8117_ehci_dev.control_buf[10] = 0x69;
            }
            desc_length = 18;
        }

        if (desctype == USB_DT_CONFIG) {
            //if ((rtl8117_ehci_dev.portnum == 1) && (usb_redirect) && (rtl8117_ehci_dev.last_use == 2))
            if ((rtl8117_ehci_dev.portnum == 1) && (usb_redirect))
            {
                //printk("CBI desc.\n");
                memcpy(rtl8117_ehci_dev.control_buf, descriptors_cbi_hid, sizeof(descriptors_cbi_hid));
                desc_length = sizeof(descriptors_cbi_hid);
            }
            else {
                memcpy(rtl8117_ehci_dev.control_buf, &msg_config_desc, USB_DT_CONFIG_SIZE);
                desc_length += USB_DT_CONFIG_SIZE;

                memcpy(rtl8117_ehci_dev.control_buf + desc_length, &fsg_intf_desc, USB_DT_INTERFACE_SIZE);
                desc_length += USB_DT_INTERFACE_SIZE;

                memcpy(rtl8117_ehci_dev.control_buf + desc_length, &fsg_fs_bulk_in_desc, USB_DT_ENDPOINT_SIZE);
                desc_length += USB_DT_ENDPOINT_SIZE;

                memcpy(rtl8117_ehci_dev.control_buf + desc_length, &fsg_fs_bulk_out_desc, USB_DT_ENDPOINT_SIZE);
                desc_length += USB_DT_ENDPOINT_SIZE;
            }

        }

        if (desctype == USB_DT_STRING) {
            if (descindex == 0) {
                memcpy(rtl8117_ehci_dev.control_buf, str0, sizeof(str0));
                desc_length = sizeof(str0);
            }
            else if (descindex == 1) {
                memcpy(rtl8117_ehci_dev.control_buf, str1, sizeof(str1));
                desc_length = sizeof(str1);
            }
            else if (descindex == 2) {
                memcpy(rtl8117_ehci_dev.control_buf, str2, sizeof(str2));
                desc_length = sizeof(str2);
            }
            else if (descindex == 3) {
                memcpy(rtl8117_ehci_dev.control_buf, str3[rtl8117_ehci_dev.usb_type], sizeof(str3[rtl8117_ehci_dev.usb_type]));
                //desc_length = sizeof(str3[rtl8117_ehci_dev.usb_type]);
            }
            desc_length = rtl8117_ehci_dev.control_buf[0];
        }
#ifdef CONFIG_USB_REDIRECTION
        if (usb_redirect) {
            if (desctype == DEVICE_REPORT_TYPE) {
                switch(request->wIndex)
                {
                case 0x01://hid: mouse=int_num1
                    desc_length = sizeof(device_report_type1);
                    memcpy(rtl8117_ehci_dev.control_buf, device_report_type1, desc_length);
                    rtl8117_ehci_dev.hid_state = ENABLED;
                    rtl8117_ehci_dev.mouse_hid_enabled = true;
                    printk("report vnc mouse hid, hid addr:%u, DEVICE ADDR:%06x\n",
			   rtl8117_ehci_dev.hid_address,
			   ehci_readl(DEVICE_ADDRESS));
                    break;

                default://hid: keyboard=int_num0
                    desc_length = sizeof(device_report_type0);
                    memcpy(rtl8117_ehci_dev.control_buf, device_report_type0, desc_length);
                    rtl8117_ehci_dev.kb_hid_enabled = true;
                    printk("report vnc keyboard hid, hid addr:%u, DEVICE ADDR:%06x\n",
			   rtl8117_ehci_dev.hid_address,
			   ehci_readl(DEVICE_ADDRESS));
                    break;
                }
            }
        }
#endif
#if 0
        //if (rtl8117_ehci_dev.in_use_redirect)
        {
            int i = 0;
            printk("\tdesc len:%d\t\n", desc_length);
            for (i=0; i< desc_length; i++) {
		if(!(i & 0x0F))
		    printk("\n\t");
                printk("%02x ",rtl8117_ehci_dev.control_buf[i]);
            }
            printk("\n");
        }
#endif

        if (length < desc_length)
            rtl8117_ehci_ep0_start_transfer(length , rtl8117_ehci_dev.control_buf, true, false);
        else
            rtl8117_ehci_ep0_start_transfer(desc_length , rtl8117_ehci_dev.control_buf, true, false);

        break;

    case USB_REQ_CLEAR_FEATURE:
        if (!usb_redirect)
        {
            //printk("USB_REQ_CLEAR_FEATURE OOB2IB.\n");
            memset(rtl8117_ehci_dev.control_buf, 0, 64);
            rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
        }
        else
        {
            if ((rtl8117_ehci_dev.in_use_redirect==1) && (rtl8117_ehci_dev.in_use_hid==0))
            {
                //printk("USB_REQ_GET_INTERFACE USB redirection.\n");
                memset(rtl8117_ehci_dev.control_buf, 0, 64);
                rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
            }
            else
            {
                type = (request->bRequestType>>5)&0x3;
                printk("USB_REQ_GET_INTERFACE KVM, type:%d, case: %d\n", type, request->bRequestType&0x1F);
                if ((type == 0))
                {
                    switch (request->bRequestType&0x1F)
                    {
                    case USB_RECIP_DEVICE:
                        memset(rtl8117_ehci_dev.control_buf, 0, 64);
                        rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
                        if (ehci_readb(DEVICE_ADDRESS + 1)!=0) {
                            rtl8117_ehci_dev.kb_hid_enabled = true;
                            rtl8117_ehci_dev.mouse_hid_enabled = true;
                        }
                        break;
                    case USB_RECIP_ENDPOINT:
                        rtl8117_ehci_dev.IN_stall = 0;
                        memset(rtl8117_ehci_dev.control_buf, 0, 64);
                        rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
                        break;
                    default:
                        memset(rtl8117_ehci_dev.control_buf, 0, 64);
                        rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
                        break;
                    }
                }
                else
                {
                    rtl8117_ehci_dev.ep0_status = EP0_IN_DATA_STATUS;
                    memset(rtl8117_ehci_dev.control_buf, 0, 64);
                    rtl8117_ehci_ep0_start_transfer(length , rtl8117_ehci_dev.control_buf, true, false);
                }
            }
        }

        break;

    case USB_REQ_GET_INTERFACE:
        request->wLength = 1;

        type = (request->bRequestType>>5)&0x3;
        if ((type == 0) || (!usb_redirect))//standard
        {
            //printk("USB_REQ_GET_INTERFACE OOB2IB or usb redirection.\n");
            rtl8117_ehci_ep0_start_transfer(request->wLength , rtl8117_ehci_dev.control_buf, true, false);
        }
        else { //HID class code: SET_IDLE
            //printk("USB_REQ_GET_INTERFACE KVM.\n");
            rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
        }
        break;

    case USB_REQ_SET_INTERFACE:
        rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
        break;

    case USB_REQ_GET_CONFIGURATION:
        request->wLength = 1;
        rtl8117_ehci_ep0_start_transfer(request->wLength, rtl8117_ehci_dev.control_buf, true, false);
        break;

    case USB_GET_IDLE_REQUEST:
        //HID class code: GET_IDLE
        rtl8117_ehci_dev.ep0_status = EP0_IN_DATA_STATUS;
        request->wLength = 1;
        rtl8117_ehci_ep0_start_transfer(request->wLength, rtl8117_ehci_dev.control_buf, true, false);
        break;

    case USB_REQ_SET_FEATURE:
        type = (request->bRequestType>>5)&0x3;
        if ((type == 0) || (!usb_redirect))//standard
        {
            //printk("USB_REQ_SET_FEATURE OOB2IB or usb redirection.\n");
            memset(rtl8117_ehci_dev.control_buf, 0, 64);
            rtl8117_ehci_dev.control_buf[0] = 0x01;
            rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
        }
        else { //HID class code: GET_PROTOCOL
            //printk("USB_REQ_SET_FEATURE KVM.\n");
            rtl8117_ehci_dev.ep0_status = EP0_IN_DATA_STATUS;
            request->wLength = 1;
            rtl8117_ehci_ep0_start_transfer(request->wLength, rtl8117_ehci_dev.control_buf, true, false);
        }
        break;

        /* LUNS */
        //case IdeGetMaxLun:
    case IdeGetMaxLun:
        redir_reboot = 1;
        //rtl8117_ehci_dev.IN_stall = 1;
        if (rtl8117_ehci_dev.usb_state == CONNECTED && rtl8117_ehci_dev.hid_state == ENABLED)
        {
            rtl8117_ehci_dev.portnum = 1;
        }
        memset(rtl8117_ehci_dev.control_buf, 0, 64);
        rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true,
					false);
        break;

    default:
        break;
    }

    return;

early_ack:
    memset(rtl8117_ehci_dev.control_buf, 0, 64);
    rtl8117_ehci_ep0_start_transfer(0 , rtl8117_ehci_dev.control_buf, true, false);
    return;

do_nothing:
    rtl8117_ehci_dev.ep0_status = EP0_IDLE;
    return;
}

int do_inquary(u8* buf,u8 *status)
{
    int len;
    *status = US_BULK_STAT_OK;
    len = sizeof(inquiry_data[rtl8117_ehci_dev.usb_type]);
    memcpy(buf, inquiry_data[rtl8117_ehci_dev.usb_type], len);
    return len;
}

int do_read_format_capacities(u8* destbuf, u8 *status)
{
    loff_t num_sectors = rtl8117_ehci_dev.num_sectors;
    unsigned int	blksize = rtl8117_ehci_dev.blksize;

    destbuf[0] = destbuf[1] = destbuf[2] = 0;
    destbuf[3] = 8;	/* Only the Current/Maximum Capacity Descriptor */
    destbuf += 4;

    put_unaligned_be32(num_sectors, &destbuf[0]);
    /* Number of blocks */
    put_unaligned_be32(blksize, &destbuf[4]);/* Block length */
    destbuf[4] = 0x02;				/* Current capacity */

    *status = US_BULK_STAT_OK;
    return 12;
}

int do_read_capacity(u8* cmnd, u8* destbuf, u8 *status)
{
    loff_t num_sectors = rtl8117_ehci_dev.num_sectors;
    unsigned int	blksize = rtl8117_ehci_dev.blksize;

    u32		lba = get_unaligned_be32(&cmnd[2]);
    int		pmi = cmnd[8];

    u8		*buf = destbuf;

    /* Check the PMI and LBA fields */
    if (pmi > 1 || (pmi == 0 && lba != 0)) {
        //curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
        *status = US_BULK_STAT_PHASE;
        return -EINVAL;
    }

    put_unaligned_be32(num_sectors - 1, &buf[0]);
    /* Max logical block */
    put_unaligned_be32(blksize, &buf[4]);/* Block length */

    *status = US_BULK_STAT_OK;
    return 8;
}

static inline u32 get_unaligned_be24(u8 *buf)
{
    return 0xffffff & (u32) get_unaligned_be32(buf - 1);
}

static int do_read(u8* cmnd, u8* destbuf, int data_size_from_cmnd, u8 *status)
{
    loff_t num_sectors = rtl8117_ehci_dev.num_sectors;
    unsigned int	blkbits = rtl8117_ehci_dev.blkbits;

    u32 lba;
    u32 amount_left;
    loff_t file_offset;
    loff_t file_offset_tmp;

    int nread = 0;
    mm_segment_t old_fs;

    if (!rtl8117_ehci_dev.filp) {
        *status = US_BULK_STAT_PHASE;
        return -EINVAL;
    }

    if (cmnd[0] == READ6)
        lba = get_unaligned_be24(&cmnd[1]);
    else {
        lba = get_unaligned_be32(&cmnd[2]);

        /*
         * We allow DPO (Disable Page Out = don't save data in the
         * cache) and FUA (Force Unit Access = don't read from the
         * cache), but we don't implement them.
         */
        if ((cmnd[1] & ~0x18) != 0) {
            *status = US_BULK_STAT_FAIL;
            return -EINVAL;
        }
    }
    if (lba >= num_sectors) {
        *status = US_BULK_STAT_FAIL;
        return -EINVAL;
    }
    file_offset = ((loff_t) lba) << blkbits;

    /* Carry out the file reads */
    amount_left = data_size_from_cmnd;
    if (unlikely(amount_left == 0))
        return -EIO;		/* No default reply */

    file_offset_tmp = 0;

    old_fs = get_fs();
    set_fs(KERNEL_DS);

    nread = vfs_read(rtl8117_ehci_dev.filp,
                     (char __user *)destbuf,
                     data_size_from_cmnd, &file_offset);

    set_fs(old_fs);

    *status = US_BULK_STAT_OK;
    return nread;
}

static int do_write(u8* cmnd, u8* destbuf, int data_size_from_cmnd ,u8 *status)
{
    unsigned int	blkbits = rtl8117_ehci_dev.blkbits;
    u32			lba;
    int			get_some_more;
    u32			amount_left_to_req, amount_left_to_write;
    loff_t			usb_offset, file_offset; //file_offset_tmp;

    int nwrite = 0;
    mm_segment_t old_fs;

#if 0
    if (curlun->ro) {
        curlun->sense_data = SS_WRITE_PROTECTED;
        return -EINVAL;
    }
#endif

    if (!rtl8117_ehci_dev.filp) {
        *status = US_BULK_STAT_PHASE;
        return -EINVAL;
    }

    spin_lock(&rtl8117_ehci_dev.filp->f_lock);
    rtl8117_ehci_dev.filp->f_flags &= ~O_SYNC;	/* Default is not to wait */
    spin_unlock(&rtl8117_ehci_dev.filp->f_lock);

    /*
     * Get the starting Logical Block Address and check that it's
     * not too big
     */
    if (cmnd[0] == WRITE6)
        lba = get_unaligned_be24(&cmnd[1]);
    else {
        lba = get_unaligned_be32(&cmnd[2]);

        /*
         * We allow DPO (Disable Page Out = don't save data in the
         * cache) and FUA (Force Unit Access = write directly to the
         * medium).  We don't implement DPO; we implement FUA by
         * performing synchronous output.
         */
        if (cmnd[1] & ~0x18) {
            //curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
            *status = US_BULK_STAT_FAIL;
            return -EINVAL;
        }

    }

    /* Carry out the file writes */
    get_some_more = 1;
    file_offset = usb_offset = ((loff_t) lba) << blkbits;
    amount_left_to_req = data_size_from_cmnd;
    amount_left_to_write = data_size_from_cmnd;

    //printk(KERN_INFO "do_write file_offset = %d, count = %d\n", (u32)file_offset, amount_left_to_req);

    old_fs = get_fs();
    set_fs(KERNEL_DS);

    nwrite = vfs_write(rtl8117_ehci_dev.filp,
                       (char __user *)destbuf,
                       data_size_from_cmnd, &file_offset);
    set_fs(old_fs);

    *status = US_BULK_STAT_OK;
    return nwrite;		/* No default reply */
}


static void do_mode_sense(u8* cmnd, u8* destbuf, u8 *status)
{
    memset(destbuf, 0x0, 8);
    *destbuf = 0x3;
}

static int do_synchronize_cache(struct file *filp, u8 *status)
{
    vfs_fsync(filp, 1);
    return 0;
}

static int do_request_sense(u8* cmnd, u8* destbuf, u8 *status, u8 lc)
{
    static u8 enter = 0;
    memset(destbuf, 0x0, 18);
    destbuf[0] = 0x70;
    destbuf[7] = 0x0A;
    if (!enter)
    {
        destbuf[2] = 0x07;
        destbuf[12] = 0x27;
    }
    else
    {
        switch (lc)
        {
        case MODE_SENSE6:
        case INQUIRY:
            destbuf[2] = 0x05;
            destbuf[12] = 0x24;//invalid command
            break;
        default:
            destbuf[2] = 0x05;
            destbuf[12] = 0x20;//invlaid operation
            break;
        }
    }
    enter = 1;
    return 18;
}


static
void __schedule_write_work(unsigned int len)
{
    static u32 write_rev_length;
    struct write_work *ww = &t_work[ehci_core.bulk_outdesc_index];
#if __DEV__
    u32 len_verify;
#endif


    ww->write_work_data = len;
    ww->flags = 0;
    ww->payload = phys_to_virt(
                      ((outindesc_r *)(ehci_core.bulk_outdesc_addr) + ehci_core.bulk_outdesc_index)->bufaddr);

    if(ehci_core.bulk_state == EHCI_STATE_BULK_CBW) {
        dev_msg("\tschedule write command\n");
        ww->flags |= WW_FIRST;
        ehci_core.bulk_state = EHCI_STATE_BULK_OUT;
        write_rev_length = rtl8117_ehci_dev.data_write_cbw.dCBWDataTransferLength;

#if __DEV__
        /* transfer len checking */
        if (rtl8117_ehci_dev.data_write_cbw.rbc[0] == WRITE10)
            len_verify = ((rtl8117_ehci_dev.data_write_cbw.rbc[7] << 8) +
                          rtl8117_ehci_dev.data_write_cbw.rbc[8]) * rtl8117_ehci_dev.blksize;
        else if (rtl8117_ehci_dev.data_write_cbw.rbc[0] == VERIFY)
            len_verify = ((rtl8117_ehci_dev.data_write_cbw.rbc[7] << 16) +
                          (rtl8117_ehci_dev.data_write_cbw.rbc[8] << 8) +
                          rtl8117_ehci_dev.data_write_cbw.rbc[9]);
        else
            len_verify = ((rtl8117_ehci_dev.data_write_cbw.rbc[6] << 24) +
                          (rtl8117_ehci_dev.data_write_cbw.rbc[7] << 16) +
                          (rtl8117_ehci_dev.data_write_cbw.rbc[8] << 8) +
                          rtl8117_ehci_dev.data_write_cbw.rbc[9]) * rtl8117_ehci_dev.blksize;

        dev_cond_msg(write_rev_length != len_verify,
                     "\t** Write Data length not equal to CBW length\n");
#endif
    } else {
        dev_cond_msg(ehci_core.bulk_state != EHCI_STATE_BULK_OUT,
                     "\t** schedule write at mismatch bulk state(%d)\n", ehci_core.bulk_state);
        dev_msg("\tschedule write data, rev:%d, len:%d\n", write_rev_length, len);

        if(write_rev_length <= len) {
            ww->flags |= WW_LAST;
            ehci_core.bulk_state = EHCI_STATE_BULK_CBW;


            /*
               [FIXME] Remote EHCI is hard to support error control since unable to
               get remote EHCI operation response, assuming operations always a success.
             */
            ww->csw.dCSWSignature = cpu_to_le32(0x53425355);
            ww->csw.dCSWTag = rtl8117_ehci_dev.data_write_cbw.dCBWTag;
            ww->csw.dCSWDataResidue =
                rtl8117_ehci_dev.data_write_cbw.dCBWDataTransferLength;
            ww->csw.bCSWStatus = 0;
        } else {
            write_rev_length -= len;
        }
    }

    schedule_work(&ww->data_write_schedule);
}

static
void __release_bulkout_desc(int idx)
{
    outindesc_r *outdesc = (outindesc_r *)(ehci_core.bulk_outdesc_addr) + idx;

    dev_msg("\t\tbulkout %d done, return memory to EHCI engine(%x)\n", idx,
            bulkout_bh_bv.counter);

    dma_sync_single_for_device(&ehci_core.pdev->dev, outdesc->bufaddr, MAX_BULK_LEN, DMA_TO_DEVICE);
    activate_bulkout_desc(idx, MAX_BULK_LEN, NULL);

    dev_msg("\t\toutdesc index:%d, bv:%x, own:%d:%d:%d:%d\n",
            ehci_core.bulk_outdesc_index,
            bulkout_bh_bv.counter,
            ((outindesc_r *) (ehci_core.bulk_outdesc_addr)+3)->own,
            ((outindesc_r *) (ehci_core.bulk_outdesc_addr)+2)->own,
            ((outindesc_r *) (ehci_core.bulk_outdesc_addr)+1)->own,
            ((outindesc_r *) (ehci_core.bulk_outdesc_addr))->own
           );
}


static u8 lc = 0;
static void rtl8117_data_read_work_func_t(struct work_struct *work)
{
    struct command_block_wrapper *cbw = &rtl8117_ehci_dev.data_read_cbw;
    u8 *databuf = rtl8117_ehci_dev.data_read_buf;

    u32 residual_length = cbw->dCBWDataTransferLength;
    int i =0;
    int ret = 0, tcp_ret = 0;
    int data_size_from_cmnd = 0;

    u8 bCSWStatus = 0;
    int transfer_left = 0, transfer_size = 0, readbuf_offset = 0;

    dev_msg("\tcbw rbc 0:%x/%x\n", cbw->rbc[0], GET_EVENT_STATUS_NOTIFICATION);

    if(!rtl8117_ehci_dev.in_use_redirect) {
	printk("[EHCI] drop Bulk READ command during USB disconnecting\n");
	return;
    }

    switch (cbw->rbc[0]) {
    case INQUIRY:
        ret = do_inquary(databuf, &bCSWStatus);
        if (ret < residual_length)
            memset(databuf+ret, 0, residual_length-ret);
        ret = residual_length;
        if (!ret )
            bCSWStatus =  1;
        break;

    case READ_FORMAT_CAPACITIES:
#ifdef CONFIG_USB_REDIRECTION
	if(rtl8117_ehci_dev.in_use_redirect)
        {
            tcp_ret = ktcp_send(csock, (unsigned char *)cbw->rbc, 16);
            if (tcp_ret == -1)
            {
                goto error;
            }
            ret = ktcp_recv(csock, databuf, 512);
            if (ret == -1)
            {
                goto error;
            }
            ret = databuf[3];
            databuf += 4;
        }
        else
#endif
            ret = do_read_format_capacities(databuf, &bCSWStatus);
        break;

    case SECURITY_PROTOCOL_IN:
        memset(databuf, 0, residual_length);
        ret = residual_length;
        break;

    case READ_CAPACITY:
#ifdef CONFIG_USB_REDIRECTION
	if(rtl8117_ehci_dev.in_use_redirect)
        {
            if (rtl8117_ehci_dev.usb_type == 0)
            {
                tcp_ret = ktcp_send(csock, (unsigned char *)cbw->rbc, 16);
                if (tcp_ret == -1)
                    goto error;
                ret = ktcp_recv(csock, databuf, 512);
                if (ret == -1)
                    goto error;
                ret = databuf[3];
                databuf += 4;
            }
            else
            {
                tcp_ret = ktcp_send(csock, (unsigned char *)cbw->rbc, 16);
                if (tcp_ret == -1)
                    goto error;
                ret = ktcp_recv(csock, databuf, 512);
                if (ret == -1)
                    goto error;
                ret = databuf[3];
                databuf += 4;
            }
            if ( rtl8117_ehci_dev.usb_type == 1)
                rtl8117_ehci_dev.blksize = 2048;
            else
                rtl8117_ehci_dev.blksize = 512;
            rtl8117_ehci_dev.block_nr = (databuf[0]<<24) + (databuf[1]<<16) + (databuf[2]<<8) + (databuf[3]);
        }
        else
#endif
            ret = do_read_capacity(cbw->rbc, databuf, &bCSWStatus);
        break;

    case READ6:
    case READ10:
#ifdef CONFIG_USB_REDIRECTION
	if(rtl8117_ehci_dev.in_use_redirect)
        {
            unsigned int netlink_rcv_len = 0;
            if (cbw->rbc[0] == READ10)
            {
                netlink_rcv_len = (cbw->rbc[2]<<24) + (cbw->rbc[3]<<16) + (cbw->rbc[4]<<8) +(cbw->rbc[5]);
                if (netlink_rcv_len > rtl8117_ehci_dev.block_nr)
                {
                    bCSWStatus = 0x02;
                    ret = 0;
                    break;
                }

                netlink_rcv_len = 0;
            }

            tcp_ret = ktcp_send(csock, (unsigned char *)cbw->rbc, 16);
            if (tcp_ret == -1)
            {
                printk("send cbw return -1.\n");
                goto error;
            }

            while (netlink_rcv_len < cbw->rbc[8]*rtl8117_ehci_dev.blksize)
            {
                tcp_ret = ktcp_recv(csock, databuf+ netlink_rcv_len, 1024);
                if (tcp_ret == -1)
                {
                    printk("read rcv return -1.\n");
                    goto error;
                }
                else
                    netlink_rcv_len += tcp_ret;
            }
            ret = netlink_rcv_len;
        }
        else
#endif
        {
            data_size_from_cmnd = residual_length;
            ret = do_read(cbw->rbc, databuf, data_size_from_cmnd, &bCSWStatus);
        }
        break;

    case MODE_SENSE6:
        do_mode_sense(cbw->rbc, databuf, &bCSWStatus);
        ret = (cbw->dCBWDataTransferLength > 8) ? 8 : cbw->dCBWDataTransferLength;
        if(ret == 4)
            ret = 3;
        break;

    case TEST_UNIT_READY:
        ret = 0;
        break;

    case PREVENT_ALLOW_MEDIUM_REMOVAL:
        bCSWStatus = cbw->rbc[4];
        ret = 0;
        break;

    case REQUEST_SENSE:
        ret = do_request_sense(cbw->rbc, databuf, &bCSWStatus, lc);
        ret = cbw->dCBWDataTransferLength;
        if (!ret )
            bCSWStatus =  1;
        break;

    case SYNC_CACHE:
        bCSWStatus = 1;
#ifndef CONFIG_USB_REDIRECTION
        ret = do_synchronize_cache(rtl8117_ehci_dev.filp, &bCSWStatus);
#else
        if (!usb_redirect)
            ret = do_synchronize_cache(rtl8117_ehci_dev.filp, &bCSWStatus);
        else
            ret = 0;
#endif
        break;

    case START_STOP_UNIT:
        ret = 0;
        bCSWStatus = 0;
        break;

    case GET_EVENT_STATUS_NOTIFICATION:
        memset(databuf, 0, residual_length);
        databuf[3] = 0x5E;
        if (!cbw->rbc[4])
        {
            databuf[0] = 0x00;
            databuf[1] = 0x02;
            databuf[2] = 0x80;
            //patch usb reconnection issue@AMD Larne platform
            ret = 8;
        }
        else
        {
            databuf[0] = 0x00;
            databuf[1] = 0x06;
            for (i = 1; i < 8; i++)
            {
                if (cbw->rbc[4] & (1 << i))
                {
                    databuf[2] = i;
                    break;
                }
            }
            if (databuf[2] == 0x04)
                databuf[5] = 0x02; //media present
            ret = residual_length;
        }
        break;

    case READ_TRACK_INFORMATION://for linux
    case READ_TOC:
    case GET_CONFIGURATION:
	if(rtl8117_ehci_dev.in_use_redirect)
        {
            tcp_ret = ktcp_send(csock, (unsigned char *)cbw->rbc, 16);
            if (tcp_ret == -1)
                goto error;
            ret = ktcp_recv(csock, databuf, 512);
            if (ret == -1)
                goto error;
            ret = cbw->dCBWDataTransferLength;
            databuf += 4;
            if ((cbw->rbc[0] == READ_TRACK_INFORMATION) && (rtl8117_ehci_dev.usb_type == 1) && (ret > 16))
            {
                for (i=0; i<4; i++)
                {
                    databuf[24+i] = (rtl8117_ehci_dev.block_nr>>(8*(3-i)))&0x00FF;
                    databuf[28+i] = ((rtl8117_ehci_dev.block_nr-1)>>(8*(3-i)))&0x00FF;
                }
            }
        }
        break;

    case MODE_SENSE10:
        ret = ((cbw->rbc[7] << 8) + cbw->rbc[8]);
        if (ret != 0)
        {
            if (ret > 40)
            {
                //rtl8117_ehci_dev.data_read_csw.dCSWDataResidue = ret - 0x40;
                //rtl8117_ehci_dev.data_read_csw.bCSWStatus = 1;
                bCSWStatus = 1;
                ret = 40;
            }

            //if(rtl8117_ehci_dev.data_read_csw.dCSWDataResidue > 512)
            //    rtl8117_ehci_dev.IN_stall = 1;

            if (rtl8117_ehci_dev.usb_type == 2)
                memcpy(databuf, mode_sense,sizeof(mode_sense));
            else
                memcpy(databuf, disc_sense, sizeof(disc_sense));
        }
        break;

    case ERASE12:
        ret = 0;
        //rtl8117_ehci_dev.IN_stall = 1;
        bCSWStatus = 1;
        rtl8117_ehci_ep_start_transfer( 0, databuf, false );
        break;

    case READ_DISC_INFORMATION:
        ret = cbw->dCBWDataTransferLength;
        databuf[1] = 0x20;
        databuf[2] = 0x0E;
        databuf[3] = 0x1;
        databuf[4] = 0x1;
        databuf[5] = 0x1;
        databuf[6] = 0x1;
        databuf[7] = 0x20;
        break;

    default:
        printk(KERN_INFO "[EHCI] unknown read cbw->rbc[0] = 0x%x!!!\n",cbw->rbc[0]);
        dump_stack();
        ret = 0;
        break;
    }

    /* data */
    if (ret > 0) {
        readbuf_offset = 0;
        transfer_left = ret;

        while(transfer_left) {
            transfer_size = (transfer_left >= MAX_BULK_LEN) ? (MAX_BULK_LEN) : (transfer_left);
            rtl8117_ehci_ep_start_transfer( transfer_size, databuf + readbuf_offset, false );
            transfer_left -= transfer_size;
            readbuf_offset += transfer_size;
        }
    }


    /* csw */
//    rtl8117_ehci_dev.bulk_state = EHCI_STATE_BULK_CBW;
    rtl8117_ehci_dev.data_read_csw.dCSWSignature = cpu_to_le32(0x53425355);
    rtl8117_ehci_dev.data_read_csw.dCSWTag = rtl8117_ehci_dev.data_read_cbw.dCBWTag;

#if 0
    rtl8117_ehci_dev.data_read_csw.dCSWDataResidue = ( residual_length - ret );
#else
    rtl8117_ehci_dev.data_read_csw.dCSWDataResidue = (ret > 0) ? (residual_length - ret) : (residual_length);
#endif

    rtl8117_ehci_dev.data_read_csw.bCSWStatus = bCSWStatus;

    lc = cbw->rbc[0];
    rtl8117_ehci_ep_start_transfer( CSW_SIZE, (u8*)&rtl8117_ehci_dev.data_read_csw, false );

    return;

error:
    stop_usb_redirection(rtl8117_ehci_dev.last_use, 0);
    return;
}

static void rtl8117_data_write_work_func_t(struct work_struct *work)
{
    struct command_block_wrapper *cbw = &rtl8117_ehci_dev.data_write_cbw;
    u8 *databuf = rtl8117_ehci_dev.data_write_buf;
    u32 residual_length = cbw->dCBWDataTransferLength;
    int ret, tcp_ret;
    u8 bCSWStatus = 0;
#ifdef CONFIG_USB_REDIRECTION
    struct write_work *worker = container_of(work, struct write_work, data_write_schedule);
    unsigned int write_flags = worker->flags;
#endif


    dev_msg("\twrite worker %d wakeup, flags:%x\n", worker - t_work, write_flags);

    if(!rtl8117_ehci_dev.in_use_redirect) {
	printk("[EHCI] drop Bulk WRITE command during USB disconnecting\n");
	return;
    }


    switch (cbw->rbc[0]) {
    case WRITE10:
    case WRITE12:
#ifdef CONFIG_USB_REDIRECTION
	if(rtl8117_ehci_dev.in_use_redirect)
        {
            if(worker->flags & WW_FIRST) {
                dev_msg("\t\tforward write command:%d\n", worker->write_work_data);
                tcp_ret = ktcp_send(csock, rtl8117_ehci_dev.data_write_cbw.rbc, worker->write_work_data);
            } else {
                dev_msg("\t\tforward write data, len: %d\n", worker->write_work_data);
                tcp_ret = ktcp_send(csock, worker->payload, worker->write_work_data);
            }
            if (tcp_ret == -1)
            {
                stop_usb_redirection(rtl8117_ehci_dev.last_use, 0);
            }

        }
        else
#endif
            ret = do_write(cbw->rbc, databuf, residual_length, &bCSWStatus);
        break;

    case MODE_SELECT6:
    case MODE_SELECT10:
    default:
        printk(KERN_INFO "[EHCI] unknown write cbw->rbc[0] = 0x%x!!!\n",cbw->rbc[0]);
        break;
    }


    __release_bulkout_desc(worker - t_work);

    if((write_flags & WW_LAST)) {
        /* update state and ack host */
        dev_msg("\t\tall write work finish\n");
        dev_msg("\t\treturn CSW: %08x %08x %08x %02x\n",
                rtl8117_ehci_dev.data_read_csw.dCSWSignature,
                rtl8117_ehci_dev.data_read_csw.dCSWTag,
                rtl8117_ehci_dev.data_read_csw.dCSWDataResidue,
                rtl8117_ehci_dev.data_read_csw.bCSWStatus
               );

        rtl8117_ehci_ep_start_transfer( CSW_SIZE, (u8*)&worker->csw, false);
    }

    return;
}

int rtl8117_ehci_device_init(void)
{
    int i;
#ifdef CONFIG_USB_REDIRECTION
    ehcidev_register();
#endif
    memset(&rtl8117_ehci_dev, 0, sizeof(struct rtl8117_ehci_device));
#if 0
    INIT_DELAYED_WORK(&rtl8117_ehci_dev.schedule, rtl8117_control_work_func_t);
#endif
    //INIT_DELAYED_WORK(&rtl8117_ehci_dev.data_write_schedule, rtl8117_data_write_work_func_t);
    for(i=0; i< OUTdescNumber; i++) {
        INIT_WORK(&t_work[i].data_write_schedule, rtl8117_data_write_work_func_t);
    }
    INIT_DELAYED_WORK(&rtl8117_ehci_dev.data_read_schedule, rtl8117_data_read_work_func_t);

    return 0;
}

void rtl8117_ehci_control_request(struct usb_ctrlrequest *request)
{
//    u8 *dbg = (u8 *) request;
//    printk(KERN_ALERT "[EHCI] Setup packet %02x %02x %02x %02x %02x %02x %02x %02x\n", *dbg, *(dbg+1), *(dbg+2), *(dbg+3), *(dbg+4), *(dbg+5), *(dbg+6), *(dbg+7));

    memcpy(&rtl8117_ehci_dev.request, request, sizeof(struct usb_ctrlrequest));
#if 0
    schedule_delayed_work(&rtl8117_ehci_dev.schedule, 0);
#else
    rtl8117_control_work_func_t();
#endif
}

void rtl8117_ehci_bulkout_request(void* ptr, u32 length)
{
    struct command_block_wrapper *cbw = ptr;
    bool is_cbw = false;
    bool write = false;


    /* cbw */
    if ((length == CBW_SIZE) && (cbw->dCBWSignature == 0x43425355)) {
#if 1
        switch (cbw->rbc[0]) {
        case WRITE10:
        case WRITE12:
        case MODE_SELECT6:
        case MODE_SELECT10:
            write = true;
            break;
        default:
            write = false;
            break;
        }
#else
        write = !(cbw->bmCBWFlags & 0x80) && cbw->dCBWDataTransferLength;
#endif
        is_cbw = true;
    }


    if (is_cbw) {
        dev_msg("bulk request: %s, cbw data len:%u\n",
                write?"WRITE":"READ", cbw->dCBWDataTransferLength);
        dev_msg("\trbc [%02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx"
                " %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx]\n",
                cbw->rbc[0], cbw->rbc[1], cbw->rbc[2], cbw->rbc[3], cbw->rbc[4],
                cbw->rbc[5], cbw->rbc[6], cbw->rbc[7], cbw->rbc[8], cbw->rbc[9],
                cbw->rbc[10], cbw->rbc[11], cbw->rbc[12],
                cbw->rbc[13], cbw->rbc[14], cbw->rbc[15]);

        if (write) {
            memcpy(&rtl8117_ehci_dev.data_write_cbw, ptr, CBW_SIZE);
            /*
               The buffer in bulkout descriptor is used during bottom-half, so it can not
               be released early.
             */
            //__release_bulkout_desc(ehci_core.bulk_outdesc_index);
            __schedule_write_work(16);
        } else {
            memcpy(&rtl8117_ehci_dev.data_read_cbw, ptr, CBW_SIZE);
            __release_bulkout_desc(ehci_core.bulk_outdesc_index);
            schedule_delayed_work(&rtl8117_ehci_dev.data_read_schedule, 0);
        }
    } else {
        __schedule_write_work(length);
    }
}

#ifdef CONFIG_USB_REDIRECTION
enum {MEMORY_NAPPING=0, RECV_MESS=1, CONNECT=2, DISCONNECT=3, USB_STATE=4};
static long ehcidev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg_)
{
    void __user *arg = (void __user *)arg_;

    u32 page_addr;
    u8 old_val;
    QHdr *qhdr = NULL;
    switch (cmd) {
    case MEMORY_NAPPING:
        page_addr = (u32)(rtl8117_ehci_dev.data_read_buf)&(~0xe0000000);
        copy_to_user(arg, &page_addr, 4);
        break;

    case CONNECT:
        if (!usb_redirect)
            usb_redirect = 1;
        rtl8117_ehci_dev.in_use_redirect = 1;
        old_val = rtl8117_ehci_dev.last_use;
        rtl8117_ehci_dev.last_use = 1;

        if (!qhdr)
            qhdr = kzalloc(sizeof(QHdr), GFP_KERNEL);
        copy_from_user(qhdr, arg, sizeof(QHdr));
        rtl8117_ehci_dev.usb_type = qhdr->option;
        if ((!ktcp_svc)||(!ktcp_svc->running))
        {
#if 0
            while(!EHCI_RST())
            {
                mdelay(1);
            };
#endif

            rtl8117_ehci_init_bulk();
            ktcp_start(qhdr);
        }
        break;
    case DISCONNECT:
#if 0
        if ((ktcp_svc!=NULL) && (ktcp_svc->running))
        {
            Wt_IBIO(PORTSC, 0x1000);
            rtl8117_ehci_writeb(0, DEVICE_ADDRESS);
            mdelay(1);
            ktcp_stop();
            //disable_vnc_usb_dev();
        }
        show_register(0);

        rtl8117_ehci_dev.in_use_redirect = 0;
        rtl8117_ehci_dev.last_use = old_val;
        if ((!rtl8117_ehci_dev.in_use_redirect) && (!rtl8117_ehci_dev.in_use_hid))
            usb_redirect = 0;
#else
        stop_usb_redirection(old_val, 1);
#endif
        break;
    case USB_STATE:
        copy_to_user(arg, &rtl8117_ehci_dev.usb_state, 1);
        break;
    default:
        return -EINVAL;
    }
    return 0;
}

static const struct file_operations echidev_fops = {
    .owner = THIS_MODULE,
    .open = simple_open,
    .unlocked_ioctl = ehcidev_ioctl,
};

static struct miscdevice ehcidev = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "ehci",
    .fops = &echidev_fops,
    .mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH,
};

static int __init ehcidev_register(void)
{
    int rc;
    rc = misc_register(&ehcidev);
    if (unlikely(rc)) {
        pr_err("registration of /dev/ehci failed\n");
        return rc;
    }
    return 0;
}

int ktcp_recv(struct socket *sock, unsigned char *buf, int len)
{
    struct msghdr msg;
    struct iovec iov;
    mm_segment_t oldfs;
    int size=0;
    int err;
    int rcv_cnt;

    if(sock==NULL) {
        //printk("krecv the csock is NULL\n");
        return -1;
    }

    if(sock->sk==NULL) return 0;
    err = import_single_range(READ, buf, len, &iov, &msg.msg_iter);
    if (unlikely(err))
        return err;
    msg.msg_control=NULL;
    msg.msg_controllen=0;
    msg.msg_flags=0;
    msg.msg_name=NULL;
    msg.msg_namelen=0;
    msg.msg_iocb = NULL;
    oldfs=get_fs();
    set_fs(KERNEL_DS);
    rcv_cnt = 0;
    while (rcv_cnt < MAX_RETRY_CNT)
    {
        size=sock_recvmsg(sock, &msg, iov_iter_count(&msg.msg_iter), 0);
        if (size > 0)
        {
            set_fs(oldfs);
            return size;
        }
        rcv_cnt++;
    }
    set_fs(oldfs);

    return -1;
}

int ktcp_send(struct socket *sock,char *buf,int len)
{
    struct msghdr msg;
    struct iovec iov;
    int size;
    mm_segment_t oldfs;
    u8 send_cnt;
    int err;
    if(sock==NULL)
    {
        printk("ksend the csock is NULL\n");
        return -1;
    }


    err = import_single_range(WRITE, buf, len, &iov, &msg.msg_iter);
    if (unlikely(err))
        return err;
    msg.msg_control=NULL;
    msg.msg_controllen=0;
    msg.msg_flags=0;
    msg.msg_name=NULL;
    msg.msg_namelen=0;

    oldfs=get_fs();
    set_fs(KERNEL_DS);
    send_cnt = 0;
    while (send_cnt < MAX_RETRY_CNT)
    {
        size=sock_sendmsg(sock,&msg);
        if (size > 0)
        {
            set_fs(oldfs);
            return size;
        }
        send_cnt++;
    }
    set_fs(oldfs);

    return -1;
}

void show_register(int mode)
{
#define START	0x50
#define END	0x74

    unsigned int i;

    printk("***********%s EHCI config REGISTERS***********\n", mode?"Init":"Stop");
    printk("HCSPARMS is 0x%x.\n", Rd_IBIO(HCSPARAMS));
    printk("EHCICONFIG is 0x%x.\n", ehci_readl(EHCICONFIG));
    printk("EHCI_IMR is 0x%x.\n", ehci_readl(EHCI_IMR));
    printk("PORTSC is 0x%x.\n", Rd_IBIO(PORTSC));
    printk("PORTSC+0x04 is 0x%x.\n", Rd_IBIO(PORTSC+0x04));
    printk("PORTSC+0x08 is 0x%x.\n", Rd_IBIO(PORTSC+0x08));
    printk("DEVICE_ADDRESS is 0x%x.\n", ehci_readb(DEVICE_ADDRESS));
    printk("DEVICE_ADDRESS1 is 0x%x.\n", ehci_readb(DEVICE_ADDRESS+1));
    printk("DEVICE_ADDRESS2 is 0x%x.\n", ehci_readb(DEVICE_ADDRESS+2));

    for (i=START; i<=END; i = i+4) {
	printk("0x%x\t %08x\n", i, ehci_readl(EHCI_BASE_ADDR + i));
    }
}

int ktcp_accept_worker(void )
{
    int error,ret;
    struct socket *socket;
    //int len=10;
    //unsigned char buf[len+1];
    struct inet_connection_sock *isock;
    //int val;

    DECLARE_WAITQUEUE(wait,current);

    mutex_lock(&nobkl);
    {
        ktcp_svc->running = 1;
        current->flags |= PF_NOFREEZE;
#if 0
        daemonize("accept worker");
#endif
        allow_signal(SIGKILL|SIGSTOP);
    }
    mutex_unlock(&nobkl);

    socket = ktcp_svc->listen_socket;
    csock=(struct socket*)kmalloc(sizeof(struct socket), GFP_KERNEL);
    error = sock_create(PF_INET,SOCK_STREAM,IPPROTO_TCP,&csock);
    if(error<0) {
        printk(KERN_ERR "CREATE CSOCKET ERROR");
        return error;
    }

    /*check the accept queue*/
    /*TODO: Because the api changes, should change to the new API*/
    isock = inet_csk(socket->sk);
    while (ktcp_svc->running == 1) {
        if(reqsk_queue_empty(&isock->icsk_accept_queue)) {
            add_wait_queue(&socket->sk->sk_wq->wait, &wait);
            __set_current_state(TASK_INTERRUPTIBLE);
            schedule_timeout(HZ);
            __set_current_state(TASK_RUNNING);
            remove_wait_queue(&socket->sk->sk_wq->wait, &wait);
            if (kthread_should_stop())
            {
                socket->ops->shutdown(csock, SHUT_RDWR);
                return 0;
            }
            continue;
        }

        ret = socket->ops->accept(socket, csock, O_NONBLOCK);
        if(ret<0) {
            sock_release(csock);
            return ret;
        }
        //val = 1;
        //kernel_setsockopt(csock, IPPROTO_TCP, TCP_NODELAY, (char *)(&val), sizeof(val));
        //usb_redirect = 1;
        if (ehci_enabled == 1)
            ehci_enabled = 0;
        Wt_IBIO(PORTSC, 0x1001);
        //disable_link_change(LCB_USB);
        show_register(1);
        rtl8117_ehci_dev.usb_state = ENABLED;
        rtl8117_ehci_dev.portnum = 0;
    }
    return ret;
}

int ktcp_start_listen(void *data)
{
    int error;
    struct socket *socket;
    //struct sockaddr_in sin,sin_send;
    struct sockaddr_in sin;
    QHdr *qhdr = (QHdr *)data;
    int val = 1;
    struct timeval tv = {.tv_sec = 2, .tv_usec = 0};
    DECLARE_WAIT_QUEUE_HEAD(wq);

    mutex_lock(&nobkl);
    {
        ktcp_svc->running = 1;
        current->flags |= PF_NOFREEZE;
        allow_signal(SIGKILL|SIGSTOP);
    }
    mutex_unlock(&nobkl);

    error = sock_create(PF_INET, SOCK_STREAM, IPPROTO_TCP, &ktcp_svc->listen_socket);

    if(error<0) {
        printk(KERN_ERR "CREATE SOCKET ERROR");
        return -1;
    }

    socket = ktcp_svc->listen_socket;
    ktcp_svc->listen_socket->sk->sk_reuse=1;
#if 0
#else
    kernel_setsockopt(socket, IPPROTO_TCP, TCP_NODELAY, (char *)(&val), sizeof(val));
    kernel_setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (char *)(&tv), sizeof(tv));
    kernel_setsockopt(socket, SOL_SOCKET, SO_SNDTIMEO, (char *)(&tv), sizeof(tv));
#endif

    sin.sin_addr.s_addr=htonl(INADDR_ANY);
    sin.sin_family=AF_INET;
    sin.sin_port=htons(qhdr->port);
    //sin.sin_port=htons(59);

    error = socket->ops->bind(socket,(struct sockaddr*)&sin,sizeof(sin));
    if(error<0) {
        printk(KERN_ERR "BIND ADDRESS");
        return -1;
    }

    error = socket->ops->listen(socket,5);
    if(error<0) {
        printk(KERN_ERR "LISTEN ERROR");
        return -1;
    }
    rtl8117_ehci_dev.usb_state = LISTENED;
    ktcp_svc->accept_worker=kthread_run((void *)ktcp_accept_worker,NULL, "ktcp-accept");
    rtl8117_ehci_dev.usb_state = ACCEPTED;
    while (1) {
        wait_event_timeout(wq,0,3*HZ);
        if (kthread_should_stop())
        {
            if (ktcp_svc->listen_socket!= NULL)
            {
                sock_release(ktcp_svc->listen_socket);
                ktcp_svc->listen_socket= NULL;
            }

            return 0;
        }
        __set_current_state(TASK_INTERRUPTIBLE);
        schedule_timeout(1*HZ);
        __set_current_state(TASK_RUNNING);
        if(signal_pending(current))
        {
            sock_release(socket);
            break;
        }
    }
    return 1;
}

int ktcp_start(QHdr *qhdr)
{
    if (!ktcp_svc)
    {
        ktcp_svc=kmalloc(sizeof(struct ktcp_service),GFP_KERNEL);
        ktcp_svc->thread = NULL;
    }
    ktcp_svc->running = 1;
    rtl8117_ehci_dev.usb_state = DISABLED;
    if (ktcp_svc->thread== NULL)
        /* kernel thread initialization */
        ktcp_svc->thread = kthread_run((void *)ktcp_start_listen, qhdr, "ktcp-listen");
    return 1;
}

void ktcp_stop(void)
{
    int err;
    if (ktcp_svc == NULL)
        return;
    if(ktcp_svc->thread==NULL)
        printk(KERN_INFO ": no kernel thread to kill\n");
    else {
        err=kthread_stop(ktcp_svc->accept_worker);
        err=kthread_stop(ktcp_svc->thread);

        /* free allocated resources before exit */
#if 0
        if (ktcp_svc->listen_socket!= NULL)
        {
            sock_release(ktcp_svc->listen_socket);
            ktcp_svc->listen_socket= NULL;
        }
#endif
        ktcp_svc->running = 0;
        ktcp_svc->thread = NULL;
        ktcp_svc->accept_worker = NULL;
        //ktcp_svc = NULL;
        rtl8117_ehci_dev.usb_state = DISCONNECTED;
        rtl8117_ehci_dev.msd_address = 0;
        redir_reboot = 0;
        printk(KERN_INFO ": module unloaded\n");
    }
    //enable_link_change(LCB_USB);
}
#endif
