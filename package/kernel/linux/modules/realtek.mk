RTK_MENU:=Realtek kernel options

define KernelPackage/ipt-android
  SUBMENU:=$(RTK_MENU)
  TITLE:=Extra Netfilter options for Android
  KCONFIG:= \
	CONFIG_NFT_COMPAT=n \
	CONFIG_NF_CT_NETLINK_HELPER=n \
	CONFIG_NF_TABLES_ARP=n \
	CONFIG_NF_TABLES_BRIDGE=n \
	$(addsuffix =y,$(KCONFIG_IPT_ANDROID))
  FILES:=
  AUTOLOAD:=
  DEPENDS:=@TARGET_rtd1295
endef

define KernelPackage/ipt-android/description
 Other Netfilter kernel builtin options for Android
endef

$(eval $(call KernelPackage,ipt-android))
