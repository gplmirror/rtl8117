/*
   CMAC application for 8117.

   This program will be hooked by the uevent - 'cmacrx'

   which defined in /etc/hotplug.d/platform/01-platform.

...   The uevent - 'cmacrx' will be generated from

struct rtl_dash_ioctl_struct {
	unsigned int   type;
	unsigned int   offset;
	unsigned int   len; //len = [8,1523]
	char data_buffer[100];
	unsigned char  *data_buffer2;
};


unsigned char  *data_buffer2;
typedef struct _OSOOBHdr {
    unsigned int len; // 4 bytes
    unsigned char type; // 0-127 DASH event, 128-255 8117 event
    unsigned char flag;
    unsigned char hostReqV;//0x92: request to FW, 0x91: response to IB
    unsigned char res; <-- unsigned char *res; ??? , 
                      2019-0522 notice by eccheng for ASUS's HeartBeat new requirments
                      res[0] : WatchDog status : enable(1)/disable(0)
                      res[1] : Heart interval : 1 - 255 second

    
} OSOOBHdr;

  v0.6 

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "cmac-app.h"


int psw_set(char* username, char* password) // username = 'root' only
{
 	char cmd[64]; //psw len <= 16 chars
 	int ret;
 	
 	sprintf(cmd,"echo 'root:%s' | chpasswd -m", password);
 	ret = system(cmd);

    if (ret == -1)
    	return 0; //fail
    else
	    return 1; //success
}


/*
input : username = "root" only
        password (16 chars max.)
        
output : 1: valid password
         0: NOT valid password

*/
/* Compile with -lcrypt */
#if ! defined(__sun)
#define _BSD_SOURCE     /* Get getpass() declaration from <unistd.h> */
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE   /* Get crypt() declaration from <unistd.h> */
#endif
#endif
#include <unistd.h>
#include <limits.h>
#include <pwd.h>
#include <shadow.h>
int psw_chk(char* username, char* password) // username = 'root' only
{
    char *encrypted, *p;
    struct passwd *pwd;
    struct spwd *spwd;
    int authOk; // 0: not ok, 1:ok

    /* Look up password and shadow password records for username */
    pwd = getpwnam(username);
    if (pwd == NULL)
    {
        printf("couldn't get password record\n");
        return 0;
    };

    printf("pw_name    [%s]\n", pwd->pw_name);    
    printf("pw_passwd  [%s]\n", pwd->pw_passwd);    

    spwd = getspnam(username);
    if (spwd == NULL)           /* If there is a shadow password record */
    {
        printf("couldn't get shadow password record (root password not set)\n");
        return 0;
    };

    printf("sp_namp  [%s]\n", spwd->sp_namp);    
    printf("sp_pwdp  [%s]\n", spwd->sp_pwdp);    


	/* encrypt the given password and check it if it is correct */
    encrypted = crypt(password, spwd->sp_pwdp);
    printf("my psw   [%s] from [%s] and  [%s]\n", encrypted, password, spwd->sp_pwdp);

    authOk = strcmp(encrypted, spwd->sp_pwdp) == 0;
    if (!authOk) {
        printf("Incorrect password\n");
        return 0 ; 
    }
    
    printf("Correct password\n");
    return 1;

}


int cmac_ack(int fd, struct rtl_dash_ioctl_struct *cmd)
{
	long ret;

	 //send OOB_ACK  <- FW
	 //00 00 00 00 02 00 92 00

	 cmd->len = 0x10;
	 cmd->data_buffer[0] = 0;
	 cmd->data_buffer[1] = 0;
	 cmd->data_buffer[2] = 0;
	 cmd->data_buffer[3] = 0;
	 cmd->data_buffer[4] = OSPUSHDATA ; //ACK Must  use this type to  ack
	 cmd->data_buffer[5] = 0;
	 cmd->data_buffer[6] = 0x92;//request(0x91), response(0x92)
	 cmd->data_buffer[7] = 0;
	 memcpy(cmd->data_buffer2, cmd->data_buffer, cmd->len);
	 
	 ret = ioctl(fd, IOCTL_SEND, cmd);
	 if (ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
		return -1;
	}
	return 0;
}


int cmac_set_psw(int fd, struct rtl_dash_ioctl_struct *cmd, char *password)
{
	int i;
	long ret;

	cmd->len = 8+1; //OOBHdr : 8 bytes, user data : 1 bytes
	for (i=0; i<cmd->len; i++)
		cmd->data_buffer[i]=0;

	memcpy(cmd->data_buffer2, cmd->data_buffer,cmd->len);
	cmd->data_buffer2[0] = 1; //user data length = 1 (int : 4 bytes)
	cmd->data_buffer2[1] = 0; 
	cmd->data_buffer2[2] = 0; 
	cmd->data_buffer2[3] = 0; 
	cmd->data_buffer2[4] = OOB_8117_SET_ADMIN_PSW;//type  : 165/166
	cmd->data_buffer2[5] = 0;//flag : 0
	cmd->data_buffer2[6] = 0x91;//request/response : 91/92
	cmd->data_buffer2[7] = 0;
						 
	cmd->data_buffer2[8] = psw_set("root", password);//data[0]  1 : success, 0 : fail

	ret = ioctl(fd, IOCTL_SEND, cmd);
	if (ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
	}

	return 0;
}

int cmac_chk_psw(int fd, struct rtl_dash_ioctl_struct *cmd, char *password)
{
	int i;
	long ret;

	cmd->len = 8+1; //OOBHdr : 8 bytes, user data : 1 bytes
	for (i=0; i<cmd->len;i++)
		cmd->data_buffer[i]=0;

	memcpy(cmd->data_buffer2, cmd->data_buffer,cmd->len);
	cmd->data_buffer2[0] = 1; //user data length = 1 (int : 4 bytes)
	cmd->data_buffer2[1] = 0; 
	cmd->data_buffer2[2] = 0; 
	cmd->data_buffer2[3] = 0; 
	cmd->data_buffer2[4] = OOB_8117_CHK_ADMIN_PSW;//type  : 165/166
	cmd->data_buffer2[5] = 0;//flag : 0
	cmd->data_buffer2[6] = 0x91;//request/response : 91/92
	cmd->data_buffer2[7] = 0;
						 
	cmd->data_buffer2[8] = psw_chk("root", password);//data[0]  1 : success, 0 : fail

	ret = ioctl(fd, IOCTL_SEND, cmd);
	if (ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
	}

	return 0;
}


#define WATCHDOG_STATUS_FILE "/etc/wd_enable"
/*
 user data byte 0
 return value: 
 0 : watchdog disable
 1 : watchdog enable
 255 : error to get the watchdog status
 */
 
#define HEARBEAT_INTER_FILE "/etc/wd_hb_inter" // <-- create the default file with default value 1 in the sdk, 
/*
 user data byte 1
 return value: 
 1 - 255 seconds
 0 : error to get the heartbeat interval
 */


#define WATCHDOG_TIMEOUT_FILE "/etc/wd_timeout"
/*
 return value: 
 1 - 255 seconds
 0 : error to get the heartbeat interval
 */


/*
 return value: 
 0  : successful 
 -1 : fail
 */
int get_bytevalue_from_file(char *file_path, unsigned char *value)
{
   char tmp_str[16];  
   FILE *fp;  

   if ((fp = fopen(file_path, "r"))== NULL) 
   {
       printf("%s open fail.\n", file_path);  
       return -1;
   }
   else
   {
   printf("fp=%p\n",fp);
       fscanf(fp, "%s\n",tmp_str);
       printf("%s\n", tmp_str);  
   }  
   
   fclose(fp);  
   
   //check if the string is a valid all-digitals string
   int i;
   int is_a_number;
   is_a_number = 1;
   for (i = 0; i < strlen(tmp_str); i++)
        if (!isdigit(tmp_str[i]))
        {
            is_a_number = 0;
            break;            
        }
   
   if (!is_a_number)
   {
      printf("%s not a number.\n", tmp_str);
      *value = 0;
      return -1;
   }
   *value = (unsigned char) (atoi(tmp_str) & 0xff); // only take the lower byte data
   printf("value = %d\n", (unsigned char) *value);
   return 0;
}



int cmac_heartbeat_sendback(int fd, struct rtl_dash_ioctl_struct *cmd)
{
	int i;
	long ret;

	cmd->len = 8+2; //OOBHdr : 8 bytes, user data : 2 bytes
	for (i=0; i<cmd->len;i++)
		cmd->data_buffer[i]=0;

	memcpy(cmd->data_buffer2, cmd->data_buffer,cmd->len);
	cmd->data_buffer2[0] = 2; //user data length = 2 (int : 4 bytes)
	cmd->data_buffer2[1] = 0; 
	cmd->data_buffer2[2] = 0; 
	cmd->data_buffer2[3] = 0; 
	cmd->data_buffer2[4] = OOB_8117_HEARTBEAT;//type  : 167
	cmd->data_buffer2[5] = 0;//flag : 0
	cmd->data_buffer2[6] = 0x91;//request/response : 91/92
	cmd->data_buffer2[7] = 0;
	
    int ret_status;
    unsigned char value; // only 1 byte space to store the number

    //fill the watchdog status to the CMAC ACK packet
    ret_status = get_bytevalue_from_file(WATCHDOG_STATUS_FILE, &value);
    if (ret_status == 0)
    {
        printf("wd_enable = %d\n", (unsigned char) value);
    	cmd->data_buffer2[8] = (unsigned char) value; //watchdog status 0 : watchdog disable,  1 : watchdog enable
    }
    else
    {
        printf("%s read fail\n", WATCHDOG_STATUS_FILE);
    	cmd->data_buffer2[8] = 255; //watchdog status 0 : watchdog disable,  1 : watchdog enable, 255 for read data fail
    }

    //fill the heatbeat interval to the CMAC ACK packet
    ret_status = get_bytevalue_from_file(HEARBEAT_INTER_FILE, &value);
    if (ret_status == 0)
    {
        printf("wd_hb_inter = %d\n", (unsigned char) value);
        cmd->data_buffer2[9] =  (unsigned char) value; //  heartbeat interval, 1-255 seconds, 0 for read data fail
    }
    else
    {
        printf("%s read fail\n", HEARBEAT_INTER_FILE);
        cmd->data_buffer2[9] = 0; //  heartbeat interval, 1-255 seconds, 0 for read data fail
    }	
						 


	ret = ioctl(fd, IOCTL_SEND, cmd);
	if (ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
	}

	return 0;

}


/*

  ip_info = ip(4 bytes) netmask(4 bytes) gw(4 bytes)
  
  ipv4 only, A(ip[0]).B(ip[1]).C(ip[2]).D(ip[3]), netmask and gw are similar to ip
  
  ip : MUST
  netmask : optional (if not all zero)
  gw      : optional (if not all zero)
  
 */
int cmac_ip_static_set(unsigned char* ip_info)
{
 	char cmd[64]; //ip_info len <= 12 unsigned chars, //buffer size MUST has enough space for the cmd!!!
 	int ret;

	printf("set IP from IB : IP   [%03d.%03d.%03d.%03d]\n", ip_info[2],ip_info[3],ip_info[4],ip_info[5]);
	printf("set IP from IB : MASK [%03d.%03d.%03d.%03d]\n", ip_info[6],ip_info[7],ip_info[8],ip_info[9]);
	printf("set IP from IB : GW   [%03d.%03d.%03d.%03d]\n", ip_info[10],ip_info[11],ip_info[12],ip_info[13]);
	printf("set IP from IB : DNS  [%03d.%03d.%03d.%03d]\n", ip_info[14],ip_info[15],ip_info[16],ip_info[17]);

 	// #ifconfig eth0 xxx.xxx.xxx.xxx netmask yyy.yyy.yyy.yyy
 	// #route add default gw zzz.zzz.zzz.zzz
 	if ((ip_info[6] != 0) && (ip_info[7] != 0) && (ip_info[8] != 0) && (ip_info[9] !=0))
 	{
 	    sprintf(cmd,"ifconfig eth0 %d.%d.%d.%d netmask %d.%d.%d.%d", ip_info[2],ip_info[3],ip_info[4],ip_info[5], ip_info[6],ip_info[7],ip_info[8],ip_info[9]);
 	}
 	else
 	{
 	    sprintf(cmd,"ifconfig eth0 %d.%d.%d.%d", ip_info[2],ip_info[3],ip_info[4],ip_info[5]);
 	}
 	
 	ret = system(cmd);
    printf("return = %d \n", ret);
    if (ret == 0) //shell fork success
    {
     	if ((ip_info[10] != 0) && (ip_info[11] != 0) && (ip_info[12] != 0) && (ip_info[13] !=0))
 	    {
 	        sprintf(cmd,"route add default gw %d.%d.%d.%d", ip_info[10],ip_info[11],ip_info[12],ip_info[13]);
 	        ret = system(cmd);
            printf("return = %d \n", ret);
            if (ret == 0) //shell fork success
    	        return 0;
            else
            {
                printf("%s fail \n",cmd);
	            return 1;
	        }
 	    }
 	    else
     	{
        	return 0;
     	}
    }
    else
    {
        printf("%s fail \n",cmd);
	    return 1;
	}
}


//To renew OOB IP while in IB mode
int cmac_ip_dhcp_renew()
{
 	char cmd[128];//buffer size MUST has enough space for the cmd!!!
 	int ret;
 	
 	
 	sprintf(cmd,"udhcpc -p /var/run/tmp.pid && sleep 3 && kill -9 `cat /var/run/tmp.pid ` && rm /var/run/tmp.pid");
 	ret = system(cmd);

    if (ret == 0) //shell fork success
    	return 0;
    else
	    return 1;
}



#define PSW_LEN 16
#define CMAC_BUF_LEN 100+1
#define IP_INFO_LEN 18 //IPver(1) isDHCP(1) IP(4) MASK(4) GW(4) DNS(4) DNS(4)

int main(void)
{
	struct rtl_dash_ioctl_struct cmd;
	int fd,i;
	long ret;	
	char psw[PSW_LEN];
	unsigned char ip_info[IP_INFO_LEN];


	fd = open(DEVFILE, O_RDWR);
	if (fd == -1){
		perror(DEVFILE);
		return -1;
	}

	memset(&cmd, 0, sizeof(cmd));
	cmd.data_buffer2 =(unsigned char*) malloc(CMAC_BUF_LEN);

	if(cmd.data_buffer2 == NULL)
		return;

	printf("cmd.data_buffer = %x cmd.data_buffer2%x\n",cmd.data_buffer,cmd.data_buffer2);

	ret = ioctl(fd, IOCTL_RECV, &cmd);
	if (ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
	}

	printf("IO Control type %d(0x%x)\n", cmd.type, cmd.type);

	printf("_OSOOBHdr length = %d [cmac header + cmac data]\n", cmd.len);
	printf("_OSOOBHdr content begin\n");
	for (i=0; i<cmd.len;i++){
		printf("%d [cmac data]\n",*(cmd.data_buffer2+i));
		if (i==4)//CMAC type
		{
			int type = *(cmd.data_buffer2+i);
			switch (type)
			{
			 	case OOB_8117_SET_ADMIN_PSW:
					printf("[8117 Event - OOB_8117_SET_ADMIN_PSW. (%x)] ", type);
					//processing - get the plaintext password
					memset(&psw, 0, sizeof(psw));
printf("user data len %d\n", (unsigned char) cmd.data_buffer2);
					memcpy(&psw, cmd.data_buffer2+8, (unsigned char) *cmd.data_buffer2);//8 
					printf("set password from IB [%s]\n", &psw);

					//ack the request
					 cmac_ack(fd, &cmd);
						
					//send back the return value
					cmac_set_psw(fd, &cmd, psw);
						
	   				break;

			   	case OOB_8117_CHK_ADMIN_PSW:
					printf("[8117 Event - OOB_8117_CHK_ADMIN_PSW. (%x)] ", type);
					//processing - get the plaintext password
					memset(&psw, 0, sizeof(psw));
printf("user data len %d\n", (unsigned char) cmd.data_buffer2);
					memcpy(&psw, cmd.data_buffer2+8, (unsigned char) *cmd.data_buffer2);//8 bytes is IO Ctrl header
					printf("chk password from IB [%s]\n", &psw);

					//ack the request
					 cmac_ack(fd, &cmd);

					//send back the return value
					cmac_chk_psw(fd, &cmd, psw);
							
	   				break;
			   		

			   	case OOB_8117_HEARTBEAT:
					printf("[8117 Event - OOB_8117_HEARTBEAT. (%x)] ", type);
										
printf("user data len %d\n", (unsigned char) cmd.data_buffer2);
					memcpy(&psw, cmd.data_buffer2+8, (unsigned char) *cmd.data_buffer2);//8 bytes is IO Ctrl header
					printf("heartbeat from IB [%s]\n", &psw);

					//ack the request
					 cmac_ack(fd, &cmd);

					//send back the return value
					cmac_heartbeat_sendback(fd, &cmd);
		
					int ret_status;
					unsigned char value;

					ret_status = get_bytevalue_from_file(WATCHDOG_TIMEOUT_FILE, &value);
					if (ret_status == 0)
					{
					    printf("wd_timeout = %d\n", (unsigned char) value);
					}
					else
					{
					    printf("%s read fail\n", WATCHDOG_TIMEOUT_FILE);
					    value = 0 ; //for error
					}		
		
					//reset the timer
					char shell_cmd[160]; //command len <= 160 
					int ret;
					 	
					sprintf(shell_cmd,"ps | grep wd_ib.sh | grep -v grep | awk '{print $1}' | xargs kill; sh /usr/local/sbin/wd_ib.sh %d >> /tmp/wd_ib.log&", value);
					ret = system(shell_cmd);



/*
					if (ret == -1)
					  	printf("system reset fail [%s] ", &shell_cmd);
					else
					  	printf("system reset... [%s] ", &shell_cmd);
*/		
		
	   				break;


			 	case OOB_8117_SET_IP_FROM_UI: //trigger by IB UI (client tool)
					printf("[8117 Event - OOB_8117_SET_IP_FROM_UI. (%x)] ", type);
					//processing - get the IP info 
					memset(&ip_info, 0, sizeof(ip_info));
				
printf("user data len %d\n", (unsigned char) cmd.data_buffer2);
					memcpy(&ip_info, cmd.data_buffer2+8, (unsigned char) *cmd.data_buffer2);//8 

					printf("set IP from IB : IPver  [%x]\n", ip_info[0]);
					printf("set IP from IB : isDHCP [%x]\n", ip_info[1]);
					printf("set IP from IB : IP   [%03d.%03d.%03d.%03d]\n", ip_info[2],ip_info[3],ip_info[4],ip_info[5]);
					printf("set IP from IB : MASK [%03d.%03d.%03d.%03d]\n", ip_info[6],ip_info[7],ip_info[8],ip_info[9]);
					printf("set IP from IB : GW   [%03d.%03d.%03d.%03d]\n", ip_info[10],ip_info[11],ip_info[12],ip_info[13]);
					printf("set IP from IB : DNS  [%03d.%03d.%03d.%03d]\n", ip_info[14],ip_info[15],ip_info[16],ip_info[17]);
			

					//ack the request
					 cmac_ack(fd, &cmd);
						
					//set IP/MASK/GW via ifconfig
					//if isDHCP == 0 (static) , use ifconfig to set with the given IP/MASK/GW
					//if isDHCP == 1 (dhcp), restart the network to get the DHCP IP
					if (ip_info[1] == 0) // static
					{
					    printf("static\n");
                        cmac_ip_static_set(&ip_info);					    
					}
					else if (ip_info[1] == 1) // dchp
					{
					    printf("dhcp\n");
                        cmac_ip_dhcp_renew();
					    
					}
					else // Not valid value
					{
					    printf("ip_info.isDHCP value not valid. (%d)\n", ip_info[1]);
					}
						
	   				break;



                //inherit OOB event number for DASH/FP (0-127)
			 	case OOB_SET_IP: //trigger by IB network IP changed
					printf("[8117 Event - OOB_SET_IP. (%x)] ", type);
					//processing - get the IP info 
					memset(&ip_info, 0, sizeof(ip_info));
					
printf("user data len %d\n", (unsigned char) cmd.data_buffer2);
					memcpy(&ip_info, cmd.data_buffer2+8, (unsigned char) *cmd.data_buffer2);//8 
					printf("set IP from IB : IPver  [%x]\n", ip_info[0]);
					printf("set IP from IB : isDHCP [%x]\n", ip_info[1]);
					printf("set IP from IB : IP   [%03d.%03d.%03d.%03d]\n", ip_info[2],ip_info[3],ip_info[4],ip_info[5]);
					printf("set IP from IB : MASK [%03d.%03d.%03d.%03d]\n", ip_info[6],ip_info[7],ip_info[8],ip_info[9]);
					printf("set IP from IB : GW   [%03d.%03d.%03d.%03d]\n", ip_info[10],ip_info[11],ip_info[12],ip_info[13]);
					printf("set IP from IB : DNS  [%03d.%03d.%03d.%03d]\n", ip_info[14],ip_info[15],ip_info[16],ip_info[17]);

					//ack the request
					 cmac_ack(fd, &cmd);
						
					//set IP/MASK/GW via ifconfig
                    cmac_ip_static_set(&ip_info);
						
	   				break;
			   				


				default:
					printf("[8117 Event but not defined yet. (%x)] ", type);
			}

		}
	}
	printf("\n_OSOOBHdr content end\n");
	printf("\n");
	
	sleep(1);
	
	printf("\n");

	if (close(fd) != 0)
		perror("close");

	printf("finished\n\n");

	return 0;
	
}



