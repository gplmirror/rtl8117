/*

 CMAC Application for 8117

 Copyright Realtek @ 2018
 */


struct rtl_dash_ioctl_struct {
	unsigned int   type;
	unsigned int   offset;
	unsigned int   len;
	char data_buffer[100];
	unsigned char  *data_buffer2;
};

/*
#include <stdint.h>
struct ib_ip_info_struct {
	uint8_t ipver;
	uint8_t isdhcp;
	uint32_t ip;
	uint32_t mask;
	uint32_t gw;
	uint32_t dns;
};
*/

#define DEVFILE "/proc/rtl8117-cmac/cmac_enabled"

#define RTLOOB_IOC_MAGIC 0x96
#define IOCTL_SEND      _IOW(RTLOOB_IOC_MAGIC, 0, struct rtl_dash_ioctl_struct)
#define IOCTL_RECV      _IOR(RTLOOB_IOC_MAGIC, 1, struct rtl_dash_ioctl_struct)


//OOB event number 0-127 for DASH/FP 
#define OSPUSHDATA 2 //uCOSII, for ACK packet type
#define OOB_SET_IP 11
 
//OOB event number 128-255for 8117
//From Client Tool in IB
#define OOB_8117_SET_ADMIN_PSW 165 //set plaintext as root's password
#define OOB_8117_CHK_ADMIN_PSW 166 //check if root's password(plaintext) is right

#define OOB_8117_HEARTBEAT 167 //check heartbeat from IB (client tool), timeout = 30 seconds

#define OOB_8117_SET_IP_FROM_UI 169 //set IP from UI (client tool) 







