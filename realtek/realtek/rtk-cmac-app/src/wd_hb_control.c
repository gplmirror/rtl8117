/*
   CMAC application for 8117.

   This program will be called by lua CGI function wd_enable_asus() of 
   
   /usr/lib/lua/luci/controller/apiapp.lua

   for sending a CMAC Request to Clent Tool (IB) without ACK 
   
   to notifiy Client Tool to enalbe/disalbe sending heartbeat
   
struct rtl_dash_ioctl_struct {
	unsigned int   type;
	unsigned int   offset;
	unsigned int   len; //len = [8,1523]
	char data_buffer[100];
	unsigned char  *data_buffer2;
};


unsigned char  *data_buffer2;
typedef struct _OSOOBHdr {
    unsigned int len; // 4 bytes
    unsigned char type; // 0-127 DASH event, 128-255 8117 event
    unsigned char flag;
    unsigned char hostReqV;//0x92: request to FW, 0x91: response to IB
    unsigned char res; <-- unsigned char *res; 
                      2019-0522 notice by eccheng for ASUS's HeartBeat new requirments
                      byte next to res[0] : WatchDog status : enable(1)/disable(0), 255 for error
                      byte next to res[1] : Heart interval : 1 - 255 second, o for error

    
} OSOOBHdr;

  v0.8  by E-Cheng Cheng 2019.05.23
  
Notice :  cmac_hearbeat_control is provided by Yukuen.  

         (original function name is cmac_heartbeat_enable)

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "cmac-app.h"
/*
#define DEVFILE "/proc/rtl8117-cmac/cmac_enabled"

#define RTLOOB_IOC_MAGIC 0x96
#define IOCTL_SEND      _IOW(RTLOOB_IOC_MAGIC, 0, struct rtl_dash_ioctl_struct)

struct rtl_dash_ioctl_struct {
	unsigned int   type;
	unsigned int   offset;
	unsigned int   len;
	char data_buffer[100];
	unsigned char  *data_buffer2;
};
*/

/*
 action :  0 for disable heartbeat, 1 for enable heartbeat

 */

int cmac_heartbeat_control(int fd, struct rtl_dash_ioctl_struct *cmd, int action)
{
	int i;
	long ret;

	cmd->len = 8+1; //OOBHdr : 8 bytes, user data : 1 bytes
	for (i=0; i<cmd->len;i++)
		cmd->data_buffer[i]=0;

	memcpy(cmd->data_buffer2, cmd->data_buffer,cmd->len);
	cmd->data_buffer2[0] = 1; //user data length = 1 (int : 4 bytes)
	cmd->data_buffer2[1] = 0; 
	cmd->data_buffer2[2] = 0; 
	cmd->data_buffer2[3] = 0; 
	cmd->data_buffer2[4] = 0xa3;//type  : 163
	cmd->data_buffer2[5] = 0;//flag : 0
	cmd->data_buffer2[6] = 0x91;//request/response : 91/92
	cmd->data_buffer2[7] = 0;
    
    cmd->data_buffer2[8] = action; //watchdog status 0 : watchdog disable,  1 : watchdog enable, 255 for read data fail

	ret = ioctl(fd, IOCTL_SEND, cmd);
	if (ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
	}

}


#define CMAC_BUF_LEN 100+1
int main(int argc, char *argv[])
{
	struct rtl_dash_ioctl_struct cmd;
	int fd;

	if (argc != 2) 
	{
    	printf("Usage : %s 1 \n heartbeat, 1 for enable hearbeat\n", argv[0]);
		return -1;
	}
	else
	{
		if ((argv[1][0] != '1')) // && ( argv[1][0] != '0'))
		{
	    	printf("error : %s is not a valid value (only 1 is valid)\n", argv[1]);
			return -1;
		}
		
		fd = open(DEVFILE, O_RDWR);
		if (fd == -1)
		{
			perror(DEVFILE);
			return -1;
		}

		memset(&cmd, 0, sizeof(cmd));
		cmd.data_buffer2 =(unsigned char*) malloc(CMAC_BUF_LEN);

		if(cmd.data_buffer2 == NULL)
			return;

		printf("cmd.data_buffer = %x cmd.data_buffer2%x\n",cmd.data_buffer,cmd.data_buffer2);


		cmac_heartbeat_control(fd, &cmd, (argv[1][0]-0x30));

		if (close(fd) != 0)
			perror("close");

		printf("finished\n\n");

		return 0;
	}
	
}



