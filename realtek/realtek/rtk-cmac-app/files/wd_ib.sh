#!/bin/sh

[ $# -eq 0 ] && { echo "Usage: $0 display_seconds "; exit 1; }

time=$1

while (true); do
  date
  sleep $time
  wait
  [ -f /etc/wd_enable ] && ENABLE=`cat /etc/wd_enable`

  if [ "$ENABLE" == "1" ]; then
     echo "enable"

     [ -f /proc/net/r8168oob/eth0/driver_rdy ] && IB=`cat /proc/net/r8168oob/eth0/driver_rdy`

     if [ "$IB" == "1" ]; then
         echo "IB time out -> reset system"
         [ -f /sys/devices/virtual/gpio/gpio9/value ] && echo 1 > /sys/devices/virtual/gpio/gpio9/value
     else
         echo "NOT IB"
     fi
  else
     echo "disable"
  fi


done

