/*
 * Realtek Semiconductor Corp.
 *
 * Board Support Package header file
 *
 * Tony Wu (tonywu@realtek.com.tw)
 * Dec. 07, 2007
 */


#ifndef  _BSP_H_
#define  _BSP_H_
#include <unistd.h>
#include <stddef.h>
#include "smbus.h"

#define AES_128		0x10
#define AES_192		0x20
#define AES_256		0x30
#define AES_CBC_ENCRYPT 	0
#define AES_CBC_DECRYPT 	1

#define AES128_CBC_ENCRYPT 	(AES_128 | AES_CBC_ENCRYPT)
#define AES128_CBC_DECRYPT 	(AES_128 | AES_CBC_DECRYPT)
#define AES192_CBC_ENCRYPT 	(AES_192 | AES_CBC_ENCRYPT)
#define AES192_CBC_DECRYPT 	(AES_192 | AES_CBC_DECRYPT)
#define AES256_CBC_ENCRYPT 	(AES_256 | AES_CBC_ENCRYPT)
#define AES256_CBC_DECRYPT 	(AES_256 | AES_CBC_DECRYPT)

#define SHA1_HASH			4
#define MD5_HASH			5
#define SHA1_HMAC_20		8
#define MD5_HMAC_16			9
//#define SHA1_HMAC_24				5
//#define MD5_HMAC_24					6
#define HASH_KEY_SIZE 36

enum {DIRTY_SET = 0, DIRTY_ADD, DIRTY_SUB};
enum {S_S0 = 0, S_S1, S_S2, S_S3, S_S4, S_S5, S_BIOS, S_UNKNOWN = 0x0E};
enum {DPCONFIGTBL = 0, USERTBL, EVENTTBL, SMBIOSTBL, PLDMTBL, SUBTBL,ROLETBL,SWTBL, ASFTBL, ENDTBL};
enum {PLDM_unit8 = 0, PLDM_sint8, PLDM_uint16, PLDM_sint16, PLDM_uint32, PLDM_sint32};
enum {
    INVALID = 0,
    ARP_PKT,
    EAP_PKT,
    V4_PKT,
    V6_PKT,
};

enum {
    SMBIOS,
    EVENTLOG,
    USERINFO,
    PLDM,
};

typedef struct
{
    unsigned char   *addr;
    unsigned char   *flashaddr;
    unsigned short  length;
    unsigned short  reset:1, dirty:1, wait:1, rsvd: 13;
} flash_data_t;

typedef struct _qhdr
{
    unsigned int cmd;
    unsigned int length;
    unsigned char  tip[4];
    unsigned short option;
    unsigned short port;
    unsigned char contype;
} QHdr;

enum { SRV_NONE = 0,
       SRV_START,
       SRV_STOP,
       SRV_RESTART,
       SRV_TX,
       SRV_RX,
       USB_SETUP_PKT,
       USB_EP0_IN_COMPLETE,
       USB_EP0_OUT_COMPLETE,
       USB_EP_IN_COMPLETE,
       USB_EP_OUT_COMPLETE,
       USB_CBW,
       USB_DATA_IN,
       USB_DATA_OUT,
       USB_CSW,
       USB_STATUS_ERROR,
       USB_BULK_TEST,
     };

/*
********************************************************************************
* BSP definition
********************************************************************************
*/
/* Register Macro */
//for access bit fields, different register lengths between E and F
//#define REGX(reg)		(*(volatile unsigned char *)(reg))
#define PA2VA(vaddr)		((unsigned int) (vaddr) | 0x80000000)
#define VA2PA(vaddr)		((unsigned int) (vaddr) & ~0xe0000000)
/*
*****************************************************************************************
* FUNCTION  PROTOTYPES
*****************************************************************************************
*/
void bsp_pldm_set(unsigned char *addr, unsigned char len);
unsigned char bsp_get_sstate(void);
void bsp_bits_set(unsigned short offset, unsigned int value, unsigned char bits, unsigned char width);
unsigned int bsp_bits_get(unsigned short offset, unsigned char field, unsigned char width);
void setPLDMEndPoint(unsigned char srcept, unsigned char dstept);
unsigned char bsp_set_pldm_slave_address(unsigned char address);
void set_pldm_snr_slave_address(unsigned char addr);
#endif /*  _BSP_H_ */
