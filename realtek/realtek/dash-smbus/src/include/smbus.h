#ifndef __SMBus_H__
#define __SMBus_H__

//number of SMBUS CHIPSETs supported
//#define NUM_SMBUS_CHIPSET  6
#define ASF_TABLE_OFFSET   36

enum {STATE_UNKNOWN = 0, STATE_NORMAL, STATE_WARNING, STATE_CRITICAL, STATE_FATAL, STATE_LOWERWARNING, STATE_LOWERCRITICAL, STATE_LOWERFATAL, STATE_UPPERWARNING, STATE_UPPERCRITICAL, STATE_UPPERFATAL};
enum {ASF_INFO = 0, ASF_ALRT, ASF_RCTL, ASF_RMCP, ASF_ADDR};
enum {ASF_RESET = 0, ASF_POWER_OFF, ASF_POWER_ON, ASF_POWER_CR};
enum {LOG_INFO = 0, LOG_HW, LOG_AUTH, LOG_WARNING, LOG_ERROR, LOG_ALL, LOG_END};
enum {EVENT_NONE = 0,  EVENT_ASSERT, EVENT_DEASSERT};
enum {BIOSPWD, CASEINTRU};
enum {SNR_UNKNOWN = 0, SNR_OTHER, SNR_TEMPERATURE, SNR_VOLTAGE, SNR_CURRENT, SNR_TACHOMETER, SNR_COUNTER };
enum {SNR_INIT = 0, SNR_EXIST, SNR_READ, SNR_INVALID, SNR_FAULT};

typedef struct _asf_ctldata
{
    unsigned char function;
    unsigned char slaveaddr;
    unsigned char cmd;
    unsigned char data;
} asf_ctldata_t;

typedef struct _asf_alert
{
    unsigned char   assert;
    unsigned char   deassert;
    unsigned char   exactmatch;
    unsigned char   logtype:4, status:4;
    unsigned char   address;
    unsigned char   command;
    unsigned char   datamsk;
    unsigned char   cmpvalue;
    unsigned char   evtsnrtype;
    unsigned char   evttype;
    unsigned char   evtoffset;
    unsigned char   evtsrctype;
    unsigned char   evtseverity;
    unsigned char   snrnum;
    unsigned char   entity;
    unsigned char   instance;
} asf_alert_t;

typedef struct _legacy_snr
{
    unsigned char  addr;
    unsigned char  command;
    unsigned char  addrplus;
    unsigned char  type;
    unsigned char  sensorindex;
} legacy_snr_t;

typedef struct _asf_config
{
    unsigned char           maxwdt;
    unsigned char           minsnrpoll;
    unsigned short          systemid;
    unsigned char           IANA[4];
    unsigned int          	pec;
    asf_ctldata_t   		asf_rctl[4];
    unsigned char           RMCPCap[7];
    unsigned char           RMCPCmpCode;
    unsigned char           RMCPIANA[4];
    unsigned char           RMCPSpecialCmd;
    unsigned char           RMCPSpecialCmdArgs[2];
    unsigned char           RMCPBootOptions[2];
    unsigned char           RMCPOEMArgs[2];
    unsigned char           numofsnr;
    legacy_snr_t    		legacysnr[16];
    unsigned char           numofalerts;
    asf_alert_t     		asfalerts[16];
} asf_config_t;

typedef struct _asf_header
{
    unsigned char  sig[4];
    unsigned int length;
    unsigned char  revision;
    unsigned char  chksum;
    unsigned char  oemid[6];
    unsigned char  oemtableid[8];
    unsigned int oemrevision;
    unsigned int creatorid;
    unsigned int creatorrev;
} asf_header_t;

typedef enum _msendtype {
    REMOTE_CONTROL = 0,
    ASF_SENSOR_POLL,
    LS_POLL,
    BMC_POLL,
    PLDM_RESPONSE,
    PLDM_REQUEST,
} msendtype;

enum {
    SMBIOS_NOT_READY = 0,
    SMBIOS_READY,
};

enum {
    INTEL = 0,
    AMD,
    NVIDIA,
    HP,
    LENOVO,
    AMDSOC
};

enum { PLDM_BLOCK_READ = 0,
       PLDM_BLOCK_WRITE,
     };

enum BIOS {
    AMIBIOS = 0,
    PHOENIXBIOS,
    DELLBIOS,
    LENOVOBIOS,
    HP_BIOS,
    ACER_BIOS,
    ASUS_BIOS,
    INSYDE_BIOS,
};

enum {
    LEGACY = 0,
    UEFI
};

enum {
    FSC = 0,
    PEGATRON,
    EVERLIGHT,
    GENERIC,
    SAMSUNG,
    DELL
};

enum {
    POLL_NONE = 0,
    POLL_LS,
    POLL_ASF_STATUS,
    POLL_ASF_PEND,
    POLL_ASF_DATA,
    POLL_SENSOR_DETECT,
    POLL_SENSOR,
    POLL_STOP
};

typedef struct _sensor
{
    unsigned char  name[32];
    unsigned char  offset[2];
    unsigned char  pollindex: 4, exist:4;
    unsigned char  event: 2, fault:6;
    int value;
    unsigned char  state;
    unsigned char  prestate;
    unsigned char  type:7, signvalue:1;
    unsigned char  index;
    unsigned int LNC;
    unsigned int UNC;
    unsigned int LC;
    unsigned int UC;
    unsigned int LF;
    unsigned int UF;
} sensor_t;

typedef struct
{
    unsigned char  pollflag;
    unsigned char  expired;
    unsigned char  bootopt[11];
    unsigned char  MsgType;
    unsigned char  LSPFlag;
    unsigned char  ASFFlag;
    unsigned char  PollTimeOut;
    unsigned char  PollType;
    unsigned char  Status[28];
    unsigned char  Flag[56];
    unsigned char  Boottime;
    unsigned char  SensorIndex;
    unsigned char  LSensor[8];
    //at most support 8 legacy sensors
    sensor_t *sensor;
    unsigned char  IBRmtl;
    unsigned char  IBRmtlCmd;
} smbiosrmcpdata;

//total 32 bytes
typedef struct _eventdata
{
    unsigned int  timestamp;
    unsigned char   Event_Sensor_Type;
    unsigned char   Event_Type;
    unsigned char   Event_Offset;
    unsigned char   Event_Source_Type;
    unsigned char   Event_Severity;
    unsigned char   Sensor_Device;
    unsigned char   Sensor_Number;
    unsigned char   Entity;
    unsigned char   Entity_Instance;
    unsigned char   EventData[8];
    unsigned char   alertnum :6, sent: 2;
    unsigned short  timeout;
    unsigned short  seqnum;
    unsigned char   interval;
    unsigned char   watchdog :2, logtype : 6;
    struct _eventdata *next;
} eventdata;

typedef struct
{
    unsigned char rtaddr;
    unsigned char cmd;
    unsigned char bytecount;
    unsigned char subcmd;
    unsigned char version;
    unsigned char val;
} smbioshdr;

typedef struct
{
    unsigned char rtslaveaddr;
    unsigned char cmd;
    unsigned char bytecount;
    unsigned char rval;
    unsigned char version;
    unsigned char sstate;
} smbiosrmcp;

typedef struct
{
    unsigned char  *addr;
    unsigned char  status;
    unsigned short length;
} smbiostable_t;

typedef struct
{
    struct
    {
        unsigned int length  : 7;
        unsigned int index   : 8;
        unsigned int eor     : 1;
        unsigned int rsvd    : 8;
        unsigned int rsvd2   : 7;
        //unsigned int mctp    :  1;
        unsigned int own     : 1;
    } st;
    unsigned char* rx_buf_addr;
} volatile smbrxdesc_t;

typedef struct
{
    //1. Return Boot Options Response
    //2. No Boot Options Response
    unsigned char BCount;
    unsigned char SubCmd;
    unsigned char VersionNumber;
    //End No Boot Options Response
    unsigned char RC_IANA[4];
    unsigned char SpecialCmd;
    unsigned char SpCmdParameter[2];
    unsigned char BootOptionsBitMask[2];
    unsigned char OEMParameter[2];
} BootOptions;

#define BIT0    0x1
#define BIT1    0x2
#define BIT2    0x4
#define BIT3    0x8
#define BIT4    0x10
#define BIT5    0x20
#define BIT6    0x40
#define BIT7    0x80
#define BIT8    0x100
#define BIT9    0x200
#define BIT10   0x400
#define BIT11   0x800
#define BIT12   0x1000
#define BIT13   0x2000
#define BIT14   0x4000
#define BIT15   0x8000
#define BIT16   0x10000
#define BIT17   0x20000
#define BIT18   0x40000
#define BIT19   0x80000
#define BIT20   0x100000
#define BIT21   0x200000
#define BIT22   0x400000
#define BIT23   0x800000
#define BIT24   0x1000000
#define BIT25   0x2000000
#define BIT26   0x4000000
#define BIT27   0x8000000
#define BIT28   0x10000000
#define BIT29   0x20000000
#define BIT30   0x40000000
#define BIT31   0x80000000

// Version/Revsion field
#define Rsvd              BIT7|BIT6
#define UDID_Ver          BIT5|BIT4|BIT3
#define Silicon_RevsionID BIT2|BIT1|BIT0

// Interface field
#define IPMI_Support      BIT6
#define ASF_Support       BIT5
#define OEM_Support       BIT4
// smbus host address C2h
#define ARP_ADDRESS     0xC2
#define SMBHost_ADDRESS 0x10

/* ARP Commands */
#define ARP_PREPARE      0x01
#define ARP_RESET_DEV    0x02
#define ARP_GET_UDID_GEN 0x03
#define ARP_ASSIGN_ADDR  0x04
#define SMBIOS_Command   0x0a

/* ASF SMBus Cmd */
#define SnsrSystemState   0x01
#define ManagementControl 0x02
#define ASFConfiguration  0x03
#define Message           0x04
/* ASF SMBus Sub-Cmd */
#define GetEventData      0x11
#define GetEventStatus    0x12
#define DeviceTypePoll    0x13
#define SetSystemState    0x18
#define StartWatchDog     0x13
#define StopWatchDog      0x14
#define ClearBootOptions  0x15
#define ReturnBootOptions 0x16
#define NoBootOptions     0x17
#define PushAlertMsgWR    0x15
#define PushAlertMsgWoR   0x16
#define SetAlertConfig    0x11
#define GetAlertConfig    0x12

#define RCA_PWR_RST       0
#define RCA_PWR_ON        1
#define RCA_PWR_OFF       2
#define RCA_PWR_PCR       3
#define RCA_CMD           4
#define RCA_ADDR          5
#define RCA_LS_ADDR0      6
#define RCA_LS_CMD0       7
#define RCA_LS_ADDR1      8
#define RCA_LS_CMD1       9

// AMI BIOS Team Cowork - SMBus Receive Byte Protocol
#define Receive_Byte_Cmd 0x0b
#define ReturnValue_Yes  0x59
#define ReturnValue_No   0x4e

/* UDID Fields */
#define ARP_CAPAB       0
#define ARP_VER         1
#define ARP_VEND        2
#define ARP_DEV         4
#define ARP_INT         6
#define ARP_SUBVEND     8
#define ARP_SUBDEV      10
#define ARP_SPECID      12

#define UDID_LENGTH     0x11

#define SMBUS_ADDRESS_SIZE      0x80
#define ARP_FREE        0
#define ARP_RESERVED    1
#define ARP_BUSY        2
//#define GetUDID_BC      0x11


#define ARP_MAX_DEVICES 8

//static unsigned char RTUDID_Value[16] = {0};
// get udid
struct arp_rtdevice
{
    int status;
    unsigned char udid[UDID_LENGTH];
    unsigned char dev_cap;
    unsigned char dev_ver;
    short dev_vid;
    unsigned short dev_did;
    short dev_INT32S;
    short dev_svid;
    short dev_sdid;
    //vendor-specific ID
    unsigned int dev_vsid;
    unsigned char saddr;
};

struct SMBDesc
{
    unsigned char slave_address;
    unsigned char command_code;
    unsigned char bytecount;
    unsigned char sub_command_code;
};

/* Client has this additional data */
struct arp_data
{
    int sysctl_id;
    //need semaphore algorithm~~~??
    //  struct semaphore update_lock;

    char valid;             /* !=0 if following fields are valid */
    unsigned long last_updated;     /* In jiffies */

    unsigned char address_pool[SMBUS_ADDRESS_SIZE];
    struct arp_rtdevice dev[ARP_MAX_DEVICES];
};
/* SMBus Protocol Structure*/
struct SMBus_ARP
{
    struct arp_rtdevice *RTUDIDptr;
    struct arp_data *RTARPdatptr;
    char arpcmd_prepare;
    char arpcmd_reset_dev;
    char arpcmd_getudid;
    char arpcmd_assign_addr;
// Reserved for future use!!
    char arpcmd_rsvd;
    int	numSMBDesc;
    struct	SMBDesc	*SMBDescArray;
    unsigned int	SMBDescBufAddr;
};

// ASF SMBus Message 2. Sub-Command Value
struct SubCmd
{
    // <1> sensor device and system state
    char GetEvent_Data;                   //nic is master.
    char GetEvent_Status;                 //nic is master.
    char Device_TypePoll;                 //nic is slave.
    char Set_SystemState;                 //nic is slave.
    // <2> watchdog timer
    char StartWdg_Timer;                  //nic is slave.
    char StopWdg_Timer;                   //nic is slave.
    // <3> get boot options
    char Clear_BootOptions;               //nic is slave.
    char Return_BootOptions;              //nic is slave.
    char No_BootOptions;                  //nic is slave.
    // <4> messaging
    char PushMsg_withRetransmission;      //nic is slave.
    char PushMsg_withoutRetransmission;   //nic is slave.
    char SetAlert_Config;                 //nic is master.
    char GetAlert_Config;                 //nic is master.
};

// ASF SMBus Message 1. Command value
struct ASF_SMBusMsg
{
    // (1)sensor device and system state
    char Sensor_Sytemstate;
    // (2)management control
    char Mag_Control;
    // (3)ASF configuration
    char ASF_Config;
    // (4)ASF Messaging
    char ASF_Msging;
    // **(5)Reserved for future: 05h~0fh
    char ASF_Rsvd;
    // sub-cmd structure
    struct SubCmd *SubCmdptr;
};

eventdata *event_log(unsigned char *addr, unsigned char byte);
unsigned char SMBus_Prepare_RmtCtrl(unsigned char MsgType, unsigned char force);
unsigned char Fan_Poll(unsigned char fanindex, unsigned char index);
void setlogtype(eventdata *eptr);
void SetSnrState(unsigned char evtoffset);
void Get_FSC_Sensor(unsigned char snrindex, unsigned char polledval);
unsigned char master_send(msendtype type, unsigned char* bufaddr, unsigned char length, int cmd);
void handle_pldm_br_ami(unsigned char *addr, unsigned int len) ;
void handle_pldm_br_phoenix(unsigned char *addr, unsigned int len) ;
void handle_pldm_bw(unsigned char *addr);
void smbus_process_polling(unsigned char *ptr);
void smbus_process_watchdog(unsigned char *ptr);
void smbus_process_alerts(unsigned char *ptr);
void handle_pldm_tags(unsigned char *addr);
void pldm_handle_setgettime(unsigned char *addr);
void smbus_process_pldm_bw(unsigned char *addr);
void smbus_process_pldm_br_ami(unsigned char *addr, unsigned int len);
void smbus_process_pldm_br_phoenix(unsigned char *addr, unsigned int len);
void set_pldm_snr_slave_address(unsigned char addr);

#endif
