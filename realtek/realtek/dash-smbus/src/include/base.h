#ifndef _BASE_H_
#define _BASE_H_
//#define DEBUG
#ifdef DEBUG
#define debug printf
#else
#define debug(...)
#endif
#define ClearBootOptions  0x15
#define SetSystemState    0x18
void netlink_create(void);
void * netlink_recv(unsigned long *len);
void netlink_send(void * buf, unsigned int len);
int rand(void);
void user_mmap_kernel_struct(void);
enum {SMBSSTATE=0, SMBGSTATE, SMARP, SMSSEND, SMMSEND, SMBSMEN, SMBOFF1, MMAP, CLAERBOOTOPT, INBAND_CHECK, IODRVAP_EXIST, SINGLE_IP};
enum {SMB_MSG=2, SMB_PLDM, EVT_MSG, FLASH_MSG} ;
#define DPCONF_ADDRESS 0x1000
#define ASFCONF_ADDRESS 0x9000
#define TIMESTAMP_ADDRESS 0x7000
#define PLDMCONF_ADDRESS 0xC000
#define EVTLOG_ADDRESS 0xA000
enum {
    HMACSHA1=1,
    HMACSHA1_24,
    AES_CBC,
};
struct master_smbus_parameters
{
    unsigned char msg_type;
    unsigned char cmd;
    unsigned char pldmfromasf;
    unsigned char pldmpec;
    unsigned char asfpec;
    unsigned char poll_type;
    unsigned char * bufaddr;
    unsigned char length;
};

struct slave_smbus_info
{
    unsigned char * buf;
    unsigned int len;
};

struct smbus_arp_info
{
    unsigned int index;
    unsigned int value;
};

struct msgstru {
    unsigned int msgtype;
    unsigned char msgtext[256];
    unsigned int len;
};

struct msgstru_smb {
    unsigned int msgtype;
    unsigned char msgtext[256];
    unsigned int len;
};
#if 1
struct msgstru_evtq {
    unsigned int msgtype;
    unsigned char msgtext[4];
    unsigned int len;
};
#endif
struct msgstru_flash {
    long msgtype;
    unsigned char flag;
    unsigned char msgtext[1024];
    unsigned int len;
};
void spi_flash_write(unsigned int addr, unsigned char *buf, unsigned int len);
void pldmconf_read(unsigned char *buf, unsigned int len);
void pldmconf_write(unsigned char *buf, unsigned int len);
void timestamp_read(unsigned char *buf, unsigned int len);
void timestamp_write(unsigned char *buf, unsigned int len);
void asfconf_read(unsigned char *buf, unsigned int len);
void asfconf_write(unsigned char *buf, unsigned int len);
void dpconf_read(unsigned char *buf, unsigned int len);
void dpconf_write(unsigned char *buf, unsigned int len);
void evtlog_write(unsigned char *buf, unsigned int len);
void dp_lock(unsigned int type);
void asf_lock(unsigned int type);
void pldm_lock(unsigned int type);
void time_lock(unsigned int type);
void evtlog_lock(unsigned int type);
#if defined(CONFIG_PLDM_SENSOR)||defined(CONFIG_VENDOR_FSC)
void pldm_task(void);
#endif
//char *strrtrim(char *str);
//char *trimspace(char *str);
//void set_time(unsigned char *timestampdataptr);

void (*handle_pldm)(unsigned char *addr, unsigned int len);
void (*smbus_process_pldm)(unsigned char *addr, unsigned int len);
int snmp_send(void *ptr);
void snmp_init(int port);
//int asf_init(int port);
//void rmcpInput(RMCPCB * rmcp, unsigned char * rx_buf);
//void rmcpInput(unsigned char * rx_buf);
//int generate_uuid(char *ptr);

struct VncInfo
{
    unsigned char NewVNCCli:1, HwVNCEnable:1, Enable:1;
};
struct VgaInfo
{
    unsigned short FB0_Vresol, FB0_Hresol, FB0_BPP, FB0_VBnum, FB0_HBnum, FB1_Vresol, FB1_Hresol, FB1_BPP, FB1_VBnum, FB1_HBnum;
};
enum {KEYBOARD=0, MOUSE, FRAME_BUFFER, VGAINFO, KEEP_NOT_ALIVE, SET_MAC_ADDR, GET_MAC_ADDR, ENABLE_USB, VGA_ENABLE, VGA_DISBALE, MMAP_VNC, SET_SOCKETOPT, GET_CHG_RES, GET_BGN_REG, GET_BFN_REG, RE_INIT, DISABLE_USB, RELEASE_FB};

#endif