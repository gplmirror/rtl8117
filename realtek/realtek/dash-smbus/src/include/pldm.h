#ifndef __PLDM_H__
#define __PLDM_H__

typedef struct _BiosAttr
{
    struct _BiosAttr *next;
    unsigned short  *attr_possible_handle;
    unsigned char   *attr_data;
    unsigned char   *attr_val_index;
    unsigned char   *attr_default_index;
    unsigned char   *attr_val_handle;
    unsigned short  attr_handle;
    unsigned short  attr_type;
    unsigned short  attr_name_handle;
    unsigned short  attr_possible_num;
    unsigned char   attr_val_possible_num;
    unsigned char   attr_default_num;
    unsigned short  option;
} BiosAttr;

typedef struct
{
    unsigned char  *ptr[3];
    unsigned int len[3];
    unsigned int tag[3];
    unsigned char  TBL1:1, TBL2:1, TBL3:1, pldmerror:1, RSVD: 4;
    unsigned char  index;
    unsigned char  valid;
    unsigned char  dirty;
    unsigned char  *xferptr[3];
    unsigned int numwrite[4];
    unsigned int xferlen[3];
    unsigned int xfertag[3];
    unsigned int chksum[3];
} pldm_t;

typedef struct
{
    unsigned char slaveaddr;
    unsigned char cmd;
    unsigned char length;
    unsigned char sslaveaddr;
    unsigned char hdrversion:1, rsvd: 5, mctpstop:1, mctpstart:1;
    unsigned char dstept;
    unsigned char srcept;
    unsigned char msgtag:3, to:1, pktseq:2, msgstop:1, msgstart:1;
    unsigned char msgtype:7, ic: 1;
    unsigned char iid: 5, rsvd2: 1, datagram: 1, rd: 1;
    unsigned char pldmtype:6, hdrver: 2;
    unsigned char pldmcmd;
    unsigned char pldmcode;
    unsigned char val[57];
    unsigned char val2[2];
} pldm_res_t;

typedef enum _pldm_base_code
{
    PLDM_SUCCESS,
    PLDM_ERROR,
    PLDM_ERROR_INVALID_DATA,
    PLDM_ERROR_INVALID_LENGTH,
    PLDM_ERROR_NOT_READY,
    PLDM_ERROR_UNSUPPORTED_PLDM_CMD,
    PLDM_INVALID_PLDM_TYPE = 0x20,
} PLDM_BASE_CODE;

//PLDM DSP 0240
enum {
    SetTID = 1,
    GetTID,
    GetPLDMVersion,
    GetPLDMTypes,
    GetPLDMCommands,
};

//PLDM DSP 0246
enum {
    GetSMBIOSStructureTableMetadata = 1,
    SetSMBIOSStructureTableMetadata,
    GetSMBIOSStructureTable,
    SetSMBIOSStructureTable,
    GetSMBIOSStructureByType,
    GetSMBIOSStructureByHandle,
};

enum {
    NO_SMBIOS_STRUCTURE_TABLE_METADATA = 0x83,
};

//PLDM DSP 0247
enum {
    GetBIOSTable = 1,
    SetBIOSTable,
    UpdateBIOSTable,
    GetBIOSTableTags,
    SetBIOSTableTags,
    AcceptBIOSAttributesPendingValues,
    SetBIOSAttributeCurrentValue,
    GetBIOSAttributeCurrentValueByHandle,
    GetBIOSAttributePendingValueByHandle,
    GetBIOSAttributeCurrentValueByType,
    GetBIOSAttributePendingValueByType,
    GetDateTime,
    SetDateTime,
    GetBIOSStringTableStringType,
    SetBIOSStringTableStringType,
    BIOS_TABLE_TAG_UNAVAILABLE = 0x86,
};

enum {
    PLDM_BIOS_TABLE_UNAVAILABLE = 0x83,
    PLDM_BIOS_TABLE_TAG_UNAVAILABLE = 0x86,
    PLDM_UNKNOWN = 0xFF,
};

enum {
    BIOSEnumeration = 0x00,
    BIOSString = 0x01,
    BIOSPassword = 0x02,
    BIOSBootConfigSetting = 0x04,
};

unsigned int CreateAttrVal(unsigned char *buf, unsigned char *seq);
BiosAttr *InitialPLDM(void);
unsigned char getPLDMElement(BiosAttr *addr, unsigned char ptype, unsigned char val, unsigned char *out);
#endif
