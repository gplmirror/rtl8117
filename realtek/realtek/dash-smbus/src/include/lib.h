/*
******************************* C HEADER FILE *******************************

Copyright (c) Realtek Semiconductor Corp. All rights reserved.

File Name:	lib.h

*****************************************************************************
*/

#ifndef _LIB_INCLUDED
#define _LIB_INCLUDED

#define CKHSUM_OFFLOAD

/*--------------------------Include Files-----------------------------------*/
#include <stdio.h>
#include <ctype.h>
#include <endian.h>
#include <time.h>

#include <openssl/ssl.h>
#include "bsp.h"

/*--------------------------Definitions------------------------------------*/

#define MAC_ADDR_LEN    6
//#define BIG_ENDIAN          2
//#define LITTLE_ENDIAN       1
//#define BYTE_ORDER LITTLE_ENDIAN

//move to inet_types.h
//#define MAC_ADDR_LEN        6
#define ETH_PADDING_SIZE    2

#define IPv4_ADR_LEN	4
#define IPv6_ADR_LEN	16

#define HTTP_AUTH_MAX  6

#define PASS	1
#define UNPASS	0

#define eth0 0

#define ETHERNET_TYPE_IPv4	0x0800
#define ETHERNET_TYPE_IPv6	0x86DD
#define ETHERNET_TYPE_ARP	0x0806

#define IPv4 4
#define IPv6 6

#define Basic 0
#define Digest 1
#define Kerb 2

#define AES_ENCRYPT     0
#define AES_DECRYPT     1
#define AES_ENCRYPT_SW     0
#define AES_DECRYPT_SW     1


#define TCP_HDR_SIZE	20
#define UDP_HDR_SIZE	8

#define IPv4_HDR_OFFSET		16
#define TCPv4_HDR_OFFSET	36
#define TCPv6_HDR_OFFSET	56

#define ETH_PKT_LEN		1516
#define ETH_PAYLOAD_LEN  1300

#define ETH_HDR_SIZE	16
#define MAX_PAGE_LINE_LEN 255
#define LEAST_PAGE_LINE_LEN 255

//size of PKT(4) + ETH_HDR_SIZE(16) + IP max header size 60 + TCP max header size 60
//Teredo Tunnel ETH_HDR_SIZE(16) + IPv4 max header size 60 +UDP Header 8+ teredo tunnel 20 byte(min)?? + IPv6 Header size 40
#define PKT_HDR_SIZE 140
#define IP_PKT_HDR_SIZE 80
//UDP_HDR_SIZE	8
#define UDP_PKT_HDR_SIZE 140

#define MAXSENDPKTS         3
#define	MAXRTTIMES		12
#define TIMEOUT             3
#define CONNECT_RETRY	3
#define SKT_DLY_TIME	20
#define off     0
#define on      1

#define NEED_PATCH 9
#define MAX_RTR_SOLICITATION_DELAY 1

#define DHCPv4_Cli 68
#define DHCPv4_Srv 67

#define DHCPv6_Cli 546
#define DHCPv6_Srv 547

#define SNMPPort   0x00A2

#define DNS_Srv_PORT 53

#define BASIC_AUTH 1
#define DIGEST_AUTH 2
#define KERB_AUTH 3
#define MAX_PS_LEN 32 //provisioning server 
#define MAX_DN_LEN 64//domain name
#define MAX_HN_LEN 16 //host name
#define MAX_FQDN_LEN 32 //fqdn (amt like)
#define MAX_PW_LEN 32 //password
#define MAX_OU_LEN 64 //ou
#define MAX_OTP_LEN 32
#define PID_LEN 8
#define PPS_LEN 32

enum {
    DASH_SUCCESS = 0,
    DASH_FAIL = 2,
};

enum {
    OPT_NOCS = 0,
    OPT_IPCS = 1,
    OPT_TCPCS = 2,
    OPT_UDPCS = 4,
    OPT_FREE  = 16,
    OPT_IPV6  = 64,
};

enum {
    DASHOK = 1<<15,
    HOSTOK = 1<<14,
    REQDMODE = 1<<13,
    OCP_ACCIB = 1 << 10,
    OCP_TER   = 1<<9,
    OCP_RER   = 1<<8,
    OCP_SW_INT = 1<<6,
    OCP_LINKCHG_INT = 1<<5,
    OCP_FOVW = 1<<4,
    OCP_TDU = 1<<3,
    OCP_TOK = 1<<2,
    OCP_RDU = 1<<1,
    OCP_ROK = 1<<0,
};

//For fwLog
//Producer Enum
enum {nobody , PRODUCER_ICMP };
//Event Type
enum { none , DETAIL ,  ALLOCATE_MEM_FAIL };

typedef struct OpaqueManagementData
{
    unsigned char Status:1, dirty:1, rw:1/*0:read, 1:write*/;
    unsigned char Owner[16];
    unsigned char ElementName[16];
    unsigned char DataFormat[8];
    unsigned int DataSize;
    unsigned int LastAccessed;
    unsigned int MaxSize;
    unsigned short WriteLimited;
    unsigned short Access;
    unsigned short DataOrganization;
    unsigned int BlockSize;
    unsigned short NumberOfBlocks;
    unsigned short ConsumableBlocks;

} OPAQDATA;

typedef struct _ServiceCfg
{
//#if WIN
//	unsigned char IPv4Enable:1, IPv6Enable:1, DHCPv4Enable:1, DHCPv6Enable:1, DHCPRTTime:4;
//	unsigned char EchoService:1, Security:3;
//#else
//Do not use char bit field in 4180
    unsigned int IPv4Enable:1, IPv6Enable:1, DHCPv4Enable:1, DHCPv6Enable:1,
             DHCPRTTime:4, DHCPv6NeighborAdver:1, EchoService:1, Security:3, Reserved:19;
    char srcMacAddr[MAC_ADDR_LEN];
//#endif
} ServiceCfg;

typedef struct _IPAddress
{
    unsigned int addr;
} IPAddress;

typedef struct _DataList {
    struct _DataList *next;
    unsigned char *addr;
    unsigned char *payLoadAddr;
    unsigned int len;
} DataList;

#define PKT_SIZE 24

typedef struct _PKT {
    struct _PKT *prev;
    struct _PKT *next;
    unsigned char *start;//Data start address
    unsigned char *wp;//Data start address
    unsigned short len; // packet lenght
    unsigned char ipVer;
    unsigned char rTime:7,queued:1;
} PKT;

typedef struct _UserInfo {
    unsigned char  name[16];
    unsigned char  passwd[16];
    unsigned char  role;
    unsigned char  opt;
    unsigned short crc16;
    unsigned char  caption[23];
    unsigned char  opaque;
    struct _UserInfo *next;
} UserInfo;

typedef struct _RoleInfo {
    unsigned char  name[23];
    unsigned char  mask;
    unsigned int privilege;
    struct _RoleInfo *next;
} RoleInfo;

typedef struct _ADUserInfo {
    unsigned char sid[28];
    unsigned char ad;
    unsigned char dummy[3];
    unsigned char role;
    unsigned char  opt;
    unsigned short crc16;
    unsigned char  caption[23];
    unsigned char  opaque;
    struct _UserInfo *next;
} ADUserInfo;

#define max_data_size 64
/*typedef struct _SubEvent {
    char subid[38];
    char notifyto[80];
    char query[80];
    unsigned short enable;
    unsigned int heartbeatLast;
    unsigned int interval;
    unsigned char opt[44];
    struct _SubEvent *next;
} SubEvent;
*/
typedef struct _pkidata
{
    unsigned char fqdn[MAX_FQDN_LEN];
    unsigned char dnssuf[MAX_FQDN_LEN];
    unsigned char certhash[6][20];
} PKIDATA;
typedef struct _pskdata
{
    unsigned char PID[PID_LEN];
    unsigned char PPS[PPS_LEN];
} PSKDATA;

typedef struct _MulticastList {
    struct _MulticastList *next;
    unsigned char addr[6];
} MulticastList;
#define MAX_INTF 2
typedef struct _dpconf
{
    UserInfo admin;
    IPAddress HostIP[MAX_INTF];
    IPAddress SubnetMask[MAX_INTF];
    IPAddress GateWayIP[MAX_INTF];
    IPAddress DNSIP[MAX_INTF];
    IPAddress ConsoleIP[MAX_INTF];

    unsigned int asfon:1, chipset: 3, numsent: 4, hbtime : 8, polltime : 6, asfpoll: 1, lspoll: 1, arpaddr : 8 ;
    unsigned int IPv4Enable:1, IPv6Enable:1, DHCPv4Enable:1, DHCPv6Enable: 1, DHCPRTTime:4, DHCPv6Dumy:1, EchoService:1, Security:3, httpService:1, httpsService:1, wsmanService:1, isDHCPv4Enable:1,rsvd3: 15;
    unsigned int  MatchSubnetMaskValue[MAX_INTF];
    unsigned char srcMacAddr[MAX_INTF][MAC_ADDR_LEN];
    unsigned short efusever;
    unsigned int fwMajorVer;
    unsigned int fwMinorVer;
    unsigned int fwExtraVer;
    unsigned int fwBuildVer;
    unsigned int builddate;
    //unsigned int wdt;

    unsigned char HostIPv6[MAX_INTF][IPv6_ADR_LEN];
    unsigned char IPv6Prefix[MAX_INTF][IPv6_ADR_LEN];
    unsigned char IPv6DNS[MAX_INTF][IPv6_ADR_LEN];
    unsigned char IPv6GateWay[MAX_INTF][IPv6_ADR_LEN];
    unsigned char IPv6PrefixLen[MAX_INTF];
    unsigned char BMCPoll:1, BMCPollTime: 7;
    unsigned char DBGMacAddr[MAC_ADDR_LEN];
    IPAddress DBGIP;
    unsigned short DASHPort;
    unsigned short DASHTLSPort;
    unsigned char  HostName[16];
    unsigned char  Profiles[12];
    unsigned char DomainName[MAX_DN_LEN];
    unsigned short ProvisioningSetup:1, ProvisioningMode:1, ProvisioningState: 2, PKIEnabled:1, PSKEnabled:1, dummy1:10;
    unsigned short ProvisionServerPort;
    IPAddress ProvisionServerIP;
    unsigned char ProvisionServerName[MAX_PS_LEN];
    struct _pkidata pkidata;
    struct _pskdata pskdata;
    unsigned char OTP[MAX_OTP_LEN];
    unsigned char OUString[MAX_OU_LEN];
    unsigned char UUID[20];
    unsigned int ipmode;
    unsigned char  aesmsg[20]; //runtime usage, to get key from setup binary
    unsigned int efusekey;
    unsigned int bios:8, biostype: 8,  vendor:8, pldmtype: 4, pldmmultitx: 1, pldmdbg:1, enpldmsnr :1, pldmreset:1;
    unsigned char  restart;    //inform dash to reget the in-band data
    unsigned char eap;
    unsigned char usbotg:1, ehcipatch:1, usbrsvd:6;
    unsigned char useTeredo:1, teredoEnable:1, teredorsvd:6;
    unsigned char teredoSrv[32];
    //PLDMRCC means the slave address of Remote Control and PLDM are the same
    unsigned int pldmsnr:8, numofsnr:8, pldmfromasf: 1, pldmpec: 1, snrpec: 1, pldmrsvd: 13;
    unsigned char  pldmslaveaddr;
    unsigned char  pldmsrvd[3];
    //struct _EAPTLS *eaptls;
    unsigned char eappro;
    unsigned char enctype;
    unsigned char enckey[16];
    //INT8U DomainRealm[MAX_DN_LEN];

    unsigned short counter; 	 //+briankuo , 20140909 : Add for recording reset times
} DPCONF;

typedef struct webToken
{
    char* token;
    char offset;
} WEBTOKEN;

enum DHCPv4States
{
    InitState = 0x00,
    SelectState = 0x01,
    RequestState = 0x02,
    BoundState = 0x03
};

typedef struct _DHCPv4Config
{
    IPAddress OfferIP;
    unsigned int DHCPv4TID;
    unsigned int DHCPv4LeaseTime;
    enum DHCPv4States DHCPv4State;
    unsigned int DHCPv4RTTime: 5;
} DHCPv4Config;

typedef struct _HTTPHdrInfo
{
    unsigned char url[32];//Tmp
    UserInfo* uInfo;
    char *httpDataPos;
    char *httpCharSet;
    unsigned short HttpHdrLen;
    unsigned short httpContentLen;
    unsigned char httpMethod:2, httpAuthMethod:2, httpAuthPass:1,
             invaildPost:1;
    unsigned char httpParseStatus:2;
} HTTPHdrInfo;

#define WEBGET		1
#define WEBPOST		2
#define WSMANPOST	3

#define HTTPINIT 0
#define	HTTPHEADER 1
#define HTTPDATA 2

#define	MIN_REQ_LEN	16		/* "GET / HTTP/1.1\n\n"		*/
#define MAX_HTTPMSG_LEN 256
#define MAX_WEBPAGE_PATH_LEN 128

typedef enum _WebPageID
{
    NOTFOUND    = 0,
    RDCSS       = 1,
    LOGOGIF     = 2,
    BANNERPAGE  = 3,
    HOMEPAGE    = 4,
    WELCOMEPAGE = 5,
    INFOPAGE    = 6,
    REMOTECTLPAGE   = 7,
    REMOTECTLINFO = 8,
    EVENTLOGPAGE = 0x61,
    BOOTSETPAGE = 9,
    SECSETPAGE  = 10,
    NETSETPAGE  = 11,
    NETSETv6PAGE  = 12,
    ABOUTPAGE	  = 13,
    AUTHPAGE	  = 15,
    AUTHFAILPAGE  = 16,
    OKPAGE = 17,
    EZSHAREPAGE = 18,
    EZSHAREBIN = 19,
    SRVSETPAGE = 0x60,
    JOBINFOPAGE = 19,
    STAINFOPAGE = 20,
    PROINFOPAGE = 21,
    MAIINFOPAGE = 22,
    SUPINFOPAGE = 23,
    WAKEINFOPAGE = 24,
    JOBLISTPAGE = 25,
    /*JOBALLPAGE = 25*/
    JOBREPORTPAGE = 26,
    GENERALPAGE = 27,
    PAPERTRAYPAGE = 28,
    OUTPUTTRAYPAGE = 29,
    DRUMPAGE = 30,
    MACHINEPAGE = 31,
    MEMORYPAGE = 32,
    COUNTERPAGE = 33,
    PAPERTRAYATTRIPAGE = 34,
    PAPERSETTINGPAGE = 35,
    POWERSETTINGPAGE = 36,
    INTERNETSETTINGPAGE = 37,
    PORTSTATUSPAGE = 38,
    ETHERNETPAGE = 39,
    USBPAGE = 40,
    SMBPAGE = 41,
    TCPIPPAGE = 42,
    LPDPAGE = 43,
    SNMPPAGE = 44,
    IPPPAGE = 45,
    PORT9100PAGE = 46,
    HTTPPAGE = 47,
    PRINTMODEPAGE = 48,
    POSTSCRIPTPAGE = 49,
    HPGl2PAGE = 50,
    TIFFPAGE = 51,
    PCLPAGE = 52,
    MEMORYSETTINGPAGE = 53,
    ERRORHISTORYPAGE = 54,
    SUPPORTPAGE = 55,
    //mail_wakeup 201211025 eccheng
    MAIL_WAKEUP_ACC = 61,
    MAIL_WAKEUP_MAT = 62,
    COMPUTERSYSPAGE = 63,
    OPERATIONSYSPAGE = 64,
    SOFTWAREIDPAGE = 65,
    BIOSPAGE = 66,
    PROCESSORPAGE = 67,
    FANPAGE = 68,
    SENSORPAGE = 69,
    ASSETPAGE = 70,
    SHOWCOUNTER = 71,
    RESETCOUNTER = 72,
    WIFICONFPAGE = 80,
    WIFIIPPAGE =81

} WebPageID;
enum PostURL
{
    REMOTECTLPOST = 0x00,
    BOOTPOST,
    SECURITYPOST,
    NETPOST,
    SRVPOST,
    AUTHPOST,
    EzSharePOST,
};

typedef struct _slist {
    struct _slist *next;
    unsigned char  *addr;
    unsigned short len;
} send_list;
typedef struct {
    char *eltname;
    char *data;
    char key; //key value , will be show in pull-response(in EPRs)
    char used;
    char idx;//the offset in the smbios structure ," but not specific the type of smbios structure ?"
    char attr;//1:byte , 2:word, Others:array? Must callgetSMBIOSElement
} XMLElt;
typedef struct _plist {
    struct _plist *next;
    char *eltname;
    char *data;
    char key;
    char used;
    char idx;
} plist;

typedef struct _DASHCB {
    UserInfo *userinfo;
    QHdr     *qhdr;
    unsigned char    *rxbuf;
    send_list *sendlist;
    int      cs;
    plist    *ProfilePtr; //a new memory space
    plist    *StartOff; //point to a specific position in ProfilePtr
    XMLElt   *URI;
    XMLElt   *PreURI;
    unsigned int   privilege;
    unsigned int   profile_priv;
    int   actiontype;
    int   count;//The "index" of instance(plist)
    short   Ptype; //about the SMBIOS data , the type of smbios structure , e.g. Type0 : BIOSinformation , Type1:System Information
    short   association;
    unsigned char    control;
    unsigned char    status;
    unsigned char    fault_id;
    unsigned char    closesend;
    unsigned char    epr;
    unsigned char    PullOption;
    char    SubClass;
    char    RegClass;//index of const RegProfile _RegProfile[]=
    unsigned char    InstanceCount;
    unsigned char    ref;
    unsigned char    ad_nego;
    unsigned char    ref_count[16];
    unsigned char    CurrentInterfaceNum; // Record which IPProtocol to be associated 0xFF for unset
    char    r_uri[64];// ResourceURI  at check_envelope
    char    q_uri[64];//<ResultClassName> at ParserBody
    char    ref_uri[64];// ResourceURI at check_envelope , from selectorSet
    int                     httpContentLen;     //Http content length
    int                     httpReadLen;        //How many data that have been received
    unsigned char                   sendPkts:5,TLSEnable:1, TLSDataStage:1;
    unsigned char           httpAuthPass:1, httpMethod:2,
             httpAuthMethod:2, httpParseStatus:3;
    unsigned char           maxConnections:3 //max connections for this service
    ,curConnections:3 //current connections
    ,delayAck:2; //has been accepted
    unsigned char           probeCount:4, probe:1, alive:1,rxEnable:1, txEnable:1;
    unsigned char hwIBPortFilterEnable:1/*Unicast*/, hwOOBPortFilterEnable:1/*Broadcast/Multicast*/, timeWaitEnable:1, specIntf:1, intf:4;
    //unsigned char httpMethod:2, httpAuthMethod:2, httpAuthPass:1,
    //         invaildPost:1;
    enum PostURL            postURL;
    SSL                     *ssl;
    char                    dest_MAR[6];
} DASHCB;


typedef struct
{
    int nr;                     /*!<  number of rounds  */
    unsigned long *rk;          /*!<  AES round keys    */
    unsigned long buf[68];      /*!<  unaligned data    */
}
aes_context, aes_context_sw;

#define mDNSA       0x01
#define DNSPTR  0x0C
#define DNSNB       0x20
#define DNSNBSTAT   0x21
#define HOST_NAME_MAX_LEN 16

#define SSL_RANDOM_SIZE 32
#define SESSION_ID_LEN  32
#define TLS_SHELLO_SIZE 74
#define MAX_KEY_BYTE_SIZE 512  /* for a 4096 bit key */
#define TLS_RECORD_SIZE 5
#define TLS_COUNTER_LEN 8

#define OS_ERR_NONE                   0u

#define OS_ERR_EVENT_TYPE             1u
#define OS_ERR_PEND_ISR               2u
#define OS_ERR_POST_NULL_PTR          3u
#define OS_ERR_PEVENT_NULL            4u
#define OS_ERR_POST_ISR               5u
#define OS_ERR_QUERY_ISR              6u
#define OS_ERR_INVALID_OPT            7u
#define OS_ERR_PDATA_NULL             9u

#define OS_ERR_TIMEOUT               10u
#define OS_ERR_EVENT_NAME_TOO_LONG   11u
#define OS_ERR_PNAME_NULL            12u
#define OS_ERR_PEND_LOCKED           13u
#define OS_ERR_PEND_ABORT            14u
#define OS_ERR_DEL_ISR               15u
#define OS_ERR_CREATE_ISR            16u
#define OS_ERR_NAME_GET_ISR          17u
#define OS_ERR_NAME_SET_ISR          18u

#define OS_ERR_MBOX_FULL             20u

#define OS_ERR_Q_FULL                30u
#define OS_ERR_Q_EMPTY               31u

#define OS_ERR_PRIO_EXIST            40u
#define OS_ERR_PRIO                  41u
#define OS_ERR_PRIO_INVALID          42u

#define OS_ERR_SEM_OVF               50u

#define OS_ERR_TASK_CREATE_ISR       60u
#define OS_ERR_TASK_DEL              61u
#define OS_ERR_TASK_DEL_IDLE         62u
#define OS_ERR_TASK_DEL_REQ          63u
#define OS_ERR_TASK_DEL_ISR          64u
#define OS_ERR_TASK_NAME_TOO_LONG    65u
#define OS_ERR_TASK_NO_MORE_TCB      66u
#define OS_ERR_TASK_NOT_EXIST        67u
#define OS_ERR_TASK_NOT_SUSPENDED    68u
#define OS_ERR_TASK_OPT              69u
#define OS_ERR_TASK_RESUME_PRIO      70u
#define OS_ERR_TASK_SUSPEND_IDLE     71u
#define OS_ERR_TASK_SUSPEND_PRIO     72u
#define OS_ERR_TASK_WAITING          73u

#define OS_ERR_TIME_NOT_DLY          80u
#define OS_ERR_TIME_INVALID_MINUTES  81u
#define OS_ERR_TIME_INVALID_SECONDS  82u
#define OS_ERR_TIME_INVALID_MS       83u
#define OS_ERR_TIME_ZERO_DLY         84u
#define OS_ERR_TIME_DLY_ISR          85u

#define OS_ERR_MEM_INVALID_PART      90u
#define OS_ERR_MEM_INVALID_BLKS      91u
#define OS_ERR_MEM_INVALID_SIZE      92u
#define OS_ERR_MEM_NO_FREE_BLKS      93u
#define OS_ERR_MEM_FULL              94u
#define OS_ERR_MEM_INVALID_PBLK      95u
#define OS_ERR_MEM_INVALID_PMEM      96u
#define OS_ERR_MEM_INVALID_PDATA     97u
#define OS_ERR_MEM_INVALID_ADDR      98u
#define OS_ERR_MEM_NAME_TOO_LONG     99u

#define OS_ERR_NOT_MUTEX_OWNER      100u

#define OS_ERR_FLAG_INVALID_PGRP    110u
#define OS_ERR_FLAG_WAIT_TYPE       111u
#define OS_ERR_FLAG_NOT_RDY         112u
#define OS_ERR_FLAG_INVALID_OPT     113u
#define OS_ERR_FLAG_GRP_DEPLETED    114u
#define OS_ERR_FLAG_NAME_TOO_LONG   115u

#define OS_ERR_PIP_LOWER            120u

#define OS_ERR_TMR_INVALID_DLY      130u
#define OS_ERR_TMR_INVALID_PERIOD   131u
#define OS_ERR_TMR_INVALID_OPT      132u
#define OS_ERR_TMR_INVALID_NAME     133u
#define OS_ERR_TMR_NON_AVAIL        134u
#define OS_ERR_TMR_INACTIVE         135u
#define OS_ERR_TMR_INVALID_DEST     136u
#define OS_ERR_TMR_INVALID_TYPE     137u
#define OS_ERR_TMR_INVALID          138u
#define OS_ERR_TMR_ISR              139u
#define OS_ERR_TMR_NAME_TOO_LONG    140u
#define OS_ERR_TMR_INVALID_STATE    141u
#define OS_ERR_TMR_STOPPED          142u
#define OS_ERR_TMR_NO_CALLBACK      143u

#ifndef false
#define false 0
#endif

#ifndef true
#define true (!false)
#endif


typedef struct
{
    unsigned char  header;
    unsigned char  length;
    unsigned char  version[3];
    unsigned char  community_header;
    unsigned char  string_length;
    unsigned char  community[6];
    unsigned char  PDU[2];
    unsigned char  EOT;
    unsigned char  EOL;
    unsigned char  SubID[9];
    unsigned char  agent_addr_type;
    unsigned char  agent_addr_length;
    //unsigned char  agent_address[4];
    unsigned int agent_address;
    unsigned char  trap_type_type;
    unsigned char  trap_type_length;
    unsigned char  trap_type;
    unsigned char  strap_type_type;
    unsigned char  strap_length;
    unsigned char  stf_reserved;
    unsigned char  stf_event_sensor;
    unsigned char  stf_event;
    unsigned char  stf_event_offset;

    //specific_trap_field
    unsigned char  time_stamp_type;
    unsigned char  time_stamp_length;
    unsigned char  time_stamp[4];
    unsigned char  variable_binding_type;
    unsigned char  variable_binding_length;
    unsigned char  variable_binding_type2;
    unsigned char  variable_binding_length2;
    unsigned char  object_type;
    unsigned char  object_length;
    unsigned char  SID2[10];
    unsigned char  value_type;
    unsigned char  value_length;
    unsigned char  guid[16];
    unsigned char  sequence_number[2];
    unsigned char  local_timestamp[4];
    unsigned char  UTC_offset[2];
    unsigned char  trap_source;
    unsigned char  event_source;
    unsigned char  event_severity;
    unsigned char  sensor_device;
    unsigned char  sensor_number;
    unsigned char  Entity;
    unsigned char  Entity_Instance;
    unsigned char  Event_Data[8];
    unsigned char  Language_code;
    unsigned char  Manufacturer_ID[4];
    unsigned char  System_ID[2];
    unsigned char  OEM_Custom;
} SNMP;

#define Realtek_IANA            0x00006a92
#define SNMPPort                    0x00A2
#define SNMP_hdr                      0x30
#define SNMP_Version              0x020100
#define PDU_Type                    0xa461
#define Enterprise_Object_Type        0x06
#define Enterprise_Object_Length      0x09
#define Agent_Address_Type            0x40
#define Agent_Address_Length          0x04
#define Trap_Type_Type                0x02
#define Trap_Type_Length              0x01
#define Trap_Type                     0x06
#define Specific_Trap_Type_Type       0x02
#define Specific_Trap_Type_Length     0x04
#define Time_Stamp_Type               0x43
#define Time_Stamp_Length             0x04
#define Variable_Binding_Type         0x30
#define Object_Type                   0x06
#define Object_Length                 0x0a
#define Value_Type                    0x04

#define WindowSIZE             0x20
//RMCP Header
#define RMCP_V1_PORT             0x26f
#define RMCP_V2_PORT             0x298
#define RMCP_HdrVersion          0x06
#define RMCP_HdrRsvd             0x00
#define RMCP_ClassofMessage      0x06
#define RMCP_ClassofACK          0x86


//RMCP Data
#define RMCP_DatRsvd      0x00
#define Payload_Type_AA   0x01
#define Payload_Type_IA   0x02

//role
#define ASF_Administrator 0x01
#define ASF_Operator      0x00

#define SIK_KEY_SIZE      0x14
#define SIK_KEY_SIZE_USE  0x0C

#define RMCP_OFFSET       (ETH_HDR_SIZE + IP_HLEN + UDP_HDR_SIZE)

//dash profile
#define DASH_PWR_On    "2"
#define DASH_Sleep     "4"
#define DASH_Reset     "5"
#define DASH_Hibernate "7"
#define DASH_Shutdown  "8"
#define DASH_PWR_CR    "9"

//in-band remote control
#define IB_Reset     0x01
#define IB_Sleep     0x02
#define IB_Hibernate 0x03
#define IB_Shutdown  0x04

//RMCP Message Type
#define RMCP_Reset   0x10
#define RMCP_PWR_On  0x11
#define RMCP_PWR_Off 0x12
#define RMCP_PWR_CR  0x13
#define RMCP_HIBER   0x14
#define RMCP_STANDBY 0x15

#define RMCP_TEST    0x20
#define RMCP_PONG    0x40
#define RMCP_CAP_RES 0x41
#define RMCP_SST_RES 0x42
#define RMCP_OPS_RES 0x43
#define RMCP_CLS_RES 0x44
#define RMCP_PING    0x80
#define RMCP_CAP_REQ 0x81
#define RMCP_SST_REQ 0x82
#define RMCP_OPS_REQ 0x83
#define RMCP_CLS_REQ 0x84
#define RMCP_RAKP_M1 0xC0
#define RMCP_RAKP_M2 0xC1
#define RMCP_RAKP_M3 0xC2
//remote control 0310

#define RMCP_SEND_NONE    0x00
#define RMCP_SEND_ACK     0x01
#define RMCP_SEND_RES     0x10

//structure for rmcp state, rmcp control block
typedef struct
{
    unsigned int session_id;  //session id
    unsigned int seq_num;  //sequence number
} RSPHdr;

typedef struct
{
    unsigned short padding;
    unsigned char  pad_len;
    unsigned char  next_header;
    unsigned char  integrity[SIK_KEY_SIZE_USE];
} RSPTrailer;

typedef struct
{
    unsigned char version;
    unsigned char reserved;
    unsigned char seq_num;
    unsigned char com; //class of message
} RMCPHdr;

typedef struct
{
    unsigned char  IANA[4];
    unsigned char  MsgType;
    unsigned char  MsgTag;
    unsigned char  rsvd;
    unsigned char  Data_Length;
} RMCPData;


typedef struct
{
    unsigned char    version;
    unsigned char    state;
    unsigned char    type;
    unsigned char    seq_num;
    unsigned char    status;
    unsigned char    role;
    unsigned char    name_length;
    unsigned char    rsvd;
    unsigned int   session_seq;
    unsigned int   client_sid;
    RSPHdr   *rsp_hdr;
    RMCPHdr  *rmcp_hdr;
    RMCPData *rmcp_data;
    unsigned char    *var_data;
    unsigned char    *key;
    unsigned char    MgtSID[4];
    unsigned char    CltRandom[16];
    unsigned char    MgtRandom[16];
    unsigned char    username[16];
} RMCPCB;
/*-----------------------------Functions------------------------------------*/
unsigned int chksum(void *data, int len);
unsigned short inet_chksum(void *data, int len);
char* strrtrim(char *ptr);
char* trimspace(char *ptr);
void set_time(unsigned char *timestampdataptr);
unsigned int ifloor(unsigned int a, unsigned int b);
unsigned int crc32(unsigned int crc, unsigned char *buf, unsigned int len);
void pldm_snr_read(unsigned char snrnum);
void getGUID(unsigned char *RTGUID);
#endif


