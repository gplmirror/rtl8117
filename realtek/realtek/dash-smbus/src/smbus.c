#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <netinet/in.h>
#include <limits.h>
#include <linux/netlink.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <mtd/mtd-user.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sched.h>
#include <semaphore.h>
#include <signal.h>
#include <pthread.h>
#include "smbus.h"
#include "bsp.h"
#include "lib.h"
#include "pldm.h"
#include "smbios.h"
#include "base.h"
#include "bsp_cfg_fp_revA.h"

#define SNMP_UDP_PORT 162
#define RMCP_ASFV1_UDP_PORT  623
#define RMCP_ASFV2_UDP_PORT  664
int asfv1_fd, asfv2_fd;

extern struct nlmsghdr *nlh;
extern struct sockaddr_nl src_addr, dest_addr;
extern struct iovec iov;
extern int sock_fd;
extern struct msghdr msg;


#define SLAVE_SMBUS 0
#define MASTER_SMBUS 1
#define SMBUS_ARP 2

#define SnsrSystemState   0x01
#define ManagementControl 0x02
#define ASFConfiguration  0x03
#define Message           0x04

#define PLDM_BLOCK_READ 0
#define PLDM_BLOCK_WRITE 1

#define    POLL_LS 1
#define    POLL_ASF_STATUS 2
#define    POLL_ASF_PEND 3
#define    POLL_ASF_DATA 4
#define    POLL_SENSOR_DETECT 5
#define    POLL_SENSOR 6
#define    POLL_STOP 7
extern int smbus_fd;
static int mtd_fd ;
key_t smb_key=0, pldm_key=0, evt_key=0, flash_key=0;
extern int evt_msgid, flash_msgid;
int smb_msgid, pldm_msgid;
extern volatile DPCONF *dpconf;
extern asf_config_t *asfconfig ;
extern pldm_t *pldmdataptr;
extern smbiosrmcpdata *smbiosrmcpdataptr;
extern volatile unsigned char *smbiosptr;
extern volatile unsigned char *timestampdataptr;
extern pldm_res_t *pldmresponse[7];
extern sensor_t *sensor;
extern unsigned char *flash_buf;
extern eventdata *eventdataptr;
extern eventdata *event_head;
extern unsigned int event_length;
extern unsigned char data_index;
unsigned char arpaddr=0xC8;
unsigned char pldmtype=1;
unsigned char smbios_PollType = 0;
const unsigned char RTKUDID[18] = { 0x11, 0x41, 0x08, 0x10, 0xEC, 0x81, 0x68, 0x00, 0x04, 0x10, 0xEC, 0x81, 0x68, 0x00, 0x00, 0x00, 0x00, 0xB1};
const unsigned char PLDMSNR[15] = { 0x00, 0x0F, 0x0C, 0xC9, 0x01, 0x00, 0x08, 0xC8, 0x01, 0x80, 0x02, 0x11, 0x05, 0x00, 0x00};
static unsigned char *pldmsnrsendbuf = NULL;

#define SHM_NAME_SEM "/memmap_sem"
void get_mac_addr(void);
void getGUID(unsigned char *RTGUID)
{
    memcpy(RTGUID, dpconf->UUID, 16);
}

void setlogtype(eventdata *eptr)
{
    //determine its log type
    if (eptr->Event_Sensor_Type == 0x20)
        eptr->logtype = LOG_ERROR;
    else if (eptr->Event_Sensor_Type == 0x05)
        eptr->logtype = LOG_WARNING;
    else if (eptr->Event_Sensor_Type == 0x01 || eptr->Event_Sensor_Type == 0x02 || eptr->Event_Sensor_Type == 0x04)
    {
        if (eptr->Event_Severity == 0x01)
            eptr->logtype = LOG_WARNING;
        else
            eptr->logtype = LOG_ERROR;
    }
    else if (eptr->Event_Sensor_Type == 0x25)
        eptr->logtype = LOG_INFO;
    else if (eptr->Event_Sensor_Type == 0x0F)
    {
        if (eptr->Event_Offset == 0x00 || eptr->Event_Offset == 0x01)
            eptr->logtype = LOG_ERROR;
        else if ((eptr->EventData[1] >= 1 && eptr->EventData[1] <= 3) || (eptr->EventData[1] >= 6 && eptr->EventData[1] <= 17))
            eptr->logtype = LOG_HW;
        else if (eptr->EventData[1] == 0x04 || eptr->EventData[1] == 0x05)
            eptr->logtype = LOG_AUTH;
        else
            eptr->logtype = LOG_INFO;
    }
}


void toSMBIOSHeader(SMBIOSHeader *h, unsigned char *data)
{
    h->type = data[0];
    h->len = data[1];
    h->handle = data[2] + (data[3]<<8);
    h->data = data;
}

unsigned char* getSMBIOSTypeAddr(enum SMBIOSType type)
{
    unsigned char* data =  (smbiosptr+32);
    int count = 0;
    int len = 0;
    //int i = 0,len=0;
    SMBIOS_Table_EP *SMBIOSTableEP = (SMBIOS_Table_EP *)smbiosptr;
    while ( len < SMBIOSTableEP->StTableLen )
    {
        SMBIOSHeader sh = {0};
        toSMBIOSHeader(&sh, data);

        if ( sh.len < 4)
        {
            printf("\nSMBIOS Header Error\n");
            return NULL;
        }
        if (sh.type == type)
        {
            return data;
        }
        len= len+ sh.len;
        //Find next SMBIOS structure
        data = data + sh.len;
        //Each structure is terminated by a double 0
        while ((data[0]!=0) || (data[1]!=0))
            data++;
        len +=2;
        data += 2;
        count++;
    }
    return 0;
}

char getSMBIOSTypeNext(unsigned char* p,enum SMBIOSType type, unsigned char** next)
{
    const char FOUND = 1;
    const char NONE = 0;
    unsigned char* data =  p;
    SMBIOSHeader sh = {0};
    toSMBIOSHeader(&sh, data);
    if ( sh.len < 4)
    {
        printf( "SMBIOS Header Error\n");
        return NONE;
    }
    data = data + sh.len;
    while ((data[0]!=0) || (data[1]!=0))
        data++;
    data += 2;
    do
    {

        toSMBIOSHeader(&sh, data);
        if ( sh.len < 4)
        {
            printf( "Next SMBIOS Header Error\n");
            return NONE;
        }
        if(sh.type == 0x7f)
            return NONE;

        //if(sh.type == SMBIOS_Inactive)
        //  continue;

        //sort issue
        if (sh.type == type)
        {
            *next = data;
            return FOUND;
        }

        data = data + sh.len;
        while ((data[0]!=0) || (data[1]!=0))
            data++;
        data += 2;
    } while (1);
}


static void GetASFSensor(int i)
{
    unsigned char index;

    for(index= 0 ; index < asfconfig->numofsnr; index++)
    {
        i++;
        sensor[i].pollindex=0;
        sensor[i].exist=SNR_READ;
        sensor[i].event=0;
        sensor[i].fault=5;
        sensor[i].value=0;
        sensor[i].state=0;
        sensor[i].prestate=0;
        sensor[i].index=i;

        //estabilish a link
        sensor[i].offset[0]= index;
        asfconfig->legacysnr[index].sensorindex = i;

        strcpy(sensor[i].name, "Legacy Sensor");
        sensor[i].type = 0x01 ;

        dpconf->numofsnr++;
    }
}

int ParserSensor(void)
{
    unsigned char *addr,*addr1,*curLoc,*curLoc1;
    unsigned short component,threshold,tmp;
    int ret,i=-1;
    int cflag = 0, sflag = 0;

    dpconf->numofsnr = 0;

    addr  = getSMBIOSTypeAddr(0x99);
    addr1 = getSMBIOSTypeAddr(0xff);

    if(addr1)
    {
        printf( "Type 0xFF\n");
        dpconf->pldmsnr = 0xff;

        //using EC for different slave address
        if (addr1[6] & 0x08)
        {
            printf( "Using EC as PLDM Sensor Controller\n");
            set_pldm_snr_slave_address((*(addr1+10) & 0xFE));
            //if(*(addr1+10) & 0x01)
            if (addr1[10] & 0x01)
                dpconf->snrpec = 1;
            else
                dpconf->snrpec = 0;
        }
        else
        {
            printf( "Using BIOS as PLDM Sensor Controller\n");
            set_pldm_snr_slave_address(dpconf->pldmslaveaddr);
            dpconf->snrpec = dpconf->pldmpec;
        }
    }
    else if(addr)
    {
        printf( "Type 0x99\n");
        dpconf->pldmsnr = 0x99;
    }
    else
    {
        printf( "ASF Type\n");
        GetASFSensor(i);
        return 1;
    }

    addr =getSMBIOSTypeAddr(0x23);
    while(addr)
    {
        cflag = 0;
        sflag = 0;
        component=*(addr+7)+(*(addr+8) <<8);
        threshold=*(addr+9)+(*(addr+10) <<8);
        addr1 =getSMBIOSTypeAddr(dpconf->pldmsnr);

        if(!addr1)
        {
            printf( "Can not find 0xFF\n");
            return 0;
        }

        while(addr1 && i<MAX_SENSOR_NUMS)
        {

            if(dpconf->pldmsnr == 0xff)
                //tmp=*(addr1+4)+(*(addr1+5) <<8);
                tmp = (addr1[5]<<8)|addr1[4];
            else
                //tmp=*(addr1+11)+(*(addr1+12) <<8);
                tmp = (addr1[12]<<8)|addr1[11];

            if(tmp==component)
            {
                i++;
                sensor[i].pollindex=0;
                sensor[i].exist=SNR_READ;
                sensor[i].event=0;
                sensor[i].fault=5;
                sensor[i].value=0;
                sensor[i].state=0;
                sensor[i].prestate=0;
                sensor[i].index=i;
                sensor[i].offset[0]=*(addr1+9);

                strcpy(sensor[i].name,( const char *)addr+11);
                dpconf->numofsnr++;
                break;
            }
            curLoc1 = addr1;
            ret =getSMBIOSTypeNext(curLoc1, dpconf->pldmsnr, &addr1);
            if(!ret)
            {
                printf( "Can not find matched sensor %d\n", component);
                //return 0;
                sflag = 1;
                break;
            }
        }

        if(sflag == 0)
        {
            addr1 =getSMBIOSTypeAddr(0x1a);
            while(addr1)
            {
                tmp=*(addr1+2)+(*(addr1+3) <<8);
                if(tmp==component)
                {
                    sensor[i].type = SNR_VOLTAGE;
                    cflag = 1;
                    break;
                }
                curLoc1 = addr1;
                ret=getSMBIOSTypeNext(curLoc1, 0x1a, &addr1);
                if(!ret)
                    break;
            }

            if(cflag == 0)
            {
                addr1 =getSMBIOSTypeAddr(0x1b);
                while(addr1)
                {
                    tmp=*(addr1+2)+(*(addr1+3) <<8);
                    if(tmp==component)
                    {
                        sensor[i].type = SNR_TACHOMETER;
                        cflag = 1;
                        //printf( "Get Tachometer %d\n", tmp);
                        break;
                    }
                    curLoc1 = addr1;
                    ret=getSMBIOSTypeNext(curLoc1, 0x1b, &addr1);
                    if(!ret)
                        break;
                }
            }

            if(cflag == 0)
            {
                addr1 =getSMBIOSTypeAddr(0x1c);
                while(addr1)
                {
                    tmp=*(addr1+2)+(*(addr1+3) <<8);
                    if(tmp==component)
                    {
                        sensor[i].type = SNR_TEMPERATURE;
                        //printf( "Get Temperature sensor %d\n", tmp);
                        break;
                    }
                    curLoc1 = addr1;
                    ret=getSMBIOSTypeNext(curLoc1, 0x1c, &addr1);
                    if(!ret)
                        break;
                }
            }

            addr1 =getSMBIOSTypeAddr(0x24);
            while(addr1)
            {
                tmp=*(addr1+2)+(*(addr1+3) <<8);
                if(tmp==threshold)
                {
                    //printf( "Setting Threshold %d\n", tmp);
                    sensor[i].LNC=*(addr1+4)+(*(addr1+5) <<8);
                    sensor[i].UNC=*(addr1+6)+(*(addr1+7) <<8);
                    sensor[i].LC=*(addr1+8)+(*(addr1+9) <<8);
                    sensor[i].UC=*(addr1+10)+(*(addr1+11) <<8);
                    sensor[i].LF=*(addr1+12)+(*(addr1+13) <<8);
                    sensor[i].UF=*(addr1+14)+(*(addr1+15) <<8);
                    break;
                }
                curLoc1 = addr1;
                ret=getSMBIOSTypeNext(curLoc1, 0x24, &addr1);
                if(!ret)
                    break;
            }
        }
        curLoc = addr;
        ret=getSMBIOSTypeNext(curLoc, 0x23, &addr);
        if(!ret)
        {
            printf( "No more 0x23\n");
            break;
        }
    }
    //GetASFSensor(i);
    return 1;
}

void set_pldm_snr_slave_address(unsigned char addr)
{
    if (pldmsnrsendbuf == NULL)
    {
        pldmsnrsendbuf = (unsigned char *)malloc(16);
        memcpy(pldmsnrsendbuf, PLDMSNR, 15);
    }
    pldmsnrsendbuf[0] = addr;
    pldmsnrsendbuf[5] = 0x0A;
    pldmsnrsendbuf[6] = 0x09;
}

void pldm_snr_read(unsigned char snrnum)
{
    static unsigned char msgtag = 0xC8;
    static unsigned char instid = 0x80;
#ifdef CONFIG_PLDM_DEBUG
    unsigned char i = 0;
#endif

    if (pldmsnrsendbuf == NULL)
    {
        pldmsnrsendbuf = (unsigned char *)malloc(16);
        memcpy(pldmsnrsendbuf, PLDMSNR, 15);
    }

    if(0xCF == msgtag)
    {
        msgtag =0xC8;
    }
    else
    {
        msgtag++;
    }

    if(0x9F == instid)
    {
        instid = 0x80;
    }
    else
    {
        ++instid;
    }

    pldmsnrsendbuf[7] = msgtag;
    pldmsnrsendbuf[9] = instid;
    pldmsnrsendbuf[12] = sensor[snrnum].offset[0];
    debug( "NIC Sends: ");
#ifdef CONFIG_PLDM_DEBUG
    for(i= 0 ; i < pldmsnrsendbuf[2]+3 ; i++)
        debug( "%02x ", pldmsnrsendbuf[i]);
    debug( "\n");
#endif
    struct master_smbus_parameters *msp = malloc(sizeof(struct master_smbus_parameters));
    msp->msg_type = PLDM_REQUEST;
    msp->cmd = 0x09;
    msp->pldmfromasf = dpconf->pldmfromasf;
    msp->pldmpec = dpconf->pldmpec;
    msp->asfpec = asfconfig->pec;
    msp->poll_type = smbiosrmcpdataptr->PollType;
    msp->bufaddr = pldmsnrsendbuf;
    msp->length = pldmsnrsendbuf[2] + 3;
    ioctl(smbus_fd, SMMSEND, msp);
    //master_send(PLDM_REQUEST, pldmsnrsendbuf, pldmsnrsendbuf[2] + 3, 0x09);
#if 0
    master_send(PLDM_RESPONSE, pldmsnrsendbuf, pldmsnrsendbuf[2] + 3, 0x09);
#endif

}

void smbus_handler(unsigned char *addr, unsigned int len);
void smbus_loop(void)
{
    unsigned char *addr = NULL;
    int ret_value = 0;
    unsigned long rcv_len=0;
    //int i=0;
    int smb_flag = 0;
    struct msgstru_smb msgs;
    while (1)
    {
        addr = netlink_recv(&rcv_len);
        if (addr)
        {
            if (addr[0] == 0xC2)
                smb_flag = 1;
            //byte read
            else if ((addr[0] != dpconf->arpaddr) && (smbiosrmcpdataptr->PollType == POLL_SENSOR))
            {
                msgs.msgtype = SMB_PLDM;
                msgs.len = rcv_len;
                memcpy(msgs.msgtext, addr, rcv_len);
                ret_value = msgsnd(pldm_msgid, &msgs, sizeof(struct msgstru_smb), IPC_NOWAIT);
                if (ret_value < 0)
                    printf("msgsnd() write msg failed.\n");
            }
            else if ((addr[0] != dpconf->arpaddr) && (smbiosrmcpdataptr->PollType == POLL_LS))
            {
                smb_flag = 1;
            }
            //check not a block read command, means a block write or byte read
            //else if (addr[2] != (dpconf->arpaddr + 1) )
            else if ((addr[0] == dpconf->arpaddr) && (addr[2] != (dpconf->arpaddr + 1) ))
            {
                //set block read type in advance for BIOS management only
                //try to get the PLDM command at the first
                handle_pldm(addr, rcv_len);
                if ((rcv_len >=12)&&(addr[1] == 0x0F && addr[10] == 0x02 && addr[11] == 0x11))
                {
                    msgs.msgtype = SMB_PLDM;
                    msgs.len = rcv_len;
                    memcpy(msgs.msgtext, addr, rcv_len);
                    ret_value = msgsnd(pldm_msgid, &msgs, sizeof(struct msgstru_smb), IPC_NOWAIT);
                    if (ret_value < 0)
                        printf("msgsnd() write msg failed.\n");
                }
                else
                {
                    smb_flag = 1;
                }

            }
            else //sould be block read
                smb_flag = 1;

            if (smb_flag == 1)
            {
                smb_flag = 0;
                smbus_handler(addr, rcv_len);
            }
            //free(addr);
        }
    };
}
void smbus_handler(unsigned char *addr, unsigned int len)
{
    if (addr[0] == dpconf->arpaddr && addr[2] == (dpconf->arpaddr +1))
    {
        //free(addr);
        return;
    }
    if (addr[0] != dpconf->arpaddr)
    {
        smbus_process_polling(addr);
    }
    else
    {
        switch (addr[1])
        {
        case SnsrSystemState:
            if ((addr[2] == 0x03) && (addr[3] == SetSystemState) && (addr[4] == 0x10))
                ioctl(smbus_fd, SMBSSTATE, addr[5]);
            break;
        case ManagementControl:
            smbus_process_watchdog(addr);
            break;
        case ASFConfiguration:
            if (addr[2] == 0x02 && addr[3] == ClearBootOptions)
            {
                if (addr[4] == 0x10)
                    ioctl(smbus_fd, CLAERBOOTOPT, NULL);
                //memset(bootopt, 0, sizeof(bootopt));
            }
            break;
        case Message:
            smbus_process_alerts(addr);
            break;
        case  0x0f:
            //Parsing PLDM
            smbus_process_pldm(addr, len);

            //tom modified for Block Read Method @20140219
            if ((*(addr+3) == 0x71 || *(addr+3) == 0x89) && (*(addr+4) == 0x01) && ((*(addr+7) & 0xC8) == 0xC8) && (*(addr+8) == 0x01) && (*(addr+10) == 0x01) && (*(addr+11) == 0x02))
                ParserSensor();
            break;
        default :
            break;
        }
    }
    //free(addr);
}

void SetSnrState(unsigned char evtoffset)
{
    switch(evtoffset)
    {
    case 0x00:
        sensor[asfconfig->legacysnr[smbiosrmcpdataptr->SensorIndex].sensorindex].state = STATE_LOWERWARNING;
        break;
    case 0x02:
        sensor[asfconfig->legacysnr[smbiosrmcpdataptr->SensorIndex].sensorindex].state = STATE_LOWERCRITICAL;
        break;
    case 0x07:
        sensor[asfconfig->legacysnr[smbiosrmcpdataptr->SensorIndex].sensorindex].state = STATE_UPPERWARNING;
        break;
    case 0x09:
        sensor[asfconfig->legacysnr[smbiosrmcpdataptr->SensorIndex].sensorindex].state = STATE_UPPERCRITICAL;
        break;
    case 0xff:
        sensor[asfconfig->legacysnr[smbiosrmcpdataptr->SensorIndex].sensorindex].state = STATE_NORMAL;
        break;
    }
}
//eventdata *evt_ptr;
void smbus_process_polling(unsigned char *ptr)
{
    unsigned char polledval, eventchanged = 0;
    eventdata evtdata;
    eventdata *evt_ptr;
    unsigned char i = 0, snrval;

    polledval = *ptr;
    struct msgstru msgs;
    if (smbiosrmcpdataptr->PollType == POLL_LS)
    {
        debug("Get Sensor index %d status %x, polled value %x\n", smbiosrmcpdataptr->SensorIndex, smbiosrmcpdataptr->LSensor[smbiosrmcpdataptr->SensorIndex], polledval);
        //detect state change
        if (smbiosrmcpdataptr->LSensor[smbiosrmcpdataptr->SensorIndex] !=polledval)
        {
            memset(&evtdata, 0, sizeof(evtdata));
            smbiosrmcpdataptr->LSensor[smbiosrmcpdataptr->SensorIndex] = polledval;
            for(i = 0 ; i < asfconfig->numofalerts; i++)
            {
                debug("Get Sensor addr %x command %x, index %x\n", asfconfig->legacysnr[smbiosrmcpdataptr->SensorIndex].addr, asfconfig->legacysnr[smbiosrmcpdataptr->SensorIndex].command, i);
                if((asfconfig->legacysnr[smbiosrmcpdataptr->SensorIndex].addr != asfconfig->asfalerts[i].address) || (asfconfig->legacysnr[smbiosrmcpdataptr->SensorIndex].command != asfconfig->asfalerts[i].command))
                    continue;
                snrval = polledval & asfconfig->asfalerts[i].datamsk;

                //Assume 0 means normal state
                if (snrval == 0)
                    SetSnrState(0xff);

                debug("Sensor Compared Value %x\n", snrval);
                if((snrval == asfconfig->asfalerts[i].cmpvalue))
                {
                    memcpy(&evtdata.Event_Sensor_Type, &asfconfig->asfalerts[i].evtsnrtype, 8);
                    SetSnrState(asfconfig->asfalerts[i].evtoffset);

                    evtdata.logtype = asfconfig->asfalerts[i].logtype;
                    evt_ptr = event_log(&evtdata.Event_Sensor_Type, 8);
                    evt_ptr->interval = (ALERT_INTERVAL/(dpconf->numsent-1));
                    evt_ptr->timeout = 0;
                    evt_ptr->alertnum = dpconf->numsent;
                    asfconfig->asfalerts[i].status = EVENT_ASSERT;

                    msgs.msgtype = EVT_MSG;
                    memcpy(msgs.msgtext, evt_ptr, sizeof(evtdata));
                    msgsnd(evt_msgid, &msgs,sizeof(struct msgstru),IPC_NOWAIT);
                }
                else
                {
                    if(asfconfig->asfalerts[i].status == EVENT_ASSERT)
                    {
                        asfconfig->asfalerts[i].evtoffset |= 0x80;
                        memcpy(&evtdata.Event_Sensor_Type, &asfconfig->asfalerts[i].evtsnrtype, 8);
                        SetSnrState(asfconfig->asfalerts[i].evtoffset);

                        evtdata.logtype = asfconfig->asfalerts[i].logtype;
                        evt_ptr = event_log(&evtdata.Event_Sensor_Type, 8);
                        evt_ptr->interval = (ALERT_INTERVAL/(dpconf->numsent-1));
                        evt_ptr->timeout = 0;
                        evt_ptr->alertnum = dpconf->numsent;
                        asfconfig->asfalerts[i].status = EVENT_DEASSERT;
                        msgs.msgtype = EVT_MSG;
                        memcpy(msgs.msgtext, evt_ptr, sizeof(evtdata));
                        msgsnd(evt_msgid, &msgs,sizeof(struct msgstru),IPC_NOWAIT);
                    }
                }
            }
        }
        else
        {
            snrval = polledval & asfconfig->asfalerts[i].datamsk;
            //Assume 0 means normal state
            if (snrval == 0)
                SetSnrState(0xff);
        }
        smbiosrmcpdataptr->PollType = POLL_NONE;
    } //end of LS POLL
    else if (smbiosrmcpdataptr->PollType == POLL_ASF_STATUS)
    {
        smbiosrmcpdataptr->ASFFlag = 0;
        //reset the index for asf sensor
        for (i = 0 ; i < ifloor(*(ptr+1), 2); i++)
        {
            if ((*(ptr+i+2) & 0x0f)	!= (smbiosrmcpdataptr->Status[i] & 0x0f))
            {
                smbiosrmcpdataptr->Flag[smbiosrmcpdataptr->ASFFlag++] = 2*i;
                eventchanged = 1;
            }
            if ((*(ptr+i+2) & 0xf0)	!= (smbiosrmcpdataptr->Status[i] & 0xf0))
            {
                smbiosrmcpdataptr->Flag[smbiosrmcpdataptr->ASFFlag++] = 2*i+1;
                eventchanged = 1;
            }
        }
        if (eventchanged)
        {
            eventchanged = 0;
            memcpy(smbiosrmcpdataptr->Status, ptr+2 , ifloor(*(ptr+1), 2));
            smbiosrmcpdataptr->PollType = POLL_ASF_PEND;
        }
        else
        {
            smbiosrmcpdataptr->PollType = POLL_NONE;
        }
    } //end of ASF sensor polling status
    else if (smbiosrmcpdataptr->PollType == POLL_ASF_DATA)
    {
        evt_ptr = event_log(ptr+2, *ptr-1);
        setlogtype(evt_ptr);
        evt_ptr->interval = (ALERT_INTERVAL/(dpconf->numsent-1));
        evt_ptr->timeout = 0;
        evt_ptr->alertnum = dpconf->numsent;
        if (--smbiosrmcpdataptr->ASFFlag == 0)
        {
            smbiosrmcpdataptr->PollType = POLL_NONE;
        }
        else
        {
            smbiosrmcpdataptr->PollType = POLL_ASF_PEND;
        }
        msgs.msgtype = EVT_MSG;
        memcpy(msgs.msgtext, evt_ptr, sizeof(evtdata));
        msgsnd(evt_msgid, &msgs,sizeof(struct msgstru),IPC_NOWAIT);
    }
}

void smbus_process_watchdog(unsigned char *ptr)
{
    eventdata wdtdata;
    struct msgstru msgs;
    if (ptr[2] == 0xc9) //PEC = 0xc9
        ;//Rese Alert Sending Device(NIC)
    else if (ptr[2] >0x0c && ptr[2] < 0x13)
    {
        if ( ptr[3] == StartWatchDog)
        {
            if ( ptr[4] ==0x10)
            {
                memset(&wdtdata, 0,  sizeof(eventdata));
                memcpy(&wdtdata.Event_Sensor_Type, ptr+7, ptr[2] - 4);
                wdtdata.timeout = (*(ptr+6) << 8 ) + *(ptr+5) + dpconf->hbtime;
                //need to add the hbtime, since the START TASK would
                //be wake up immediately
                wdtdata.interval = (ALERT_INTERVAL/(dpconf->numsent-1));
                wdtdata.alertnum = dpconf->numsent;
                wdtdata.watchdog = 1;
                msgs.msgtype = EVT_MSG;
                memcpy(msgs.msgtext, &wdtdata, sizeof(wdtdata));
                msgsnd(evt_msgid, &msgs,sizeof(struct msgstru),IPC_NOWAIT);
                smbiosrmcpdataptr->expired = 0;
            }
        }
    }
    else if (ptr[2] == 0x02)
    {
        if (ptr[3] == StopWatchDog)
        {
            if ( ptr[4] == 0x10)
            {
                //force the queue to stop push again
                wdtdata.alertnum = 0;
                wdtdata.watchdog = 0;
            }
        }
    }
}

void smbus_process_alerts(unsigned char *ptr)
{
    eventdata *evt_ptr;
    struct msgstru msgs;

    if (ptr[2] == UDID_LENGTH)
    {
        if (!memcmp(RTKUDID, ptr+3, UDID_LENGTH))
        {
            dpconf->arpaddr = *(ptr+19);
        }
    }
    else if (ptr[2] > 0x0a && ptr[2] < 0x11)
    {
        if (ptr[3] == PushAlertMsgWR)
        {
            if (ptr[4] == 0x10)
            {
                evt_ptr =  event_log(ptr+5, ptr[2]-2);
                setlogtype(evt_ptr);
                evt_ptr->interval = (ALERT_INTERVAL/(dpconf->numsent-1));
                evt_ptr->alertnum = dpconf->numsent ;

                evt_ptr->timeout  =  0;
                msgs.msgtype = EVT_MSG;
                memcpy(msgs.msgtext, evt_ptr, sizeof(eventdata));
                msgsnd(evt_msgid, &msgs,sizeof(struct msgstru),IPC_NOWAIT);
            }
        }
        else if (ptr[3] == PushAlertMsgWoR)
        {
            if (ptr[4] == 0x10)
            {
                evt_ptr =  event_log(ptr+5, ptr[2]-2);
                setlogtype(evt_ptr);
                evt_ptr->timeout = 0;
                evt_ptr->alertnum = 1;
                msgs.msgtype = EVT_MSG;
                memcpy(msgs.msgtext, evt_ptr, sizeof(eventdata));
                msgsnd(evt_msgid, &msgs,sizeof(struct msgstru),IPC_NOWAIT);
            }
        }
    }
}

void evtq_handler(int a)
{
    struct msgstru msgs;
    eventdata *pevent;
    ssize_t ret;

    ret = msgrcv(evt_msgid, &msgs, sizeof(struct msgstru), 0, 0);
    if (ret < 0)
        return;
    //msgrcv(evt_msgid, &msgs, sizeof(struct msgstru), 0, 0);
    pevent = msgs.msgtext;
    pevent->timeout = (pevent->timeout >0)?pevent->timeout-1:0;
    if (!pevent->timeout)
    {
        if (pevent->watchdog)
        {
            if (pevent->alertnum > 0)
            {
                smbiosrmcpdataptr->expired = 1;
                pevent->watchdog = 0;
                //log this event and post to queue for handling
                pevent->logtype = LOG_ERROR;
                pevent = event_log(&pevent->Event_Sensor_Type, 10);
                msgs.msgtype = EVT_MSG;
                memcpy(msgs.msgtext, pevent, sizeof(eventdata));
                msgsnd(evt_msgid, &msgs,sizeof(struct msgstru), IPC_NOWAIT);
            }
            //receive a stop watchdog timer
            else
                smbiosrmcpdataptr->expired = 0;
        }
        else if (--pevent->alertnum > 0)
        {
            //handling PushAlertMsgWR
            pevent->timeout = pevent->interval;
            msgs.msgtype = EVT_MSG;
            memcpy(msgs.msgtext, pevent, sizeof(eventdata));
            msgsnd(evt_msgid, &msgs,sizeof(struct msgstru), IPC_NOWAIT);
        }

        if (!pevent->watchdog)
        {
            //only asf alerts need to retransmission
            //dash use tcp, no need to do restransmission
            snmp_send(pevent);
        }
    }
    else if (pevent->alertnum > 0)
        //if not timeout => put it back, should be the watch dog case
    {
        msgs.msgtype = EVT_MSG;
        memcpy(msgs.msgtext, pevent, sizeof(eventdata));
        msgsnd(evt_msgid, &msgs,sizeof(struct msgstru), IPC_NOWAIT);
    }
    //end of while
}

void evtq_loop(void)
{
    struct itimerval t;
    t.it_interval.tv_usec = 0;
    t.it_interval.tv_sec = 1;
    t.it_value.tv_usec = 0;
    t.it_value.tv_sec = 5;

    if (setitimer(ITIMER_REAL, &t, NULL)<0)
        exit(-1);
    signal(SIGALRM, evtq_handler);
    while (1)
    {
        sleep(1);
    }
}

void _smbus_init(void)
{
    debug("_smbus_init 1, %d.\n", dpconf->pldmtype);
    if(dpconf->pldmtype == PLDM_BLOCK_READ)
    {
        debug("_smbus_init read.\n");
        if(dpconf->bios == PHOENIXBIOS)
        {
            debug("_smbus_init read 1.\n");
            handle_pldm = handle_pldm_br_phoenix;
            smbus_process_pldm = smbus_process_pldm_br_phoenix;
        }
        else
        {
            debug("_smbus_init read 2.\n");
            handle_pldm = handle_pldm_br_ami;
            smbus_process_pldm = smbus_process_pldm_br_ami;
        }
    }
    else
    {
        debug("_smbus_init write.\n");
        smbus_process_pldm = smbus_process_pldm_bw;
        handle_pldm = handle_pldm_bw;
    }
    debug("_smbus_init over.\n");
}

unsigned char Fan_Poll(unsigned char fanindex, unsigned char index)
{
    unsigned char ret = 0;
    unsigned char bptr[3];
    struct master_smbus_parameters *msp = NULL;

    bptr[0] = 0xE6;
    bptr[1] = sensor[fanindex].offset[index];
    smbiosrmcpdataptr->sensor = &sensor[fanindex];
    bptr[2] = bptr[1] + 1;

    msp = malloc(sizeof(struct master_smbus_parameters));
    msp->msg_type = BMC_POLL;
    msp->cmd = 0x06;
    msp->pldmfromasf = dpconf->pldmfromasf;
    msp->pldmpec = dpconf->pldmpec;
    msp->asfpec = asfconfig->pec;
    msp->poll_type = smbiosrmcpdataptr->PollType;
    msp->bufaddr = bptr;
    msp->length =  3;
    ioctl(smbus_fd, SMMSEND, msp);
    free(msp);
    return ret;
}

void pldm_task(void)
{
    //int ret_value = 0;
    struct msgstru msgs;
    unsigned char *addr = NULL;
    unsigned char err;
    unsigned char snrnum = 0;
    unsigned char errcount = 0;

#if CONFIG_VENDOR_FSC
    unsigned char curindex = 0;
    static const unsigned char snraddr[18] = {0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0x59, 0x69, 0x79, 0x89, 0x99, 0xA9, 0xB9, 0xC9, 0xD9, 0xE9, 0xF9};
    static const unsigned char snrtype[18] = {SNR_TACHOMETER,SNR_TACHOMETER,SNR_TACHOMETER,SNR_TACHOMETER,SNR_TACHOMETER,SNR_TACHOMETER,SNR_TACHOMETER,SNR_TEMPERATURE,SNR_TEMPERATURE,SNR_TEMPERATURE,SNR_TEMPERATURE,SNR_TEMPERATURE,SNR_TEMPERATURE,SNR_TEMPERATURE,SNR_TEMPERATURE,SNR_TEMPERATURE,SNR_TEMPERATURE, SNR_TEMPERATURE};
    static const unsigned char snrname[18][20] = {"FAN1", "FAN2", "FAN3", "FAN4", "FAN5", "FAN6", "FANPS", "CPU1 SEN1", "CPU1 SEN2", "CPU2 SEN1", "SYS1", "SYS2", "SYS3", "HDD", "SYS4", "SYS5", "SYS6", "SYS7"};

    dpconf->numofsnr = 0;
    while(curindex <18)
    {
        if(smbiosrmcpdataptr->PollType == POLL_NONE)
        {
            sensor[dpconf->numofsnr].offset[0] = snraddr[curindex];
            sensor[dpconf->numofsnr].index = dpconf->numofsnr;
            sensor[dpconf->numofsnr].type  = snrtype[curindex];

            if(!Fan_Poll(dpconf->numofsnr, 0))
            {
                smbiosrmcpdataptr->PollType = POLL_SENSOR;
                smbiosrmcpdataptr->PollTimeOut = 0;
            }
            else
            {
                sleep(1);
                continue;
            }

            err = msgrcv(pldm_msgid, &msgs, sizeof(struct msgstru_smb), 0, 0);
            smbiosrmcpdataptr->PollType = POLL_NONE;

            if (err == OS_ERR_NONE)
            {
                addr = msgs.msgtext;
                Get_FSC_Sensor(dpconf->numofsnr, *addr);

                if(sensor[dpconf->numofsnr].exist == SNR_READ)
                {
                    strcpy(sensor[dpconf->numofsnr].name, snrname[curindex]);
                    if(sensor[dpconf->numofsnr].type == SNR_TACHOMETER)
                        sensor[dpconf->numofsnr++].offset[1] = snraddr[curindex] + 7;
                    else
                        sensor[dpconf->numofsnr++].offset[1] = snraddr[curindex] - 1;
                }
                free(addr);
            }
            curindex++;
        }
        else
            sleep(1);
    }
#endif

    while (1)
    {
        snrnum = 0;
        while (snrnum != (dpconf->numofsnr-asfconfig->numofsnr))
        {
            if(sensor[snrnum].exist != SNR_READ)
                break;

            if ((bsp_get_sstate() != S_S5) && (smbiosrmcpdataptr->PollType != POLL_STOP))   //add polltype for sensor request timing@20141104
            {
#ifdef CONFIG_PLDM_SENSOR
                pldm_snr_read(snrnum);
                err = msgrcv(pldm_msgid, &msgs, sizeof(struct msgstru_smb), 0, 0);
                if (err == OS_ERR_NONE)
                {
                    addr = msgs.msgtext;
                    if((addr[13] == PLDM_sint8) || (addr[13] == PLDM_sint16) || (addr[13] == PLDM_sint32))
                        sensor[snrnum].signvalue = 1;
                    else
                        sensor[snrnum].signvalue = 0;

                    sensor[snrnum].value  = addr[19];
                    if(addr[13] >= PLDM_uint16)
                        sensor[snrnum].value += (addr[20] << 8);
                    if(addr[13] >= PLDM_uint32)
                    {
                        sensor[snrnum].value += (addr[21] << 16);
                        sensor[snrnum].value += (addr[22] << 24);
                    }

                    //for SMSC 5504, need to do conversion
                    if(dpconf->pldmsnr == 0x99)
                    {
                        if (sensor[snrnum].type == SNR_TACHOMETER)
                            sensor[snrnum].value = 5400000/sensor[snrnum].value;
                        else
                            sensor[snrnum].value = addr[20] - 64;
                    }
                    sensor[snrnum].fault = 5;
                    sensor[snrnum].state = addr[16];
                    free(addr);
                    snrnum++;
                    errcount = 0;
                }
                else
                    errcount++;
#else
                if(smbiosrmcpdataptr->PollType == POLL_NONE)
                {
                    if(!Fan_Poll(snrnum, 1))
                    {
                        smbiosrmcpdataptr->PollType = POLL_SENSOR;
                        smbiosrmcpdataptr->PollTimeOut = 0;
                    }
                    else
                    {
                        sleep(1);
                        continue;
                    }

                    err = msgrcv(pldm_msgid, &msgs, sizeof(struct msgstru_smb), 0, 0);
                    smbiosrmcpdataptr->PollType = POLL_NONE;

                    if (err == OS_ERR_NONE)
                    {
                        addr = msgs.msgtext;
                        if (sensor[snrnum].type == SNR_TACHOMETER)
                        {
                            sensor[snrnum].signvalue = 0;
                            //from RPS to RPM
                            sensor[snrnum].value = *addr*60;
                        }
                        else
                        {
                            sensor[snrnum].signvalue = 1;
                            sensor[snrnum].value = *addr - 128;
                            //may has some problem for SYS3 sensor
                        }
                        free(addr);
                        snrnum++;
                        errcount = 0;
                    }
                    else
                        errcount++;
                }
                else
                    sleep(1);
#endif
                if (errcount == 4)
                {
                    sensor[snrnum].value = 0;
                    sensor[snrnum].fault = 0;
                    sensor[snrnum].state = 0;
                    snrnum++;
                    errcount = 0;
                }
                sleep(10);
            }
            else {
                while ((snrnum != (dpconf->numofsnr-asfconfig->numofsnr))&&(snrnum < MAX_SENSOR_NUMS)) {
                    sensor[snrnum].state = 0;
                    snrnum++;
                }
                break;
            }
        }
    }
}
//void user_mmap_kernel_struct(void);
void generate_uuid(void);
void msg_create(void);
void bsp_config_init(int reset)
{
#if 0
    int smbiosrmcp_shmid, pldmresponse_shmid[7], sensor_shmid, smbiosptr_shmid;
    key_t smbiosrmcp_key, pldmresponse_key[7], sensor_key, smbiosptr_key;
    int dpconf_shmid, asfconf_shmid, pldmdataptr_shmid, timestampdataptr_shmid;
    key_t dpconf_key, asfconf_key, pldmdataptr_key, timestampdataptr_key;
#else
    int sensor_shmid;
    key_t sensor_key;
#endif
    //int i;

    msg_create();
    if (!flash_buf)
        flash_buf = mmap(NULL, 0x10000, PROT_READ|PROT_WRITE, MAP_SHARED, mtd_fd, 0);
    eventdataptr = malloc(2048);

    if (!reset)
    {
        if (!sensor)
        {
            open("/tmp/smbus/sensor", O_RDWR | O_CREAT, S_IRUSR | S_IRGRP | S_IROTH);

            sensor_key = ftok("/tmp/smbus/sensor", 'a');
            if(sensor_key == -1)
                debug("ftok.\n");
            sensor_shmid = shmget(sensor_key, sizeof(sensor_t)*MAX_SENSOR_NUMS, 0666|IPC_CREAT);
            if(sensor_shmid < 0)
            {
                debug("shmget.\n");
                exit(-1);
            }
            sensor = shmat(sensor_shmid, NULL, 0);
        }
    }

    user_mmap_kernel_struct();
    _smbus_init();
    generate_uuid();
}





void generate_uuid(void)
{
    static int clock_sequence = 1;
    unsigned long long longTimeVal;
    int timeLow = 0;
    int timeMid = 0;
    int timeHigh = 0;
    int i;
    struct timeval tv;

    get_mac_addr();

    // get time data
    gettimeofday( &tv, NULL );
    longTimeVal = (unsigned long long)1000000 * (unsigned long long)tv.tv_sec + (unsigned long long)tv.tv_usec;
    timeLow = longTimeVal & 0xffffffff;     // lower 32 bits
    timeMid = (longTimeVal >> 32) & 0xffff; // middle 16 bits
    timeHigh = (longTimeVal >> 32) & 0xfff; // upper 12 bits
    //printf("timeLow is 0x%x, timeMid is 0x%x, timeHigh is 0x%x.\n", timeLow, timeMid, timeHigh);

    // update clock sequence number
    clock_sequence++;

    for ( i = 0; i < 6; i++ )
        dpconf->UUID[i] = dpconf->srcMacAddr[i];                       // mac address

    dpconf->UUID[6] = clock_sequence & 0xff;                // clock seq. low
    dpconf->UUID[7] = 0x80 | ((clock_sequence >> 8) & 0x3f);// clock seq. high and variant
    dpconf->UUID[8] = timeHigh & 0xff;                      // time high/version low bits
    dpconf->UUID[9] = timeHigh >> 8 | 0x10;                 // time high/version high bits
    *(short int*)(dpconf->UUID+10) = (short int)timeMid;
    *(int*)(dpconf->UUID+12) = timeLow;
}

void msg_create(void)
{
    struct stat st = {0};

    if (stat("/tmp/smbus", &st) == -1)
        mkdir("/tmp/smbus", 0700);

    open("/tmp/smbus/smbus", O_RDWR | O_CREAT, S_IRUSR | S_IRGRP | S_IROTH);
    open("/tmp/smbus/pldm", O_RDWR | O_CREAT, S_IRUSR | S_IRGRP | S_IROTH);
    open("/tmp/smbus/event", O_RDWR | O_CREAT, S_IRUSR | S_IRGRP | S_IROTH);
    open("/tmp/smbus/flash", O_RDWR | O_CREAT, S_IRUSR | S_IRGRP | S_IROTH);
    smb_key=ftok("/tmp/smbus/smbus",'a');
    pldm_key=ftok("/tmp/smbus/pldm",'a');
    evt_key=ftok("/tmp/smbus/event",'a');
    flash_key=ftok("/tmp/smbus/flash", 'a');
    smb_msgid = msgget(smb_key, IPC_EXCL);
    if (smb_msgid < 0)
        smb_msgid = msgget(smb_key, IPC_CREAT|0666);
    pldm_msgid = msgget(pldm_key, IPC_EXCL);
    if (pldm_msgid < 0)
        pldm_msgid = msgget(pldm_key, IPC_CREAT|0666);
    evt_msgid =  msgget(evt_key, IPC_EXCL);
    if (evt_msgid < 0)
        evt_msgid = msgget(evt_key, IPC_CREAT|0666);
    flash_msgid = msgget(flash_key, IPC_EXCL);
    if (flash_msgid)
        flash_msgid = msgget(flash_key, IPC_CREAT|0666);
}

void get_mac_addr(void)
{
    struct ifreq ifr;
    struct ifconf ifc;
    char buf[1024];
    int success = 0;

    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sock == -1) {
        /* handle error*/
    };

    ifc.ifc_len = sizeof(buf);
    ifc.ifc_buf = buf;
    if (ioctl(sock, SIOCGIFCONF, &ifc) == -1) {
        /* handle error */
    }

    struct ifreq* it = ifc.ifc_req;
    const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));

    for (; it != end; ++it)
    {
        strcpy(ifr.ifr_name, it->ifr_name);
        if (ioctl(sock, SIOCGIFFLAGS, &ifr) == 0) {
            if (! (ifr.ifr_flags & IFF_LOOPBACK)) { // don't count loopback
                if (ioctl(sock, SIOCGIFHWADDR, &ifr) == 0) {
                    success = 1;
                    break;
                }
            }
        }
        else {
            /* handle error */
        }
    }

    if (success)
        memcpy(dpconf->srcMacAddr, ifr.ifr_hwaddr.sa_data, 6);
}

sig_atomic_t child_exit_status;

void clean_up_child_process(int signal_num)
{
    /* clean up child process */
    int status;
    wait (&status);

    /* store its exit status in a global variable */
    child_exit_status = status;
}

int main(int argc, char **argv)
{
    debug("smbus main process.\n");
    pid_t smbus_pid;
    //pid_t smbus_pid, pldm_pid;
    //struct sched_param param;
    //int snmp_pid;
    //int asf_pid;
    struct sigaction sigchild_action;

    if (!sock_fd)
        netlink_create();

    if (!smbus_fd)
    {
        smbus_fd = open("/dev/smbusdev", O_RDWR);
        //printf("smbus_fd is %d.\n", smbus_fd);
        if (smbus_fd <= 0)
        {
            close(smbus_fd);
            return -1;
        }
    }
    if (!mtd_fd)
    {
        mtd_fd = open("/dev/mtdblock2", O_RDWR);
        if (mtd_fd <= 0)
        {
            close(mtd_fd);
            return -1;
        }
    }
    bsp_config_init(0);

    // /generate_uuid(dpconf->UUID);
    debug("UUID is 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x.\n",
          dpconf->UUID[0], dpconf->UUID[1], dpconf->UUID[2], dpconf->UUID[3], dpconf->UUID[4], dpconf->UUID[5], dpconf->UUID[6], dpconf->UUID[7],
          dpconf->UUID[8], dpconf->UUID[9], dpconf->UUID[10], dpconf->UUID[11], dpconf->UUID[12], dpconf->UUID[13], dpconf->UUID[14], dpconf->UUID[15]);
    //ioctl(smbus_fd, 12, NULL);

    snmp_init(SNMP_UDP_PORT);

    memset(&sigchild_action, 0, sizeof(sigchild_action));
    sigchild_action.sa_handler = &clean_up_child_process;
    sigaction(SIGCHLD, &sigchild_action, NULL);

    smbus_pid = fork();
    if (smbus_pid < 0)
    {
        printf("create smbus child pid fail.\n");
    }
    else if (smbus_pid == 0)
    {
        //child process
        strcpy(argv[0], "snmp");
        evtq_loop();
    }
    else
    {
        //strcpy(argv[0], "smbus");
        unsigned char v= 0xC8;
        netlink_send(&v, 1);
        smbus_loop();
    }

    close(smbus_fd);
    free(eventdataptr);
    munmap(flash_buf, 0x10000);
    shmdt(sensor);
    close(sock_fd);
    snmp_close();

    return 0;
}
