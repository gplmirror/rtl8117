#ifndef __VNC_H__
#define __VNC_H__

#define SetPixelFormat          0
#define FixColourMapEntries     1
#define SetEncodings            2
#define FramebufferUpdateRequest 3
#define KeyEvent                4
#define PointerEvent            5
#define ClientCutText           6


typedef struct __attribute__((packed)) _VNCPixelFormat{
    unsigned char bpp;          // bit per pixel
    unsigned char depth;
    unsigned char be_flag;      // big-endian flag
    unsigned char true_colour_flag;
    unsigned short red_max;
    unsigned short green_max;
    unsigned short blue_max;
    unsigned char red_shift;
    unsigned char green_shift;
    unsigned char blue_shift;
    unsigned char padding[3];
}VNCPixelFormat;

typedef struct __attribute__((packed)) _VNCSetPixelFormat{
    unsigned char msg_type;
    unsigned char padding[3];
    VNCPixelFormat pixel_format;
}VNCSetPixelFormat;

typedef struct __attribute__((packed)) _VNCFixColourMapEntries{
    unsigned char msg_type;
    unsigned char padding;
    unsigned short first_colour;
    unsigned short number_colour;
}VNCFixColourMapEntries;

typedef struct __attribute__((packed)) _VNCColour{
    unsigned short r;
    unsigned short g;
    unsigned short b;
}VNCColour;

typedef struct __attribute__((packed)) _VNCSetEncodings{
    unsigned char msg_type;
    unsigned char padding;
    unsigned short number_encoding;
}VNCSetEncodings;

typedef unsigned int VNCEncoding;

typedef struct __attribute__((packed)) _VNCFramebufferUpdateRequest{
    unsigned char msg_type;
    unsigned char incremental;
    unsigned short x;
    unsigned short y;
    unsigned short width;
    unsigned short height;
}VNCFramebufferUpdateRequest;

typedef struct __attribute__((packed)) _VNCKeyEvent{
    unsigned char msg_type;
    unsigned char down_flag;
    unsigned char padding[2];
    unsigned int key;
}VNCKeyEvent;

typedef struct __attribute__((packed)) _VNCPointerEvent{
    unsigned char msg_type;
    unsigned char button_mask;
    unsigned short x;
    unsigned short y;
}VNCPointerEvent;

typedef struct __attribute__((packed)) _VNCClientCutText{
    unsigned char msg_type;
    unsigned char padding[3];
    unsigned int len;
}VNCClientCutText;
#endif
