#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <limits.h>
#include <linux/netlink.h>
#include <sys/stat.h>
#include <time.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <netinet/in.h>
#include <sys/msg.h>
#include <sys/mman.h>
#include <openssl/des.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include "bsp.h"
#include "lib.h"
#include "smbios.h"
#include "base.h"
#include "vnc.h"

#define VNC_16BIT

#define VNC_AUT_NONE 1
#define VNC_AUT_VNC 2
#define VNC_AUT_ALG VNC_AUT_NONE
#define VNC_CHALLENGE_LEN 16
#define VNC_KEY_RECONNECT 1

#define VNCPROTOCOLVER "RFB 003.00R\n"
#define FB1_DDR_base 0x08000000
#define ETH_PAYLOAD_LEN 1300

#define VGA_BLK_SIZE            0x40
#define VGA_BLK_SIZE_SHIFT      6
#define VGA_BLK_BV_SIZE         0x20
#define VGA_BLK_BV_SIZE_SHIFT   5



extern DPCONF *dpconf;
void * map_buf = NULL;
unsigned char *vir_addr = NULL;
int vnc_fd=0;
struct VgaInfo *VGAInfo;
struct VncInfo *VNCInfo;
int sock;
unsigned int count = 0;

enum _VNCclientStates
{
    PtlVerState = 0x00,
    SecurityState = 0x01,
    ClientInitState = 0x02,
    ClientAuthState = 0x03,
    ServerInitState = 0x10,
    ClientToSrvMsgState = 0x11
} VNCclientStates;

enum _VNKeysym
{
    VNC_KEY_BACKSPACE = 0xFF08,
    VNC_KEY_TAB = 0xFF09,
    VNC_KEY_ENTER = 0xFF0D,
    VNC_KEY_ESCAPE = 0xFF1B,
    VNC_KEY_INSERT = 0xFF63,
    VNC_KEY_KEY_DELETE = 0xFFFF,
    VNC_KEY_HOME = 0xFF50,
    VNC_KEY_LEFT_ARROW = 0xFF51,
    VNC_KEY_UP_ARROW = 0xFF52,
    VNC_KEY_RIGHT_ARROW = 0xFF53,
    VNC_KEY_DOWN_ARROW = 0xFF54,
    VNC_KEY_PAGE_UP = 0xFF55,
    VNC_KEY_PAGE_DOWN = 0xFF56,
    VNC_KEY_END = 0xFF57
} VNKeysym;

typedef struct _PIXEL_FORMAT
{
    unsigned char bit_per_pixel;
    unsigned char depth;
    unsigned char big_endian_flag;
    unsigned char true_color_flag;
    unsigned short red_max;
    unsigned short green_max;
    unsigned short blue_max;
    unsigned char red_shift;
    unsigned char green_shift;
    unsigned char blue_shift;
    unsigned char padding[3];
} PIXEL_FORMAT;

struct _framebufferPara
{
    unsigned short width;
    unsigned short height;
    PIXEL_FORMAT pf;
    unsigned int name_len;
} FramebufferPara;

struct _framebufferUpdate
{
    unsigned char msg_type;
    unsigned char padding;
    unsigned short numRect;
    unsigned short x;
    unsigned short y;
    unsigned short width;
    unsigned short height;
    unsigned int encoding_type;
} framebufferUpdate;

struct _VNCHIDLastState
{
    short diffx;
    short diffy;
    unsigned short x;
    unsigned short y;
} VNCHIDLastState;

struct _HIDKeyReport
{
    char mod;
    char rsv;
    char key0;
    char key1;
    char key2;
    char key3;
    char key4;
    char key5;
    char key6;
} HIDKeyReport;


typedef struct _VNC_client {
    int socket;
    pthread_t rx;               /* receive requests */
    pthread_t tx;
    pthread_mutex_t tx_mutex;   /* mutex for inter-thread communication */
    pthread_cond_t tx_cond;     /* cond for inter-thread communication */
    unsigned char tx_pending;   /* has newly fb update request */
    unsigned char bgn;          /* number of block groups */
    unsigned short hblk;        /* number of horizontal blocks */
    unsigned short vblk;        /* number of vertical blocks */
    unsigned short vga_blk;     /* total blocks */
} VNC_client_t;


static VNC_client_t vnc_client; /* TODO:linked-list for multiple clients */
static int ssocket;             /* server socket */


inline
void reset_vnc_client(VNC_client_t *client)
{
    client->tx_pending = false;
    /* Unlock or Re-init mutex */
#if 1
    pthread_mutex_unlock(&client->tx_mutex);
#else
    pthread_mutex_destroy(&client->tx_mutex);
    pthread_cond_destroy(&client->tx_cond);
    pthread_mutex_init(&client->tx_mutex, NULL);
    pthread_cond_init(&client->tx_cond, NULL);
#endif
}

void flip_VNC_PWD(char *in, char *out, int len)
{
    int l = 0;
    int i = 0;

    memset(out, 0, len);
    for(l = 0; l <len; l++)
    {
        //one char have 8 bit
        for(i = 0; i < 8; i++)
        {
            if(in[l]& (1<<i))
                out[l] += (1<<(7-i));
        }
    }
}


static void sig_urg(int signum)
{
    unsigned char rx_buf[4096];
    int rx_len;
    if ((rx_len=recv(sock, &rx_buf, 4096, 0))<0)
    {
        printf("recv err.\n");
        return;
    }
    if (rx_len >= 1)
    {
        alarm(0);
        count = 0;
    }
}
static void sig_alarm(int signum)
{
    if (++count>10)
        exit(0);
    write(sock, "1", 1);
    alarm(1);
}


void vga_send_black(int fd, char* addr, int x ,int y)
{
    int rfblen = 64*64*4;
    int t;

    memset(&framebufferUpdate, 0, sizeof(framebufferUpdate));
    framebufferUpdate.msg_type = 0;
    framebufferUpdate.numRect = htons(1);
    framebufferUpdate.x = htons(x);
    framebufferUpdate.y = htons(y);
    framebufferUpdate.height = htons(64);
    framebufferUpdate.width = htons(64);
    framebufferUpdate.encoding_type = 0;
    write(fd, (char *)&framebufferUpdate, sizeof(framebufferUpdate));

    while(rfblen > 0)
    {
        if(rfblen >= ETH_PAYLOAD_LEN)
        {
            t = ETH_PAYLOAD_LEN;
            memset(addr, 0, ETH_PAYLOAD_LEN );
        }
        else
        {
            t = rfblen;
            memset(addr, 0, t );
        }
        write(fd, addr, ETH_PAYLOAD_LEN);
        rfblen = rfblen - ETH_PAYLOAD_LEN;
    }
}

void RGB32to16(void *dest, void *src, int pix)
{
    int i = 0;
    unsigned short *b16 = (unsigned short *)dest;
    unsigned int *b32 = (unsigned int*)src;

    for(; i < pix ; i++, b16++, b32++)
        /* Convert RGB32 to RGB16 for little-endian machine */
        *b16 = ((*b32&0x00f80000)>>8) | ((*b32&0x0000fc00)>>5) | ((*b32&0x000000f8)>>3);
}

void vga_send(int fd, char* addr, int x, int y)
{
    static char txbuf[VGA_BLK_SIZE*VGA_BLK_SIZE*2] = {0};
    char *txloc;
    unsigned int txlen;
    int width = VGA_BLK_SIZE;
    int height = VGA_BLK_SIZE;
    /* If using FB1_BPP as pixel width, the initial black screen will not be pure. */
#if 1
    unsigned char pixw = VGAInfo->FB1_BPP;
#else
    unsigned char pixw = 4;
#endif
    unsigned int rfblen = pixw;
    unsigned short src_col_offset = pixw * VGAInfo->FB1_Hresol;
    unsigned short tx_col_offset;
    unsigned char yloc;


    if((x+VGA_BLK_SIZE) > VGAInfo->FB1_Hresol) {
        width = VGAInfo->FB1_Hresol - x;
        rfblen *= width;
    } else {
        rfblen = rfblen << VGA_BLK_SIZE_SHIFT;
    }

    if((y+VGA_BLK_SIZE) > VGAInfo->FB1_Vresol) {
        height = VGAInfo->FB1_Vresol - y;
        rfblen *= height;
    } else {
        rfblen = rfblen << VGA_BLK_SIZE_SHIFT;
    }


    framebufferUpdate.msg_type = 0;
    framebufferUpdate.padding = 0;
    framebufferUpdate.numRect = htons(1);
    framebufferUpdate.x = htons(x);
    framebufferUpdate.y = htons(y);
    framebufferUpdate.height = htons(height);
    framebufferUpdate.width = htons(width);
    framebufferUpdate.encoding_type = 0;
    write(fd, (char *)&framebufferUpdate, sizeof(framebufferUpdate));


#ifdef VNC_16BIT
    if(pixw == 4) {
        txlen = rfblen >> 1;
        tx_col_offset = 2 * width;
        txloc = txbuf;
        for(yloc=0; yloc<height; yloc++) {
            RGB32to16(txloc, addr, width);

            txloc += tx_col_offset;
            addr += src_col_offset;
        }

        write(fd, txbuf, txlen);
    } else
#endif
    {
        txlen = rfblen;
        tx_col_offset = pixw * width;
        txloc = addr;
        write(fd, txloc, txlen);
    }
}


void get_blk_resolution(VNC_client_t *client)
{
#define CEILING(x, u)   (((x) + ((u)-1)) / (u))

    client->hblk = CEILING(VGAInfo->FB1_Hresol, VGA_BLK_SIZE);
    client->vblk = CEILING(VGAInfo->FB1_Vresol, VGA_BLK_SIZE);
    client->vga_blk = client->hblk * client->vblk;
    client->bgn = CEILING(client->vga_blk, VGA_BLK_BV_SIZE);
}

void* update_frame_buffer(void *arg)
{
    unsigned int FB1_BGN_reg, FB1_BFN_reg[17];
    unsigned char dummy;
    unsigned int diff_block_start_addr, diff_block_unit, diff_block_x, diff_block_y;
    unsigned short blk_group, blk, bfn;
    VNC_client_t *client = (VNC_client_t *)arg;

#define bit_test(bv, bit)       ((bv) & (0x01 << (bit)))

    int s = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    if (s != 0)
        debug("pthread_setcancelstate return %d\n", s);

    diff_block_unit = VGA_BLK_SIZE*VGAInfo->FB1_BPP;
    get_blk_resolution(client);

    while(1) {
        /* wait for requests of updating frame */
        pthread_mutex_lock(&client->tx_mutex);
        while(!client->tx_pending)
            pthread_cond_wait(&client->tx_cond, &client->tx_mutex);
        client->tx_pending = false;
        pthread_mutex_unlock(&client->tx_mutex);


        ioctl(vnc_fd, GET_CHG_RES, &dummy);
        if(dummy == 0x66)
        {
            ioctl(vnc_fd, RE_INIT, NULL);
            diff_block_unit = VGA_BLK_SIZE*VGAInfo->FB1_BPP;
            get_blk_resolution(client);
        }

        ioctl(vnc_fd, GET_BGN_REG, &FB1_BGN_reg);
        ioctl(vnc_fd, GET_BFN_REG, FB1_BFN_reg);

        for(blk_group=0; blk_group<client->bgn; blk_group++) {
            if(bit_test(FB1_BGN_reg, blk_group)) {
                bfn = blk_group << VGA_BLK_BV_SIZE_SHIFT;
                diff_block_x = (bfn%client->hblk) << VGA_BLK_SIZE_SHIFT;
                diff_block_y = (bfn/client->hblk) << VGA_BLK_SIZE_SHIFT;
                diff_block_start_addr = vir_addr +
                                        diff_block_y*VGAInfo->FB1_Hresol*VGAInfo->FB1_BPP +
                                        diff_block_x*VGAInfo->FB1_BPP;

                for(blk=0; blk<VGA_BLK_BV_SIZE; blk++) {
                    if(bit_test(FB1_BFN_reg[blk_group], blk)) {
                        debug("BGN: 0x%08x, FBN: 0x%08x, fn:%x, x:%u, y:%u\n",
                              FB1_BGN_reg, FB1_BFN_reg[blk_group], bfn+blk,
                              diff_block_x, diff_block_y);

                        vga_send(client->socket, (char*)diff_block_start_addr, diff_block_x, diff_block_y);
                    }

                    diff_block_x += VGA_BLK_SIZE;
                    if(diff_block_x >= VGAInfo->FB1_Hresol) {
                        diff_block_x = 0;
                        diff_block_y += VGA_BLK_SIZE;
                        if(diff_block_y >= VGAInfo->FB1_Vresol)
                            diff_block_y = 0;
                        diff_block_start_addr = vir_addr + diff_block_y*VGAInfo->FB1_Hresol*VGAInfo->FB1_BPP;
                    } else {
                        diff_block_start_addr += diff_block_unit;
                    }
                }
            }
        }
    }

    return NULL;
}

enum {
    VNC_UPDATE_FB
};

void enqueue_tx_job(VNC_client_t *client, int op)
{
    /*
       Uncomment followings for following purposes:
         a) Reduce frame rate for development
         b) Extend other operation
     */
//    static struct timeval ts = {0}, curr;
//    gettimeofday(&curr, NULL);

//    switch(op){
//        case VNC_UPDATE_FB:
//            if((curr.tv_usec - ts.tv_usec) & 0x0080000UL){
//                ts.tv_usec = curr.tv_usec;
//            if((curr.tv_sec - ts.tv_sec) >= 1){
//                ts.tv_sec = curr.tv_sec;
    pthread_mutex_lock(&client->tx_mutex);
    if(!client->tx_pending) {
        client->tx_pending = true;
        pthread_cond_broadcast(&client->tx_cond);
    }
    pthread_mutex_unlock(&client->tx_mutex);
//            }
//            break;
//    }
}

void *connection_handler(void *arg)
{
    ssize_t rx_len;
    unsigned char vnc_client_state;
    unsigned int  i = 0, cmd_shift;
    unsigned char buf[16] = {0};
    unsigned char tx_buf[1024];
    char VNCChallengeMsg[VNC_CHALLENGE_LEN];
    char pwd[16] = {0};
    char key[8] = {0};
    unsigned char rx_buf[1024] = {0}, *cmd;
    VNC_client_t *client = (VNC_client_t*)arg;
    sock = client->socket;
    int val = 1;
//    struct sched_param param;

//    param.sched_priority = sched_get_priority_max(SCHED_FIFO);
//    sched_setscheduler(getpid(), SCHED_FIFO, &param);
    setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *)(&val), sizeof(val));
    signal(SIGURG, sig_urg);
    signal(SIGALRM, sig_alarm);
    //strcpy(pwd, dpconf->admin.passwd);
    strcpy(pwd, "Realtek");
    memset(&VNCHIDLastState, 0, sizeof(VNCHIDLastState));
    vnc_client_state = PtlVerState;
    memcpy(tx_buf, VNCPROTOCOLVER, strlen(VNCPROTOCOLVER));
    write(sock, tx_buf, strlen(VNCPROTOCOLVER));

    struct timeval tv = {.tv_sec = 0, .tv_usec = 10};
    setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char *)&tv, sizeof(tv));


    do {
        rx_len = recv(sock, rx_buf, 1024, 0);
        if (rx_len<=0)
            continue;

        if (vnc_client_state == ClientAuthState)
        {
            memset(tx_buf, 0 ,4);
            flip_VNC_PWD(pwd, key, sizeof(pwd));
            DES_key_schedule keysched;
            DES_set_key((C_Block *)key, &keysched);
            DES_ecb_encrypt((C_Block *)tx_buf,(C_Block *)buf, &keysched, DES_ENCRYPT);
            DES_ecb_encrypt((C_Block *)(tx_buf+8),(C_Block *)(buf+8), &keysched, DES_ENCRYPT);

            if (memcmp(VNCChallengeMsg, buf, VNC_CHALLENGE_LEN))
                tx_buf[3] = 1;
            memset(VNCChallengeMsg, 0, VNC_CHALLENGE_LEN);
            vnc_client_state = ClientInitState;
            write(sock, tx_buf, 4);
        }
        else if (vnc_client_state == ClientInitState)
        {
            vnc_client_state = ServerInitState;
            memset(tx_buf, 0, sizeof(FramebufferPara) + 7);
            FramebufferPara.height = htons(768);//tmp fixed
            FramebufferPara.width = htons(1024);//tmp fixed

#ifndef VNC_16BIT
            FramebufferPara.pf.bit_per_pixel = VGAInfo->FB1_BPP*8;//RGBA, 8 bit
#else
            FramebufferPara.pf.bit_per_pixel = 2*8;
#endif

#ifndef VNC_16BIT
            if(VGAInfo->FB1_BPP == 4)
            {
                FramebufferPara.pf.depth = 24;
                FramebufferPara.pf.red_max = htons(255);
                FramebufferPara.pf.green_max = htons(255);
                FramebufferPara.pf.blue_max = htons(255);
                FramebufferPara.pf.red_shift= 16;
                FramebufferPara.pf.green_shift = 8;
                FramebufferPara.pf.blue_shift = 0;
            }

            if(VGAInfo->FB1_BPP == 2)
#endif
            {
                //RGB565
                FramebufferPara.pf.depth = 16;
                FramebufferPara.pf.red_max = htons(31);
                FramebufferPara.pf.green_max = htons(63);
                FramebufferPara.pf.blue_max = htons(31);
                FramebufferPara.pf.red_shift= 11;
                FramebufferPara.pf.green_shift = 5;
                FramebufferPara.pf.blue_shift = 0;
            }

            FramebufferPara.name_len = htonl(7);
            memcpy(tx_buf + sizeof(FramebufferPara), "RtkDash", 7);
            //FramebufferPara.pf.big_endian_flag = 0;
            FramebufferPara.pf.true_color_flag = 1;
            memcpy(tx_buf, &FramebufferPara, sizeof(FramebufferPara));
            write(sock, tx_buf, sizeof(FramebufferPara) + 7);

            vnc_client_state = ClientToSrvMsgState;
            break;
        }

        if (vnc_client_state == SecurityState)
        {
            if(VNC_AUT_ALG == VNC_AUT_NONE)
            {
                memset(tx_buf, 0, 4);
                if (rx_buf[0] != VNC_AUT_NONE)
                    tx_buf[0] = VNC_AUT_NONE;

                vnc_client_state = ClientInitState;
                write(sock, tx_buf, 4);
            }
            else
            {
                if (rx_buf[0] != VNC_AUT_VNC)
                    close(sock);
                else
                {
                    for(i = 0; i < VNC_CHALLENGE_LEN; i++)
                    {
                        VNCChallengeMsg[i] = rand() % 256;
                    }
                    memcpy(tx_buf, VNCChallengeMsg, VNC_CHALLENGE_LEN);
                    vnc_client_state = ClientAuthState;
                    write(sock, tx_buf, VNC_CHALLENGE_LEN);
                }
            }
        }
        if (vnc_client_state == PtlVerState)
        {
            if(memcmp(rx_buf, VNCPROTOCOLVER, strlen(VNCPROTOCOLVER)) == 0)
            {
                //memset(tx_buf, 0, 2);
                vnc_client_state = SecurityState;
                tx_buf[0] = 1;
                tx_buf[1] = VNC_AUT_ALG;
                write(sock, tx_buf, 2);
            }
        }
    } while(!VNCInfo->NewVNCCli);


    while(!VNCInfo->NewVNCCli)
    {
        rx_len = recv(sock, rx_buf, 1024, 0);
        cmd = rx_buf;
        if(rx_len > 0)
        {
            while(rx_len > 0) {
                debug("cmd(%d), len:%d\n", cmd[0], rx_len);

                switch(cmd[0]) {

#define _next_cmd(L)      \
                    do{\
                        cmd += (L);\
                        rx_len -= (L);\
                    }while(0)

                case SetPixelFormat:
                    debug("SetPixelFormat:%d/%d\n", sizeof(VNCSetPixelFormat), rx_len);
                    _next_cmd(sizeof(VNCSetPixelFormat));
                    break;
                case FixColourMapEntries:
                    cmd_shift = ntohs(((VNCFixColourMapEntries*)cmd)->number_colour) * sizeof(VNCColour);
                    debug("FixColourMapEntries:%d/%d, shift:%u\n",
                          sizeof(VNCFixColourMapEntries), rx_len, cmd_shift);
                    _next_cmd(cmd_shift);
                    _next_cmd(sizeof(VNCFixColourMapEntries));
                    break;
                case SetEncodings:
                    cmd_shift = ntohs(((VNCSetEncodings*)cmd)->number_encoding) * sizeof(VNCEncoding);
                    debug("SetEncodings:%d/%d, shitf:%u\n", sizeof(VNCSetEncodings),
                          rx_len, cmd_shift);
                    _next_cmd(cmd_shift);
                    _next_cmd(sizeof(VNCSetEncodings));
                    break;
                case FramebufferUpdateRequest:
                    debug("FramebufferUpdateRequest:%d/%d\n", sizeof(VNCFramebufferUpdateRequest), rx_len);
                    enqueue_tx_job(client, VNC_UPDATE_FB);
                    _next_cmd(sizeof(VNCFramebufferUpdateRequest));
                    break;
                case KeyEvent:
                    debug("KeyEvent:%d/%d, %02x %02x %02x %02x %02x %02x %02x %02x\n",
                          sizeof(VNCKeyEvent), rx_len, cmd[0], cmd[1], cmd[2],
                          cmd[3], cmd[4], cmd[5], cmd[6], cmd[7]);
                    ioctl(vnc_fd, KEYBOARD, cmd);
                    _next_cmd(sizeof(VNCKeyEvent));
                    break;
                case PointerEvent:
                    debug("PointerEvent:%d:%d\n", sizeof(VNCPointerEvent), rx_len);
                    ioctl(vnc_fd, MOUSE, cmd);
                    _next_cmd(sizeof(VNCPointerEvent));
                    break;
                case ClientCutText:
                    cmd_shift = ntohl(((VNCClientCutText*)cmd)->len);
                    debug("ClientCutText:%d/%d, cont len: %u\n",
                          sizeof(ClientCutText), rx_len, cmd_shift);
                    _next_cmd(cmd_shift);
                    _next_cmd(sizeof(VNCClientCutText));
                    break;
                default:
                    debug("unknown cmd(0x%02x), len:%d\n", cmd[0], rx_len);
                    rx_len = 0;
                    break;
#undef _next_cmd
                }
            }
        } else {
            enqueue_tx_job(client, VNC_UPDATE_FB);
        }
    }

    debug("cancel tx\n");
    int s = pthread_cancel(client->tx);
    debug("tx canceled(%d)\n", s);

    return NULL;
}

inline
void wait_client_finish(VNC_client_t *client)
{
    int s;

    pthread_join(client->tx, &s);
    if(s != PTHREAD_CANCELED)
        debug("failed to canceled tx\n");
    debug("tx terminated\n");

    pthread_join(client->rx, NULL);
    debug("rx terminated\n");

    close(client->socket);
    ioctl(vnc_fd, VGA_DISBALE, NULL);

    reset_vnc_client(client);
}

int c_server(void)
{
    int c, ret_ptx, ret_prx, addr_reuse;
    VNC_client_t vnc_client;
    struct sockaddr_in server, client;
    int mem_fd;
    unsigned int fb_phy_addr;

    mem_fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (mem_fd <= 0)
    {
        printf("open devmem failed.\n");
        close(mem_fd);
        return 1;
    }

    pthread_mutex_init(&vnc_client.tx_mutex, NULL);
    pthread_cond_init(&vnc_client.tx_cond, NULL);


    //Create socket
    ssocket = socket(AF_INET , SOCK_STREAM , 0);
    if (ssocket == -1)
    {
        printf("Could not create socket\n");
    }
    debug("Socket created\n");

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 5900 );

    addr_reuse = 1;
    setsockopt(ssocket, SOL_SOCKET, SO_REUSEADDR, &addr_reuse, sizeof(addr_reuse));

    //Bind
    if( bind(ssocket,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        printf("vnc bind failed. Error:%d\n", errno);
        return 1;
    }
    debug("bind done\n");

    //Listen
    listen(ssocket , 3);

    //Accept and incoming connection
    debug("Waiting for incoming connections...\n");

    //Accept and incoming connection
    c = sizeof(struct sockaddr_in);
    while( (vnc_client.socket = accept(ssocket, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        debug("vnc Connection accepted\n");

        if(!VNCInfo->Enable)
        {
            close(vnc_client.socket);
            continue;
        }

        ioctl(vnc_fd, VGA_ENABLE, &fb_phy_addr);
        vir_addr = mmap(NULL, 0x400000, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, fb_phy_addr);

        ret_ptx = pthread_create(&vnc_client.tx, NULL, update_frame_buffer,
                                 (void*)&vnc_client);
        ret_prx = pthread_create(&vnc_client.rx, NULL, connection_handler,
                                 (void*)&vnc_client);

        if(ret_ptx || ret_prx)
        {
            perror("could not create thread\n");

            if(!ret_ptx) {
                pthread_cancel(vnc_client.tx);
                pthread_join(vnc_client.tx, NULL);
            }

            if(!ret_prx) {
                pthread_join(vnc_client.rx, NULL);
            }

            ioctl(vnc_fd, VGA_DISBALE, NULL);
            munmap(vir_addr, 0x400000);
            close(mem_fd);
            close(vnc_client.socket);
        } else {
            debug("Handler assigned, wait client finish\n");
            wait_client_finish(&vnc_client);
            munmap(vir_addr, 0x400000);
        }
    }

    return 0;
}

static
void term(int sig)
{
    debug("vncs terminating(%d)\n", sig);

    shutdown(vnc_client.socket, 2);
    close(vnc_client.socket);
    shutdown(ssocket, 2);
    close(ssocket);

    if(VNCInfo->Enable){
        ioctl(vnc_fd, VGA_DISBALE, NULL);
        VNCInfo->Enable = false;
    }

    ioctl(vnc_fd, KEEP_NOT_ALIVE, NULL);
    ioctl(vnc_fd, RELEASE_FB, NULL);
    close(vnc_fd);

    exit((sig == SIGKILL || sig == SIGTERM)?EXIT_SUCCESS:EXIT_FAILURE);
}

int main(void)
{
    /* code */
    struct VncInfo *vncinfo = mmap_kernel_VNC_struct();
    VNCInfo = vncinfo;
    struct VgaInfo *vgainfo = (struct VncInfo *)((unsigned char *)vncinfo + 0x100);
    VGAInfo = vgainfo;

    vnc_fd = open("/dev/vnc", O_RDWR);
    if(vnc_fd<0)
    {
        close(vnc_fd);
        return 1;
    }

    /* insert signal handler */
    signal(SIGKILL, term);
    signal(SIGTERM, term);
    signal(SIGQUIT, term);
    signal(SIGABRT, term);
    signal(SIGINT, term);
    signal(SIGSEGV, term);

    c_server();
    return 0;
}
