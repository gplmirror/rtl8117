#include <sys/socket.h>
#include <linux/netlink.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <netinet/in.h>
#ifdef __linux__
#include <linux/in.h>
#endif
#include "lib.h"
#include "bsp.h"
#include "base.h"


int sockfd;
struct sockaddr_in servaddr;
unsigned char mesg[1000];
socklen_t clientlen;
extern asf_config_t *asfconfig;
const unsigned char SNMPVersion[] = {0x02, 0x01, 0x00};
const unsigned char Language_code[]= { 0x19 };
const unsigned char Sub_identifier[]= {
    0x2b,0x06,0x01,0x04,0x01,
    0x98,0x6f,0x01,0x01,0x01
};
const unsigned char community[]= {
    0x70,0x75,0x62,0x6c,0x69,
    0x63,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00
};
const unsigned char Manufacturer_ID[]= {
    0x00,0x00,0x6a,0x92
};
const unsigned char OEM_Custom[]= { 0xc1};
unsigned short sequence_num = 0;
extern eventdata *event_ptr;
extern volatile DPCONF *dpconf;

int snmp_send(void *ptr);
void snmp_init(int port)
{
    //int bytes_recved;
    //socklen_t len;
    sockfd=socket(AF_INET,SOCK_DGRAM,0);
    debug("Starting SNMP Server on Port %d\n",port);
}

void snmp_close(void)
{
    close(sockfd);
}

void SNMPPktGen(SNMP *SNMPPkt, eventdata *pevent)
{
    unsigned short bseqnum;
    unsigned int timestamp = htonl(time(NULL));

    SNMPPkt->header = SNMP_hdr;
    SNMPPkt->length = sizeof(SNMP) - 2;
    memcpy(SNMPPkt->version, SNMPVersion, 3);
    SNMPPkt->community_header = 0x04;
    SNMPPkt->string_length = 0x06;
    memcpy(SNMPPkt->community, community, 6);
    SNMPPkt->PDU[0] = ((PDU_Type >> 8) & 0xFF);
    SNMPPkt->PDU[1] = (PDU_Type & 0xFF);
    SNMPPkt->EOT = Enterprise_Object_Type;
    SNMPPkt->EOL = Enterprise_Object_Length;

    //heartbeat
    memcpy(SNMPPkt->SubID, Sub_identifier, 9);
    SNMPPkt->agent_addr_type = Agent_Address_Type;
    SNMPPkt->agent_addr_length = Agent_Address_Length;
    SNMPPkt->agent_address = (int)dpconf->HostIP[0].addr;
    SNMPPkt->trap_type_type = Trap_Type_Type;
    SNMPPkt->trap_type_length = Trap_Type_Length;
    SNMPPkt->trap_type = Trap_Type;
    SNMPPkt->strap_type_type = Specific_Trap_Type_Type;
    SNMPPkt->strap_length = Specific_Trap_Type_Length;
    SNMPPkt->stf_reserved = 0x00;
    if (pevent != NULL)
    {
        SNMPPkt->stf_event_sensor = pevent->Event_Sensor_Type;
        SNMPPkt->stf_event = pevent->Event_Type;
        SNMPPkt->stf_event_offset = pevent->Event_Offset;
    }
    else
    {
        SNMPPkt->stf_event_sensor = 0x25; //heartbeat
        SNMPPkt->stf_event = 0x6f;
        SNMPPkt->stf_event_offset = 0x00;
    }
    SNMPPkt->time_stamp_type = Time_Stamp_Type;
    SNMPPkt->time_stamp_length = Time_Stamp_Length;
    memcpy(SNMPPkt->time_stamp, &timestamp, sizeof(timestamp));
    SNMPPkt->variable_binding_type = Variable_Binding_Type;
    SNMPPkt->variable_binding_length = 0x3f;
    SNMPPkt->variable_binding_type2 = Variable_Binding_Type;
    SNMPPkt->variable_binding_length2 = Variable_Binding_Type;
    SNMPPkt->variable_binding_length2 = 0x3d;
    SNMPPkt->object_type = Object_Type ;
    SNMPPkt->object_length = Object_Length;
    memcpy(SNMPPkt->SID2, Sub_identifier, 10);
    SNMPPkt->value_type = Value_Type;
    SNMPPkt->value_length = 0x2f;
    getGUID(SNMPPkt->guid);
    sequence_num++;
    if(pevent)
    {
        if(pevent->seqnum == 0)
            pevent->seqnum = sequence_num;
    }
    bseqnum = htons(sequence_num);
    memcpy(SNMPPkt->sequence_number, &bseqnum, sizeof(sequence_num));
    memset(SNMPPkt->local_timestamp, 0, 4);
    SNMPPkt->UTC_offset[0] = 0xff;
    SNMPPkt->UTC_offset[1] = 0xff;
    if (pevent != NULL)
    {
        memcpy(&SNMPPkt->event_source, &pevent->Event_Source_Type, 12);
        memset(&SNMPPkt->Event_Data[6], 0 ,2);
    }
    else
    {
        SNMPPkt->event_source  = 0x68;
        SNMPPkt->event_severity= 0x01;
        SNMPPkt->sensor_device = dpconf->arpaddr;
        SNMPPkt->sensor_number = 0x01;
        SNMPPkt->Entity        = 0x17;
        SNMPPkt->Entity_Instance= 0x00;
        memset(SNMPPkt->Event_Data, 0, 8);
    }
    SNMPPkt->trap_source=0x50;
    SNMPPkt->Language_code = Language_code[0];
    memcpy(SNMPPkt->Manufacturer_ID, Manufacturer_ID, 4);
    memcpy(SNMPPkt->System_ID, &asfconfig->systemid, 2);
    SNMPPkt->OEM_Custom = OEM_Custom[0];
}
unsigned int get_server_ip(void)
{
    return dpconf->ConsoleIP[0].addr;
}
int snmp_send(void *ptr)
{
    eventdata *pevent;
    pevent = (eventdata *)ptr;
    int len;
    SNMP *outPkt;

    bzero(&servaddr,sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr=dpconf->ConsoleIP[0].addr;////inet_addr("192.168.0.11");  //htonl(INADDR_ANY);
    servaddr.sin_port=htons(162);
    outPkt = malloc(sizeof(SNMP)+4);
    outPkt = (SNMP *)((((unsigned int)outPkt)+3)&(~0x3));
    SNMPPktGen(outPkt, pevent);
    len = sendto(sockfd, outPkt, sizeof(SNMP), 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
    if (len < 0)
        printf("ERROR in sendto.\n");
}

