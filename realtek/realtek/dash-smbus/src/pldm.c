#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <pldm.h>
#include <smbus.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <sys/stat.h>
#include <mtd/mtd-user.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include "bsp.h"
#include "lib.h"
#include "smbios.h"
//#include "usb.h"
#include "base.h"
#include "bsp_cfg_fp_revA.h"
//#include "dash.h"
//#include "rmcp.h"

flash_data_t dirty_ext[10];
const unsigned char asf_legacy_sensor_num = 0;
const asf_ctldata_t asf_ctldata[4] = {
    {0x00, 0x88, 0x00, 0x03}, {0x01, 0x88, 0x00, 0x02},
    {0x02, 0x88, 0x00, 0x01}, {0x03, 0x88, 0x00, 0x04}
};
const asf_alert_t asfalerts[16] = {
    {0x01, 0x01, 0x00, 0x04, 0x00, 0x88, 0x04, 0x04, 0x04, 0x07, 0x6F, 0x04, 0x18, 0x10, 0xFF, 0x03, 0x00},
    {0x01, 0x01, 0x00, 0x04, 0x00, 0x88, 0x04, 0x02, 0x02, 0x01, 0x6F, 0x03, 0x18, 0x10, 0xFF, 0x00, 0x00},
    {0x01, 0x01, 0x00, 0x04, 0x00, 0x88, 0x04, 0x01, 0x01, 0x05, 0x6F, 0x00, 0x18, 0x10, 0xFF, 0x00, 0x00}
};
const legacy_snr_t snrinfo[16] = { {0x88, 0x04, 0x89}};
const unsigned char asf_alerts_num = 0;
const unsigned char RMCP_IANA[] = {0x00, 0x00 , 0x11, 0xBE};
unsigned char pldmtmp[14];
unsigned char pldmsuccessbr[14] = {0x70, 0x0F, 0x0A, 0x89, 0xC1, 0x08, 0x01, 0xC0, 0x01, 0x00, 0x03, 0x0d, 0x00};
unsigned char pldmsuccessbw[14] = {0x70, 0x0F, 0x0A, 0xC9, 0x01, 0x0A, 0x09, 0xC8, 0x01, 0x00, 0x03, 0x04, 0x00};
unsigned char MCTPVerSupport[18] = {0x70,0x0F,0x0E, 0xC9, 0x01, 0x0A, 0x09, 0xC8, 0x00, 0x00, 0x04, 0x00, 0x01, 0xF1, 0xF0, 0xFF, 0x00};

volatile unsigned char *smbiosptr;
unsigned int smbiosdatalen;
volatile DPCONF *dpconf=NULL;
pldm_t *pldmdataptr=NULL;
pldm_res_t *pldmresponse[7];
smbiosrmcpdata *smbiosrmcpdataptr=NULL;
volatile unsigned char *timestampdataptr=NULL;
sensor_t *sensor=NULL;
int smbus_fd;
unsigned char *flash_buf=NULL;
asf_config_t *asfconfig=NULL;
unsigned char  *xferptr[3];
unsigned int xferlen[3];
unsigned char  *pldmdataptr_ptr[3];
int evt_msgid, flash_msgid;
pldm_t pldmdata;
BiosAttr *attrhead=NULL,*attrtail=NULL;
eventdata *eventdataptr;
eventdata *event_head;
unsigned int event_length=0;
unsigned char data_index;

void AddItem(BiosAttr *NewItem)
{
    if (!attrhead)
        attrhead=NewItem;
    NewItem->next = NULL;
    if (attrtail)
        attrtail->next =NewItem;
    attrtail = NewItem;
}

void ReleasePldm(void)
{
    BiosAttr *mfreeptr,*mptr;
    mptr=attrhead;
    while (mptr)
    {
        mfreeptr = mptr;
        mptr = mptr->next;
        free(mfreeptr);
    }
    attrhead=NULL;
    attrtail=NULL;
}

BiosAttr *InitialPLDM(void)
{
    unsigned char *sptr,*tptr;
    BiosAttr *Item;
    unsigned long i=0,j=0,k=0;
    unsigned short handle = 0;
    unsigned char  attr_type = 0;
    unsigned short  cur_val = 0;

    sptr = pldmdata.ptr[1];
    tptr = pldmdata.ptr[2];

    if (pldmdata.TBL2)
    {
        ReleasePldm();
        while (i < pldmdata.len[1]-4)
        {
            Item=malloc(sizeof(BiosAttr));
            memset(Item, 0 , sizeof(BiosAttr));
            Item->attr_handle= *(sptr+i) + (*(sptr+i+1) << 8);
            i=i+2;
            Item->attr_type=*(sptr+i);
            i++;
            Item->attr_name_handle= *(sptr+i) + (*(sptr+i+1) << 8);
            i=i+2;
            if (Item->attr_type == BIOSString || Item->attr_type == BIOSPassword)
            {
                i=i+5;
                //get the default string length
                Item->attr_possible_num=*(sptr+i) + (*(sptr+i+1) << 8);
                i=i+2;
                Item->attr_data=sptr+i;
                i=i+(Item->attr_possible_num)*1;
            }
            else if (Item->attr_type == BIOSBootConfigSetting)
            {
                i=i+4;
                Item->attr_possible_num=*(sptr+i);
                i++;
                Item->attr_possible_handle= (unsigned short *) (sptr+i);
                i=i+(Item->attr_possible_num)*2;
                i=i+2;;
            }
            else
            {
                Item->attr_possible_num=*(sptr+i);
                i++;
                Item->attr_possible_handle= (unsigned short *) (sptr+i);
                i=i+(Item->attr_possible_num)*2;
                Item->attr_default_num=*(sptr+i);
                i++;
                Item->attr_default_index=sptr+i;
                i++;
            }
            AddItem(Item);
        }
    }

    //add the attribute value table into Item
    if (pldmdata.TBL3)
    {
        for (Item = attrhead; Item; Item = Item->next)
        {
            j = 0;
            while (j < pldmdata.len[2]-4)
            {
                k=j;
                handle = *(tptr + j)  + (*(tptr+j+1) << 8);
                attr_type = *(tptr + j +2);

                if (attr_type == BIOSBootConfigSetting)
                    j=j+5;
                else
                    j=j+3;

                if (attr_type== BIOSString || attr_type == BIOSPassword)
                {
                    cur_val =*(tptr+j) + (*(tptr+j+1) << 8);
                    j=j+2;
                }
                else
                {
                    cur_val=*(tptr+j);
                    j++;
                }

                if (handle == Item->attr_handle)
                {
                    Item->attr_val_handle=tptr+k;
                    Item->attr_val_index=tptr+j;
                    Item->attr_val_possible_num=cur_val;
                    break;
                }
                j=j+cur_val*1;
            }
        }
    }
    return attrhead;
}

unsigned char getPLDMElement(BiosAttr *addr, unsigned char ptype, unsigned char val, unsigned char *out)
{
    BiosAttr *ptr;
    ptr=addr;

    if (ptype==0)
        SearchAttr(ptr->attr_name_handle,out);
    else
        SearchAttr(*(ptr->attr_possible_handle+val),out);

    return 0;
}
unsigned int CreateAttrVal(unsigned char *buf,unsigned char *seq)
{
    BiosAttr *ptr =  attrhead;
    unsigned char  i=0;
    unsigned char  j=0;

    while (ptr)
    {
        if (ptr->attr_type == BIOSBootConfigSetting)
        {

#ifndef CONFIG_BLOCK_WRITE
            for (i = 0; i < ptr->attr_val_possible_num; i++)
            {
                if (dpconf->bios == AMIBIOS && dpconf->biostype == LEGACY)
                {
                    //AMI use the string table index as a boot index
                    if (*(ptr->attr_val_index+i) != *(seq + i))
                    {
                        unsigned short tmphandle = 0xFFFF;
                        if (tmphandle == 0xFFFF)
                        {
                            j = i;
                            tmphandle = *(ptr->attr_possible_handle + i);
                        }
                        else
                        {
                            *(ptr->attr_possible_handle+j) = *(ptr->attr_possible_handle+i);
                            *(ptr->attr_possible_handle+i) = tmphandle;
                        }
                    }
                }
                *(ptr->attr_val_index +i) = *(seq + i);
            }
#else
            for (i = 0; i < ptr->attr_val_possible_num; i++)
                *(ptr->attr_val_index +i) = *(seq + i);
#endif
            for (i=0,j=0; i< ptr->attr_val_possible_num+6; i++)
            {
                if (i>5)
                {
                    *(buf+i)=*(seq+j);
                    j++;
                }
                else
                    *(buf+i)=*(ptr->attr_val_index-6+i);
            }
            return (ptr->attr_val_possible_num + 6);
        }
        ptr = ptr->next;
    }
    return 0;
}

void bsp_pldm_set(unsigned char *buf, unsigned char len)
{
    unsigned char pad = 0;
    unsigned int crc;
    unsigned char *ptr ;
    unsigned char *tmp;

    if (pldmdataptr->valid)
    {
        if (dpconf->bios == PHOENIXBIOS && dpconf->pldmtype == PLDM_BLOCK_READ)
        {
            //set the pointer to the starting point of middle packet
            //which is used to hold the pending value table
            ptr = ((unsigned char *) pldmresponse[1]) + 8;
            pad  =  (len % 4) ? (4 - (len %4 )) : 0 ;
            memcpy(ptr, buf, len);
            if (pad != 0)
                memset(&ptr[len], 0, pad);
            crc = crc32(0, ptr, len+pad);
            memcpy(&ptr[len+pad], &crc, 4);
            pldmresponse[1]->length = len + pad + 4 + 5;
        }
        else if(dpconf->pldmtype == PLDM_BLOCK_READ)
        {
            tmp = malloc(64);
            ptr = ((unsigned char *) pldmresponse[0]) + 18;
            pad  =  (len % 4) ? (4 - (len %4 )) : 0 ;
            memcpy(tmp, buf, len);
            memset(tmp+len , 0 , pad);
            len += pad;
            crc = crc32(0, tmp, len);
            memcpy(tmp+len, &crc, 4);
            len += 4;
            if (len <= (31-15))//bytecount max value 0x1F
            {
                memcpy(ptr, tmp, len);
                pldmresponse[0]->length = len + 0x0f;
                pldmresponse[0]->msgstop = 1;//eom=1
            }
            else
            {
                memcpy(ptr, tmp, 16);
                pldmresponse[0]->length = 0x1F;
                pldmresponse[0]->msgstop = 0;//eom=0
                memcpy(&pldmresponse[1]->sslaveaddr, tmp+16, len-16);
                pldmresponse[1]->length = len-16;
            }
            free(tmp);
        }
        else
        {
            ptr = &pldmresponse[0]->val[5];
            pad  =  (len % 4) ? (4 - (len %4 )) : 0 ;
            memcpy(ptr, buf, len);
            ///pldmresponse[0]->val[8]= 0x03;
            memset(ptr+len, 0 ,pad);
            len += pad;
            crc = crc32(0, ptr, len);
            memcpy(ptr+len, &crc, 4);
            len += 4;
            pldmresponse[0]->length = len + 0x0f;
        }
        //AMD need the tags to be the same for accepting pending value tables

        smbiosrmcpdataptr->bootopt[2] = 0x0;
        pldmdataptr->dirty = 1;
    }
    else
    {   //asf method
        smbiosrmcpdataptr->bootopt[4] = buf[6] + 1;
        smbiosrmcpdataptr->bootopt[2] = 0x11;
        //transform from 0x00 - 0x04 to 0x01 - 0x05
        //asf spec
    }
}

void SearchAttr(unsigned short tmp_handle,unsigned char *out)
{
    unsigned char *sptr;
    unsigned short handle,len;
    unsigned long i=0;

    //prevent null condition, but later would use strlen to count the name
    out[0] = 0;
    sptr=pldmdataptr_ptr[0];
    while (i < pldmdataptr->len[0])
    {
        handle=*(sptr+i) + (*(sptr+i+1) << 8);
        i += 2;
        len=*(sptr+i) + (*(sptr+i+1) << 8);
        i += 2;
        if (handle==tmp_handle)
        {
            strncpy(out, sptr+i, len);
            break;
        }
        i += len;
    }
    out=trimspace(out);
}

void handle_pldm_br_phoenix(unsigned char *addr, unsigned int len)
{
    if (*(addr+1) == 0x0f && *(addr+10) == 0x03 && (*(addr+4) & *(addr+7) & 0x80) == 0x80)
    {
        //rtk_set_lock(lock_fd, F_WRLCK);
        switch (*(addr+11))
        {
        case GetBIOSTableTags:
            pldmdataptr->pldmerror = 0;
            if (pldmdataptr->valid)
            {
                pldmdataptr->index = 5;
                memcpy(&pldmresponse[pldmdataptr->index]->val[0], &pldmdataptr->tag[0], 3*sizeof(pldmdataptr->tag[0]));
            }
            else
            {
                pldmdataptr->index = 4;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_BIOS_TABLE_TAG_UNAVAILABLE;
                pldmresponse[pldmdataptr->index]->length = 0x0A;
            }
            break;

        case GetBIOSTable:
            if (addr[17] == 0x03 && pldmdataptr->dirty)
            {
                pldmdataptr->index = 0;
                //pldmdataptr->dirty = 0;
            }
            else
            {
                pldmdataptr->index = 4;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_BIOS_TABLE_UNAVAILABLE;
                pldmresponse[pldmdataptr->index]->length = 0x0A;
                //may only end here
                smbiosrmcpdataptr->PollType = POLL_NONE;
            }
            break;

        case AcceptBIOSAttributesPendingValues:
            pldmdataptr->dirty = 0;

        default:
            //default length
            pldmdataptr->index = 4;
            pldmresponse[pldmdataptr->index]->length = 0x0A;
            pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
            break;
        }
        pldmresponse[pldmdataptr->index]->pldmtype = addr[10];
        pldmresponse[pldmdataptr->index]->pldmcmd = addr[11];
        //rtk_set_lock(lock_fd, F_UNLCK);
    }
}

void handle_pldm_br_ami(unsigned char *addr, unsigned int len)
{
    SMBIOS_Table_EP *smbiosmeta ;
    unsigned short tmp_len;
    unsigned char *tmp_rom = NULL;
    unsigned int chk_diff = 0;
    int i = 0;
    int fd;
    unsigned int offset;
    erase_info_t ei;

    if ((len >=12) && addr[1] == 0x0f && addr[10] == 0x01 &&
            (addr[7] & 0x80) == 0x80 && addr[4] == 0x01 && (addr[11] == 0x02))
    {
        smbiosmeta = (SMBIOS_Table_EP *)(timestampdataptr + SMBIOS_DATA_OFFSET);

        memcpy(smbiosmeta, addr+20, 4);
        memcpy(&smbiosmeta->majorVer, addr+12, 4);
        memcpy(&smbiosmeta->StTableLen, addr+16, 2);
        memcpy(&smbiosmeta->StNum, addr+18, 2);

        tmp_len = *((unsigned short *)(addr+16));
        tmp_len += SMBIOS_DATA_OFFSET + SMBIOS_HEADER_SIZE;

        memset(timestampdataptr + tmp_len, 0, MAX_SMBIOS_SIZE - tmp_len);
        tmp_rom = malloc(0x2000);
        memcpy(tmp_rom, flash_buf + 0x6000, 0x2000);
        for (i=0; i<0x2000; i++)
        {
            if (tmp_rom[i] != timestampdataptr[i])
            {
                chk_diff = 1;
                break;
            }
        }
        free(tmp_rom);
        if (chk_diff)
        {
            fd = open("/dev/mtd2", O_RDWR);
            ei.length = 0x2000;
            ei.start = 0x6000;
            ioctl(fd, MEMUNLOCK, &ei);
            ioctl(fd, MEMERASE, &ei);

            offset = SMBIOS_ROM_START_ADDR - DPCONF_ADDR_CACHED;
            lseek(fd, offset, SEEK_SET);
            write(fd, timestampdataptr, 0x2000);
            close(fd);
        }
    }
}

void handle_pldm_bw(unsigned char *addr)
{
    return ;
}

void setPLDMEndPoint(unsigned char srcept, unsigned char dstept)
{
    int i = 0;
    if (srcept == 0 && dstept == 0)
        return;

    for (i=0 ; i < 7 ; i++)
    {
        pldmresponse[i]->srcept = srcept;
        pldmresponse[i]->dstept = dstept;
    }
}


void smbus_process_pldm_br_ami(unsigned char *addr, unsigned int len)
{
    unsigned char pldmaddr = dpconf->pldmslaveaddr + 1;
    unsigned char command;
    static unsigned char *pldmptr = NULL;
    static unsigned int *pldmlen = NULL;
    static unsigned char length = 0;
    //int i = 0;

    if ((len >= 12)  &&  addr[4] == 0x01 && addr[7] == 0xC8 && \
            addr[11] != 0x02  && addr[8] == 0x01 && addr[10] == 0x03)
    {
        command = addr[11];
        switch (command)
        {
        case 0x05: //Set bios table tag
            handle_pldm_tags(addr);
            break;

        case 0x0d: //Set datetime
            //set datatime is the first entry for PLDM
            pldm_handle_setgettime(addr);
            pldmptr = xferptr[0];
            pldmlen = &xferlen[0];
            break;
        }
    }
    else if ((len >= 12) &&  addr[4] == 0x01 && (addr[7] & 0x88) == 0x88  &&  \
             (addr[10] == 0x03 || addr[10] == 0x01))
    {
        command = addr[11];
        if (command == 0x02)
        {
            smbiosrmcpdataptr->PollType = POLL_STOP;
            //stop again, since the table is processing

            if(addr[10] == 0x03) {    //set bios table
                pldmptr = xferptr[addr[17]];
                pldmlen = &xferlen[addr[17]];

                length = addr[2];
                memcpy(pldmptr, addr+18, length-15);
            }
            else if(addr[10] == 0x01)
            {
                pldmptr = xferptr[addr[19]];
                pldmlen = &xferlen[addr[19]];

                length = addr[2];
                memcpy(pldmptr, addr+20, length-15);
            }
            pldmptr += length - 15;
            *pldmlen += length - 15;
        }
        //smbios data
        else if (command == 0x04)
        {   ////block read : pldm cmd=0x04 , 1st packet of first transfer
            pldmptr = timestampdataptr + SMBIOS_DATA_OFFSET + SMBIOS_HEADER_SIZE;
            pldmlen = &smbiosdatalen;
            *pldmlen = 0;

            length = addr[2];
            memcpy(pldmptr, addr+17, length-14);
            pldmptr += length - 14;
            *pldmlen += length - 14;
        }
    }
    else
    {
        if ((len >= 11) && addr[3] == pldmaddr &&  addr[4] == 0x01 && (addr[7] & 0x08) == 0x08 && \
                addr[8] == 0x80 && ((addr[9] == 0x03 && addr[10] == 0x02) || (addr[9] == 0x01 && addr[10] == 0x04)))
        {
            if (addr[9] == 0x03)
            {
                length = addr[2];
                memcpy(pldmptr, addr+17, length-14);
                pldmptr += length -14;
                *pldmlen += length - 14;
            }
            else if (addr[9] == 0x01)
            {
                length = addr[2];
                memcpy(pldmptr, addr+16, length-13);
                pldmptr += length -13;
                *pldmlen += length - 13;
            }
        }
        else
        {   //block read : pldm cmd=0x04 , 2nd packet
            if (pldmptr) {
                length = addr[2];
                memcpy(pldmptr, addr+3, length);
                pldmptr += length;
                *pldmlen += length;
            }
        }
    }
}


void smbus_process_pldm_br_phoenix(unsigned char *addr, unsigned int len)
{
    unsigned char pldmaddr = dpconf->pldmslaveaddr + 1;
    unsigned char command;
    static unsigned char *pldmptr = NULL;
    static unsigned int *pldmlen = NULL;
    static unsigned char length = 0;

    if (*(addr+2) > 0x20 || *(addr+2) == 0)
    {
        debug("Get Block Write Data : %02x %02x %02x %02x %02x %02x %02x %02x\n", addr[0], addr[1], addr[2], addr[3], addr[4], addr[5], addr[6], addr[7] );

    }
    else if (*(addr+3) == pldmaddr && *(addr+4) == 0xc1 && *(addr+7) == 0xc0 && *(addr+8) == 0x01 && *(addr+10) == 0x03)
    {
        command = *(addr+11);
        switch (command)
        {
        case 0x05: //Set bios table tag

            handle_pldm_tags(addr);
            break;

        case 0x0d: //Set datetime
            //set datatime is the first entry for PLDM
            pldm_handle_setgettime(addr);
            pldmptr = xferptr[0];
            pldmlen = &xferlen[0];
            break;
        }
    }
    else if ( *(addr+4) == 0x81 && (*(addr+7)&0x80)==0x80 && *(addr+8)==0x01 && *(addr+10)==0x03 && *(addr+11)==0x02)
    {
        if (*(addr+11) == 0x02)
        {
            //OS_ENTER_CRITICAL();
            smbiosrmcpdataptr->PollType = POLL_STOP;
            ///smbiosrmcpdataptr->BIOSBoot = 1;
            //OS_EXIT_CRITICAL();
            //stop again, since the table is processing

            //length = *(addr+2);
            //tbltype = *(addr+17);
            pldmptr = xferptr[*(addr+17)];
            pldmlen = &xferlen[*(addr+17)];

            //since it is sequential updating
            //pldmxferptr->ptr[tbltype] = pldmptr;
            //pldmlen = &pldmxferptr->len[tbltype];

            length = *(addr+2);
            memcpy(pldmptr, addr+18, length-15);
            pldmptr += length - 15;
            *pldmlen += length - 15;
        }
    }
    else
    {
        length = *(addr+2);
        memcpy(pldmptr, addr+8, length-5);
        pldmptr += length-5;
        *pldmlen += length - 5;
    }
}
void smbus_process_pldm_bw(unsigned char *addr)
{
    unsigned char pldmaddr = dpconf->pldmslaveaddr + 1;
    unsigned char command;

    //OS_CPU_SR cpu_sr = 0;
    static unsigned char *pldmptr;
    static unsigned int *pldmlen;
    static unsigned char length = 0;
    static unsigned char pldmres = 1;
    static unsigned int pldmhandler = 0;
    SMBIOS_Table_EP *smbiosmeta ;
    unsigned int crc;
    unsigned char snrnum;
    eventdata evtdata;
    eventdata *evt_ptr;
    unsigned char pldmcode;
    unsigned char instance_id;
    static unsigned char temp = 0;

    if (*(addr+3) == pldmaddr && (*(addr+4) == 0x01) && ((*(addr+7) & 0xC8) == 0xC8))
    {
        if (*(addr+8) == 0x01 && *(addr+10) == 0x03)
        {
            //offset 11 is the command code for PLDM
            command = *(addr+11);
            switch (command)
            {
            case GetBIOSTableTags:
                pldmdataptr->pldmerror = 0;
                if (pldmdataptr->valid)
                {
                    pldmdataptr->index = 5;
                    pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
                    memcpy(&pldmresponse[pldmdataptr->index]->val[0], &pldmdataptr->tag[0], 3*sizeof(pldmdataptr->tag[0]));
                }
                else
                {
                    pldmdataptr->index = 4;
                    pldmresponse[pldmdataptr->index]->length = 0x0A;
                    pldmresponse[pldmdataptr->index]->pldmcode = PLDM_BIOS_TABLE_TAG_UNAVAILABLE;
                    pldmptr = xferptr[0];
                    pldmlen = &xferlen[0];
                }
                break;

            case GetBIOSTable:
                if (addr[17] == 0x03 && pldmdataptr->dirty)
                {
                    pldmdataptr->index = 0;
                    //pldmdataptr->dirty = 0;
                }
                else
                {
                    pldmdataptr->index = 4;
                    pldmresponse[pldmdataptr->index]->length = 0x0A;
                    pldmresponse[pldmdataptr->index]->pldmcode = PLDM_BIOS_TABLE_UNAVAILABLE;
                    //may only end here
                    smbiosrmcpdataptr->PollType = POLL_NONE;
                }
                break;

            case 0x02: //Set bios table
                pldmres = 0;
                //OS_ENTER_CRITICAL();
                smbiosrmcpdataptr->PollType = POLL_STOP;
                ///smbiosrmcpdataptr->BIOSBoot = 1;
                //OS_EXIT_CRITICAL();
                //stop again, since the table is processing

                //length = *(addr+2);
                //tbltype = *(addr+17);
                //only response when start bit is set
                if ((*(addr+16) & 0x01)== 0x01)
                {
                    memcpy(&pldmhandler, addr+12, 4);
                    pldmptr = xferptr[*(addr+17)];
                    pldmlen = &xferlen[*(addr+17)];
                }

                //since it is sequential updating
                //pldmxferptr->ptr[tbltype] = pldmptr;
                //pldmlen = &pldmxferptr->len[tbltype];

                length = *(addr+2);
                pldmhandler += (length-15);
                if(((*(addr+17) == 0) && (pldmhandler >= PLDM_STR_TBL_SIZE)) || (((*(addr+17) == 1) || (*(addr+17) == 2)) && (pldmhandler >= PLDM_ATT_TBL_SIZE)))
                {
                    pldmdataptr->pldmerror = 1;
                    pldmcode = PLDM_ERROR;
                }
                else
                {
                    pldmcode = PLDM_SUCCESS;
                    memcpy(pldmptr, addr+18, length-15);
                    pldmptr += length - 15;
                    *pldmlen += length - 15;
                }

                //only response when end bit is set
                if ((addr[16] & 0x04) == 0x04)
                {
                    pldmdataptr->index = 4;
                    pldmresponse[pldmdataptr->index]->length = 0x0A;
                    pldmresponse[pldmdataptr->index]->pldmcode = pldmcode;
                    pldmres = 1;
                }
                else if(dpconf->pldmmultitx)
                {
                    pldmdataptr->index = 4;
                    pldmresponse[pldmdataptr->index]->length = 0x0E;
                    pldmresponse[pldmdataptr->index]->pldmcode = pldmcode;
                    memcpy(pldmresponse[pldmdataptr->index]->val, &pldmhandler, 4);
                    pldmres = 1;
                }
                break;
            case 0x05: //Set bios table tag

                pldmdataptr->index = 4;
                pldmresponse[pldmdataptr->index]->length = 0x0A;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
                handle_pldm_tags(addr);

                break;
            case 0x0d: //Set datetime
                //set datatime is the first entry for PLDM
                pldmdataptr->index = 4;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
                pldmresponse[pldmdataptr->index]->length = 0x0A;

                pldm_handle_setgettime(addr);

                break;
            case AcceptBIOSAttributesPendingValues:
                pldmdataptr->dirty = 0;

            default:
                pldmdataptr->index = 4;
                pldmresponse[pldmdataptr->index]->length = 0x0A;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
                break;

            }
            if (pldmres)
            {
                pldmresponse[pldmdataptr->index]->pldmtype = addr[10];
                pldmresponse[pldmdataptr->index]->pldmcmd = addr[11];
                //master_send(PLDM_RESPONSE, (unsigned char *)pldmresponse[pldmdataptr->index], pldmresponse[pldmdataptr->index]->length + 3, 0x09);
                struct master_smbus_parameters *msp = malloc(sizeof(struct master_smbus_parameters));
                msp->msg_type = PLDM_RESPONSE;
                msp->cmd = 0x09;
                msp->pldmfromasf = dpconf->pldmfromasf;
                msp->pldmpec = dpconf->pldmpec;
                msp->asfpec = asfconfig->pec;
                msp->poll_type = smbiosrmcpdataptr->PollType;
                msp->bufaddr = (unsigned char *)pldmresponse[pldmdataptr->index];
                msp->length = pldmresponse[pldmdataptr->index]->length + 3;
                ioctl(smbus_fd, SMMSEND, msp);
                //patching the change case for DELL
                if (dpconf->pldmreset && (command == AcceptBIOSAttributesPendingValues))
                    SMBus_Prepare_RmtCtrl(RMCP_Reset,1);
            }
        }
        else if (*(addr+1) == 0x0F && *(addr+8) == 0x00 )
        {
            //MCTP Version
            pldmresponse[6]->pldmtype = *(addr+10);
            if (*(addr+10) == 0x02) //get endpoint ID
            {
                pldmresponse[6]->dstept = *(addr+6);
                pldmresponse[6]->srcept = 0x09;
                pldmresponse[6]->length = 0x0C;
                pldmresponse[6]->pldmcode = 0x09;
                pldmresponse[6]->val[0] = 0x01;
                pldmresponse[6]->val[1] = 0x00;
            }
            //Get MCTP version command
            else if (*(addr+10) == 0x04 && *(addr+11) == 0x00)
            {
                setPLDMEndPoint(*(addr+5), *(addr+6));
                pldmresponse[6]->length = 0x0E;
                pldmresponse[6]->pldmcode = 0x01;
                pldmresponse[6]->val[0] = 0xF1;
                pldmresponse[6]->val[1] = 0xF0;
            }
            else if (*(addr+10) == 0x05)
            {
                pldmresponse[6]->length = 0x0C;
                pldmresponse[6]->pldmcode = 0x02;
                pldmresponse[6]->val[0] = 0x00;
                pldmresponse[6]->val[1] = 0x01;
            }

            struct master_smbus_parameters *msp = malloc(sizeof(struct master_smbus_parameters));
            msp->msg_type = PLDM_RESPONSE;
            msp->cmd = 0x09;
            msp->pldmfromasf = dpconf->pldmfromasf;
            msp->pldmpec = dpconf->pldmpec;
            msp->asfpec = asfconfig->pec;
            msp->poll_type = smbiosrmcpdataptr->PollType;
            msp->bufaddr = (unsigned char *) pldmresponse[6];
            msp->length = pldmresponse[6]->length + 3;
            ioctl(smbus_fd, SMMSEND, msp);
            //master_send(PLDM_RESPONSE, (unsigned char *) pldmresponse[6], pldmresponse[6]->length + 3, 0x09);
        }
//PLDM 0240 PLDM Messaging Control and Discovery
        else if (*(addr+8) == 0x01 && *(addr+10) == 0x00)
        {
            pldmdataptr->index = 4;
            pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
            if (*(addr+11) == GetPLDMVersion && *(addr+17) == 0x00)
            {
                pldmresponse[pldmdataptr->index]->length = 0x17;
                pldmresponse[pldmdataptr->index]->val[0] = 0x08;
                pldmresponse[pldmdataptr->index]->val[4] = 0x05;
                pldmresponse[pldmdataptr->index]->val[5] = 0xF1;
                pldmresponse[pldmdataptr->index]->val[6] = 0xF0;
                pldmresponse[pldmdataptr->index]->val[7] = 0xF0;
                pldmresponse[pldmdataptr->index]->val[8] = 0x00;
                //iid is different ?
                crc = crc32(0, &pldmresponse[pldmdataptr->index]->val[5],4);
                memcpy(&pldmresponse[pldmdataptr->index]->val[9], (const char *)crc, 4);

            }
            else if (*(addr+11) == GetPLDMTypes)
            {
                pldmresponse[pldmdataptr->index]->length = 0x12;
                pldmresponse[pldmdataptr->index]->val[0] = 0x0F;
                memset(&pldmresponse[pldmdataptr->index]->val[1], 0 , 7);

            }
            else if (*(addr+11) == GetPLDMCommands)
            {
                pldmresponse[pldmdataptr->index]->length = 0x2A;
                pldmresponse[pldmdataptr->index]->val[0] = 0x1F;
                memset(&pldmresponse[pldmdataptr->index]->val[1], 0 , 31);
            }
            else if (*(addr+11) == GetTID)
            {
                pldmresponse[pldmdataptr->index]->length = 0x0B;
                pldmresponse[pldmdataptr->index]->val[0] = 0x00;
            }
            else
            {
                pldmresponse[pldmdataptr->index]->length = 0x0A;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_ERROR_UNSUPPORTED_PLDM_CMD;
            }
            pldmresponse[pldmdataptr->index]->pldmtype = addr[10];
            pldmresponse[pldmdataptr->index]->pldmcmd = addr[11];
            //master_send(PLDM_RESPONSE, (unsigned char *)pldmresponse[4], pldmresponse[4]->length + 3, 0x09);
            struct master_smbus_parameters *msp = malloc(sizeof(struct master_smbus_parameters));
            msp->msg_type = PLDM_RESPONSE;
            msp->cmd = 0x09;
            msp->pldmfromasf = dpconf->pldmfromasf;
            msp->pldmpec = dpconf->pldmpec;
            msp->asfpec = asfconfig->pec;
            msp->poll_type = smbiosrmcpdataptr->PollType;
            msp->bufaddr = (unsigned char *) pldmresponse[4];
            msp->length = pldmresponse[4]->length + 3;
            ioctl(smbus_fd, SMMSEND, msp);
        }
        else if (*(addr+8) == 0x01 && *(addr+10) == 0x01)
        {
            command = *(addr+11);
            instance_id = *(addr+9);//avoid to bios retransmission
            switch (command)
            {
            case GetSMBIOSStructureTableMetadata:
                pldmdataptr->pldmerror = 0;
                pldmdataptr->index = 5;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
                smbiosmeta = (SMBIOS_Table_EP *) (timestampdataptr + SMBIOS_DATA_OFFSET);
                memcpy(&pldmresponse[pldmdataptr->index]->val[0], &smbiosmeta->majorVer , 4);
                memcpy(&pldmresponse[pldmdataptr->index]->val[4], &smbiosmeta->StTableLen , 2);
                memcpy(&pldmresponse[pldmdataptr->index]->val[6], &smbiosmeta->StNum , 2);
                memcpy(&pldmresponse[pldmdataptr->index]->val[8], smbiosmeta , 4);
                break;

            case SetSMBIOSStructureTableMetadata:
                if(pldmdataptr->pldmerror)
                {
                    pldmdataptr->pldmerror = 0;
                    break;
                }
                //smbios_flag=1;
                //smbiosptr = (unsigned char *) (timestampdataptr + SMBIOS_DATA_OFFSET);
                //memcpy(smbiosptr, timestampdataptr + SMBIOS_DATA_OFFSET, 4096);
                smbiosmeta = (SMBIOS_Table_EP *) (timestampdataptr + SMBIOS_DATA_OFFSET);
                memcpy(smbiosmeta,addr+20 ,4);
                memcpy(&smbiosmeta->majorVer, addr+12, 4);
                memcpy(&smbiosmeta->StTableLen, addr+16, 2);
                memcpy(&smbiosmeta->StNum, addr+18, 2);

                unsigned tmp_len = *(unsigned short *) (addr+16) + SMBIOS_DATA_OFFSET + SMBIOS_HEADER_SIZE;
                memset(timestampdataptr + tmp_len, 0 , MAX_SMBIOS_SIZE-tmp_len);
#if 1
                struct msgstru_flash msgs_flash;
                msgs_flash.msgtype = FLASH_MSG;
                memcpy(msgs_flash.msgtext, timestampdataptr, MAX_SMBIOS_SIZE);
                msgs_flash.len = MAX_SMBIOS_SIZE;
                msgs_flash.flag = 0x08;
                msgsnd(flash_msgid, &msgs_flash, sizeof(struct msgstru_flash), IPC_NOWAIT);
#else
                dirty[SMBIOSTBL].length = *(unsigned short *) (addr+16) + SMBIOS_DATA_OFFSET + SMBIOS_HEADER_SIZE;
                memset(timestampdataptr + dirty[SMBIOSTBL].length, 0 , MAX_SMBIOS_SIZE-dirty[SMBIOSTBL].length);
                setdirty(SMBIOSTBL);
#endif
                pldmdataptr->index = 4;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
                //ParserSensor();
#if 0
                if(usbcb.usbstate == DISABLED || usbcb.usbstate == DISCONNECTED)//patch for pldm_smbios_data_transfer keyboard fail
                {
                    debug("enable usb_connection:0x%x\n",usbcb.usbstate);
                    //#if CONFIG_VNC_ENABLED
                    //enable_vnc_usb_dev();
                    //#endif
                }
#endif
                break;

            case SetSMBIOSStructureTable:
                //change to the ROM one
                //smbiosptr = (unsigned char *) (SMBIOS_ROM_START_ADDR + SMBIOS_DATA_OFFSET);
                pldmres = 0;

                if ((*(addr+16) & 0x01)== 0x01)
                {
                    pldmptr = timestampdataptr + SMBIOS_DATA_OFFSET + SMBIOS_HEADER_SIZE;
                    pldmlen = &smbiosdatalen;
                    *pldmlen = 0;
                    memcpy(&pldmhandler, addr+12, 4);
                    temp = 0;
#if 0
                    if(usbcb.usbstate == DISABLED || usbcb.usbstate == DISCONNECTED)//patch for pldm_smbios_data_transfer keyboard fail
                    {
                        debug("disabled usb_connection: 0x%x\n",usbcb.usbstate);
                        //#if CONFIG_VNC_ENABLED
                        //disable_vnc_usb_dev();
                        //#endif
                    }
#endif
                }
                if(temp != instance_id)
                {
                    length = *(addr+2);
                    pldmhandler += (length - 14);
                }
//                debug("[pldm_bw] -->temp = 0x%x instance_id = 0x%x ,ptr=0x%x\n",temp,instance_id,pldmptr);
                //maximal SMBIOS table length should be 8192
                if(pldmhandler >= 8000)
                {
                    pldmdataptr->pldmerror = 1;
                    pldmcode = PLDM_ERROR;

                }
                else if(temp != instance_id)
                {
                    pldmcode = PLDM_SUCCESS;
                    memcpy(pldmptr, addr+17, length-14);
                    pldmptr += length - 14;
                    *pldmlen += length - 14;
                }


                if ((*(addr+16) & 0x04)== 0x04)
                {
                    pldmres = 1;
                    pldmdataptr->index = 4;
                    pldmresponse[pldmdataptr->index]->length = 0x0A;
                    pldmresponse[pldmdataptr->index]->pldmcode = pldmcode;
                }
                else if (dpconf->pldmmultitx)
                {
                    pldmdataptr->index = 4;
                    pldmresponse[pldmdataptr->index]->length = 0x0E;
                    debug("[pldmmultitx] ->pldmcode = 0x%x\n",pldmresponse[pldmdataptr->index]->pldmcode);
                    if(temp != instance_id)
                        pldmresponse[pldmdataptr->index]->pldmcode = pldmcode;

                    debug("[pldmmultitx] <-pldmcode = 0x%x\n",pldmresponse[pldmdataptr->index]->pldmcode);
                    memcpy(pldmresponse[pldmdataptr->index]->val, &pldmhandler, 4);
                    pldmres = 1;

                }
                temp = instance_id;
//                debug("[pldm_bw] <--temp = 0x%x instance_id = 0x%x ,ptr=0x%x\n",temp,instance_id,pldmptr);
                /*
                        length = *(addr+2);
                        memcpy(pldmptr, addr+17, length-14);
                        pldmptr += length - 14;
                        *pldmlen += length - 14;
                */
                break;

            default:
                pldmdataptr->index = 4;
                pldmresponse[pldmdataptr->index]->length = 0x0A;
                pldmresponse[pldmdataptr->index]->pldmcode = PLDM_SUCCESS;
                break;
            }
            if (pldmres)
            {
                pldmresponse[pldmdataptr->index]->pldmtype = addr[10];
                pldmresponse[pldmdataptr->index]->pldmcmd = addr[11];
                struct master_smbus_parameters *msp = malloc(sizeof(struct master_smbus_parameters));
                msp->msg_type = PLDM_RESPONSE;
                msp->cmd = 0x09;
                msp->pldmfromasf = dpconf->pldmfromasf;
                msp->pldmpec = dpconf->pldmpec;
                msp->asfpec = asfconfig->pec;
                msp->poll_type = smbiosrmcpdataptr->PollType;
                msp->bufaddr = (unsigned char *)pldmresponse[pldmdataptr->index];
                msp->length = pldmresponse[pldmdataptr->index]->length + 3;
                ioctl(smbus_fd, SMMSEND, msp);
                //master_send(PLDM_RESPONSE, (unsigned char *)pldmresponse[pldmdataptr->index], pldmresponse[pldmdataptr->index]->length + 3, 0x09);
            }
        }
    }
    //PlatformEventMessage
    else if (*(addr+3) == 0xDB && *(addr+10) == 0x02 && *(addr+11) == 0x0A )
    {
        for (snrnum = 0; snrnum < MAX_SENSOR_NUMS; snrnum++)
            if (sensor[snrnum].offset[0] == *(addr+15))
                break;
        //find it

        sensor[snrnum].state = *(addr+18);
        //only generate assert alerts
        if ((sensor[snrnum].state != 0x01) && (sensor[snrnum].state != sensor[snrnum].prestate))
        {
            sensor[snrnum].prestate = sensor[snrnum].state;
            memset(&evtdata, 0, sizeof(evtdata));

            evtdata.Sensor_Number = snrnum;
            evtdata.Entity = 0x13;
            evtdata.Sensor_Device = sensor[snrnum].offset[0];
            strcpy((char *)evtdata.EventData, "PLDM");

            if (sensor[snrnum].state == 6)
                evtdata.Event_Offset = 2;
            else if (sensor[snrnum].state == 5)
                evtdata.Event_Offset = 0;
            else if (sensor[snrnum].state == 8)
                evtdata.Event_Offset = 7;
            else if (sensor[snrnum].state == 9)
                evtdata.Event_Offset = 9;
            else
                evtdata.Event_Offset = 11;

            if (sensor[snrnum].type == SNR_TACHOMETER)
            {
                evtdata.Event_Sensor_Type = 0x04;
                if ((*(addr+21) == 0xFF) && (*(addr+22) == 0xFF))
                {
                    evtdata.Event_Offset = 2;
                    evtdata.Event_Type = 0x07;
                }
                else
                {
                    evtdata.Event_Type = 0x01;
                    evtdata.Event_Offset = (evtdata.Event_Offset >= 7) ? (evtdata.Event_Offset -7): (evtdata.Event_Offset +7);
                }
            }
            else
            {
                evtdata.Event_Sensor_Type = 0x01;
                evtdata.Event_Type = 0x01;
            }

            evtdata.Event_Severity = 0x10;
            evtdata.logtype = LOG_WARNING;
            evt_ptr = event_log(&evtdata.Event_Sensor_Type, 16);
            evt_ptr->interval = (ALERT_INTERVAL/(dpconf->numsent-1));
            evt_ptr->timeout = 0;
            evt_ptr->alertnum = dpconf->numsent;
            //OSQPost(EVTQ, evt_ptr);
            struct msgstru msgs;
            msgs.msgtype = EVT_MSG;
            memcpy(msgs.msgtext, evt_ptr, sizeof(evtdata));
            msgsnd(evt_msgid, &msgs,sizeof(struct msgstru),IPC_NOWAIT);
        }
    }
}

void handle_pldm_tags(unsigned char *addr)
{
    unsigned char tagerr = 0;
    unsigned char i;
    unsigned char tblindex;
    unsigned int tmptag;
    unsigned char *tmpptr;
    unsigned char j=0;
    unsigned int offset = 0;
    int fd;
    unsigned int length;
    erase_info_t ei;

    if(pldmdataptr->pldmerror)
    {
        pldmdataptr->pldmerror = 0;
        return;
    }

    for (i = 0; i < addr[12]; i++)
    {
        tblindex = addr[13+i*5];
        memcpy(&tmptag, addr+14+i*5, 4);
        if (tmptag == pldmdataptr->tag[tblindex])
        {
            pldmdataptr->xfertag[tblindex] = 0;
            continue;
        }
        pldmdataptr->xfertag[tblindex] = tmptag;

        if (xferlen[tblindex] < 4)
        {
            tagerr = 1;
            break;
        }

        pldmdataptr->chksum[tblindex] = crc32(0, xferptr[tblindex], xferlen[tblindex] - 4);
        if (pldmdataptr->chksum[tblindex] != pldmdataptr->xfertag[tblindex])
        {
            tagerr = 1;
            break;
        }
    }
    if (tagerr)
    {
        debug("tags mismatch.\n");
        return ;
    }

    for (i = 0; i < addr[12]; i++)
    {
        tblindex = addr[13+i*5];
        if (pldmdataptr->xfertag[tblindex])
        {
            //4 bytes for CRC32
            pldmdataptr->len[tblindex] = xferlen[tblindex]-4;
            pldmdataptr->tag[tblindex] = pldmdataptr->xfertag[tblindex];

            tmpptr = xferptr[tblindex];
            xferptr[tblindex] = pldmdataptr_ptr[tblindex];
            pldmdataptr_ptr[tblindex] = tmpptr;
            pldmdataptr->numwrite[tblindex+1] = pldmdataptr->numwrite[tblindex] + (pldmdataptr->len[tblindex] - 1 + FLASH_WRITE_SIZE) / FLASH_WRITE_SIZE ;

            if (tblindex == 0)
                pldmdataptr->TBL1 = 1;
            else if (tblindex == 1)
                pldmdataptr->TBL2 = 1;
            else if (tblindex == 2)
                pldmdataptr->TBL3 = 1;
        }
    }

    pldmdataptr->valid = 1;
    smbiosrmcpdataptr->PollType = POLL_NONE;

    fd = open("/dev/mtd2", O_RDWR);
    ei.length = 0x1000;
    ei.start = 0xB000;
    ioctl(fd, MEMUNLOCK, &ei);
    ioctl(fd, MEMERASE, &ei);
    for (i=0; i<3; i++)
    {
        if (i== 0)
            offset = PLDM_STR_TBL - DPCONF_ADDR_CACHED;
        else if(i==1)
            offset = PLDM_ATR_TBL - DPCONF_ADDR_CACHED;
        else if(i==2)
            offset = PLDM_VAL_TBL - DPCONF_ADDR_CACHED;
        lseek(fd, offset, SEEK_SET);
        length = write(fd, pldmdataptr_ptr[i], pldmdataptr->len[i]);
        printf("spi_flash_write 4, len is %d.\n", length);
        if (length<0)
            debug("write dpconf to flash failed.\n");
    }
    offset = SYSTEM_PLDM_DATA - DPCONF_ADDR_CACHED;
    lseek(fd, offset, SEEK_SET);
    length = write(fd, pldmdataptr, sizeof(pldm_t));
    if (length<0)
        debug("write dpconf to flash failed.\n");
    close(fd);
}

void pldm_handle_setgettime(unsigned char *addr)
{
    smbiosrmcpdataptr->PollType = POLL_STOP;

    //clear the corresponding fields
    memset(&xferlen, 0 , 48);
#if 1
    *timestampdataptr = addr[17];
    *(timestampdataptr + 1) = addr[16];
    *(timestampdataptr + 2) = addr[15];
    *(timestampdataptr + 3) = addr[14];
    *(timestampdataptr + 4) = addr[13];
    *(timestampdataptr + 5) = addr[12];
    set_time(timestampdataptr);
#else
    unsigned char tmp[6];
    tmp[0] = addr[17];
    tmp[1] = addr[16];
    tmp[2] = addr[15];
    tmp[3] = addr[14];
    tmp[4] = addr[13];
    tmp[5] = addr[12];
    set_time(tmp);
#endif
    //should only use in Revision A
    if (dpconf->vendor != FSC)
        ioctl(smbus_fd, SMBSSTATE, S_S0);
}


eventdata *event_log(unsigned char *addr, unsigned char byte)
{
    static unsigned char recall = 1;
    eventdata *eptr, *recalllist, *nextlist;
    unsigned char i = 1;

    eptr = eventdataptr + data_index;
    data_index = (data_index + 1) % MAX_EVENT_ENTRY;
    nextlist = eventdataptr + data_index;

    memset(eptr, 0, sizeof(eventdata));
    eptr->timestamp = time(0);
    memcpy(&eptr->Event_Sensor_Type, addr, byte);

    if (eptr->timestamp == 0)
        recall = 1;

    if (nextlist->next == eptr)
        nextlist->next = NULL;

    eptr->next = event_head;
    event_head = eptr;

    //recall these events, happened before timestamp
    //no need to recall
    if (recall && eptr->timestamp != 0)
    {
        recalllist = eptr->next;
        while (recalllist && recalllist->timestamp == 0)
        {
            recalllist->timestamp = eptr->timestamp - i++;
            recalllist = recalllist->next;
            //dirty[EVENTTBL].length++;
            event_length++;
            //recall == 0 would check the boundary again
        }
        recall = 0;
    }

    //not write back timestamp 0 to flash
    if (!recall)
    {
        if (event_length < MAX_EVENT_ENTRY)
            event_length++;
        else
            event_length = MAX_EVENT_ENTRY;
    }
    return eptr;
}

unsigned char bsp_get_sstate(void)
{
    unsigned char state;
    ioctl(smbus_fd, SMBGSTATE, &state);
    return state;
}

unsigned char SMBus_Prepare_RmtCtrl(unsigned char MsgType, unsigned char force)
{
    unsigned char bptr[3];
    unsigned char ret = 0;
    static unsigned char enter = 0;
    unsigned char state;
    //prevent re-entry, possibly caused by OSTimeDly
    if (enter)
        return DASH_FAIL;

    state = bsp_get_sstate();
    if (dpconf->vendor == FSC)
    {
        if ((MsgType == RMCP_PWR_Off || MsgType == RMCP_Reset || MsgType == RMCP_PWR_CR) && (state == S_S3 || state == S_S4 || state == S_S5))
            return DASH_FAIL;

        else if (MsgType == RMCP_PWR_On && state == S_S0)
            return DASH_FAIL;

        else if (state == S_UNKNOWN)
            return DASH_FAIL;
    }
    enter = 1;
    //in-band does not exist, but issue a hibernate or standby
    switch(MsgType)
    {
    case RMCP_Reset:
        MsgType = ASF_RESET;
        break;

    case RMCP_PWR_On:
        MsgType = ASF_POWER_ON;
        break;

    case RMCP_PWR_Off:
    case RMCP_HIBER:
    case RMCP_STANDBY:
        MsgType = ASF_POWER_OFF;
        break;

    case RMCP_PWR_CR:
        MsgType = ASF_POWER_CR;
        break;
    }
    memcpy(bptr, &asfconfig->asf_rctl[MsgType].slaveaddr, 3);
    struct master_smbus_parameters *msp = malloc(sizeof(struct master_smbus_parameters));
    msp->msg_type = REMOTE_CONTROL;
    msp->cmd = 0x04;
    msp->pldmfromasf = dpconf->pldmfromasf;
    msp->pldmpec = dpconf->pldmpec;
    msp->asfpec = asfconfig->pec;
    msp->poll_type = smbiosrmcpdataptr->PollType;
    msp->bufaddr = bptr;
    msp->length = 3;
    ioctl(smbus_fd, SMMSEND, msp);
    enter = 0;

    if (ret) ret = DASH_FAIL;
    return ret;

}

unsigned char bsp_set_asftbl(asf_header_t *ptr)
{
    unsigned char *asftbl;
    if(memcmp(ptr, "ASF!", 4))
        return 1;
    asftbl = malloc (ptr->length);
    memcpy((void *) asftbl, (void *) ptr, ptr->length);
    dirty_ext[ASFTBL].addr = (unsigned char *) asftbl;
    dirty_ext[ASFTBL].flashaddr = (unsigned char *) SYSTEM_ASF_TABLE;
    dirty_ext[ASFTBL].length = ptr->length;
    return 0;
}

unsigned char bsp_set_pldm_slave_address(unsigned char address)
{
    unsigned char i = 0;

    dpconf->pldmslaveaddr = address;
    for(i = 0 ; i < 7; i++)
    {
        if(dpconf->pldmtype == PLDM_BLOCK_READ)
            pldmresponse[i]->sslaveaddr = dpconf->pldmslaveaddr;
        else
            pldmresponse[i]->slaveaddr  = dpconf->pldmslaveaddr;
    }
    return 0;
}

void ParserASFTable(asf_header_t *ptr)
{
    unsigned char  *asftbl = dirty_ext[ASFTBL].addr;
    unsigned char  type;
    unsigned char  index, i, j;
    unsigned short length;
    unsigned char  checkpec=0;
    unsigned char  snrindex=0;

    //get the start address of information
    asftbl += ASF_TABLE_OFFSET;
    asfconfig->numofalerts = 0;
    asfconfig->numofsnr = 0;
    dpconf->lspoll = 0;
    memset(asfconfig->legacysnr, 0, sizeof(asfconfig->legacysnr));

    while(asftbl < (dirty_ext[ASFTBL].addr + dirty_ext[ASFTBL].length))
    {
        type = (*asftbl & 0x7F);
        length = *(asftbl+2) + ( *(asftbl+3) << 8);

        switch(type)
        {
        case ASF_INFO:
            asfconfig->maxwdt = *(asftbl+4);
            asfconfig->minsnrpoll = *(asftbl+5);
            asfconfig->systemid = *(asftbl+6) + (*(asftbl+7) << 8);
            memcpy(asfconfig->IANA, asftbl+8, 4);
            break;
        case ASF_ALRT:
            index = *(asftbl+6);
            asfconfig->numofalerts += index;

            //support maximal 16 events
            if(asfconfig->numofalerts > 16)
                break;
            for(i = 0 ; i < index; i++)
            {
                memcpy(&asfconfig->asfalerts[snrindex].address, asftbl+8+12*i, 12);
                if(asfconfig->asfalerts[snrindex].evtseverity == 0)
                    asfconfig->asfalerts[snrindex].logtype = LOG_INFO;
                else if (asfconfig->asfalerts[snrindex].evtseverity <= 8)
                    asfconfig->asfalerts[snrindex].logtype = LOG_WARNING;
                else
                    asfconfig->asfalerts[snrindex].logtype = LOG_ERROR;

                if((asfconfig->asfalerts[snrindex].address & 0x01))
                {
                    asfconfig->asfalerts[snrindex].exactmatch = 1;
                    asfconfig->asfalerts[snrindex].address &= 0xFE;
                }
                else
                    asfconfig->asfalerts[snrindex].exactmatch = 0;

                for(j=0; j <= asfconfig->numofsnr; j++)
                {
                    if((asfconfig->legacysnr[j].addr == asfconfig->asfalerts[snrindex].address) && (asfconfig->legacysnr[j].command == asfconfig->asfalerts[snrindex].command))
                        break;
                }

                if(j > asfconfig->numofsnr)
                {
                    asfconfig->legacysnr[asfconfig->numofsnr].addr = asfconfig->asfalerts[snrindex].address;
                    asfconfig->legacysnr[asfconfig->numofsnr].command = asfconfig->asfalerts[snrindex].command;
                    asfconfig->legacysnr[asfconfig->numofsnr].addrplus = asfconfig->asfalerts[snrindex].address+1;
                    asfconfig->numofsnr++;
                }

                if((*(asftbl+4) >> i) & 0x01)
                    asfconfig->asfalerts[snrindex].assert = 0;
                else
                    asfconfig->asfalerts[snrindex].assert = 1;

                if((*(asftbl+5) >> i) & 0x01)
                    asfconfig->asfalerts[snrindex].deassert = 0;
                else
                    asfconfig->asfalerts[snrindex].deassert = 1;
                snrindex++;
            }
            break;
        case ASF_RCTL:
            for(i = 0; i < *(asftbl+4); i++)
            {
                index = *(asftbl+8+i*4);
                memcpy(&asfconfig->asf_rctl[index], asftbl+8+i*4, 4);

                if(!checkpec)
                {
                    if(asfconfig->asf_rctl[index].slaveaddr & 0x01)
                    {
                        unsigned char tmp = 0xCF;
                        ioctl(smbus_fd, SMBSMEN, &tmp);
                        asfconfig->pec = 1;
                    }
                    else
                    {
                        unsigned char tmp = 0xCD;
                        ioctl(smbus_fd, SMBSMEN, &tmp);
                        asfconfig->pec = 0;
                    }

                    if(dpconf->pldmfromasf)
                        bsp_set_pldm_slave_address(asfconfig->asf_rctl[index].slaveaddr & 0xFE);
                    checkpec = 1;
                }
                asfconfig->asf_rctl[index].slaveaddr &= 0xFE;
            }
            break;
        case ASF_RMCP:
            memcpy(asfconfig->RMCPCap, asftbl+4, 7);
            asfconfig->RMCPCmpCode = *(asftbl+11);
            if(!asfconfig->RMCPCmpCode)
            {
                memcpy(asfconfig->RMCPIANA, asftbl+12, 4);
                asfconfig->RMCPSpecialCmd = *(asftbl+16);
                memcpy(asfconfig->RMCPSpecialCmdArgs, asftbl+17, 2);
                memcpy(asfconfig->RMCPBootOptions, asftbl+19, 2);
                memcpy(asfconfig->RMCPOEMArgs, asftbl+21, 2);
            }
            break;
        case ASF_ADDR:
            break;
        }
        asftbl += length;
    }
    if(asfconfig->numofsnr)
        dpconf->lspoll = 1;
}


void user_mmap_kernel_struct(void)
{
    static unsigned char init = 0;
    //printf("user_mmap_kernel_struct.\n");
    if (dpconf!=NULL)
    {
        printf("dpconf is not NULL.\n");
        return ;
    }
    int mem_fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (mem_fd <= 0)
        close(mem_fd);
    unsigned int phy_addr;
    int i = 0;
    unsigned char *vir_addr = NULL;
    if (!smbus_fd)
    {
        smbus_fd = open("/dev/smbusdev", O_RDWR);
        //printf("smbus_fd is %d.\n", smbus_fd);
        if (smbus_fd <= 0)
        {
            close(smbus_fd);
            return -1;
        }
    }
    ioctl(smbus_fd, MMAP, &phy_addr);

    vir_addr = mmap(NULL, 0x4000, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, phy_addr);
    close(mem_fd);
    debug("page addr is 0x%x, vir_addr is 0x%x.\n", phy_addr, vir_addr);

    dpconf = vir_addr;
    debug("vendor is %d, bios is %d, pldmtype is %d.\n", VENDOR, BIOSVEN, PLDMTYPE);
    //dpconf->HostIP.addr= 0x0A00A8C0;
    if (!flash_buf)
    {
        int mtd_fd;
        mtd_fd = open("/dev/mtdblock2", O_RDWR);
        if (mtd_fd <= 0)
        {
            close(mtd_fd);
            return -1;
        }
        flash_buf = mmap(NULL, 0x10000, PROT_READ|PROT_WRITE, MAP_SHARED, mtd_fd, 0);
    }
    asfconfig = vir_addr + 0xb00;
    pldmdataptr = vir_addr + 0x800;
    xferptr[0] = vir_addr + 0x2200;
    xferptr[1] = vir_addr + 0x2800;
    xferptr[2] = vir_addr + 0x2E00;
    pldmdataptr_ptr[0] = vir_addr + 0x1000;
    pldmdataptr_ptr[1] = vir_addr + 0x1600;
    pldmdataptr_ptr[2] = vir_addr + 0x1C00;
    if (!timestampdataptr)
	{
        timestampdataptr = vir_addr + 0xe00;
    	smbiosptr = timestampdataptr + 0x10;
	}
if (!init)
{
    dpconf->pldmtype = PLDMTYPE;
    dpconf->bios=BIOSVEN;
    dpconf->pldmslaveaddr = 0x88;
    dpconf->chipset = CHIPSET;
    dpconf->arpaddr = 0xC8;
    dpconf->vendor = VENDOR;

    //printf("dpconf pointer addr is %p.\n", &dpconf);
    //printf("dpconf is 0x%x.\n", dpconf);


    if(!bsp_set_asftbl((asf_header_t *) ((unsigned char *)flash_buf + 0x8000)))
        ParserASFTable((asf_header_t *) ((unsigned char *)flash_buf + 0x8000));
    if(dpconf->pldmfromasf)
        bsp_set_pldm_slave_address(asfconfig->asf_rctl[0].slaveaddr & 0xFE);

    pldmresponse[0] = vir_addr+0x100 + 2;
    pldmresponse[1] = pldmresponse[0] + 1;
    pldmresponse[2] = pldmresponse[0] + 2;
    pldmresponse[3] = vir_addr + 0x300 + 2+0x100;
    pldmresponse[4] = pldmresponse[3] + 1;
    pldmresponse[5] = pldmresponse[3] + 2;
    pldmresponse[6] = pldmresponse[3] + 3;
    if(dpconf->pldmtype == PLDM_BLOCK_READ)
    {
        memcpy(pldmtmp, pldmsuccessbr, 14);
        pldmtmp[3] = dpconf->pldmslaveaddr;
        //smbus_param[dpconf->chipset][5];
        if (dpconf->bios == AMIBIOS)
        {
            pldmtmp[4] = 0x01;
            pldmtmp[6] = 0x00;
            pldmtmp[7] = 0xC8;
        }
    }
    else
    {
        memcpy(pldmtmp, pldmsuccessbw, 14);
        //assign smbus address of the chipset
        pldmtmp[0] =  dpconf->pldmslaveaddr;
    }
    memcpy(pldmresponse[0], pldmtmp, 14);
    pldmresponse[0]->mctpstop = 0;
    pldmresponse[0]->length = 0x0f;
    pldmresponse[0]->val[4] = 0x05;
    pldmresponse[0]->pldmcmd = 0x01;
    //pldmresponse[0]->val[3] = 0x04;
    if (dpconf->bios == PHOENIXBIOS)
    {
        memcpy(pldmresponse[1], pldmtmp, 14);
        pldmresponse[1]->mctpstart = 0;
    }
    else
        memset(pldmresponse[1], 0 , 39);
    memcpy(pldmresponse[2], pldmtmp, 14);
    memcpy(pldmresponse[3], pldmtmp, 14);
    //4 is always used to transfer byte count 0x0A
    memcpy(pldmresponse[4], pldmtmp, 14);
    //5 is alwyas used to tranfer table tags, which is 0x16 long
    memcpy(pldmresponse[5], pldmtmp, 14);
    pldmresponse[5]->length = 0x16;
    memcpy(pldmresponse[6],MCTPVerSupport,18);
    for (i = 0; i < 7; i++)
        pldmresponse[i]->sslaveaddr = 0x88;

	memcpy((void *) pldmdataptr, (unsigned char *)flash_buf + 0xB000, sizeof(pldm_t));
    pldmdataptr->numwrite[0] = (sizeof(pldm_t) - 1 + 64)/64;

    pldmdataptr->dirty = 0;
    pldmdataptr->TBL1 = 1;
    pldmdataptr->TBL2 = 1;
    pldmdataptr->TBL3 = 1;
    pldmdataptr->pldmerror = 0;


    memcpy(pldmdataptr_ptr[0], (unsigned char *)flash_buf+0xB100, pldmdataptr->len[0]);
    memcpy(pldmdataptr_ptr[1], (unsigned char *)flash_buf+0xB800, pldmdataptr->len[1]);
    memcpy(pldmdataptr_ptr[2], (unsigned char *)flash_buf+0xBC00, pldmdataptr->len[2]);
    
    //asfconfig = mmap(NULL, 0x100, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, phy_addr+0xb00);
    memset(asfconfig,0,0x100);
    memcpy(asfconfig->asf_rctl, asf_ctldata, sizeof(asf_ctldata));
    for(i=0; i < 4; i++)
        asfconfig->asf_rctl[i].slaveaddr &= 0xFE;
    //Set defualt slave address of PLDM Responder
    if (dpconf->pldmfromasf)
    {
        dpconf->pldmslaveaddr = asfconfig->asf_rctl[0].slaveaddr;
        dpconf->pldmpec = asfconfig->pec;
    }
    memcpy(asfconfig->IANA, RMCP_IANA, sizeof(RMCP_IANA));
    asfconfig->RMCPCap[0] = 0x67;
    asfconfig->RMCPCap[1] = 0xF8;
    asfconfig->RMCPCap[4] = 0x00;
    asfconfig->RMCPCap[5] = 0x1F;
    asfconfig->RMCPCap[6] = 0xFF;

    if(asf_legacy_sensor_num)
    {
        memcpy(asfconfig->legacysnr, snrinfo, sizeof(snrinfo));
        asfconfig->numofsnr = asf_legacy_sensor_num;
        memcpy(asfconfig->asfalerts, asfalerts, sizeof(asfalerts));
        asfconfig->numofalerts = asf_alerts_num;
        dpconf->lspoll = 1;
    }
    if (dpconf->chipset == INTEL || dpconf->chipset == AMDSOC)
        asfconfig->pec = 0;
    else if (dpconf->chipset == AMD) {
#if CONFIG_ENABLED_DEFAULT_PEC
        asfconfig->pec = 1;
#else
        asfconfig->pec = 0;
#endif
    } else {

        asfconfig->pec = 1;
    }
    if (!smbiosrmcpdataptr)
    {
        smbiosrmcpdataptr = vir_addr + 0xc00;
        smbiosrmcpdataptr->PollType = POLL_STOP;
    }
}

    init = 1;
}

struct VncInfo * mmap_kernel_VNC_struct(void)
{
    //int fd = open("/dev/vnc", O_RDWR|O_NDELAY);
    //struct  VncInfo * map_buf;
#if 0
    int fd = open("/dev/mchar", O_RDWR|O_NDELAY);
    struct  VncInfo * map_buf;
    if(fd >= 0)
        map_buf = mmap(0, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    //VGAInfo = *((struct VGAInfo *)map_buf);
    printf("fd is %d, map_buf is 0x%x.\n", fd, map_buf);
    close(fd);
    return map_buf;
#else
    int fd = open("/dev/vnc", O_RDWR|O_NDELAY);
    int mem_fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (mem_fd <= 0)
        close(mem_fd);
    unsigned int phy_addr;
    unsigned char *vir_addr = NULL;
    ioctl(fd, MMAP_VNC, &phy_addr);
    printf("phy_addr is 0x%x.\n", phy_addr);

    vir_addr = mmap(NULL, 0x200, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, phy_addr);
    printf("vir_addr is 0x%p.\n", vir_addr);
    close(fd);
    close(mem_fd);
    return vir_addr;
#endif

}
