#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>

/*
DBG_LEVEL 0 // no debug message
DBG_LEVEL 1 // minimal debug message (some header information) [default]
DBG_LEVEL 3 // more debug message (some header and its data)
DBG_LEVEL 5 // max debug message (all header and its data)
*/
#define DBG_LEVEL 3 //1


#define DXE_UDP_PORT 60000
#define DXE_UDP_DIP "0.0.0.0"
#define DXE_MSG_LEN_MIN 1
#define DXE_MSG_LEN_MAX 1024
#define DXE_UEFI_HDR_LEN 4+2+4 //UEFI Header (RTK defined) - magic number(4 bytes),msg len(2 bytes),timeout value(4 bytes in ms) 
#define DXE_BUF_LEN DXE_UEFI_HDR_LEN+DXE_MSG_LEN_MAX 
#define DXE_MAGIC 0xA5A58117
#define DXE_TIMEOUT_MIN 1000    //1 second (1000 ms) ,united in second
#define DXE_TIMEOUT_MAX	30*1000 //30 seconds ,united in second

#define DXE_RING_BUF_LEN 1024*1024*2 //2MB
#define DXE_INFO_FORMAT_HDR_LEN 2 + 4 + 2 + 4 
#define HDR_OFFSET 2 //format converting from UEFI(hdr len 12) to DXE INFO(hdr len 14)
static int first_dxe_info_pkt = 1;// 1 for yes, 0 for no (get the first valid dxe info packet will trigger the timer) to switch NIC back to normal mode if timeout happened)
struct dxe_info_format {
	unsigned char dxe_hdr_len[2];	// The size of Header length	 : 2 bytes // little endian
	unsigned char dxe_counter[4];	// The size of Counter			 : 4 bytes // little endian
	unsigned char dxe_info_len[2];	// The size of DXE Info length	 : 2 bytes // little endian
	unsigned char dxe_rsv[4];		// The size of Reserve			 : 4 bytes // little endian
	unsigned char dxe_info[DXE_MSG_LEN_MAX];//max 2^16 bytes(ASUS), we keep the max DXE_MSG_LEN_MAX(1024) bytes fro UEFI application 
};

int get_dxe_hdr_len(struct dxe_info_format *buf_dxe) //little endian
{
    return  buf_dxe->dxe_hdr_len[0] + 
           (buf_dxe->dxe_hdr_len[1] << 8);
}

int set_dxe_hdr_len(struct dxe_info_format *buf_dxe, int value) //little endian
{
    buf_dxe->dxe_hdr_len[0] =  value & 0x00ff;
    buf_dxe->dxe_hdr_len[1] = (value & 0xff00) >> 8;
    return 0;
}

int get_dxe_counter(struct dxe_info_format *buf_dxe) //little endian
{
    return  buf_dxe->dxe_counter[0] + 
           (buf_dxe->dxe_counter[1] << 8) +
           (buf_dxe->dxe_counter[2] << 16)  +
           (buf_dxe->dxe_counter[3] << 24)  ;
}

int set_dxe_counter(struct dxe_info_format *buf_dxe, int value) //little endian
{
    buf_dxe->dxe_counter[0] =  value & 0x000000ff;
    buf_dxe->dxe_counter[1] = (value & 0x0000ff00) >> 8;
    buf_dxe->dxe_counter[2] = (value & 0x00ff0000) >> 16;
    buf_dxe->dxe_counter[3] = (value & 0xff000000) >> 24;
    return 0;
}

int get_dxe_info_len(struct dxe_info_format *buf_dxe) //little endian
{
    return  buf_dxe->dxe_info_len[0] + 
           (buf_dxe->dxe_info_len[1] << 8);
}

int set_dxe_info_len(struct dxe_info_format *buf_dxe, int value) //little endian
{
    buf_dxe->dxe_info_len[0] =  value & 0x00ff;
    buf_dxe->dxe_info_len[1] = (value & 0xff00) >> 8;
    return 0;
}

int get_dxe_rsv(struct dxe_info_format *buf_dxe) //little endian
{
    return  buf_dxe->dxe_rsv[0] + 
           (buf_dxe->dxe_rsv[1] << 8) +
           (buf_dxe->dxe_rsv[2] << 16)  +
           (buf_dxe->dxe_rsv[3] << 24)  ;
}

int set_dxe_rsv(struct dxe_info_format *buf_dxe, int value) //little endian
{
    buf_dxe->dxe_rsv[0] =  value & 0x000000ff;
    buf_dxe->dxe_rsv[1] = (value & 0x0000ff00) >> 8;
    buf_dxe->dxe_rsv[2] = (value & 0x00ff0000) >> 16;
    buf_dxe->dxe_rsv[3] = (value & 0xff000000) >> 24;
    return 0;
}


struct ring_buffer {
	unsigned int size;
	unsigned int start;
	unsigned int count; // number of elements in buffer
	unsigned char *element;//it is united in struct dxe_info_format
};
typedef struct ring_buffer ring_buffer_t;
unsigned int counter;

int rb_init(ring_buffer_t *buffer) {
	buffer->size = DXE_RING_BUF_LEN; //size;
	buffer->start = 0;
	buffer->count = 0;

	buffer->element = malloc(buffer->size);
	if (buffer->element)
	{
		memset(buffer->element, 0, buffer->size );
		#if DBG_LEVEL > 0
		printf("[agent]memory allocated in %d byte\n", buffer->size );
		#endif
		return 0;
	}
	else
	{
	    #if DBG_LEVEL > 0
		printf("[agent]memory CANNOT allocated in %d byte\n", buffer->size );
		#endif
		return -1;
	}
}


void rb_close(ring_buffer_t *buffer) {
	free(buffer->element);
}


void rb_push(ring_buffer_t *buffer, unsigned char *data) {
	unsigned int index, len, i;
	struct dxe_info_format *one_dxe_info;
	
	one_dxe_info = (struct dxe_info_format *) data;
	
    len = get_dxe_info_len(one_dxe_info) + get_dxe_hdr_len(one_dxe_info);
    #if DBG_LEVEL >= 3
	printf("[agent push]msg len = %d\n", len);
	printf("[agent push]-----------------------------------\n");
	printf("dxe_hdr_len : %d\n", get_dxe_hdr_len(one_dxe_info));
    #endif
    
	#if DBG_LEVEL > 0
	printf("dxe_counter : %d\n", get_dxe_counter(one_dxe_info));
	#endif
	
    #if DBG_LEVEL >= 3
	printf("dxe_info_len : %d\n", get_dxe_info_len(one_dxe_info));
	printf("dxe_rsv : %d\n", get_dxe_rsv(one_dxe_info));
	#endif
	
    #if DBG_LEVEL >= 5
	for (i=0; i<get_dxe_info_len(one_dxe_info); i++)
	{
	   printf("%x ", one_dxe_info->dxe_info[i]);
	}
	printf("\n[agent push]-----------------------------------\n\n");
	#endif



    #if DBG_LEVEL >= 5
	printf("[agent push]-----------------------------------\n");
	for (i=0; i<len; i++)
	{
	   printf("%02x ", (unsigned char)*(data+i));

	}
	printf("\n[agent push]-----------------------------------\n\n");
    #endif

	index = (buffer->start + buffer->count) % buffer->size;// MUST before find space_need
	#if DBG_LEVEL > 0
	printf("[agent push]index=%d start=%d count=%d size=%d\n", index, buffer->start, buffer->count, buffer->size);
	#endif
	
    int space_need, one_len, new_start;
   	struct dxe_info_format *tmp;
	int ring_full;
	
    new_start = buffer->start;
	ring_full = 0;
	if ((buffer->count + len) > buffer->size) // full, need to find enough space for this data
	{
        #if DBG_LEVEL > 0
        printf("[ring buffer full]reset space not enough...find need space ... \n");
        #endif
        ring_full = 1;
	    space_need = (buffer->count + len) - buffer->size;
	    
	    while (space_need  > 0)
	    {
	        tmp = (struct dxe_info_format *) &buffer->element[new_start];
            #if DBG_LEVEL > 0   
	        printf("dxe_hdr_len : %d\n", get_dxe_hdr_len(tmp));
        	printf("dxe_counter : %d\n", get_dxe_counter(tmp));
        	printf("dxe_info_len : %d\n", get_dxe_info_len(tmp));
        	printf("dxe_rsv : %d\n", get_dxe_rsv(tmp));
        	#endif
        	#if DBG_LEVEL >= 5
         	for (i=0; i<get_dxe_info_len(tmp); i++)
         	{
         	   printf("%x ", tmp->dxe_info[i]);

         	}
            #endif
	        one_len = get_dxe_hdr_len(tmp)+get_dxe_info_len(tmp);
	        space_need = space_need - one_len;
	        new_start = (new_start + one_len) % buffer->size;
	        #if DBG_LEVEL > 0
	        printf("[%02x] dxe_hdr_len = %d, dxe_info_len = %d,total=%d\n", buffer->element[new_start], get_dxe_hdr_len(tmp), get_dxe_info_len(tmp),  get_dxe_hdr_len(tmp)+get_dxe_info_len(tmp));
	        printf("space_need = %d, new_start = %d\n", space_need, new_start);
	        #endif

	    }
	    
	    buffer->start = new_start;   
        buffer->count = buffer->size + space_need;
        #if DBG_LEVEL > 0
        printf("start = %d, count = %d\n", buffer->start, buffer->count);
        #endif
	}

	for (i = 0; i < len; i++)
	{	
		if ((index + i) <= (buffer->size - 1)) // -1 for array index is from zero
		{
			buffer->element[index + i] = (unsigned char *) *(data+i);
			#if DBG_LEVEL >= 5
			printf("index %d, start = %d, count %d, i %d, data[i]=%02x\n", index, buffer->start, buffer->count, i, *(data+i));
            #endif
			if (!ring_full) buffer->count++;

		}
		else
		{
			buffer->element[(index + i) % buffer->size] = *(data+i);
		}

	}
	
}

void rb_dump(ring_buffer_t *buffer) {
	int i;

	printf("[agent] start=%d, count = %d --------------------------------\n", buffer->start, buffer->count);
	for (i = 0; i < buffer->count; i++)
	{
		if ((i % 16)==0) printf("%08x  ",i);
		
		printf("%02x",buffer->element[i]);

		if ((i % 2)==1) printf(" ");
		if ((i % 16)==15) printf("\n");
	}
	printf("\n[agent]--------------------------------\n\n");

}


#define DXE_INFO_FILE "/tmp/dxe_info.dat"
int rb_popall(ring_buffer_t *buffer) {
	FILE *fp;
	int index,i;

	index = buffer->start + buffer->count;


	fp = fopen(DXE_INFO_FILE,"w" );
	if( NULL == fp ){
        #if DBG_LEVEL > 0
		printf("[agent]%s open failure\n",DXE_INFO_FILE);
        #endif
        
		return -1;

	}else{
        #if DBG_LEVEL > 0
		printf("[agent]pop\n");
		printf("[agent] start = %d, count = %d --------------------------------\n",buffer->start, buffer->count);
		#endif
		for (i = buffer->start; i < buffer->start + buffer->count; i++)
		{
			if (i <= (buffer->size - 1)) // -1 for array index is from zero
			{
			    #if DBG_LEVEL >= 5
				printf("%02x ",buffer->element[i]);
				#endif
				fwrite(&buffer->element[i], 1, sizeof(buffer->element[i]), fp);
			}
			else
			{
			    #if DBG_LEVEL >= 5
				printf("%20x ",buffer->element[i % buffer->size]);
				#endif
				fwrite(&buffer->element[i % buffer->size], 1, sizeof(buffer->element[i % buffer->size]), fp);
			}
		}
		#if DBG_LEVEL > 0
		printf("\n[agent]--------------------------------\n\n");
		#endif
		fflush(fp);

	}
	#if DBG_LEVEL > 0
    printf("total recieved %d packets\n", counter);
    #endif
	fclose(fp);
	return 0;
}


#define NIC_MODE_CONTROL_FILE "/proc/net/r8168oob/eth0/loopback" // 0 : nornal mode, 1 : lookback mode
#define NIC_LOOPBACK 1 
#define NIC_NORMAL 0
int set_nic_mode(unsigned char mode)
{
    FILE *fp;
    char cmd[256];
    
    if ((mode == NIC_LOOPBACK) || (mode == NIC_NORMAL))
    { 
  	  fp = fopen(NIC_MODE_CONTROL_FILE, "w");
	  if( NULL == fp )
	  {
          #if DBG_LEVEL > 0
		  printf("[agent]%s open failure\n",NIC_MODE_CONTROL_FILE);
          #endif
		  return -1;
	  }
      fclose(fp);
	  
	  sprintf(cmd, "echo %d > %s", mode, NIC_MODE_CONTROL_FILE);
      #if DBG_LEVEL >= 3
	  printf("[agent]set_nic_mode cmd = %s\n ", cmd);
      #endif
	  system(cmd);
      
      #if DBG_LEVEL >= 3
	  printf("[agent]set_nic_mode mode=%d ", mode);
      if (mode == NIC_LOOPBACK)
	     printf("(NIC_LOOPBACK)\n");
	  else
	     printf("(NIC_NORMAL)\n");
      #endif
      return 0;
    }
    else
    {
      #if DBG_LEVEL >= 3
	  printf("[agent]set_nic_mode mode=%d not supported\n", mode);
      #endif       
      return -1;
    }
}
void timeout_handler()
{
    set_nic_mode(NIC_NORMAL);
    #if DBG_LEVEL >= 3
    printf("[agent]timeout : swtich NIC back to normal mode\n");
    #endif
}


int main()
{
	int sock;
	struct sockaddr_in addr;
	struct sockaddr_in senderinfo;
	socklen_t addrlen;
	
	//we add more 2(HDR_OFFSET) bytes for the header of DXE_INFO_FORMAT
	//DXE_UEFI_HDR_LEN 4+2+4
	//DXE_INFO_FORMAT HDR 2+4+2+4
	//HDR_OFFSET = (2+4+2+4) - (4+2+4) = 2
	//To reduce one packet memory copy between format converting from UEFI to DXE INFO
	unsigned char buf[HDR_OFFSET+DXE_BUF_LEN];//DXE_BUF_LEN = DXE_UEFI_HDR_LEN+DXE_MSG_LEN_MAX

	struct dxe_info_format *buf_dxe;
	unsigned int i, n;
	ring_buffer_t r_buffer;


	/* (1) */
	sock = socket(AF_INET, SOCK_DGRAM, 0);

	/* (2) */
	addr.sin_family = AF_INET;
	addr.sin_port = htons(DXE_UDP_PORT);
	addr.sin_addr.s_addr = INADDR_ANY; //inet_pton(AF_INET, DXE_UDP_DIP, &addr.sin_addr.s_addr);
	bind(sock, (struct sockaddr *)&addr, sizeof(addr));

	/* (3) */
	addrlen = sizeof(senderinfo);
	
	if (rb_init(&r_buffer) == -1)
		 return -1 ; //memory allocation fail.

    counter = 0;
	while (1)
	{
		 n = recvfrom(sock, &buf[HDR_OFFSET], sizeof(buf)-HDR_OFFSET, 0, (struct sockaddr *)&senderinfo, &addrlen);

         #if DBG_LEVEL > 0
		 printf("[agent] packet size = %d -----------------------------------\n", n);
		 #endif
		 #if DBG_LEVEL >= 5
		 for (i=0; i<n; i++)
		 {
		   printf("%x ", buf[HDR_OFFSET+i]);
		 }
		 printf("\n[agent]-----------------------------------\n\n");
	     #endif

          #if DBG_LEVEL >= 3
		  printf("[agent]magic number : %x %x %x %x\n\n", buf[HDR_OFFSET+0], buf[HDR_OFFSET+1], buf[HDR_OFFSET+HDR_OFFSET], buf[HDR_OFFSET+3]);
		  #endif
		  if ((buf[HDR_OFFSET+0]==0xa5)&&(buf[HDR_OFFSET+1]==0xa5)&&(buf[HDR_OFFSET+2]==0x81)&&(buf[HDR_OFFSET+3]==0x17))
		  {
		        #if DBG_LEVEL >= 5
				printf("[agent] DXE DATA size = %d -----------------------------------\n", n);
				for (i=0; i<(buf[HDR_OFFSET+5]+(buf[HDR_OFFSET+4] << 8)); i++)
				{
				   printf("%x ", buf[HDR_OFFSET+i+DXE_UEFI_HDR_LEN]);

				}
				printf("\n[agent]-----------------------------------\n\n");
                #endif

			  unsigned int msg_len;
			  unsigned int timeout;
			  msg_len = buf[HDR_OFFSET+5]+(buf[HDR_OFFSET+4] << 8); //big endian
			  timeout = (buf[HDR_OFFSET+9]+(buf[HDR_OFFSET+8] << 8)+(buf[HDR_OFFSET+7] << 16)+(buf[HDR_OFFSET+6]  << 24)) / 1000; //big endian, united in second
			  
			  #if DBG_LEVEL >= 3
			  printf("[agent]dxe info traffic:\n");
			  printf("[agent]message length = %d \n", msg_len);
			  printf("[agent]timeout value in second = %d \n", timeout); // big endian
              #endif

              //time-out value for switch NIC loopback mode to normal mode if BIOS is fail              
			  if ((timeout < DXE_TIMEOUT_MIN) && (timeout > DXE_TIMEOUT_MAX))
			  {
			    #if DBG_LEVEL >= 3
			    printf("[agent]timeout value not valid between [%d, %d] in ms\n", DXE_TIMEOUT_MIN, DXE_TIMEOUT_MAX);
			    #endif
              }
              else
              {
                //timeout initilization...
                if (first_dxe_info_pkt==1) // the first valide dxe info packet, start-up timer
                {
                  #if DBG_LEVEL >= 3
                  printf("[agent]the first valid dxe info packet\n ");
                  #endif
                  signal(SIGALRM, timeout_handler);// hook timer handler
                  alarm(timeout);
                  first_dxe_info_pkt = 0;
                }
                else // reset timer to the new timeout value
                {
                  alarm(0); // stop timer
                  alarm(timeout);//new timer
                  #if DBG_LEVEL >= 3
                  printf("[agent]reset timer to %d seconds\n", timeout);
                  #endif
                }
                
              
			    if ((msg_len >= DXE_MSG_LEN_MIN) && (msg_len <= DXE_MSG_LEN_MAX))
  			    {
	  			  buf_dxe = (struct dxe_info_format *) buf;

				  set_dxe_hdr_len(buf_dxe, DXE_INFO_FORMAT_HDR_LEN);			
				  set_dxe_counter(buf_dxe, counter++); 
			      set_dxe_info_len(buf_dxe, msg_len);			    
			      set_dxe_rsv(buf_dxe, 0);
			    
				  //put the buffer into ring buffer
				  rb_push(&r_buffer, buf_dxe);
				
				  #if DBG_LEVEL > 0
				  printf("[agent] bufffer is pushed.\n");
				  #endif

			    }
			    else
			    {
  			        #if DBG_LEVEL > 0
				    printf("[agent]Message length too large. [%d %d]\n", DXE_MSG_LEN_MIN, DXE_MSG_LEN_MAX);
				    #endif
			    }
			  }
		  }
		  else
		  {
			//special package for dump ring buffer to /tmp/dxe_info.dat
			if ((buf[HDR_OFFSET+0]==0xa5)&&(buf[HDR_OFFSET+1]==0xa5)&&(buf[HDR_OFFSET+2]==0xa5)&&(buf[HDR_OFFSET+3]==0x01))
			{
			    #if DBG_LEVEL > 0
				printf("[agent]special package \n");
				#endif
				
				rb_popall(&r_buffer);//save ring buffer to file
			}
			else
			{
			    #if DBG_LEVEL > 0
				printf("[agent]not legal packet.\n");
				#endif
			}
		  }
          #if DBG_LEVEL > 0   
		  printf("\n");
		  #endif

	}
	/* (4) */
	close(sock);
	rb_close(&r_buffer);

	return 0;
}
