#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char *argv[])
{
  int sock;
  struct sockaddr_in addr;
  int n;
  int msg_len;

  if (argc == 2) {
    printf( "default len = 8 , total is 10(hdr) + 8 = 18 bytes\n");
    msg_len = 1012;//8;
  }
  else if (argc == 3) {
    printf( "dxe info len = %d , total is 10(hdr) + %d = %d bytes\n", atoi(argv[2]), atoi(argv[2]), 10+atoi(argv[2]));
    msg_len = 10 + atoi(argv[2]);
  }
  else {
    fprintf(stderr, "Usage : %s dstipaddr dxe_info_len(1 - 1024)\n", argv[0]);
    return 1;
  }



  sock = socket(AF_INET, SOCK_DGRAM, 0);

  addr.sin_family = AF_INET;
  addr.sin_port = htons(60000);
  inet_pton(AF_INET, argv[1], &addr.sin_addr.s_addr);

/*
  int i;
  unsigned char buf_ok[]={0xa5,0xa5,0x81,0x17, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x30};
  unsigned char buf_nok[]={0x11,0x22,0x33,0x17, 0x01, 0x00, 0xff, 0xff, 0xff, 0xff, 0x30};
printf("dxe client test begin ...\n");
  for (i = 0; i<1; i++)
  {
	  n = sendto(sock, buf_ok, sizeof(buf_ok), 0, (struct sockaddr *)&addr, sizeof(addr));
	  if (n < 1) {
	    perror("sendto");
	    return 1;
	  }

	  n = sendto(sock, buf_nok, sizeof(buf_ok), 0, (struct sockaddr *)&addr, sizeof(addr));
	  if (n < 1) {
	    perror("sendto");
	    return 1;
	  }

  }
*/

  int i;
  unsigned char tt[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
  unsigned char buf_ok[4+2+4+1024];
  
  buf_ok[0] = 0xa5;
  buf_ok[1] = 0xa5;
  buf_ok[2] = 0x81;
  buf_ok[3] = 0x17;

  buf_ok[4] = (msg_len & 0xff00) >> 8 ; //big endian
  buf_ok[5] = msg_len & 0x00ff; //0x04;

  buf_ok[6] = 0x00;
  buf_ok[7] = 0x00;
  buf_ok[8] = 0x04*3;
  buf_ok[9] = 0x00;

  for (i=4+2+4; i<4+2+4+msg_len; i++)
      buf_ok[i] = tt[(i-4-2-4) % 10];

for (i=0; i<2001; i++)
{
printf("%05d ", i);      
printf("buf_ok size = %ld ", sizeof(buf_ok));

//  n = sendto(sock, buf_ok, sizeof(buf_ok), 0, (struct sockaddr *)&addr, sizeof(addr));
  n = sendto(sock, buf_ok, 4+2+4+msg_len, 0, (struct sockaddr *)&addr, sizeof(addr));
  if (n < 1) {
	    perror("sendto");
	    return 1;
  }

printf("sent bytes = %d\n", n);
usleep(500*1000);//to slow down the sending period
}

  close(sock);
  printf("dxe client test finish\n");

  return 0;
}
