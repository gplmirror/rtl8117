#!/bin/sh

rm /etc/initialized
UUID=$(cat /etc/bios_uuid)
if [ "$UUID" == "" ]; then
    echo root:root | chpasswd -m
    echo root:root:1:00000007 > /etc/wsmand/account
    echo "[APRO][Backup] bstatus: 1" > /dev/kmsg
    echo bstatus:1 | chpasswd -e
else
    echo root:${UUID:17} | chpasswd -m
    echo root:${UUID:17}:1:00000007 > /etc/wsmand/account
    echo "[APRO][Backup] bstatus: 2" > /dev/kmsg
    echo bstatus:2 | chpasswd -e
fi

cp -p /etc/shadow /mnt/aproData/shadow
sync
cp -p /etc/wsmand/account /mnt/aproData/account
sync
