#!/bin/sh

FIREWALL_TABLE=/etc/firewall_table
FIREWALL_MODE=/etc/firewall_mode
MODELNAME_FILE=/etc/modelname
BACKUP_FILE=/mnt/aproData/modelname

backup_mode=$1

case ${backup_mode} in
	1)
		# restore user data
		cp -p ${BACKUP_FILE} ${MODELNAME_FILE}

		# firewall
		mode=$(cat ${MODELNAME_FILE} | grep fiRewalL_mOde | sed 's/^fiRewalL_mOde=//g')
		if [ "$mode" == "" ]; then
			echo 0 > ${FIREWALL_MODE}
			echo "fiRewalL_mOde=0" >> ${MODELNAME_FILE}
		else
			echo ${mode} > ${FIREWALL_MODE}
		fi

		num=$(cat ${MODELNAME_FILE} | grep fiRewalL_indEx | wc -l)
		if [ "$num" != "0" ]; then
			rm ${FIREWALL_TABLE}
			for i in $(seq 0 $((${num}-1)))
			do
				ip1=$(cat ${MODELNAME_FILE} | grep "fiRewalL_indEx=${i}," | sed 's/^.*ip1=//g' | sed 's/,ip2=.*$//g')
				ip2=$(cat ${MODELNAME_FILE} | grep "fiRewalL_indEx=${i}," | sed 's/^.*ip2=//g')
				echo "index=${i} ip1=${ip1} ip2=${ip2}" >> ${FIREWALL_TABLE}
			done
		else
			rm ${FIREWALL_TABLE}
			for i in $(seq 0 7)
			do
				echo "index=${i} ip1=0.0.0.0 ip2=0.0.0.0" >> ${FIREWALL_TABLE}
				echo "fiRewalL_indEx=${i},ip1=0.0.0.0,ip2=0.0.0.0" >> ${MODELNAME_FILE}
			done
		fi
	;;
	*)
		# backup user data

		# firewall
		mode=$(cat ${FIREWALL_MODE})
		sed -i '/fiRewalL_mOde=/d' ${MODELNAME_FILE} && echo "fiRewalL_mOde=${mode}" >> ${MODELNAME_FILE}

		sed -i '/fiRewalL_indEx/d' ${MODELNAME_FILE}
		num=$(cat ${FIREWALL_TABLE} | wc -l)
		for i in $(seq 0 $((${num}-1)))
		do
			ip1=$(cat ${FIREWALL_TABLE} | grep "index=${i} " | sed 's/^.*ip1=//g' | sed 's/ ip2=.*$//g')
			ip2=$(cat ${FIREWALL_TABLE} | grep "index=${i} " | sed 's/^.*ip2=//g')
			echo "fiRewalL_indEx=${i},ip1=${ip1},ip2=${ip2}" >> ${MODELNAME_FILE}
		done
	;;
esac
cp -p ${MODELNAME_FILE} ${BACKUP_FILE}
sync

