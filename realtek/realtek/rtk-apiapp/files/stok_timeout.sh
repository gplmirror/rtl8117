#!/bin/sh

STOK_MAP=/tmp/stok_appuid.map
STOK_TIMEOUT=86400
count=0

# stok 86400sec timeout (= 24hr)
# User register stok will restart this script.
echo "Start stok timer"
while (true); do
    sleep $STOK_TIMEOUT

    if [ -f "$STOK_MAP" ]; then
        rm $STOK_MAP
        echo "Clear the stok."
    fi
done
