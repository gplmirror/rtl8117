#!/bin/sh

# Dump RMA from flash
dd if=/dev/mtd13 of=/tmp/RMA.dat bs=64K count=1

# Push RMA_DATA to the RMA
dd if=/tmp/RMA.dat of=/tmp/RMA bs=32 seek=1 count=2047
rc=$?; if [[ $rc != 0 ]]; then rm /tmp/RMA*; sync; echo 3 > /proc/sys/vm/drop_caches; exit 1; fi

# Save RMA to flash
mtd write /tmp/RMA /dev/mtd13
rc=$?; if [[ $rc != 0 ]]; then rm /tmp/RMA*; sync; echo 3 > /proc/sys/vm/drop_caches; exit 2; fi

# Release Memory
rm /tmp/RMA*
sync
echo 3 > /proc/sys/vm/drop_caches

exit 0
