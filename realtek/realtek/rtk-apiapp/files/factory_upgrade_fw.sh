#!/bin/sh

FW_IMG=/tmp/openwrt-rtl8117-factory-bootcode.img

# Wating for the PC enter S5 state
while (true); do
    sleep 5
    PC_STATE=$( cat /sys/class/apro-ctrl/aproctrl/pcstate )
    echo "Wating for the PC enter S5 state..."
    if [ "$PC_STATE" == "0" ]; then
        break;
    fi
done

echo "The PC is on S5 state."

echo "Save apro information"
# Check if /mnt/aproData is work
IS_MNT=$( mount | grep /dev/mtdblock14 | wc -l )
if [ "$IS_MNT" == "0" ]; then
    mtd erase /dev/mtd14
    dd if=/dev/zero of=/tmp/aproData bs=1K count=64
    mkfs.ext2 /tmp/aproData
    mtd write /tmp/aproData /dev/mtd14
    mount -t ext2 /dev/mtdblock14 /mnt/aproData

    rm /tmp/aproData
fi

# Save apro information
cp -p /etc/shadow /mnt/aproData/shadow; sync
cp -p /etc/wsmand/account /mnt/aproData/account; sync
cp -p /etc/bios_uuid /mnt/aproData/bios_uuid; sync
cp -p /etc/uPath /mnt/aproData/uPath; sync
cp -p /etc/modelname /mnt/aproData/modelname; sync

echo 3 > /proc/sys/vm/drop_caches
sync

echo "Start sysupgrade flash"
echo 0 > /sys/class/apro-ctrl/aproctrl/rtl8117_ready
killall vncs && sleep 1 && killall dropbear uhttpd; sleep 1; /sbin/sysupgrade -d 1 -n $FW_IMG

